call mvn install:install-file -Dfile=./lib/jme3.3.2/jme3-core.jar -DgroupId=org.jmonkeyengine -DartifactId=jme3-core -Dversion=3.3.2-stable-mod -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/jme3.3.2/jme3-niftygui.jar -DgroupId=org.jmonkeyengine -DartifactId=jme3-niftygui -Dversion=3.3.2-stable-mod -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/jme3.3.2/nifty-default-controls.jar -DgroupId=com.github.nifty-gui -DartifactId=nifty-default-controls -Dversion=1.4.3-mod -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/oculus.jar -DgroupId=eu.opends.oculusRift -DartifactId=oculusRift -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/contre.jar -DgroupId=eu.opends.taskDescription.contreTask -DartifactId=contreTask -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/tvpt.jar -DgroupId=eu.opends.taskDescription.tvpTask -DartifactId=tvpTask -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/simphynity.jar -DgroupId=eu.opends.simphynity -DartifactId=simphynity -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/CarOntology.jar -DgroupId=de.dfki.automotive.kapcom -DartifactId=CarOntology -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/KAPcomConnector.jar -DgroupId=de.dfki.automotive.kapcom -DartifactId=KAPcomConnector -Dversion=1.0 -Dpackaging=jar
echo on
call mvn install:install-file -Dfile=./lib/NetMessageProtocol.jar -DgroupId=de.dfki.automotive.netprotocol -DartifactId=NetMessageProtocol -Dversion=1.0 -Dpackaging=jar
echo on
call mvn clean javadoc:javadoc package
echo on
pause