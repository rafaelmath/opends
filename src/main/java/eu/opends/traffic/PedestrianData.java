/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.traffic;

/**
 * 
 * @author Rafael Math
 */
public class PedestrianData 
{
	private String name;
	private boolean enabled;
	private String animationStand;
	private String animationWalk;
	private float minForwardSafetyDistance;
	private float minLateralSafetyDistance;
	private String modelPath;
	private FollowBoxSettings followBoxSettings;

	
	public PedestrianData(String name, boolean enabled, String animationStand, String animationWalk, 
			float minForwardSafetyDistance, float minLateralSafetyDistance, String modelPath, 
			FollowBoxSettings followBoxSettings) 
	{
		this.name = name;
		this.enabled = enabled;
		this.animationStand = animationStand;
		this.animationWalk = animationWalk;
		this.minForwardSafetyDistance = minForwardSafetyDistance;
		this.minLateralSafetyDistance = minLateralSafetyDistance;
		this.modelPath = modelPath;
		this.followBoxSettings = followBoxSettings;
	}


	public String getName() {
		return name;
	}
	

	public boolean getEnabled() {
		return enabled;
	}


	public String getAnimationStand() {
		return animationStand;
	}

	
	public String getAnimationWalk() {
		return animationWalk;
	}
	
	
	public float getMinForwardSafetyDistance() {
		return minForwardSafetyDistance;
	}
	

	public float getMinLateralSafetyDistance() {
		return minLateralSafetyDistance;
	}

	
	public String getModelPath() {
		return modelPath;
	}


	public FollowBoxSettings getFollowBoxSettings() {
		return followBoxSettings;
	}


	public String toXML()
	{
		
		String preferredSegments = "";
		
		if(!followBoxSettings.getPreferredSegmentsStringList().isEmpty())
		{
			preferredSegments = "\t\t\t<segments>";
			for(String segmentString : followBoxSettings.getPreferredSegmentsStringList())
				preferredSegments += "<segment ref=\"" + segmentString + "\"/>";
			preferredSegments += "</segments>\n";
		}	
		
		return "\t\t<pedestrian id=\"" + name + "\">\n" +
			   "\t\t\t<modelPath>" + modelPath + "</modelPath>\n" + 
			   "\t\t\t<animationWalk>"+ animationWalk + "</animationWalk>\n" + 
			   "\t\t\t<animationStand>"+ animationStand + "</animationStand>\n" + 
			   "\t\t\t<mass>"+ minForwardSafetyDistance + "</mass>\n" + 
			   "\t\t\t<scale>"+ minLateralSafetyDistance + "</scale>\n" + 
			   "\t\t\t<maxSpeed>"+ followBoxSettings.getMaxSpeed() + "</maxSpeed>\n" + 							  
			   "\t\t\t<giveWayDistance>"+ followBoxSettings.getGiveWayDistance() + "</giveWayDistance>\n" + 
			   "\t\t\t<intersectionObservationDistance>"+ followBoxSettings.getIntersectionObservationDistance() + "</intersectionObservationDistance>\n" + 
			   "\t\t\t<minIntersectionClearance>"+ followBoxSettings.getMinIntersectionClearance() + "</minIntersectionClearance>\n" + 
			   "\t\t\t<enabled>"+ enabled + "</enabled>\n" + 
			   "\t\t\t<minDistanceFromPath>"+ followBoxSettings.getMinDistance() + "</minDistanceFromPath>\n" + 
			   "\t\t\t<maxDistanceFromPath>"+ followBoxSettings.getMaxDistance() + "</maxDistanceFromPath>\n" + 
			   "\t\t\t<startWayPoint>"+ followBoxSettings.getStartWayPointID() + "</startWayPoint>\n" + 
			   preferredSegments + 
			   "\t\t</pedestrian>";	
	}

}
