/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.traffic;

import java.util.ArrayList;

import eu.opends.basics.SimulationBasics;

/**
 * 
 * @author Rafael Math
 */
public class StaticTraffic
{
	private static ArrayList<TrafficCarData> vehicleDataList = new ArrayList<TrafficCarData>();
	private static ArrayList<PedestrianData> pedestrianDataList = new ArrayList<PedestrianData>();
	private static ArrayList<OpenDRIVECarData> openDRIVEDataList = new ArrayList<OpenDRIVECarData>();
	
    private static ArrayList<StaticTrafficObject> trafficObjectList = new ArrayList<StaticTrafficObject>();

       
	public StaticTraffic(SimulationBasics sim)
	{
		/*
		for(TrafficCarData vehicleData : vehicleDataList)
		{
			// build and add traffic cars
			trafficObjectList.add(new TrafficCar(sim, vehicleData));
		}
		*/
		for(PedestrianData pedestrianData : pedestrianDataList)
		{
			// build and add pedestrians
			trafficObjectList.add(new StaticPedestrian(sim, pedestrianData));
		}
		
		for(OpenDRIVECarData openDRIVECarData : openDRIVEDataList)
		{
			// build and add traffic cars
			trafficObjectList.add(new StaticOpenDRIVECar(sim, openDRIVECarData));
		}
	}

	
    public static ArrayList<TrafficCarData> getVehicleDataList()
    {
    	return vehicleDataList;
    }
    
    
    public static ArrayList<PedestrianData> getPedestrianDataList()
    {
    	return pedestrianDataList;
    }

    
    public static ArrayList<OpenDRIVECarData> getOpenDRIVECarDataList()
    {
    	return openDRIVEDataList;
    }
    
    
	public static ArrayList<StaticTrafficObject> getStaticTrafficObjectList() 
	{
		return trafficObjectList;		
	}

	
	public StaticTrafficObject getStaticTrafficObject(String trafficObjectName) 
	{
		for(StaticTrafficObject trafficObject : trafficObjectList)
		{
			//System.err.println(trafficObject.getName());
			if(trafficObject.getName().equals(trafficObjectName))
				return trafficObject;
		}
		
		return null;
	}



}
