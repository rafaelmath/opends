/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.traffic;

import java.util.HashSet;
import java.util.Properties;

import com.jme3.bounding.BoundingBox;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.shape.Box;

import eu.opends.basics.SimulationBasics;
import eu.opends.opendrive.processed.ODLane;
import eu.opends.tools.Util;

/**
 * 
 * @author Rafael Math
 */
public class StaticOpenDRIVECar implements StaticTrafficObject
{
	private SimulationBasics sim;
	
	private String name;
	@Override
	public String getName()
	{
		return name;
	}
	
	private Node carNode;
	public Node getCarNode()
	{
		return carNode;
	}
	
	public StaticOpenDRIVECar(SimulationBasics sim, OpenDRIVECarData carData)
	{
		this.sim = sim;
		
		name = carData.getName();
		String modelPath = carData.getModelPath();
		
		// load settings from car properties file
		String propertiesPath = modelPath.replace(".j3o", ".properties");
		propertiesPath = propertiesPath.replace(".scene", ".properties");
		Properties properties = (Properties) sim.getAssetManager().loadAsset(propertiesPath);
			
		// chassis properties
		Vector3f chassisScale = new Vector3f(getVector3f(properties, "chassisScale", 1));
		Vector3f chassisPosition = new Vector3f(getVector3f(properties, "staticChassisPosition", 0)).mult(chassisScale);
		
		// wheel properties
		float wheelScale;
		String wheelScaleString = properties.getProperty("wheelScale");
		if(wheelScaleString != null)
			wheelScale = Float.parseFloat(wheelScaleString);
		else
			wheelScale = chassisScale.getY();

		
  		// wheel positions
   		float frontAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("frontAxlePos"));
   		float backAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("backAxlePos"));
   		float leftWheelsPos = chassisScale.x * Float.parseFloat(properties.getProperty("leftWheelsPos"));
   		float rightWheelsPos = chassisScale.x * Float.parseFloat(properties.getProperty("rightWheelsPos"));
   		float frontAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("frontAxleHeight"));
   		float backAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("backAxleHeight"));
   		
   		if(properties.getProperty("staticFrontAxlePos") != null)
   			frontAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("staticFrontAxlePos"));
   		
   		if(properties.getProperty("staticBackAxlePos") != null)
   			backAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("staticBackAxlePos"));
   		
   		if(properties.getProperty("staticLeftWheelsPos") != null)
   			leftWheelsPos = chassisScale.x * Float.parseFloat(properties.getProperty("staticLeftWheelsPos"));
   		
   		if(properties.getProperty("staticRightWheelsPos") != null)
   			rightWheelsPos = chassisScale.x * Float.parseFloat(properties.getProperty("staticRightWheelsPos"));
   		
   		if(properties.getProperty("staticFrontAxleHeight") != null)
   			frontAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("staticFrontAxleHeight"));
   		
   		if(properties.getProperty("staticBackAxleHeight") != null)
   			backAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("staticBackAxleHeight"));

   		
       	carNode = (Node)sim.getAssetManager().loadModel(modelPath);
        	
        // get chassis geometry and corresponding node
        Geometry chassis = Util.findGeom(carNode, "Chassis");

        //chassis.getMaterial().setColor("GlowColor", ColorRGBA.Orange);
        Node chassisNode = chassis.getParent();

        // scale chassis
        for(Geometry geo : Util.getAllGeometries(chassisNode))
        	geo.setLocalScale(chassisScale);
        
        for(Spatial s : carNode.getChildren())
        	s.setLocalTranslation(chassisPosition);

        //extra static scale
        Vector3f staticScale = new Vector3f(getVector3f(properties, "staticScale", 1));
        carNode.setLocalScale(staticScale);
        
        // add front right wheel
        Geometry geom_wheel_fr = Util.findGeom(carNode, "WheelFrontRight");
        geom_wheel_fr.setLocalScale(wheelScale);
        geom_wheel_fr.setLocalTranslation(rightWheelsPos, frontAxleHeight, frontAxlePos);
        
        // add front left wheel
        Geometry geom_wheel_fl = Util.findGeom(carNode, "WheelFrontLeft");
        geom_wheel_fl.setLocalScale(wheelScale);
        geom_wheel_fl.setLocalTranslation(leftWheelsPos, frontAxleHeight, frontAxlePos);
        
        // add back right wheel
        Geometry geom_wheel_br = Util.findGeom(carNode, "WheelBackRight");
        geom_wheel_br.setLocalScale(wheelScale);
        geom_wheel_br.setLocalTranslation(rightWheelsPos, backAxleHeight, backAxlePos);

        
        // add back left wheel
        Geometry geom_wheel_bl = Util.findGeom(carNode, "WheelBackLeft");
        geom_wheel_bl.setLocalScale(wheelScale);
        geom_wheel_bl.setLocalTranslation(leftWheelsPos, backAxleHeight, backAxlePos);
        
        
        if(properties.getProperty("thirdAxlePos") != null && properties.getProperty("thirdAxleHeight") != null)
        {
        	float thirdAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("thirdAxlePos"));
    		float thirdAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("thirdAxleHeight"));
    		
    		if(properties.getProperty("staticThirdAxlePos") != null)
       			thirdAxlePos = chassisScale.z * Float.parseFloat(properties.getProperty("staticThirdAxlePos"));
       		
       		if(properties.getProperty("staticThirdAxleHeight") != null)
       			thirdAxleHeight = chassisScale.y * Float.parseFloat(properties.getProperty("staticThirdAxleHeight"));
       		
       		
	        // add back right wheel 2
	        Geometry geom_wheel_br2 = Util.findGeom(carNode, "WheelBackRight2");
	        geom_wheel_br2.setLocalScale(wheelScale);
	        geom_wheel_br2.setLocalTranslation(rightWheelsPos, thirdAxleHeight, thirdAxlePos);
	        
	        
	        // add back left wheel 2
	        Geometry geom_wheel_bl2 = Util.findGeom(carNode, "WheelBackLeft2");
	        geom_wheel_bl2.setLocalScale(wheelScale);
	        geom_wheel_bl2.setLocalTranslation(leftWheelsPos, thirdAxleHeight, thirdAxlePos);
        }
        
        // add car node to rendering node
        sim.getSceneNode().attachChild(carNode);
        
        // setup reference points
        setupReferencePoints();
	}


	private void flipGeometry(Geometry geom)
	{
		// flip wheels if native Bullet is used
		Quaternion q = new Quaternion();
		q.fromAngles(0, FastMath.PI, 0);
		geom.setLocalRotation(q);
	}


	private Vector3f getCollisionShapeOffset(Properties properties) 
	{
		float offsetX = 0;
        float offsetY = 0;
        float offsetZ = 0;
        
        if(properties.getProperty("collisionShapePos.x") != null)
        	offsetX = Float.parseFloat(properties.getProperty("collisionShapePos.x"));
        
        if(properties.getProperty("collisionShapePos.y") != null)
        	offsetY = Float.parseFloat(properties.getProperty("collisionShapePos.y"));
        
        if(properties.getProperty("collisionShapePos.z") != null)
        	offsetZ = Float.parseFloat(properties.getProperty("collisionShapePos.z"));

        return new Vector3f(offsetX, offsetY ,offsetZ);
	}


	private Spatial findLargestSpatial(Node chassisNode) 
	{
		// if no child larger than chassisNode available, return chassisNode
		Spatial largestSpatial = chassisNode;
        int vertexCount = 0;
        
        for(Spatial n : chassisNode.getChildren())
        {
        	if(n.getVertexCount() > vertexCount)
        	{
        		largestSpatial = n;
        		vertexCount = n.getVertexCount();
        	}
        }
        
		return largestSpatial;
	}

	private void setupReferencePoints()
	{
		Node leftPoint = new Node("leftPoint");
		leftPoint.setLocalTranslation(-1, 1, 0);
		carNode.attachChild(leftPoint);
		
		Node rightPoint = new Node("rightPoint");
		rightPoint.setLocalTranslation(1, 1, 0);
		carNode.attachChild(rightPoint);
		
		Node frontPoint = new Node("frontPoint");
		frontPoint.setLocalTranslation(0, 1, -2);
		carNode.attachChild(frontPoint);
		
		Node backPoint = new Node("backPoint");
		backPoint.setLocalTranslation(0, 1, 2);
		carNode.attachChild(backPoint);
	}
	


	
	private Vector3f getVector3f(Properties properties, String key, float defaultValue)
	{
		float x = defaultValue;
        float y = defaultValue;
        float z = defaultValue;
        
		String xValue = properties.getProperty(key + ".x");
		if(xValue != null)
			x = Float.parseFloat(xValue);
		
		String yValue = properties.getProperty(key + ".y");
		if(yValue != null)
			y = Float.parseFloat(yValue);
		
		String zValue = properties.getProperty(key + ".z");
		if(zValue != null)
			z = Float.parseFloat(zValue);

        return new Vector3f(x,y,z);
	}


	@Override
	public void setPosition(Vector3f position)
	{
		carNode.setLocalTranslation(position);
		updateCurrentODPosition();
	}


	@Override
	public void setRotation(Quaternion rotation)
	{
		carNode.setLocalRotation(rotation);
	}

	
	private ODLane lane = null;
	private double s = 0;
	private void updateCurrentODPosition()
	{
		Vector3f carPos = carNode.getWorldTranslation();
		HashSet<ODLane> emptySet = new HashSet<ODLane>();
		lane = sim.getOpenDriveCenter().getMostProbableLane(carPos, emptySet, emptySet);
		
		if(lane != null)
			s = lane.getCurrentInnerBorderPoint().getS();
		else
			s = 0;
	}
	
	
	public ODLane getCurrentLane()
	{
		return lane;
	}

	
	public double getCurrentS()
	{
		return s;
	}
	
}
