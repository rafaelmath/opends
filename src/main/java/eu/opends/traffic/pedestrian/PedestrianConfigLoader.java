/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.traffic.pedestrian;

import java.io.File;
import java.util.HashMap;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import eu.opends.traffic.pedestrian.data.CollisionShape;
import eu.opends.traffic.pedestrian.data.Animation;
import eu.opends.traffic.pedestrian.data.Character;
import eu.opends.traffic.pedestrian.data.ObjectFactory;
import eu.opends.traffic.pedestrian.data.Pedestrian;


public class PedestrianConfigLoader
{
	private static String schemaFile = "assets/DrivingTasks/Schema/pedestrian.xsd";
	private Unmarshaller jaxbUnmarshaller;
	private float radius = 0.3f;
	private float height = 1.8f;
	private float mass = 5.0f;
	private Vector3f translation = new Vector3f(0, 0, 0);
	private Quaternion rotation = new Quaternion();
	private Vector3f scale = new Vector3f(1, 1, 1);
	private float brightness = 0.7f;
	private boolean isCastShadow = true;
	private HashMap<String,Animation> animationMap = new HashMap<String,Animation>(); 
	
	
	public PedestrianConfigLoader(String modelPath)
	{
		try {

			// init unmarshaller
			JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			// set schema file used to validate XML file
			Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(schemaFile));
			jaxbUnmarshaller.setSchema(schema);
			
			loadPedestrianConfigFile(modelPath);
		
		} catch (jakarta.xml.bind.UnmarshalException e){
			
			System.err.println(e.getLinkedException().toString());
			
		} catch (JAXBException e){
		
			e.printStackTrace();
			
		} catch (SAXException e){
		
			e.printStackTrace();
		}
	}

	
	public void loadPedestrianConfigFile(String modelPath)
	{	
		try{
			
			File modelFile = new File(modelPath);
			String configPath = modelFile.getPath().replace(modelFile.getName(), "config.xml");
			if(!configPath.startsWith("assets"))
				configPath = "assets/" + configPath;

			Object object = jaxbUnmarshaller.unmarshal(new File(configPath));
			if(object instanceof JAXBElement)
			{
				JAXBElement<?> jaxbElement = (JAXBElement<?>) object;
				Object object2 = jaxbElement.getValue();
				if(object2 instanceof Pedestrian)
				{
					Pedestrian pedestrianConfig = (Pedestrian) object2;

					CollisionShape collisionShape = pedestrianConfig.getCollisionShape();
					if(collisionShape != null)
					{
						if(collisionShape.getRadius() != null)
							radius = collisionShape.getRadius();

						if(collisionShape.getHeight() != null)
							height = collisionShape.getHeight();
						
						if(collisionShape.getMass() != null)
							mass = collisionShape.getMass();
					}
					
					Character character = pedestrianConfig.getCharacter();
					if(character != null)
					{
						if(character.getTranslation() != null)
						{
							float x = character.getTranslation().getX();
							float y = character.getTranslation().getY();
							float z = character.getTranslation().getZ();
							translation.set(x, y, z);
						}
							
						if(character.getRotation() != null)
						{
							float x = FastMath.DEG_TO_RAD * character.getRotation().getX();
							float y = FastMath.DEG_TO_RAD * character.getRotation().getY();
							float z = FastMath.DEG_TO_RAD * character.getRotation().getZ();
							rotation.fromAngles(x, y, z);
						}
						
						if(character.getScale() != null)
						{
							float x = character.getScale().getX();
							float y = character.getScale().getY();
							float z = character.getScale().getZ();
							scale.set(x, y, z);
						}
						
						if(character.getBrightness() != null)
							brightness = character.getBrightness();
						
						if(character.isCastShadow() != null)
							isCastShadow = character.isCastShadow();
					}
					
					if(pedestrianConfig.getAnimationList() != null)
					{
						for(Animation animation : pedestrianConfig.getAnimationList().getAnimation())
						{
							if(animation.getName() != null && !animation.getName().isEmpty())
							{
								if(animation.getSpeed() == null || animation.getSpeed() <= 0)
									animation.setSpeed(1.0f);
								 
								if(animation.isCharacterInMotion() == null)
									animation.setCharacterInMotion(false);
								 
								animationMap.put(animation.getName(), animation);
							}	 
						}
					}
				}
			}
		} 
		
		catch (JAXBException e){
			
			//e.printStackTrace();
			System.err.println("PedestrianConfigLoader: Failed to parse XML file. (Error message: " 
					+ e.getLinkedException().getLocalizedMessage() + ")");
		}
		
		catch (IllegalArgumentException e){
			
			//e.printStackTrace();
			System.err.println("PedestrianConfigLoader: Failed to parse XML file: " 
					+ e.getMessage());
		}
	}


	public float getRadius()
	{
		return radius;
	}


	public float getHeight()
	{
		return height;
	}


	public float getMass()
	{
		return mass;
	}


	public Vector3f getTranslation()
	{
		return translation;
	}


	public Quaternion getRotation()
	{
		return rotation;
	}


	public Vector3f getScale()
	{
		return scale;
	}


	public float getBrightness()
	{
		return brightness;
	}


	public boolean isCastShadow()
	{
		return isCastShadow;
	}


	public HashMap<String,Animation> getAnimationMap()
	{
		return animationMap;
	}
}
