/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.traffic;

import com.jme3.asset.AssetManager;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Node;

import eu.opends.basics.SimulationBasics;
import eu.opends.traffic.pedestrian.PedestrianConfigLoader;
import eu.opends.traffic.pedestrian.data.Animation;

import java.util.HashMap;

/**
 * @author Rafael Math
 */
public class StaticPedestrian implements AnimationListener, StaticTrafficObject
{
	private Node pedestrianNode = new Node();
	private String name;
	private Animation standAnimation = null;
	private PedestrianConfigLoader configLoader;
	
	
    public StaticPedestrian(SimulationBasics sim, PedestrianData pedestrianData)
    {
    	name = pedestrianData.getName();

		AssetManager assetManager = sim.getAssetManager();
		String modelPath = pedestrianData.getModelPath();
		Node pedestrianModelNode = (Node) assetManager.loadModel(modelPath);
		pedestrianNode.attachChild(pedestrianModelNode);
		
		// configLoader originates from config.xml in the folder of the respective pedestrian model
		configLoader = new PedestrianConfigLoader(modelPath);
		
		// adjust scale, translation, and rotation of model
		pedestrianModelNode.setLocalScale(configLoader.getScale());
		pedestrianModelNode.setLocalTranslation(configLoader.getTranslation());
		pedestrianModelNode.setLocalRotation(configLoader.getRotation());
		
		// adjust ambient light
		AmbientLight light = new AmbientLight();
		light.setColor(ColorRGBA.White.mult(configLoader.getBrightness()));
		pedestrianModelNode.addLight(light);
		
		// shadow of character
		if(configLoader.isCastShadow())
			pedestrianModelNode.setShadowMode(ShadowMode.Cast);
		else
			pedestrianModelNode.setShadowMode(ShadowMode.Off);
		
		// init animation
		setAnimation("Stand_With_Stiff_Arms"/*pedestrianData.getAnimationStand()*/);

		sim.getSceneNode().attachChild(pedestrianNode);
		
		AnimationController animationController = new AnimationController(pedestrianNode);
		
		// play stand animation
		if (standAnimation != null)
		{
			float animationSpeed = (1.0f/configLoader.getScale().getZ()) * standAnimation.getSpeed();
			animationController.animate(standAnimation.getName(), animationSpeed, 1f, 0);
		}
    }


	public void setAnimation(String animationName)
	{
		HashMap<String, Animation> animationMap = configLoader.getAnimationMap();
		
		Animation animation = animationMap.get(animationName);
		if(animation == null)
		{
			// look up in list for the first animation of same type
			for(Animation a : animationMap.values())
			{
				if(!a.isCharacterInMotion())
				{
					animation = a;
					break;
				}
			}
			
			System.err.println("Pedestrian '" + name + "' does not have an animation named '" + animationName + "'");

			if(animation != null)
				System.err.println("Using animation '" + animation.getName() + "' instead.");
		}
		
		standAnimation = animation;
	}
    
	
    @Override
    public void onAnimCycleDone(final String animationName)
    {
    	
    }

    
	public String getName() 
	{
		return name;
	}

	
	@Override
	public void setPosition(Vector3f position)
	{
		pedestrianNode.setLocalTranslation(position);
	}


	@Override
	public void setRotation(Quaternion rotation)
	{
		pedestrianNode.setLocalRotation(rotation);
	}

}