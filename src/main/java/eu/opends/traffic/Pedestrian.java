/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.traffic;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Node;

import eu.opends.environment.TrafficLightCenter;
import eu.opends.infrastructure.Segment;
import eu.opends.infrastructure.Waypoint;
import eu.opends.main.Simulator;
import eu.opends.tools.Util;
import eu.opends.traffic.pedestrian.PedestrianConfigLoader;
import eu.opends.traffic.pedestrian.data.Animation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Tommi S.E. Laukkanen, Rafael Math
 */
public class Pedestrian implements AnimationListener, TrafficObject
{
	private Simulator sim;
	private Node pedestrianNode = new Node("pedestrianNode");
	private BetterCharacterControl characterControl;
	private AnimationController animationController;
	private FollowBox followBox;
	private String name;
	private float mass = 5f;
	private float airTime = 0;
	private HashMap<String, Animation> animationMap;
	
	// the animation commands
	private Animation standAnimation = null;
	private Animation walkAnimation = null;
	
	// safety distances (distance where pedestrian will stop walking)
	private float minLateralSafetyDistance = 2.0f;
	private float minForwardSafetyDistance = 4.0f;

	// walking speed of the character
	private float walkingSpeedKmh = 4f;
	private boolean walkingSpeedChanged = true;
	private boolean enabled = true;

	private boolean externalControl = false;
	private float externalHeading = 0;
	private float externalSpeedKmh = 4;
	
	private PedestrianData pedestrianData;
	private PedestrianConfigLoader configLoader;
	private boolean initialized = false;
	
	
    public Pedestrian(Simulator sim, PedestrianData pedestrianData) 
    {
    	this.sim = sim;
    	
    	// pedestrianData originates from scenario.xml
    	this.pedestrianData = pedestrianData;
    
    	name = pedestrianData.getName();
    	enabled = pedestrianData.getEnabled();

		AssetManager assetManager = sim.getAssetManager();
		String modelPath = pedestrianData.getModelPath();
		Node pedestrianModelNode = (Node) assetManager.loadModel(modelPath);
		pedestrianNode.attachChild(pedestrianModelNode);
		
		// configLoader originates from config.xml in the folder of the respective pedestrian model
		configLoader = new PedestrianConfigLoader(modelPath);
		
		// set safety distances (distance where pedestrian will stop walking)
		minLateralSafetyDistance = pedestrianData.getMinLateralSafetyDistance();
		minForwardSafetyDistance = pedestrianData.getMinForwardSafetyDistance();
		
		// adjust scale, translation, and rotation of model
		pedestrianModelNode.setLocalScale(configLoader.getScale());
		pedestrianModelNode.setLocalTranslation(configLoader.getTranslation());
		pedestrianModelNode.setLocalRotation(configLoader.getRotation());
		
		// adjust ambient light
		AmbientLight light = new AmbientLight();
		light.setColor(ColorRGBA.White.mult(configLoader.getBrightness()));
		pedestrianModelNode.addLight(light);
		
		// shadow of character
		if(configLoader.isCastShadow())
			pedestrianModelNode.setShadowMode(ShadowMode.Cast);
		else
			pedestrianModelNode.setShadowMode(ShadowMode.Off);
		
		// construct character (if character bounces, try increasing height and weight)
		mass = configLoader.getMass();
		characterControl = new BetterCharacterControl(configLoader.getRadius(), configLoader.getHeight(), mass);
		pedestrianNode.addControl(characterControl);
		
		// init animations
		animationMap = configLoader.getAnimationMap();
		setAnimation(pedestrianData.getAnimationStand(), false);
		setAnimation(pedestrianData.getAnimationWalk(), true);
    }


	public void setAnimation(String animationName, boolean isCharacterInMotion)
	{
		Animation animation = animationMap.get(animationName);
		if(animation == null)
		{
			// look up in list for the first animation of same type
			for(Animation a : animationMap.values())
			{
				if(a.isCharacterInMotion() == isCharacterInMotion)
				{
					animation = a;
					break;
				}
			}
			
			System.err.println("Pedestrian '" + name + "' does not have an animation named '" + animationName + "'");

			if(animation != null)
				System.err.println("Using animation '" + animation.getName() + "' instead.");
		}
		
		if(isCharacterInMotion)
			walkAnimation = animation;
		else
			standAnimation = animation;
			
	}
    
    
    private void init()
    {
		// add to physics state
		sim.getBulletAppState().getPhysicsSpace().add(characterControl); 
		sim.getBulletAppState().getPhysicsSpace().addAll(pedestrianNode); 
		sim.getSceneNode().attachChild(pedestrianNode);
		
		animationController = new AnimationController(pedestrianNode);
		animationController.setAnimationListener(this);
		
		//printAvailableAnimations("Body");
		
		followBox = new FollowBox(sim, this, pedestrianData.getFollowBoxSettings(), true);
		
		initialized = true;
    }
    
    
    @Override
	public void update(float tpf, ArrayList<TrafficObject> vehicleList) 
    {
    	// prevent pedestrians from high jump when adding to the physics engine
    	if(tpf < 1.0f && !initialized)
    		init();
    	
        if(!externalControl)
        {
	    	if(initialized)
	    	{
				// update movement of follow box according to pedestrians's position (not affected by sim.isPause())
				followBox.update(tpf, pedestrianNode.getWorldTranslation());
				
				if(!sim.isPause())
				{
					// maximum speed for current way point segment
					float nextWalkingSpeedKmh = followBox.getSpeed();
			    	
			    	if(nextWalkingSpeedKmh != walkingSpeedKmh)
			    	{
			    		walkingSpeedKmh = nextWalkingSpeedKmh;
			    		walkingSpeedChanged = true;
			    	}
			    	
			        if (!characterControl.isOnGround()) 
			            airTime += tpf;
			        else
			            airTime = 0;
			        
			        // compute view direction (towards follow box) in upright walking position (y = 0)
			        Vector3f viewDirection = followBox.getPosition().subtract(pedestrianNode.getWorldTranslation());
			        viewDirection.setY(0);
			        
			        float distance = viewDirection.length();
			        if (distance != 0)
			        	characterControl.setViewDirection(viewDirection);
			       
			        
			        if (distance < 0.1f || obstaclesInTheWay(vehicleList) || !enabled || followBox.isStopped())
			        { 
			        	playStandAnimation();
			        	characterControl.setWalkDirection(new Vector3f(0,0,0)); // stop walking
			        } 
			        else 
			        {
			            if (airTime > 0.3f)
			            	playStandAnimation();
			            else
							playWalkAnimation(walkingSpeedKmh);
			            
			            // the use of the multiplier is to control the rate of movement for character walk speed (in m/s)
			            characterControl.setWalkDirection(viewDirection.normalize().multLocal((walkingSpeedKmh/3.6f)));
			        }
			
			        //System.err.println("Current speed of character '" + name + "': " + getCurrentSpeedKmh());
			        
			    	animationController.update(tpf);   	
			    }
	    	}
        }
        else
        {
        	if (!characterControl.isOnGround()) 
	            airTime += tpf;
	        else
	            airTime = 0;
	        
	        // compute view direction (towards car) in upright walking position (y = 0)
        	float x = FastMath.cos(externalHeading);
        	float z = FastMath.sin(externalHeading);
        	Vector3f viewDirection = new Vector3f(x, 0, z);
	        
	        float distance = viewDirection.length();
	        if (distance != 0)
	        	characterControl.setViewDirection(viewDirection);
	       
            if (airTime > 0.3f)
            	playStandAnimation();
            else 
            	playWalkAnimation(externalSpeedKmh);

            
            // the use of the multiplier is to control the rate of movement for character walk speed (in m/s)
            characterControl.setWalkDirection(viewDirection.normalize().multLocal((externalSpeedKmh/3.6f)));
	        
	    	animationController.update(tpf);
        }
    }


	private void playWalkAnimation(float speedKmh)
	{
		if (walkAnimation != null && 
				(!walkAnimation.getName().equals(animationController.getAnimationName()) || walkingSpeedChanged))
         {
			float animationSpeed = 0.25f * speedKmh * (1.0f/configLoader.getScale().getZ()) * walkAnimation.getSpeed();
			animationController.animate(walkAnimation.getName(), animationSpeed, 0.7f, 0);
			walkingSpeedChanged = false;
         }
	}


	private void playStandAnimation()
	{
		if (standAnimation != null && !standAnimation.getName().equals(animationController.getAnimationName()))
		{
			float animationSpeed = (1.0f/configLoader.getScale().getZ()) * standAnimation.getSpeed();
			animationController.animate(standAnimation.getName(), animationSpeed, 1f, 0);
		}
	}
    
    
    public void setEnabled(boolean enabled)
    {
    	this.enabled = enabled;
    }
    
    
    public float getCurrentSpeedKmh()
    {
    	return characterControl.getVelocity().length() * 3.6f;
    }
    
    
    public float getCurrentSpeedMs()
    {
    	return characterControl.getVelocity().length();
    }
    
    
    /*
	private void printAvailableAnimations(String mainMeshName) 
	{
		System.out.println("Animated spatials: " + animationController.getSpatialNamesWithAnimations());
		
		String mainSpatialName = null;
		for (final String spatialName : animationController.getSpatialNamesWithAnimations()) 
		{
		    if (spatialName.startsWith(mainMeshName))
		        mainSpatialName =  spatialName;
		}
		
		if (mainSpatialName != null)
		{		
			System.out.println("Main mesh: " + mainSpatialName);
			
			final AnimControl control = animationController.getAnimControl(mainSpatialName);
			if (control != null)
			{
				ArrayList<String> animations = new ArrayList<String>(control.getAnimationNames());
			    System.out.println("Available animation commands: " + animations);
			}
		}
		else
			System.out.println("No animation commands available");
	}
    */
    
	
	public void setToWayPoint(String wayPointID) 
	{
    	if(initialized)
    		followBox.setToWayPoint(wayPointID);
	}
	
	
    @Override
    public void onAnimCycleDone(final String animationName)
    {
    	
    }

    
	@Override
	public Vector3f getPosition()
	{
		return pedestrianNode.getWorldTranslation();
	}
	

	@Override
	public void setPositionRotation(Vector3f position, Quaternion quaternion) 
	{
		// set position only; automatic orientation in next update()
		characterControl.warp(position);
	}


	@Override
	public float getMaxBrakeForce() 
	{
		// needed for follow box reduced speed computation
		// not relevant for pedestrians --> return 0
		return 0;
	}


	@Override
	public float getMass() 
	{
		return mass;
	}


	@Override
	public String getName() 
	{
		return name;
	}
	
	
	private boolean obstaclesInTheWay(ArrayList<TrafficObject> trafficObjectList)
	{		
		// check distance from user-controlled car
		if(obstacleTooClose(sim.getCar().getPosition()))
			return true;

		// check distance from other cars (exclude pedestrians)
		for(TrafficObject vehicle : trafficObjectList)
		{
			if(!vehicle.getName().equals(name) && vehicle instanceof TrafficCar)		
				if(obstacleTooClose(vehicle.getPosition()))
					return true;
		}
		

		// check if red traffic light ahead
		Waypoint nextWayPoint = followBox.getNextWayPoint();
		if(TrafficLightCenter.hasRedTrafficLight(nextWayPoint))
			if(obstacleTooClose(nextWayPoint.getPosition()))
				return true;

		return false;
	}


	private boolean obstacleTooClose(Vector3f obstaclePos)
	{
		float distanceToObstacle = obstaclePos.distance(getPosition());
		
		// angle between view direction of pedestrian and direction towards obstacle
		// (consider 3D space, because obstacle could be located on a bridge above pedestrian)
		Vector3f viewDirection = characterControl.getViewDirection().normalize();
		Vector3f obstacleDirection = obstaclePos.subtract(this.getPosition()).normalize();
		
		float angle = viewDirection.angleBetween(obstacleDirection);
			
		//if(name.equals("pedestrian01"))
		//	System.out.println(angle * FastMath.RAD_TO_DEG);
		
		if(belowSafetyDistance(angle, distanceToObstacle))
			return true;

		// considering direction towards next way point (if available)
		Waypoint nextWP = followBox.getNextWayPoint();
		if(nextWP != null)
		{
			// angle between direction towards next WP and direction towards obstacle
			// (consider 3D space, because obstacle could be located on a bridge above pedestrian)
			angle = Util.getAngleBetweenPoints(nextWP.getPosition(), this.getPosition(), obstaclePos, false);			
			if(belowSafetyDistance(angle, distanceToObstacle))
				return true;
		}

		return false;
	}
	
	
	private boolean belowSafetyDistance(float angle, float distance) 
	{	
		float lateralDistance = distance * FastMath.sin(angle);
		float forwardDistance = distance * FastMath.cos(angle);
		
		//if(name.equals("pedestrian01"))
		//	System.out.println(lateralDistance + " *** " + forwardDistance);
		
		if((lateralDistance < minLateralSafetyDistance) && (forwardDistance > 0) && (forwardDistance < minForwardSafetyDistance))
		{
			return true;
		}
		
		return false;
	}


	@Override
	public Segment getCurrentSegment()
	{
    	if(initialized)
			return followBox.getCurrentSegment();
    	else
			return null;
	}

	
	@Override
	public float getTraveledDistance()
	{
		if(initialized)
			return followBox.getTraveledDistance();
		else
			return 0;
	}


	@Override
	public float getDistanceToNextWP()
	{
		if(initialized)
			return followBox.getDistanceToNextWP();
		else
			return Float.MAX_VALUE;
	}


	public void useExternalControl()
	{
		externalControl = true;	
	}


	public void setHeading(float heading)
	{
		externalHeading = heading;
	}


	public void setSpeed(float speed)
	{
		if(speed != externalSpeedKmh)
    	{
			externalSpeedKmh = speed;
    		walkingSpeedChanged = true;
    	}
	}


	public float getHeading()
	{
        Vector3f viewDirection = characterControl.getViewDirection();
		return FastMath.atan2(viewDirection.x, -viewDirection.z);
	}
	
	
	public float getHeadingDegree()
	{
		return getHeading() * FastMath.RAD_TO_DEG;
	}


	private float previousSpeed = 0;
	public double getSpeedDerivative(float secondsSinceLastUpdate)
	{
		float currentSpeed = getCurrentSpeedMs();

		float currentAcceleration = (currentSpeed - previousSpeed) / secondsSinceLastUpdate; // in m/s^2
	    previousSpeed = currentSpeed;

		return currentAcceleration;
	}

	
	public float getHdgDiff(float referenceHdg)
	{
		float hdgDiff = referenceHdg - getHeading();
		
    	if(hdgDiff > FastMath.PI)  // 180
    		hdgDiff -= FastMath.TWO_PI;  // 360
    	
    	if(hdgDiff < -FastMath.PI)  // 180
    		hdgDiff += FastMath.TWO_PI;  // 360
    	
		return hdgDiff;
	}


	private float previousHdgDiff = 0;
	public float getHdgDiffDerivative(float referenceHdg, float secondsSinceLastUpdate)
	{
		float currentHdgDiff = getHdgDiff(referenceHdg);

		float currentHdgDiffDerivative = (currentHdgDiff - previousHdgDiff) / secondsSinceLastUpdate; // in rad/s
	    previousHdgDiff = currentHdgDiff;

		return currentHdgDiffDerivative;
	}


	public Node getPedestrianNode()
	{
		return pedestrianNode;
	}

  
}