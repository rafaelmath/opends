/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.analyzer;

import java.util.ArrayList;
import java.util.Date;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import eu.opends.car.LightTexturesContainer.TurnSignalState;
import eu.opends.environment.TrafficLight.TrafficLightState;
import eu.opends.gesture.RecordedReferenceObject;
import eu.opends.opendrive.data.ELaneType;
import eu.opends.opendrive.processed.ODLane.AdasisLaneInformation;
import eu.opends.opendrive.processed.ODLane.AdasisLaneType;
import eu.opends.opendrive.processed.ODLane.AdasisLineType;

public class DataUnit implements Cloneable
{
	private Date date = null;
	private String timestamp = null;
	private String simulatorState = null;
	private Float speedKmh = null;
	private Float rpm = null;
	private Float steeringWheelPos = null;
	private Float acceleratorPedalPos = null;
	private Float brakePedalPos = null;
	private Float clutchPedalPos = null;
	private Boolean isHandBrakeEngaged = null;
	private Boolean isEngineOn = null;
	private TurnSignalState turnSignalState = null;
	private Boolean isLightOn = null;
	private Float lightIntensity = null;
	private Boolean isAutoPilotOn = null;
	private Boolean isBrakeLightOn = null;
	private Boolean isCruiseControlOn = null;
	private Float cruiseControlTargetSpeedKmh = null;
	private Boolean isAutomatic = null;
	private Integer selectedGear = null;
	private Float milage = null;
	private Float headingDegree = null;
	private Vector3f position = null;
	private Quaternion rotation = null;
	private Float slopeDegree = null;	
	private Float accelerationLgt = null;
	private Float accelerationLat = null;
	private Float snowingPercentage = null;
	private Float rainingPercentage = null;
	private Float fogPercentage = null;
	private String roadID = null;
	private Integer laneID = null;
	private Float s = null;
	private Float hdgLane = null;
	private Float hdgCar = null;
	private Float hdgDiff = null;
	private Boolean isWrongWay = null;
	private ELaneType laneType = null;	
	private AdasisLaneType lanePosition = null;
	private Integer nrObjs = null;
	private ArrayList<String> objName = null;
	private ArrayList<Integer> objClass = null; 
	private ArrayList<String> objClassString = null;
	private ArrayList<Float> objX = null;
	private ArrayList<Float> objY = null;
	private ArrayList<Float> objDist = null;
	private ArrayList<Float> objDirection = null;
	private ArrayList<Float> objVel = null;
	private ArrayList<Vector3f> objPos = null;
	private ArrayList<Quaternion> objRot = null;
	private Float laneWidth = null;
	private Float latOffsLineR = null;
	private Float latOffsLineL = null;
	private Float laneCrvt = null;
	private AdasisLineType leftLineType = null;
	private AdasisLineType rightLineType = null;
	private AdasisLaneInformation leftLaneInfo = null; 
	private AdasisLaneInformation rightLaneInfo = null;
	private Boolean sideObstacleLeft = null;
	private Boolean sideObstacleRight = null;
	private Boolean blindSpotObstacleLeft = null;
	private Boolean blindSpotObstacleRight = null;
	private Integer nrLanesDrivingDirection = null;
	private Integer nrLanesOppositeDirection = null;
	private Integer currentSpeedLimit = null;
	private Integer nrSpeedLimits = null;
	private ArrayList<Float> speedLimitDist = null;
	private ArrayList<Integer> speedLimitValues = null;
	private Float intersectionDistance = null;
	private Boolean trafficLightAhead = null;
	private Float trafficLightDist = null;    
	private ArrayList<TrafficLightState> trafficLightStates = null;
	private ArrayList<Float> trafficLightTimesToChange = null;
	private Float targetDistance = null;
	private Boolean leadingCarPresent = null;
	private String leadingCarName = null;
	private String leadingCarRoadID = null;
	private Integer leadingCarLaneID = null;
	private Float leadingCarS = null;
	private Float leadingCarDist = null;
	private Float leadingCarSpeedKmh = null;
	private Date eyeTrackerDate = null;
	private String eyeTrackerTimestamp = null;
	private Boolean isValidLeftEye = null;
	private Boolean isValidRightEye = null;
	private Vector2f gazeCoordinate = null;
	private Vector2f gazeCoordinateAvg = null;
	private Vector3f gazeRayOrigin = null;
	private Vector3f gazeRayDirection = null;
	private String closestHitByGazeRay = null;
	private Vector3f closestContactPointByGazeRay = null;
	private Vector3f frontPosition = new Vector3f();
	private String referenceObjectData = null;
	private String aoi2D = null;
	private ArrayList<RecordedReferenceObject> referenceObjectList = new ArrayList<RecordedReferenceObject>();
	private Float traveledDistance = null;
	
	
	public DataUnit()
	{
		
	}
	
	
	public Date getDate()
	{
		return date;
	}
	
	
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	
	public String getTimestamp()
	{
		return timestamp;
	}
	
	
	public void setTimestamp(String timestamp)
	{
		this.timestamp = timestamp;
	}
	
	
	public String getSimulatorState()
	{
		return simulatorState;
	}
	
	
	public void setSimulatorState(String simulatorState)
	{
		this.simulatorState = simulatorState;
	}
	
	
	public Float getSpeedKmh()
	{
		return speedKmh;
	}
	
	
	public void setSpeedKmh(Float speedKmh)
	{
		this.speedKmh = speedKmh;
	}
	
	
	public Float getRpm()
	{
		return rpm;
	}
	
	
	public void setRpm(Float rpm)
	{
		this.rpm = rpm;
	}
	
	
	public Float getSteeringWheelPos()
	{
		return steeringWheelPos;
	}
	
	
	public void setSteeringWheelPos(Float steeringWheelPos)
	{
		this.steeringWheelPos = steeringWheelPos;
	}
	
	
	public Float getAcceleratorPedalPos()
	{
		return acceleratorPedalPos;
	}
	
	
	public void setAcceleratorPedalPos(Float acceleratorPedalPos)
	{
		this.acceleratorPedalPos = acceleratorPedalPos;
	}
	
	
	public Float getBrakePedalPos()
	{
		return brakePedalPos;
	}
	
	
	public void setBrakePedalPos(Float brakePedalPos)
	{
		this.brakePedalPos = brakePedalPos;
	}
	
	
	public Float getClutchPedalPos()
	{
		return clutchPedalPos;
	}
	
	
	public void setClutchPedalPos(Float clutchPedalPos)
	{
		this.clutchPedalPos = clutchPedalPos;
	}
	
	
	public Boolean getIsHandBrakeEngaged()
	{
		return isHandBrakeEngaged;
	}
	
	
	public void setIsHandBrakeEngaged(Boolean isHandBrakeEngaged)
	{
		this.isHandBrakeEngaged = isHandBrakeEngaged;
	}
	
	
	public Boolean getIsEngineOn()
	{
		return isEngineOn;
	}
	
	
	public void setIsEngineOn(Boolean isEngineOn)
	{
		this.isEngineOn = isEngineOn;
	}
	
	
	public TurnSignalState getTurnSignalState()
	{
		return turnSignalState;
	}
	
	
	public void setTurnSignalState(TurnSignalState turnSignalState)
	{
		this.turnSignalState = turnSignalState;
	}
	
	
	public Boolean getIsLightOn()
	{
		return isLightOn;
	}
	
	
	public void setIsLightOn(Boolean isLightOn)
	{
		this.isLightOn = isLightOn;
	}
	
	
	public Float getLightIntensity()
	{
		return lightIntensity;
	}
	
	
	public void setLightIntensity(Float lightIntensity)
	{
		this.lightIntensity = lightIntensity;
	}
	
	
	public Boolean getIsAutoPilotOn()
	{
		return isAutoPilotOn;
	}
	
	
	public void setIsAutoPilotOn(Boolean isAutoPilotOn)
	{
		this.isAutoPilotOn = isAutoPilotOn;
	}
	
	
	public Boolean getIsBrakeLightOn()
	{
		return isBrakeLightOn;
	}
	
	
	public void setIsBrakeLightOn(Boolean isBrakeLightOn)
	{
		this.isBrakeLightOn = isBrakeLightOn;
	}
	
	
	public Boolean getIsCruiseControlOn()
	{
		return isCruiseControlOn;
	}
	
	
	public void setIsCruiseControlOn(Boolean isCruiseControlOn)
	{
		this.isCruiseControlOn = isCruiseControlOn;
	}
	
	
	public Float getCruiseControlTargetSpeedKmh()
	{
		return cruiseControlTargetSpeedKmh;
	}
	
	
	public void setCruiseControlTargetSpeedKmh(Float cruiseControlTargetSpeedKmh)
	{
		this.cruiseControlTargetSpeedKmh = cruiseControlTargetSpeedKmh;
	}
	
	
	public Boolean getIsAutomatic()
	{
		return isAutomatic;
	}
	
	
	public void setIsAutomatic(Boolean isAutomatic)
	{
		this.isAutomatic = isAutomatic;
	}
	
	
	public Integer getSelectedGear()
	{
		return selectedGear;
	}
	
	
	public void setSelectedGear(Integer selectedGear)
	{
		this.selectedGear = selectedGear;
	}
	
	
	public Float getMilage()
	{
		return milage;
	}
	
	
	public void setMilage(Float milage)
	{
		this.milage = milage;
	}
	
	
	public Float getHeadingDegree()
	{
		return headingDegree;
	}
	
	
	public void setHeadingDegree(Float headingDegree)
	{
		this.headingDegree = headingDegree;
	}
	
	
	public Vector3f getCarPosition()
	{
		return position;
	}
	
	
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	
	
	public Quaternion getCarRotation()
	{
		return rotation;
	}
	
	
	public void setRotation(Quaternion rotation)
	{
		this.rotation = rotation;
	}
	
	
	public Float getSlopeDegree()
	{
		return slopeDegree;
	}
	
	
	public void setSlopeDegree(Float slopeDegree)
	{
		this.slopeDegree = slopeDegree;
	}
	
	
	public Float getAccelerationLgt()
	{
		return accelerationLgt;
	}
	
	
	public void setAccelerationLgt(Float accelerationLgt)
	{
		this.accelerationLgt = accelerationLgt;
	}
	
	
	public Float getAccelerationLat()
	{
		return accelerationLat;
	}
	
	
	public void setAccelerationLat(Float accelerationLat)
	{
		this.accelerationLat = accelerationLat;
	}
	
	
	public Float getSnowingPercentage()
	{
		return snowingPercentage;
	}
	
	
	public void setSnowingPercentage(Float snowingPercentage)
	{
		this.snowingPercentage = snowingPercentage;
	}
	
	
	public Float getRainingPercentage()
	{
		return rainingPercentage;
	}
	
	
	public void setRainingPercentage(Float rainingPercentage)
	{
		this.rainingPercentage = rainingPercentage;
	}
	
	
	public Float getFogPercentage()
	{
		return fogPercentage;
	}
	
	
	public void setFogPercentage(Float fogPercentage)
	{
		this.fogPercentage = fogPercentage;
	}
	
	
	public String getRoadID()
	{
		return roadID;
	}
	
	
	public void setRoadID(String roadID)
	{
		this.roadID = roadID;
	}
	
	
	public Integer getLaneID()
	{
		return laneID;
	}
	
	
	public void setLaneID(Integer laneID)
	{
		this.laneID = laneID;
	}
	
	
	public Float getS()
	{
		return s;
	}
	
	
	public void setS(Float s)
	{
		this.s = s;
	}
	
	
	public Float getHdgLane()
	{
		return hdgLane;
	}
	
	
	public void setHdgLane(Float hdgLane)
	{
		this.hdgLane = hdgLane;
	}
	
	
	public Float getHdgCar()
	{
		return hdgCar;
	}
	
	
	public void setHdgCar(Float hdgCar)
	{
		this.hdgCar = hdgCar;
	}
	
	
	public Float getHdgDiff()
	{
		return hdgDiff;
	}
	
	
	public void setHdgDiff(Float hdgDiff)
	{
		this.hdgDiff = hdgDiff;
	}
	
	
	public Boolean getIsWrongWay()
	{
		return isWrongWay;
	}
	
	
	public void setIsWrongWay(Boolean isWrongWay)
	{
		this.isWrongWay = isWrongWay;
	}
	
	
	public ELaneType getLaneType()
	{
		return laneType;
	}
	
	
	public void setLaneType(ELaneType laneType)
	{
		this.laneType = laneType;
	}
	
	
	public AdasisLaneType getLanePosition()
	{
		return lanePosition;
	}
	
	
	public void setLanePosition(AdasisLaneType lanePosition)
	{
		this.lanePosition = lanePosition;
	}
	
	
	public Integer getNrObjs()
	{
		return nrObjs;
	}
	
	
	public void setNrObjs(Integer nrObjs)
	{
		this.nrObjs = nrObjs;
	}
	
	
	public ArrayList<String> getObjName()
	{
		return objName;
	}
	
	
	public void setObjName(ArrayList<String> objNameList)
	{
		this.objName = objNameList;
	}
	
	
	public ArrayList<Integer> getObjClass()
	{
		return objClass;
	}
	
	
	public void setObjClass(ArrayList<Integer> objClass)
	{
		this.objClass = objClass;
	}
	
	
	public ArrayList<String> getObjClassString()
	{
		return objClassString;
	}
	
	
	public void setObjClassString(ArrayList<String> objClassString)
	{
		this.objClassString = objClassString;
	}
	
	
	public ArrayList<Float> getObjX()
	{
		return objX;
	}
	
	
	public void setObjX(ArrayList<Float> objX)
	{
		this.objX = objX;
	}
	
	
	public ArrayList<Float> getObjY()
	{
		return objY;
	}
	
	
	public void setObjY(ArrayList<Float> objY)
	{
		this.objY = objY;
	}
	
	
	public ArrayList<Float> getObjDist()
	{
		return objDist;
	}
	
	
	public void setObjDist(ArrayList<Float> objDist)
	{
		this.objDist = objDist;
	}
	
	
	public ArrayList<Float> getObjDirection()
	{
		return objDirection;
	}
	
	
	public void setObjDirection(ArrayList<Float> objDirection)
	{
		this.objDirection = objDirection;
	}
	
	
	public ArrayList<Float> getObjVel()
	{
		return objVel;
	}
	
	
	public void setObjVel(ArrayList<Float> objVel)
	{
		this.objVel = objVel;
	}
	
	
	public ArrayList<Vector3f> getObjPos()
	{
		return objPos;
	}
	
	
	public void setObjPos(ArrayList<Vector3f> objPos)
	{
		this.objPos = objPos;
	}
	
	
	public ArrayList<Quaternion> getObjRot()
	{
		return objRot;
	}
	
	
	public void setObjRot(ArrayList<Quaternion> objRot)
	{
		this.objRot = objRot;
	}
	
	
	public Float getLaneWidth()
	{
		return laneWidth;
	}
	
	
	public void setLaneWidth(Float laneWidth)
	{
		this.laneWidth = laneWidth;
	}
	
	
	public Float getLatOffsLineR()
	{
		return latOffsLineR;
	}
	
	
	public void setLatOffsLineR(Float latOffsLineR)
	{
		this.latOffsLineR = latOffsLineR;
	}
	
	
	public Float getLatOffsLineL()
	{
		return latOffsLineL;
	}
	
	
	public void setLatOffsLineL(Float latOffsLineL)
	{
		this.latOffsLineL = latOffsLineL;
	}
	
	
	public Float getLaneCrvt()
	{
		return laneCrvt;
	}
	
	
	public void setLaneCrvt(Float laneCrvt)
	{
		this.laneCrvt = laneCrvt;
	}
	
	
	public AdasisLineType getLeftLineType()
	{
		return leftLineType;
	}
	
	
	public void setLeftLineType(AdasisLineType leftLineType)
	{
		this.leftLineType = leftLineType;
	}
	
	
	public AdasisLineType getRightLineType()
	{
		return rightLineType;
	}
	
	
	public void setRightLineType(AdasisLineType rightLineType)
	{
		this.rightLineType = rightLineType;
	}
	
	
	public AdasisLaneInformation getLeftLaneInfo()
	{
		return leftLaneInfo;
	}
	
	
	public void setLeftLaneInfo(AdasisLaneInformation leftLaneInfo)
	{
		this.leftLaneInfo = leftLaneInfo;
	}
	
	
	public AdasisLaneInformation getRightLaneInfo()
	{
		return rightLaneInfo;
	}
	
	
	public void setRightLaneInfo(AdasisLaneInformation rightLaneInfo)
	{
		this.rightLaneInfo = rightLaneInfo;
	}
	
	
	public Boolean getSideObstacleLeft()
	{
		return sideObstacleLeft;
	}
	
	
	public void setSideObstacleLeft(Boolean sideObstacleLeft)
	{
		this.sideObstacleLeft = sideObstacleLeft;
	}
	
	
	public Boolean getSideObstacleRight()
	{
		return sideObstacleRight;
	}
	
	
	public void setSideObstacleRight(Boolean sideObstacleRight)
	{
		this.sideObstacleRight = sideObstacleRight;
	}
	
	
	public Boolean getBlindSpotObstacleLeft()
	{
		return blindSpotObstacleLeft;
	}
	
	
	public void setBlindSpotObstacleLeft(Boolean blindSpotObstacleLeft)
	{
		this.blindSpotObstacleLeft = blindSpotObstacleLeft;
	}
	
	
	public Boolean getBlindSpotObstacleRight()
	{
		return blindSpotObstacleRight;
	}
	
	
	public void setBlindSpotObstacleRight(Boolean blindSpotObstacleRight)
	{
		this.blindSpotObstacleRight = blindSpotObstacleRight;
	}
	
	
	public Integer getNrLanesDrivingDirection()
	{
		return nrLanesDrivingDirection;
	}
	
	
	public void setNrLanesDrivingDirection(Integer nrLanesDrivingDirection)
	{
		this.nrLanesDrivingDirection = nrLanesDrivingDirection;
	}
	
	
	public Integer getNrLanesOppositeDirection()
	{
		return nrLanesOppositeDirection;
	}
	
	
	public void setNrLanesOppositeDirection(Integer nrLanesOppositeDirection)
	{
		this.nrLanesOppositeDirection = nrLanesOppositeDirection;
	}
	
	
	public Integer getCurrentSpeedLimit()
	{
		return currentSpeedLimit;
	}
	
	
	public void setCurrentSpeedLimit(Integer currentSpeedLimit)
	{
		this.currentSpeedLimit = currentSpeedLimit;
	}
	
	
	public Integer getNrSpeedLimits()
	{
		return nrSpeedLimits;
	}
	
	
	public void setNrSpeedLimits(Integer nrSpeedLimits)
	{
		this.nrSpeedLimits = nrSpeedLimits;
	}
	
	
	public ArrayList<Float> getSpeedLimitDist()
	{
		return speedLimitDist;
	}
	
	
	public void setSpeedLimitDist(ArrayList<Float> speedLimitDist)
	{
		this.speedLimitDist = speedLimitDist;
	}
	
	
	public ArrayList<Integer> getSpeedLimitValues()
	{
		return speedLimitValues;
	}
	
	
	public void setSpeedLimitValues(ArrayList<Integer> speedLimitValues)
	{
		this.speedLimitValues = speedLimitValues;
	}
	
	
	public Float getIntersectionDistance()
	{
		return intersectionDistance;
	}
	
	
	public void setIntersectionDistance(Float intersectionDistance)
	{
		this.intersectionDistance = intersectionDistance;
	}
	
	
	public Boolean getTrafficLightAhead()
	{
		return trafficLightAhead;
	}
	
	
	public void setTrafficLightAhead(Boolean trafficLightAhead)
	{
		this.trafficLightAhead = trafficLightAhead;
	}
	
	
	public Float getTrafficLightDist()
	{
		return trafficLightDist;
	}
	
	
	public void setTrafficLightDist(Float trafficLightDist)
	{
		this.trafficLightDist = trafficLightDist;
	}
	
	
	public ArrayList<TrafficLightState> getTrafficLightStates()
	{
		return trafficLightStates;
	}
	
	
	public void setTrafficLightStates(ArrayList<TrafficLightState> trafficLightStates)
	{
		this.trafficLightStates = trafficLightStates;
	}
	
	
	public ArrayList<Float> getTrafficLightTimesToChange()
	{
		return trafficLightTimesToChange;
	}
	
	
	public void setTrafficLightTimesToChange(ArrayList<Float> trafficLightTimesToChange)
	{
		this.trafficLightTimesToChange = trafficLightTimesToChange;
	}
	
	
	public Float getTargetDistance()
	{
		return targetDistance;
	}
	
	
	public void setTargetDistance(Float targetDistance)
	{
		this.targetDistance = targetDistance;
	}
	
	
	public Boolean getLeadingCarPresent()
	{
		return leadingCarPresent;
	}
	
	
	public void setLeadingCarPresent(Boolean leadingCarPresent)
	{
		this.leadingCarPresent = leadingCarPresent;
	}
	
	
	public String getLeadingCarName()
	{
		return leadingCarName;
	}
	
	
	public void setLeadingCarName(String leadingCarName)
	{
		this.leadingCarName = leadingCarName;
	}
	
	
	public String getLeadingCarRoadID()
	{
		return leadingCarRoadID;
	}
	
	
	public void setLeadingCarRoadID(String leadingCarRoadID)
	{
		this.leadingCarRoadID = leadingCarRoadID;
	}
	
	
	public Integer getLeadingCarLaneID()
	{
		return leadingCarLaneID;
	}
	
	
	public void setLeadingCarLaneID(Integer leadingCarLaneID)
	{
		this.leadingCarLaneID = leadingCarLaneID;
	}
	
	
	public Float getLeadingCarS()
	{
		return leadingCarS;
	}
	
	
	public void setLeadingCarS(Float leadingCarS)
	{
		this.leadingCarS = leadingCarS;
	}
	
	
	public Float getLeadingCarDist()
	{
		return leadingCarDist;
	}
	
	
	public void setLeadingCarDist(Float leadingCarDist)
	{
		this.leadingCarDist = leadingCarDist;
	}
	
	
	public Float getLeadingCarSpeedKmh()
	{
		return leadingCarSpeedKmh;
	}
	
	
	public void setLeadingCarSpeedKmh(Float leadingCarSpeedKmh)
	{
		this.leadingCarSpeedKmh = leadingCarSpeedKmh;
	}

	
	public Date getEyeTrackerDate()
	{
		return eyeTrackerDate;
	}
	
	
	public void setEyeTrackerDate(Date eyeTrackerDate)
	{
		this.eyeTrackerDate = eyeTrackerDate;
	}
	
	
	public String getEyeTrackerTimestamp()
	{
		return eyeTrackerTimestamp;
	}
	
	
	public void setEyeTrackerTimestamp(String eyeTrackerTimestamp)
	{
		this.eyeTrackerTimestamp = eyeTrackerTimestamp;
	}
	

	public Vector2f getGazeCoordinate()
	{
		return gazeCoordinate;
	}
	
	
	public void setGazeCoordinate(Vector2f gazeCoordinate)
	{
		this.gazeCoordinate = gazeCoordinate;
	}
	
	
	public Vector2f getGazeCoordinateAvg()
	{
		return gazeCoordinateAvg;
	}
	
	
	public void setGazeCoordinateAvg(Vector2f gazeCoordinateAvg)
	{
		this.gazeCoordinateAvg = gazeCoordinateAvg;
	}
	
	
	public Boolean isValidLeftEye()
	{
		return isValidLeftEye;
	}
	
	
	public void setValidLeftEye(Boolean isValidLeftEye)
	{
		this.isValidLeftEye = isValidLeftEye;
	}
	
	
	public Boolean isValidRightEye()
	{
		return isValidRightEye;
	}
	
	
	public void setValidRightEye(Boolean isValidRightEye)
	{
		this.isValidRightEye = isValidRightEye;
	}
	
	
	public Vector3f getGazeRayOrigin()
	{
		return gazeRayOrigin;
	}
	
	
	public void setGazeRayOrigin(Vector3f gazeRayOrigin)
	{
		this.gazeRayOrigin = gazeRayOrigin;
	}
	
	
	public Vector3f getGazeRayDirection()
	{
		return gazeRayDirection;
	}
	
	
	public void setGazeRayDirection(Vector3f gazeRayDirection)
	{
		this.gazeRayDirection = gazeRayDirection;
	}
	
	
	public String getClosestHitByGazeRay()
	{
		return closestHitByGazeRay;
	}
	
	
	public void setClosestHit(String closestHitByGazeRay)
	{
		this.closestHitByGazeRay = closestHitByGazeRay;
	}
	
	
	public Vector3f getClosestContactPointByGazeRay()
	{
		return closestContactPointByGazeRay;
	}
	
	
	public void setClosestContactPointByGazeRay(Vector3f closestContactPointByGazeRay)
	{
		this.closestContactPointByGazeRay = closestContactPointByGazeRay;
	}

	
	public Vector3f getFrontPosition()
	{
		return frontPosition;
	}
	
	
	public void setFrontPosition(Vector3f frontPosition)
	{
		this.frontPosition = frontPosition;
	}
	
	
	public String getReferenceObjectData()
	{
		return referenceObjectData;
	}
	
	
	public void setReferenceObjectData(String referenceObjectData)
	{
		this.referenceObjectData = referenceObjectData;
	}


	public String getAoi2D()
	{
		return aoi2D;
	}
	
	
	public void setAoi2D(String aoi2D)
	{
		this.aoi2D = aoi2D;
	}
	
	
	public ArrayList<RecordedReferenceObject> getReferenceObjectList()
	{
		return referenceObjectList;
	}
	
	
	public void setReferenceObjectList(ArrayList<RecordedReferenceObject> referenceObjectList)
	{
		this.referenceObjectList = referenceObjectList;
	}

	
	public Float getTraveledDistance()
	{
		return traveledDistance;
	}
	
	
	public void setTraveledDistance(Float traveledDistance)
	{
		this.traveledDistance = traveledDistance;		
	}

	
	@Override
    public DataUnit clone() {
        try {
            return (DataUnit) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(); // can not happen
        }
    }
	
	
}
