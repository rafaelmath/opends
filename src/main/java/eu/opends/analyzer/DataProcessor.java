/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.analyzer;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import eu.opends.analyzer.data.LogParameters;
import eu.opends.car.LightTexturesContainer.TurnSignalState;
import eu.opends.environment.TrafficLight.TrafficLightState;
import eu.opends.gesture.RecordedReferenceObject;
import eu.opends.opendrive.data.ELaneType;
import eu.opends.opendrive.processed.ODLane.AdasisLaneInformation;
import eu.opends.opendrive.processed.ODLane.AdasisLaneType;
import eu.opends.opendrive.processed.ODLane.AdasisLineType;
import javafx.util.Pair;



public class DataProcessor
{
	private String valueSeparator;
	private String listSeparator;
	private String coordinateSeparator;
	private boolean verbose;
	private LinkedList<DataUnit> dataUnitList = new LinkedList<DataUnit>();
	private ArrayList<Pair<String,Boolean>> availableParameterList = new ArrayList<Pair<String,Boolean>>();
	private ArrayList<Vector3f> carPositionList = new ArrayList<Vector3f>();
	private Float traveledDistance = 0f;
	private Vector3f previousPos = null;
	
	
	public DataProcessor(String valueSeparator, String listSeparator, String coordinateSeparator, boolean verbose) 
	{
		this.valueSeparator = valueSeparator;
		this.listSeparator = listSeparator;
		this.coordinateSeparator = coordinateSeparator;
		this.verbose = verbose;
	}
	
	
	public void processHeader(String inputLine)
	{
		ArrayList<String> allParametersList = LogParameters.getAllParametersList();		
		String[] splittedLineArray = inputLine.split(valueSeparator);
		for(String parameter : splittedLineArray)
		{
			boolean skipParameter = false;
			for(Pair<String,Boolean> entry : availableParameterList)
			{
				if(entry.getKey().equals(parameter))
					skipParameter = true;
			}

			if(skipParameter)
			{
				availableParameterList.add(new Pair<String,Boolean>(parameter,false));
				if(verbose)
					System.err.println("DataProcessor: '" + parameter + "' occurs more than once and will be skipped!");
			}
			else if(allParametersList.contains(parameter))
			{
				availableParameterList.add(new Pair<String,Boolean>(parameter,true));
			}
			else
			{
				availableParameterList.add(new Pair<String,Boolean>(parameter,false));
				if(verbose)
					System.err.println("DataProcessor: Parameter '" + parameter + "' cannot be processed (not implemented)");
			}
		}
	}
	
	
	public void addDataUnit(String inputLine)
	{
		String[] splittedLineArray = inputLine.split(valueSeparator, -1);
		DataUnit dataUnit = new DataUnit();
		
		// both list and array must have same length
		if(availableParameterList.size() != splittedLineArray.length)
		{
			if(verbose)
			{
				System.err.println("DataProcessor: number of header elements (" + availableParameterList.size() 
				+ ") does not match with number of data elements(" + splittedLineArray.length + ").");
			}
			return;
		}
		
		for(int i=0; i<splittedLineArray.length; i++)
		{
			Pair<String,Boolean> entry = availableParameterList.get(i);
			String param = entry.getKey();
			boolean isValid = entry.getValue();
			String value = splittedLineArray[i];

			if(!isValid)
				continue;
			
			try {
				
				if(param.equals("milliseconds"))
				{
					long milliseconds = Long.parseLong(value);
					dataUnit.setDate(new Date(milliseconds)); 
				}
				
				else if(param.equals("timestamp"))
				{
					dataUnit.setTimestamp(value); 
				}
				
				else if(param.equals("simulatorState"))
				{
					dataUnit.setSimulatorState(value); 
				}
				
				else if(param.equals("position"))
				{
					Vector3f position = parseVector3f(value);
					if(position != null)
					{
						dataUnit.setPosition(position);
						
						// car position list
						carPositionList.add(position);
						
						// traveled distance
						if(previousPos == null)
							previousPos = position;
						traveledDistance += position.distance(previousPos);
						previousPos = position;
						
						dataUnit.setTraveledDistance(traveledDistance);
					}
					else if(verbose)
						System.err.println("DataProcessor: '" + value + "' is an invalid position");	
				}
			
				else if(param.equals("rotation"))
				{
					Quaternion rotation = parseQuaternion(value);
					dataUnit.setRotation(rotation);
					
					if(rotation == null && verbose)
						System.err.println("DataProcessor: '" + value + "' is an invalid rotation");
				}
			
				else if(param.equals("speedKmh"))
				{
					dataUnit.setSpeedKmh(Float.parseFloat(value));
				}
				
				else if(param.equals("rpm"))
				{
					dataUnit.setRpm(Float.parseFloat(value));
				}
				
				else if(param.equals("steeringWheelPos"))
				{
					dataUnit.setSteeringWheelPos(Float.parseFloat(value));
				}
				
				else if(param.equals("acceleratorPedalPos"))
				{
					dataUnit.setAcceleratorPedalPos(Float.parseFloat(value));
				}
				
				else if(param.equals("brakePedalPos"))
				{
					dataUnit.setBrakePedalPos(Float.parseFloat(value));
				}
				
				else if(param.equals("clutchPedalPos"))
				{
					dataUnit.setClutchPedalPos(Float.parseFloat(value));
				}
				
				else if(param.equals("isHandBrakeEngaged"))
				{
					dataUnit.setIsHandBrakeEngaged(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("isEngineOn"))
				{
					dataUnit.setIsEngineOn(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("turnSignalState"))
				{
					dataUnit.setTurnSignalState(TurnSignalState.valueOf(value));
				}
				
				else if(param.equals("isLightOn"))
				{
					dataUnit.setIsLightOn(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("lightIntensity"))
				{
					dataUnit.setLightIntensity(Float.parseFloat(value));
				}
				
				else if(param.equals("isAutoPilotOn"))
				{
					dataUnit.setIsAutoPilotOn(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("isBrakeLightOn"))
				{
					dataUnit.setIsBrakeLightOn(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("isCruiseControlOn"))
				{
					dataUnit.setIsCruiseControlOn(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("cruiseControlTargetSpeedKmh"))
				{
					dataUnit.setCruiseControlTargetSpeedKmh(Float.parseFloat(value));
				}

				else if(param.equals("isAutomatic"))
				{
					dataUnit.setIsAutomatic(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("selectedGear"))
				{
					dataUnit.setSelectedGear(Integer.parseInt(value));
				}
				
				else if(param.equals("milage"))
				{
					dataUnit.setMilage(Float.parseFloat(value));
				}
				
				else if(param.equals("headingDegree"))
				{
					dataUnit.setHeadingDegree(Float.parseFloat(value));
				}
				
				else if(param.equals("slopeDegree"))
				{
					dataUnit.setSlopeDegree(Float.parseFloat(value));
				}
				
				else if(param.equals("accelerationLgt"))
				{
					dataUnit.setAccelerationLgt(Float.parseFloat(value));
				}
				
				else if(param.equals("accelerationLat"))
				{
					dataUnit.setAccelerationLat(Float.parseFloat(value));
				}

				else if(param.equals("snowingPercentage"))
				{
					dataUnit.setSnowingPercentage(Float.parseFloat(value));
				}
				
				else if(param.equals("rainingPercentage"))
				{
					dataUnit.setRainingPercentage(Float.parseFloat(value));
				}
				
				else if(param.equals("fogPercentage"))
				{
					dataUnit.setFogPercentage(Float.parseFloat(value));
				}
				
				else if(param.equals("roadID"))
				{
					dataUnit.setRoadID(value);
				}

				else if(param.equals("laneID"))
				{
					dataUnit.setLaneID(Integer.parseInt(value));
				}
				
				else if(param.equals("s"))
				{
					dataUnit.setS(Float.parseFloat(value));
				}

				else if(param.equals("hdgLane"))
				{
					dataUnit.setHdgLane(Float.parseFloat(value));
				}
				
				else if(param.equals("hdgCar"))
				{
					dataUnit.setHdgCar(Float.parseFloat(value));
				}
				
				else if(param.equals("hdgDiff"))
				{
					dataUnit.setHdgDiff(Float.parseFloat(value));
				}

				else if(param.equals("isWrongWay"))
				{
					dataUnit.setIsWrongWay(Boolean.parseBoolean(value));
				}

				else if(param.equals("laneType"))
				{
					dataUnit.setLaneType(ELaneType.valueOf(value));
				}

				else if(param.equals("lanePosition"))
				{
					dataUnit.setLanePosition(AdasisLaneType.valueOf(value));
				}
				
				else if(param.equals("nrObjs"))
				{
					dataUnit.setNrObjs(Integer.parseInt(value));
				}

				else if(param.equals("objName"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjNameString = value.split(listSeparator);
					
					ArrayList<String> objNameList = new ArrayList<String>();
					
					for(String objName : splittedObjNameString)
						if(!objName.isEmpty())
							objNameList.add(objName);
					
					dataUnit.setObjName(objNameList);
				}
				
				else if(param.equals("objClass"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjClassString = value.split(listSeparator);
					
					ArrayList<Integer> objClassList = new ArrayList<Integer>();
					
					for(String objClass : splittedObjClassString)
						if(!objClass.isEmpty())
							objClassList.add(Integer.parseInt(objClass));
					
					dataUnit.setObjClass(objClassList);
				}
				
				else if(param.equals("objClassString"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjClassStringString = value.split(listSeparator);
					
					ArrayList<String> objClassStringList = new ArrayList<String>();
					
					for(String objClassString : splittedObjClassStringString)
						if(!objClassString.isEmpty())
							objClassStringList.add(objClassString);
					
					dataUnit.setObjClassString(objClassStringList);
				}
				
				else if(param.equals("objX"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjXString = value.split(listSeparator);
					
					ArrayList<Float> objXList = new ArrayList<Float>();
					
					for(String objX : splittedObjXString)
						if(!objX.isEmpty())
							objXList.add(Float.parseFloat(objX));
					
					dataUnit.setObjX(objXList);
				}

				else if(param.equals("objY"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjYString = value.split(listSeparator);
					
					ArrayList<Float> objYList = new ArrayList<Float>();
					
					for(String objY : splittedObjYString)
						if(!objY.isEmpty())
							objYList.add(Float.parseFloat(objY));
					
					dataUnit.setObjY(objYList);
				}
				
				else if(param.equals("objDist"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjDistString = value.split(listSeparator);
					
					ArrayList<Float> objDistList = new ArrayList<Float>();
					
					for(String objDist : splittedObjDistString)
						if(!objDist.isEmpty())
							objDistList.add(Float.parseFloat(objDist));
					
					dataUnit.setObjDist(objDistList);
				}
				
				else if(param.equals("objDirection"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjDirectionString = value.split(listSeparator);
					
					ArrayList<Float> objDirectionList = new ArrayList<Float>();
					
					for(String objDirection : splittedObjDirectionString)
						if(!objDirection.isEmpty())
							objDirectionList.add(Float.parseFloat(objDirection));
					
					dataUnit.setObjDirection(objDirectionList);
				}
				
				else if(param.equals("objVel"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjVelString = value.split(listSeparator);
					
					ArrayList<Float> objVelList = new ArrayList<Float>();
					
					for(String objVel : splittedObjVelString)
						if(!objVel.isEmpty())
							objVelList.add(Float.parseFloat(objVel));
					
					dataUnit.setObjVel(objVelList);
				}
				
				else if(param.equals("objPos"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjPosString = value.split(listSeparator);
					
					ArrayList<Vector3f> objPosList = new ArrayList<Vector3f>();
					
					for(String objPos : splittedObjPosString)
						if(!objPos.isEmpty())
							objPosList.add(parseVector3f(objPos));
					
					dataUnit.setObjPos(objPosList);
				}

				else if(param.equals("objRot"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedObjRotString = value.split(listSeparator);
					
					ArrayList<Quaternion> objRotList = new ArrayList<Quaternion>();
					
					for(String objRot : splittedObjRotString)
						if(!objRot.isEmpty())
							objRotList.add(parseQuaternion(objRot));
					
					dataUnit.setObjRot(objRotList);
				}

				else if(param.equals("laneWidth"))
				{
					dataUnit.setLaneWidth(Float.parseFloat(value));
				}
				
				else if(param.equals("latOffsLineR"))
				{
					dataUnit.setLatOffsLineR(Float.parseFloat(value));
				}
				
				else if(param.equals("latOffsLineL"))
				{
					dataUnit.setLatOffsLineL(Float.parseFloat(value));
				}
				
				else if(param.equals("laneCrvt"))
				{
					dataUnit.setLaneCrvt(Float.parseFloat(value));
				}
				
				else if(param.equals("leftLineType"))
				{
					dataUnit.setLeftLineType(AdasisLineType.valueOf(value));
				}
				
				else if(param.equals("rightLineType"))
				{
					dataUnit.setRightLineType(AdasisLineType.valueOf(value));
				}
				
				else if(param.equals("leftLaneInfo"))
				{
					dataUnit.setLeftLaneInfo(AdasisLaneInformation.valueOf(value));
				}

				else if(param.equals("rightLaneInfo"))
				{
					dataUnit.setRightLaneInfo(AdasisLaneInformation.valueOf(value));
				}

				else if(param.equals("sideObstacleLeft"))
				{
					dataUnit.setSideObstacleLeft(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("sideObstacleRight"))
				{
					dataUnit.setSideObstacleRight(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("blindSpotObstacleLeft"))
				{
					dataUnit.setBlindSpotObstacleLeft(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("blindSpotObstacleRight"))
				{
					dataUnit.setBlindSpotObstacleRight(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("nrLanesDrivingDirection"))
				{
					dataUnit.setNrLanesDrivingDirection(Integer.parseInt(value));
				}
				
				else if(param.equals("nrLanesOppositeDirection"))
				{
					dataUnit.setNrLanesOppositeDirection(Integer.parseInt(value));
				}
				
				else if(param.equals("currentSpeedLimit"))
				{
					dataUnit.setCurrentSpeedLimit(Integer.parseInt(value));
				}
				
				else if(param.equals("nrSpeedLimits"))
				{
					dataUnit.setNrSpeedLimits(Integer.parseInt(value));
				}
			
				else if(param.equals("speedLimitDist"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedSpeedLimitDistString = value.split(listSeparator);
					
					ArrayList<Float> speedLimitDistList = new ArrayList<Float>();
					
					for(String speedLimitDist : splittedSpeedLimitDistString)
						if(!speedLimitDist.isEmpty())
							speedLimitDistList.add(Float.parseFloat(speedLimitDist));
					
					dataUnit.setSpeedLimitDist(speedLimitDistList);
				}

				else if(param.equals("speedLimitValues"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedSpeedLimitValuesString = value.split(listSeparator);
					
					ArrayList<Integer> speedLimitValuesList = new ArrayList<Integer>();
					
					for(String speedLimitValues : splittedSpeedLimitValuesString)
						if(!speedLimitValues.isEmpty())
							speedLimitValuesList.add(Integer.parseInt(speedLimitValues));
					
					dataUnit.setSpeedLimitValues(speedLimitValuesList);
				}
				
				else if(param.equals("intersectionDistance"))
				{
					dataUnit.setIntersectionDistance(Float.parseFloat(value));
				}
				
				else if(param.equals("trafficLightAhead"))
				{
					dataUnit.setTrafficLightAhead(Boolean.parseBoolean(value));
				}

				else if(param.equals("trafficLightDist"))
				{
					dataUnit.setTrafficLightDist(Float.parseFloat(value));
				}
  
				else if(param.equals("trafficLightStates"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedTrafficLightStatesString = value.split(listSeparator);
					
					ArrayList<TrafficLightState> trafficLightStatesList = new ArrayList<TrafficLightState>();
					
					for(String trafficLightState : splittedTrafficLightStatesString)
						if(!trafficLightState.isEmpty())
							trafficLightStatesList.add(TrafficLightState.valueOf(trafficLightState));
					
					dataUnit.setTrafficLightStates(trafficLightStatesList);
				}
				
				else if(param.equals("trafficLightTimesToChange"))
				{
					value = value.replace("[", "").replace("]", "");
					String[] splittedTrafficLightTimesToChangeString = value.split(listSeparator);
					
					ArrayList<Float> trafficLightTimesToChangeList = new ArrayList<Float>();
					
					for(String trafficLightTimesToChange : splittedTrafficLightTimesToChangeString)
						if(!trafficLightTimesToChange.isEmpty())
							trafficLightTimesToChangeList.add(Float.parseFloat(trafficLightTimesToChange));
					
					dataUnit.setTrafficLightTimesToChange(trafficLightTimesToChangeList);
				}

				else if(param.equals("targetDistance"))
				{
					dataUnit.setTargetDistance(Float.parseFloat(value));
				}
				
				else if(param.equals("leadingCarPresent"))
				{
					dataUnit.setLeadingCarPresent(Boolean.parseBoolean(value));
				}
				
				else if(param.equals("leadingCarName"))
				{
					dataUnit.setLeadingCarName(value);
				}

				else if(param.equals("leadingCarRoadID"))
				{
					dataUnit.setLeadingCarRoadID(value);
				}
				
				else if(param.equals("leadingCarLaneID"))
				{
					dataUnit.setLeadingCarLaneID(Integer.parseInt(value));
				}

				else if(param.equals("leadingCarS"))
				{
					dataUnit.setLeadingCarS(Float.parseFloat(value));
				}
				
				else if(param.equals("leadingCarDist"))
				{
					dataUnit.setLeadingCarDist(Float.parseFloat(value));
				}
				
				else if(param.equals("leadingCarSpeedKmh"))
				{
					dataUnit.setLeadingCarSpeedKmh(Float.parseFloat(value));
				}

				else if (param.equals("eyeTrackerMilliseconds"))
				{
					if(!value.isEmpty())
					{
						long eyeTrackerMilliseconds = Long.parseLong(value);
						dataUnit.setEyeTrackerDate(new Date(eyeTrackerMilliseconds));
					}
				}
				
				else if (param.equals("eyeTrackerTimestamp"))
				{
					if(!value.isEmpty())
						dataUnit.setEyeTrackerTimestamp(value);
				}
				
				else if (param.equals("isValidLeftEye"))
				{
					if(!value.isEmpty())
						dataUnit.setValidLeftEye(Boolean.parseBoolean(value));
				}
				
				else if (param.equals("isValidRightEye"))
				{
					if(!value.isEmpty())
						dataUnit.setValidRightEye(Boolean.parseBoolean(value));
				}
				
				else if (param.equals("gazeCoordinate"))
				{
					if(!value.isEmpty())
					{
						Vector2f gazeCoordinate = parseVector2f(value);
						dataUnit.setGazeCoordinate(gazeCoordinate);
					
						if(gazeCoordinate == null && verbose)
							System.err.println("DataProcessor: '" + value + "' is an invalid gaze coordinate (Vector2f)");
					}
				}

				else if (param.equals("gazeCoordinateAvg"))
				{
					if(!value.isEmpty())
					{
						Vector2f gazeCoordinateAvg = parseVector2f(value);
						dataUnit.setGazeCoordinateAvg(gazeCoordinateAvg);
					
						if(gazeCoordinateAvg == null && verbose)
							System.err.println("DataProcessor: '" + value + "' is an invalid averaged gaze coordinate (Vector2f)");
					}
				}
				
				else if (param.equals("gazeRayOrigin"))
				{
					if(!value.isEmpty())
					{
						Vector3f gazeRayOrigin = parseVector3f(value);
						dataUnit.setGazeRayOrigin(gazeRayOrigin);
					
						if(gazeRayOrigin == null && verbose)
							System.err.println("DataProcessor: '" + value + "' is an invalid gaze ray origin (Vector3f)");
					}
				}
				
				else if (param.equals("gazeRayDirection"))
				{
					if(!value.isEmpty())
					{
						Vector3f gazeRayDirection = parseVector3f(value);
						dataUnit.setGazeRayDirection(gazeRayDirection);
					
						if(gazeRayDirection == null && verbose)
							System.err.println("DataProcessor: '" + value + "' is an invalid gaze ray direction (Vector3f)");
					}
				}
				
				else if (param.equals("closestHitByGazeRay"))
				{
					if(!value.isEmpty())
						dataUnit.setClosestHit(value);
				}
				
				else if (param.equals("closestContactPointByGazeRay"))
				{
					if(!value.isEmpty())
					{
						Vector3f closestContactPointByGazeRay = parseVector3f(value);
						dataUnit.setClosestContactPointByGazeRay(closestContactPointByGazeRay);
					
						if(closestContactPointByGazeRay == null && verbose)
							System.err.println("DataProcessor: '" + value + "' is an invalid closest contact point (Vector3f)");
					}
				}

				else if(param.equals("frontPosition"))
				{
					Vector3f frontPosition = parseVector3f(value);
					dataUnit.setFrontPosition(frontPosition);
					
					if(frontPosition == null && verbose)
						System.err.println("DataProcessor: '" + value + "' is an invalid front position"); //TODO check
				}
			
				else if(param.equals("referenceObjectData"))
				{
					dataUnit.setReferenceObjectList(parseRecordedReferenceData(value));
				}
				
				
			} catch (Exception e) {
				System.err.println("DataProcessor: Parsing error in parameter: '" + param + "'");
				e.printStackTrace();
			}
		}
		
		dataUnitList.add(dataUnit);
	}
	
	
	private Vector3f parseVector3f(String vectorString)
	{
		// parse (1.0/2.0/3.0)
		vectorString = vectorString.replace("(", "").replace(")", "");
		String[] array = vectorString.split(coordinateSeparator);
		
		if(array.length == 3)
			return new Vector3f(Float.parseFloat(array[0]), Float.parseFloat(array[1]), 
					Float.parseFloat(array[2]));
		else
			return null;
	}

	
	private Vector2f parseVector2f(String vectorString)
	{
		// parse (1.0/2.0)
		vectorString = vectorString.replace("(", "").replace(")", "");
		String[] array = vectorString.split(coordinateSeparator);
		
		if(array.length == 2)
			return new Vector2f(Float.parseFloat(array[0]), Float.parseFloat(array[1]));
		else
			return null;
	}
	
	
	private Quaternion parseQuaternion(String quaternionString)
	{
		// parse (1.0/2.0/3.0/4.0)
		quaternionString = quaternionString.replace("(", "").replace(")", "");
		String[] array = quaternionString.split(coordinateSeparator);
		
		if(array.length == 4)
			return new Quaternion(Float.parseFloat(array[0]), Float.parseFloat(array[1]), 
					Float.parseFloat(array[2]), Float.parseFloat(array[3]));
		else
			return null;
	}
	
	
	private ArrayList<RecordedReferenceObject> parseRecordedReferenceData(String recordedReferenceDataString)
	{
		ArrayList<RecordedReferenceObject> recordedReferenceObjectList= new ArrayList<RecordedReferenceObject>();
		
		recordedReferenceDataString = recordedReferenceDataString.replace("[", "");
		recordedReferenceDataString = recordedReferenceDataString.replace("]", "");
		
		if(!recordedReferenceDataString.isEmpty())
		{
			String[] referenceObjectStringArray = recordedReferenceDataString.split(";"); // FIXME separator
			for(String referenceObjectString : referenceObjectStringArray)
			{
				// parse group4_building1(-1.0781823, -0.8631047, -0.010994518, 0.10122352, false)
			
				referenceObjectString = referenceObjectString.replace(")", "");
				String[] nameAndPropertiesArray = referenceObjectString.split("\\(");
				
				String name = nameAndPropertiesArray[0].trim();
				String propertiesString = nameAndPropertiesArray[1];
			
				String[] propertiesArray = propertiesString.split(","); // FIXME separator
				float minLatAngle = Float.parseFloat(propertiesArray[0]);
				float maxLatAngle = Float.parseFloat(propertiesArray[1]);
				float minVertAngle = Float.parseFloat(propertiesArray[2]);
				float maxVertAngle = Float.parseFloat(propertiesArray[3]);
				boolean isActive = Boolean.parseBoolean(propertiesArray[4].trim());
			
				recordedReferenceObjectList.add(new RecordedReferenceObject(name, minLatAngle, maxLatAngle, minVertAngle, 
					maxVertAngle, isActive));
			}
		}
		
		return recordedReferenceObjectList;
	}
	
	
	public static DataUnit interpolate(DataUnit previousDataUnit, DataUnit nextDataUnit, long currentRecordingTime) 
	{
		// time at previous recorded data unit
		long timeAtPreviousTarget = previousDataUnit.getDate().getTime();
		
		// time at next recorded data unit
		long timeAtNextTarget = nextDataUnit.getDate().getTime();
		
		// current progress (%) between previous and next recorded data unit
		long timeElapsedSincePreviousTarget = currentRecordingTime - timeAtPreviousTarget;
		long totalTimeBetweenTargets = timeAtNextTarget -  timeAtPreviousTarget;
		float percentage = (float) timeElapsedSincePreviousTarget/totalTimeBetweenTargets;
		
		
		// save interpolated time stamp
		Date date = new Date(currentRecordingTime);
		
		
		// interpolate position
		Vector3f previousCarPos = previousDataUnit.getCarPosition();
		Vector3f nextCarPos = nextDataUnit.getCarPosition();
		Vector3f track = nextCarPos.subtract(previousCarPos);
		Vector3f position = previousCarPos.add(track.multLocal(percentage));


		// interpolate rotation
		Quaternion previousCarRot = previousDataUnit.getCarRotation();
		Quaternion nextCarRot = nextDataUnit.getCarRotation();
		
		float[] previousAngles = previousCarRot.toAngles(null);
		float[] nextAngles = nextCarRot.toAngles(null);
		float[] currentAngles = new float [3];
		
		for(int i=0; i<3; i++)
		{
			// normalize all values to range 0 - 2*PI
			previousAngles[i] = (previousAngles[i] + FastMath.TWO_PI) % FastMath.TWO_PI;
			nextAngles[i] = (nextAngles[i] + FastMath.TWO_PI) % FastMath.TWO_PI;
			
			// If one angle smaller than 2*PI (360 degrees) and the other greater
			// add 2*PI (360 degrees) to the smaller angle. This avoids jumping.
			if(Math.abs(nextAngles[i] - previousAngles[i]) > FastMath.PI)
				if(nextAngles[i] < previousAngles[i])
					nextAngles[i] += FastMath.TWO_PI;
				else
					previousAngles[i] += FastMath.TWO_PI;
			
			// intermediate angle according to progress between previous and next angle
			currentAngles[i] = previousAngles[i] + ((nextAngles[i] - previousAngles[i]) * percentage);
		}	
		Quaternion rotation = (new Quaternion()).fromAngles(currentAngles);
		
		
		// interpolate speed		
		float previousSpeed = previousDataUnit.getSpeedKmh();
		float nextSpeed = nextDataUnit.getSpeedKmh();
		float speedDiff = nextSpeed - previousSpeed;
		float speed = previousSpeed + (speedDiff * percentage);
		
		
		// interpolate steering wheel position
		float previousSWPos = previousDataUnit.getSteeringWheelPos();
		float nextSWPos = nextDataUnit.getSteeringWheelPos();
		float sWPosDiff = nextSWPos - previousSWPos;
		float steeringWheelPos = previousSWPos + (sWPosDiff * percentage);
		

		// interpolate accelerator pedal position
		float previousAccPos = previousDataUnit.getAcceleratorPedalPos();
		float nextAccPos = nextDataUnit.getAcceleratorPedalPos();
		float accPosDiff = nextAccPos - previousAccPos;
		float gasPedalPos = previousAccPos + (accPosDiff * percentage);

		
		// interpolate brake pedal position
		float previousBrakePos = previousDataUnit.getBrakePedalPos();
		float nextBrakePos = nextDataUnit.getBrakePedalPos();
		float brakePosDiff = nextBrakePos - previousBrakePos;
		float brakePedalPos = previousBrakePos + (brakePosDiff * percentage);
		
		
		// pass previous engine state, front position, and reference object list
		boolean isEngineOn = previousDataUnit.getIsEngineOn();
		Vector3f frontPosition = previousDataUnit.getFrontPosition();
		ArrayList<RecordedReferenceObject> referenceObjectList = previousDataUnit.getReferenceObjectList();
		
		
		// interpolate traveled distance
		float previousTraveledDistance = previousDataUnit.getTraveledDistance();
		float nextTraveledDistance = nextDataUnit.getTraveledDistance();
		float traveledDistanceDiff = nextTraveledDistance - previousTraveledDistance;
		float traveledDistance = previousTraveledDistance + (traveledDistanceDiff * percentage);
		
		//TODO add further parameters
		DataUnit dataUnit = previousDataUnit.clone();
		dataUnit.setDate(date);
		dataUnit.setPosition(position);
		dataUnit.setRotation(rotation);
		dataUnit.setSpeedKmh(speed);
		dataUnit.setSteeringWheelPos(steeringWheelPos);
		dataUnit.setAcceleratorPedalPos(gasPedalPos);
		dataUnit.setBrakePedalPos(brakePedalPos);
		//dataUnit.setIsEngineOn(isEngineOn);
		dataUnit.setTraveledDistance(traveledDistance);
		//dataUnit.setFrontPosition(frontPosition);
		//dataUnit.setReferenceObjectList(referenceObjectList);
		
		return dataUnit;
	}


	public LinkedList<DataUnit> getDataUnitList()
	{
		return dataUnitList;
	}
	
	
	public float getTotalDistance()
	{
		return traveledDistance;
	}


	public ArrayList<Vector3f> getCarPositionList()
	{
		return carPositionList;
	}


}
