/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.analyzer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import eu.opends.analyzer.data.DataSet;
import eu.opends.analyzer.data.LogParameters;
import eu.opends.main.SimulationDefaults;
import eu.opends.main.Simulator;
import eu.opends.tools.Util;

public class DataWriter
{
	/**
	 * An array list for not having to write every row directly to file.
	 */
	private ArrayList<DataSet> dataSetList;
	private BufferedWriter out;
	private File outFile;
	private String newLine = System.getProperty("line.separator");
	private long timeSinceLastDataFlush;
	private Simulator sim;
	private File analyzerDataFile;
	private String driverName;
	private LogParameters logParameters;
	private boolean isRecording = false;
	
	private boolean writeHeader = true;
	private String logParametersFilePath = "logParameters.properties";
	private String valueSeparator = ";";
	private long startTime;
	
	
	private String referenceObjectData = "[]";
	public void setReferenceObjectData(String referenceObjectData)
	{
		this.referenceObjectData = referenceObjectData;
	}
	
	
	public DataWriter(Simulator sim)
	{
		this.sim = sim;
		this.driverName = SimulationDefaults.driverName;
		logParameters = new LogParameters(logParametersFilePath);
	}
	
	
	public void startRecording(int trackNumber)
	{
		if(isRecording)
			return;
		
		String outputFolder = Simulator.getOutputFolder();
		Util.makeDirectory(outputFolder);

		if(trackNumber >= 0)
			analyzerDataFile = new File(outputFolder + "/carData_track" + trackNumber + ".txt");
		else
			analyzerDataFile = new File(outputFolder + "/carData.txt");

		
		if (analyzerDataFile.getAbsolutePath() == null) 
		{
			System.err.println("Parameter not accepted at method initWriter.");
			return;
		}
		
		outFile = new File(analyzerDataFile.getAbsolutePath());
		
		int i = 2;
		while(outFile.exists()) 
		{
			if(trackNumber >= 0)
				analyzerDataFile = new File(outputFolder + "/carData_track" + trackNumber + "(" + i + ").txt");
			else
				analyzerDataFile = new File(outputFolder + "/carData(" + i + ").txt");
			
			outFile = new File(analyzerDataFile.getAbsolutePath());
			i++;
		}
		
		try {
			out = new BufferedWriter(new FileWriter(outFile));
			
			if(writeHeader)
			{
				String relativeDrivingTaskPath = getRelativePath(SimulationDefaults.drivingTaskFileName);
				out.write("Driving Task: " + relativeDrivingTaskPath + newLine);
				out.write("Creation Time: "
					+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
							.format(new Date()) + newLine);
				out.write("Driver: " + driverName + newLine);
				out.write(logParameters.getHeaderString(valueSeparator) + newLine);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		dataSetList = new ArrayList<DataSet>();
		long now = System.currentTimeMillis();
		timeSinceLastDataFlush = now;
		
		startTime = now;
		
		isRecording = true;
	}
	
	
	private String getRelativePath(String absolutePath)
	{
		URI baseURI = new File("./").toURI();
		URI absoluteURI = new File(absolutePath).toURI();
		URI relativeURI = baseURI.relativize(absoluteURI);
		
		return relativeURI.getPath();
	}
	

	/**
	 * Save the log data at a frequency of 20Hz. That class should be called in
	 * the update-method <code>Simulator.java</code>.
	 */
	public void update(float tpf)
	{
		if(isRecording && !sim.isPause())
		{
			int updateInterval = 50; // = 1000/20
			
			Date curDate = new Date();

			if (curDate.getTime() - timeSinceLastDataFlush >= 2*updateInterval) 
			{
				timeSinceLastDataFlush = curDate.getTime() - 2*updateInterval;
			}

			if (curDate.getTime() - timeSinceLastDataFlush >= updateInterval) 
			{
				// write data to the data pool
				dataSetList.add(new DataSet(sim, curDate, logParameters, valueSeparator, referenceObjectData));
				
				// after 50 data sets, the pool is flushed to the file
				if (dataSetList.size() > 50)
					flush();

				timeSinceLastDataFlush += updateInterval;
			}
		}
	}
	
	
	private void flush() 
	{	
		try {
			
			StringBuffer sb = new StringBuffer();
			
			for (DataSet dataSet : dataSetList)
				sb.append(dataSet.getDataString() + newLine);
			
			out.write(sb.toString());
			
			dataSetList.clear();
			
			out.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	
	public String stopRecording() 
	{
		if(!isRecording)
			return "Recording has never been started!";
		
		isRecording = false;
		
		flush();
		
		try {
			if (out != null)
				out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		long now = System.currentTimeMillis();
	    long elapsedMilliseconds = now - startTime;
	    return "Time elapsed: " + new SimpleDateFormat("mm:ss.SSS").format(elapsedMilliseconds);
	}

	
	public void close()
	{
		stopRecording();
	}


	public boolean isRecording()
	{
		return isRecording;
	}

}
