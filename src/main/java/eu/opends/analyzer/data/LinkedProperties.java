/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.analyzer.data;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Properties;

public class LinkedProperties extends Properties
{
	private static final long serialVersionUID = 5173409326954817646L;
	private final HashSet<Object> keys = new LinkedHashSet<Object>();

	
    public LinkedProperties(){
    }

    public Iterable<Object> orderedKeys()
    {
        return Collections.list(keys());
    }

    public Enumeration<Object> keys()
    {
        return Collections.<Object>enumeration(keys);
    }

    public Object put(Object key, Object value)
    {
        keys.add(key);
        return super.put(key, value);
    }
}