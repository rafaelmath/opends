/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.analyzer.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import eu.opends.analyzer.DataUnit;
import eu.opends.drools.toc.EnvironmentParams;
import eu.opends.drools.toc.VehicleParams;
import eu.opends.eyetracker.GazeLog;
import eu.opends.main.Simulator;
import eu.opends.trigger.TriggerCenter;

public class DataSet
{
	private Simulator sim;
	private Date timeStamp;
	private String referenceObjectData;
	private String stringRepresentation = "";

	
	public DataSet(Simulator sim, Date timeStamp, LogParameters logParameters, String valueSeparator, String referenceObjectData)
	{
		this.sim = sim;
		this.timeStamp = timeStamp;
		this.referenceObjectData = referenceObjectData;
		ArrayList<String> selectedParameterList = logParameters.getSelectedParametersList(); 
		
		for(int i=0; i<selectedParameterList.size(); i++)
		{
			String parameterName = selectedParameterList.get(i);
			String value = getValue(parameterName);
			
			if(value != null)
			{
				if(i == 0)
					stringRepresentation = value;
				else
					stringRepresentation += valueSeparator + value;
			}
			else
				System.err.println("DataSet: '" + parameterName + "' is not a valid log parameter");
		}
	}
	
	
	public DataSet(DataUnit dataUnit, LogParameters logParameters, String valueSeparator)
	{
		ArrayList<String> selectedParameterList = logParameters.getSelectedParametersList(); 
		
		for(int i=0; i<selectedParameterList.size(); i++)
		{
			String parameterName = selectedParameterList.get(i);
			String value = getValue(dataUnit, parameterName);
			
			if(value != null)
			{
				if(i == 0)
					stringRepresentation = value;
				else
					stringRepresentation += valueSeparator + value;
			}
			else
				System.err.println("DataSet (DriveAnalyzer): '" + parameterName + "' is not a valid log parameter");
		}
	}


	private String getValue(String parameterName)
	{
		VehicleParams vehicleParams = sim.getToCRulesCenter().getVehicleParams();
		EnvironmentParams environmentParams = sim.getToCRulesCenter().getEnvironmentParams();
		GazeLog gazeLog = sim.getEyetrackerCenter().getGazeLog();
		
		if(parameterName.equals("milliseconds"))
		{
			return Long.toString(timeStamp.getTime());
		}
		
		else if (parameterName.equals("timestamp"))
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			return sdf.format(timeStamp);
		}
		
		else if (parameterName.equals("simulatorState"))
		{
			return TriggerCenter.getActiveExecutionClassString();
		}
		
		else if (parameterName.equals("position"))
		{
			Vector3f pos = vehicleParams.position;
			return "(" + pos.getX() + "/" + pos.getY() + "/" + pos.getZ() + ")";
		}
		
		else if (parameterName.equals("rotation"))
		{
			Quaternion rot = vehicleParams.rotation;
			return "(" + rot.getX() + "/" + rot.getY() + "/" + rot.getZ() + "/" + rot.getW() + ")";
		}
		
		else if (parameterName.equals("speedKmh"))
		{
			return Float.toString(vehicleParams.speedKmh);
		}
		
		else if (parameterName.equals("steeringWheelPos"))
		{
			return Float.toString(vehicleParams.steeringWheelPos);
		}
		
		else if (parameterName.equals("acceleratorPedalPos"))
		{
			return Float.toString(vehicleParams.acceleratorPedalPos);
		}
		
		else if (parameterName.equals("brakePedalPos"))
		{
			return Float.toString(vehicleParams.brakePedalPos);
		}
		
		else if (parameterName.equals("clutchPedalPos"))
		{
			return Float.toString(vehicleParams.clutchPedalPos);
		}
		
		else if (parameterName.equals("isEngineOn"))
		{
			return Boolean.toString(vehicleParams.isEngineOn);
		}
		
		else if (parameterName.equals("rpm"))
		{
			return Float.toString(vehicleParams.rpm);
		}

		else if (parameterName.equals("isHandBrakeEngaged"))
		{
			return Boolean.toString(vehicleParams.isHandBrakeEngaged);
		}

		else if (parameterName.equals("turnSignalState"))
		{
			return vehicleParams.turnSignalState.toString();
		}

		else if (parameterName.equals("isLightOn"))
		{
			return Boolean.toString(vehicleParams.isLightOn);
		}

		else if (parameterName.equals("lightIntensity"))
		{
			return Float.toString(vehicleParams.lightIntensity);
		}

		else if (parameterName.equals("isAutoPilotOn"))
		{
			return Boolean.toString(vehicleParams.isAutoPilotOn);
		}

		else if (parameterName.equals("isBrakeLightOn"))
		{
			return Boolean.toString(vehicleParams.isBrakeLightOn);
		}

		else if (parameterName.equals("isCruiseControlOn"))
		{
			return Boolean.toString(vehicleParams.isCruiseControlOn);
		}

		else if (parameterName.equals("cruiseControlTargetSpeedKmh"))
		{
			return Float.toString(vehicleParams.cruiseControlTargetSpeedKmh);
		}

		else if (parameterName.equals("isAutomatic"))
		{
			return Boolean.toString(vehicleParams.isAutomatic);
		}

		else if (parameterName.equals("selectedGear"))
		{
			return Integer.toString(vehicleParams.selectedGear);
		}

		else if (parameterName.equals("milage"))
		{
			return Float.toString(vehicleParams.milage);
		}

		else if (parameterName.equals("headingDegree"))
		{
			return Float.toString(vehicleParams.headingDegree);
		}

		else if (parameterName.equals("slopeDegree"))
		{
			return Float.toString(vehicleParams.slopeDegree);
		}

		else if (parameterName.equals("accelerationLgt"))
		{
			return Float.toString(vehicleParams.accelerationLgt);
		}

		else if (parameterName.equals("accelerationLat"))
		{
			return Float.toString(vehicleParams.accelerationLat);
		}

		else if (parameterName.equals("snowingPercentage"))
		{
			return Float.toString(environmentParams.snowingPercentage);
		}

		else if (parameterName.equals("rainingPercentage"))
		{
			return Float.toString(environmentParams.rainingPercentage);
		}

		else if (parameterName.equals("fogPercentage"))
		{
			return Float.toString(environmentParams.fogPercentage);
		}

		else if (parameterName.equals("roadID"))
		{
			return environmentParams.roadID;
		}

		else if (parameterName.equals("laneID"))
		{
			return Integer.toString(environmentParams.laneID);
		}

		else if (parameterName.equals("s"))
		{
			return Float.toString(environmentParams.s);
		}

		else if (parameterName.equals("hdgLane"))
		{
			return Float.toString(environmentParams.hdgLane);
		}

		else if (parameterName.equals("hdgCar"))
		{
			return Float.toString(environmentParams.hdgCar);
		}

		else if (parameterName.equals("hdgDiff"))
		{
			return Float.toString(environmentParams.hdgDiff);
		}

		else if (parameterName.equals("isWrongWay"))
		{
			return Boolean.toString(environmentParams.isWrongWay);
		}

		else if (parameterName.equals("laneType"))
		{
			return environmentParams.laneType.toString();
		}

		else if (parameterName.equals("lanePosition"))
		{
			return environmentParams.lanePosition.toString();
		}

		else if (parameterName.equals("nrObjs"))
		{
			return Integer.toString(environmentParams.nrObjs);
		}

		else if (parameterName.equals("objName"))
		{
			return environmentParams.objName;
		}

		else if (parameterName.equals("objClass"))
		{
			return environmentParams.objClass;
		}
		
		else if (parameterName.equals("objClassString"))
		{
			return environmentParams.objClassString;
		}

		else if (parameterName.equals("objX"))
		{
			return environmentParams.objX;
		}

		else if (parameterName.equals("objY"))
		{
			return environmentParams.objY;
		}

		else if (parameterName.equals("objDist"))
		{
			return environmentParams.objDist;
		}

		else if (parameterName.equals("objDirection"))
		{
			return environmentParams.objDirection;
		}

		else if (parameterName.equals("objVel"))
		{
			return environmentParams.objVel;
		}
	
		else if (parameterName.equals("objPos"))
		{
			return environmentParams.objPos;
		}
		
		else if (parameterName.equals("objRot"))
		{
			return environmentParams.objRot;
		}

		else if (parameterName.equals("laneWidth"))
		{
			return Float.toString(environmentParams.laneWidth);
		}

		else if (parameterName.equals("latOffsLineR"))
		{
			return Float.toString(environmentParams.latOffsLineR);
		}

		else if (parameterName.equals("latOffsLineL"))
		{
			return Float.toString(environmentParams.latOffsLineL);
		}

		else if (parameterName.equals("laneCrvt"))
		{
			return Float.toString(environmentParams.laneCrvt);
		}

		else if (parameterName.equals("leftLineType"))
		{
			return environmentParams.leftLineType.toString();
		}

		else if (parameterName.equals("rightLineType"))
		{
			return environmentParams.rightLineType.toString();
		}

		else if (parameterName.equals("leftLaneInfo"))
		{
			return environmentParams.leftLaneInfo.toString();
		}

		else if (parameterName.equals("rightLaneInfo"))
		{
			return environmentParams.rightLaneInfo.toString();
		}

		else if (parameterName.equals("sideObstacleLeft"))
		{
			return Boolean.toString(environmentParams.sideObstacleLeft);
		}

		else if (parameterName.equals("sideObstacleRight"))
		{
			return Boolean.toString(environmentParams.sideObstacleRight);
		}

		else if (parameterName.equals("blindSpotObstacleLeft"))
		{
			return Boolean.toString(environmentParams.blindSpotObstacleLeft);
		}

		else if (parameterName.equals("blindSpotObstacleRight"))
		{
			return Boolean.toString(environmentParams.blindSpotObstacleRight);
		}

		else if (parameterName.equals("nrLanesDrivingDirection"))
		{
			return Integer.toString(environmentParams.nrLanesDrivingDirection);
		}

		else if (parameterName.equals("nrLanesOppositeDirection"))
		{
			return Integer.toString(environmentParams.nrLanesOppositeDirection);
		}

		else if (parameterName.equals("currentSpeedLimit"))
		{
			return Integer.toString(environmentParams.currentSpeedLimit);
		}

		else if (parameterName.equals("nrSpeedLimits"))
		{
			return Integer.toString(environmentParams.nrSpeedLimits);
		}

		else if (parameterName.equals("speedLimitDist"))
		{
			return environmentParams.speedLimitDist;
		}

		else if (parameterName.equals("speedLimitValues"))
		{
			return environmentParams.speedLimitValues;
		}

		else if (parameterName.equals("intersectionDistance"))
		{
			return Float.toString(environmentParams.intersectionDistance);
		}

		else if (parameterName.equals("trafficLightAhead"))
		{
			return Boolean.toString(environmentParams.trafficLightAhead);
		}

		else if (parameterName.equals("trafficLightDist"))
		{
			return Float.toString(environmentParams.trafficLightDist);
		}

		else if (parameterName.equals("trafficLightStates"))
		{
			return environmentParams.trafficLightStates;
		}

		else if (parameterName.equals("trafficLightTimesToChange"))
		{
			return environmentParams.trafficLightTimesToChange;
		}

		else if (parameterName.equals("targetDistance"))
		{
			return Float.toString(environmentParams.targetDistance);
		}

		else if (parameterName.equals("leadingCarPresent"))
		{
			return Boolean.toString(environmentParams.leadingCarPresent);
		}

		else if (parameterName.equals("leadingCarName"))
		{
			return environmentParams.leadingCarName;
		}

		else if (parameterName.equals("leadingCarRoadID"))
		{
			return environmentParams.leadingCarRoadID;
		}

		else if (parameterName.equals("leadingCarLaneID"))
		{
			return Integer.toString(environmentParams.leadingCarLaneID);
		}

		else if (parameterName.equals("leadingCarS"))
		{
			return Float.toString(environmentParams.leadingCarS);
		}

		else if (parameterName.equals("leadingCarDist"))
		{
			return Float.toString(environmentParams.leadingCarDist);
		}

		else if (parameterName.equals("leadingCarSpeedKmh"))
		{
			return Float.toString(environmentParams.leadingCarSpeedKmh);
		}
		
		else if (parameterName.equals("eyeTrackerMilliseconds"))
		{
			Long eyeTrackerMilliseconds = gazeLog.getEyeTrackerMilliseconds();
			if(eyeTrackerMilliseconds != null)
				return Long.toString(eyeTrackerMilliseconds);
			else
				return "";
		}
		
		else if (parameterName.equals("eyeTrackerTimestamp"))
		{
			String eyeTrackerTimestamp = gazeLog.getEyeTrackerTimestamp();
			if(eyeTrackerTimestamp != null)
				return eyeTrackerTimestamp;
			else
				return "";
		}
		
		else if (parameterName.equals("isValidLeftEye"))
		{
			return Boolean.toString(gazeLog.isValidLeftEye());
		}
		
		else if (parameterName.equals("isValidRightEye"))
		{
			return Boolean.toString(gazeLog.isValidRightEye());
		}
		
		else if (parameterName.equals("gazeCoordinate"))
		{
			Vector2f gazeCoord = gazeLog.getGazeCoord();
			if(gazeCoord != null)
				return "(" + gazeCoord.getX() + "/" + gazeCoord.getY() + ")";
			else
				return "";
		}

		else if (parameterName.equals("gazeCoordinateAvg"))
		{
			Vector2f gazeCoordAvg = gazeLog.getGazeCoordAverage();
			if(gazeCoordAvg != null)
				return "(" + gazeCoordAvg.getX() + "/" + gazeCoordAvg.getY() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("gazeRayOrigin"))
		{
			Vector3f gazeRayOrigin = gazeLog.getRayOrigin();
			if(gazeRayOrigin != null)
				return "(" + gazeRayOrigin.getX() + "/" + gazeRayOrigin.getY() + "/" + gazeRayOrigin.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("gazeRayDirection"))
		{
			Vector3f gazeRayDirection = gazeLog.getRayDirection();
			if(gazeRayDirection != null)
				return "(" + gazeRayDirection.getX() + "/" + gazeRayDirection.getY() + "/" + gazeRayDirection.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("closestHitByGazeRay"))
		{
			String closestHit = gazeLog.getClosestHit();
			if(closestHit != null)
				return closestHit;
			else
				return "";
		}
		
		else if (parameterName.equals("closestContactPointByGazeRay"))
		{
			Vector3f closestContactPoint = gazeLog.getClosestContactPoint();
			if(closestContactPoint != null)
				return "(" + closestContactPoint.getX() + "/" + closestContactPoint.getY() + "/" + closestContactPoint.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("frontPosition"))
		{
			// be aware: for comparison of frontPos with driverPos use the following convention:
			// driverPos = carPos.add(0, 1, 0);
			// since frontPos is already raised by 1 m
			Vector3f frontPos = sim.getCar().getFrontGeometry().getWorldTranslation().add(0, 1, 0);
			return "(" + frontPos.getX() + "/" + frontPos.getY() + "/" + frontPos.getZ() + ")";
		}

		else if (parameterName.equals("referenceObjectData"))
		{
			return referenceObjectData;
		}
		
		else if (parameterName.equals("aoi2D"))
		{
			return null;
		}
		
		return null;
	}

	
	private String getValue(DataUnit dataUnit, String parameterName)
	{
		if(parameterName.equals("milliseconds"))
		{
			return Long.toString(dataUnit.getDate().getTime());
		}
		
		else if (parameterName.equals("timestamp"))
		{
			return dataUnit.getTimestamp();
		}
		
		else if (parameterName.equals("simulatorState"))
		{
			return dataUnit.getSimulatorState();
		}
		
		else if (parameterName.equals("position"))
		{
			Vector3f pos = dataUnit.getCarPosition();
			return "(" + pos.getX() + "/" + pos.getY() + "/" + pos.getZ() + ")";
		}
		
		else if (parameterName.equals("rotation"))
		{
			Quaternion rot = dataUnit.getCarRotation();
			return "(" + rot.getX() + "/" + rot.getY() + "/" + rot.getZ() + "/" + rot.getW() + ")";
		}
		
		else if (parameterName.equals("speedKmh"))
		{
			return Float.toString(dataUnit.getSpeedKmh());
		}
		
		else if (parameterName.equals("steeringWheelPos"))
		{
			return Float.toString(dataUnit.getSteeringWheelPos());
		}
		
		else if (parameterName.equals("acceleratorPedalPos"))
		{
			return Float.toString(dataUnit.getAcceleratorPedalPos());
		}
		
		else if (parameterName.equals("brakePedalPos"))
		{
			return Float.toString(dataUnit.getBrakePedalPos());
		}
		
		else if (parameterName.equals("clutchPedalPos"))
		{
			return Float.toString(dataUnit.getClutchPedalPos());
		}
		
		else if (parameterName.equals("isEngineOn"))
		{
			return Boolean.toString(dataUnit.getIsEngineOn());
		}
		
		else if (parameterName.equals("rpm"))
		{
			return Float.toString(dataUnit.getRpm());
		}

		else if (parameterName.equals("isHandBrakeEngaged"))
		{
			return Boolean.toString(dataUnit.getIsHandBrakeEngaged());
		}

		else if (parameterName.equals("turnSignalState"))
		{
			return dataUnit.getTurnSignalState().toString();
		}

		else if (parameterName.equals("isLightOn"))
		{
			return Boolean.toString(dataUnit.getIsLightOn());
		}

		else if (parameterName.equals("lightIntensity"))
		{
			return Float.toString(dataUnit.getLightIntensity());
		}

		else if (parameterName.equals("isAutoPilotOn"))
		{
			return Boolean.toString(dataUnit.getIsAutoPilotOn());
		}

		else if (parameterName.equals("isBrakeLightOn"))
		{
			return Boolean.toString(dataUnit.getIsBrakeLightOn());
		}

		else if (parameterName.equals("isCruiseControlOn"))
		{
			return Boolean.toString(dataUnit.getIsCruiseControlOn());
		}

		else if (parameterName.equals("cruiseControlTargetSpeedKmh"))
		{
			return Float.toString(dataUnit.getCruiseControlTargetSpeedKmh());
		}

		else if (parameterName.equals("isAutomatic"))
		{
			return Boolean.toString(dataUnit.getIsAutomatic());
		}

		else if (parameterName.equals("selectedGear"))
		{
			return Integer.toString(dataUnit.getSelectedGear());
		}

		else if (parameterName.equals("milage"))
		{
			return Float.toString(dataUnit.getMilage());
		}

		else if (parameterName.equals("headingDegree"))
		{
			return Float.toString(dataUnit.getHeadingDegree());
		}

		else if (parameterName.equals("slopeDegree"))
		{
			return Float.toString(dataUnit.getSlopeDegree());
		}

		else if (parameterName.equals("accelerationLgt"))
		{
			return Float.toString(dataUnit.getAccelerationLgt());
		}

		else if (parameterName.equals("accelerationLat"))
		{
			return Float.toString(dataUnit.getAccelerationLat());
		}

		else if (parameterName.equals("snowingPercentage"))
		{
			return Float.toString(dataUnit.getSnowingPercentage());
		}

		else if (parameterName.equals("rainingPercentage"))
		{
			return Float.toString(dataUnit.getRainingPercentage());
		}

		else if (parameterName.equals("fogPercentage"))
		{
			return Float.toString(dataUnit.getFogPercentage());
		}

		else if (parameterName.equals("roadID"))
		{
			return dataUnit.getRoadID();
		}

		else if (parameterName.equals("laneID"))
		{
			return Integer.toString(dataUnit.getLaneID());
		}

		else if (parameterName.equals("s"))
		{
			return Float.toString(dataUnit.getS());
		}

		else if (parameterName.equals("hdgLane"))
		{
			return Float.toString(dataUnit.getHdgLane());
		}

		else if (parameterName.equals("hdgCar"))
		{
			return Float.toString(dataUnit.getHdgCar());
		}

		else if (parameterName.equals("hdgDiff"))
		{
			return Float.toString(dataUnit.getHdgDiff());
		}

		else if (parameterName.equals("isWrongWay"))
		{
			if(dataUnit.getIsWrongWay() == null)
				return "";
			else
				return Boolean.toString(dataUnit.getIsWrongWay());
		}

		else if (parameterName.equals("laneType"))
		{
			return dataUnit.getLaneType().toString();
		}

		else if (parameterName.equals("lanePosition"))
		{
			return dataUnit.getLanePosition().toString();
		}

		else if (parameterName.equals("nrObjs"))
		{
			return Integer.toString(dataUnit.getNrObjs());
		}

		else if (parameterName.equals("objName"))
		{
			return arrayToString(dataUnit.getObjName());
		}

		else if (parameterName.equals("objClass"))
		{
			return arrayToString(dataUnit.getObjClass());
		}
		
		else if (parameterName.equals("objClassString"))
		{
			return arrayToString(dataUnit.getObjClassString());
		}

		else if (parameterName.equals("objX"))
		{
			return arrayToString(dataUnit.getObjX());
		}

		else if (parameterName.equals("objY"))
		{
			return arrayToString(dataUnit.getObjY());
		}

		else if (parameterName.equals("objDist"))
		{
			return arrayToString(dataUnit.getObjDist());
		}

		else if (parameterName.equals("objDirection"))
		{
			return arrayToString(dataUnit.getObjDirection());
		}

		else if (parameterName.equals("objVel"))
		{
			return arrayToString(dataUnit.getObjVel());
		}
	
		else if (parameterName.equals("objPos"))
		{
			return vector3fArrayToString(dataUnit.getObjPos());
		}
		
		else if (parameterName.equals("objRot"))
		{
			return quaternionArrayToString(dataUnit.getObjRot());
		}

		else if (parameterName.equals("laneWidth"))
		{
			return Float.toString(dataUnit.getLaneWidth());
		}

		else if (parameterName.equals("latOffsLineR"))
		{
			return Float.toString(dataUnit.getLatOffsLineR());
		}

		else if (parameterName.equals("latOffsLineL"))
		{
			return Float.toString(dataUnit.getLatOffsLineL());
		}

		else if (parameterName.equals("laneCrvt"))
		{
			return Float.toString(dataUnit.getLaneCrvt());
		}

		else if (parameterName.equals("leftLineType"))
		{
			return dataUnit.getLeftLineType().toString();
		}

		else if (parameterName.equals("rightLineType"))
		{
			return dataUnit.getRightLineType().toString();
		}

		else if (parameterName.equals("leftLaneInfo"))
		{
			return dataUnit.getLeftLaneInfo().toString();
		}

		else if (parameterName.equals("rightLaneInfo"))
		{
			return dataUnit.getRightLaneInfo().toString();
		}

		else if (parameterName.equals("sideObstacleLeft"))
		{
			return Boolean.toString(dataUnit.getSideObstacleLeft());
		}

		else if (parameterName.equals("sideObstacleRight"))
		{
			return Boolean.toString(dataUnit.getSideObstacleRight());
		}

		else if (parameterName.equals("blindSpotObstacleLeft"))
		{
			return Boolean.toString(dataUnit.getBlindSpotObstacleLeft());
		}

		else if (parameterName.equals("blindSpotObstacleRight"))
		{
			return Boolean.toString(dataUnit.getBlindSpotObstacleRight());
		}

		else if (parameterName.equals("nrLanesDrivingDirection"))
		{
			return Integer.toString(dataUnit.getNrLanesDrivingDirection());
		}

		else if (parameterName.equals("nrLanesOppositeDirection"))
		{
			return Integer.toString(dataUnit.getNrLanesOppositeDirection());
		}

		else if (parameterName.equals("currentSpeedLimit"))
		{
			return Integer.toString(dataUnit.getCurrentSpeedLimit());
		}

		else if (parameterName.equals("nrSpeedLimits"))
		{
			return Integer.toString(dataUnit.getNrSpeedLimits());
		}

		else if (parameterName.equals("speedLimitDist"))
		{
			return arrayToString(dataUnit.getSpeedLimitDist());
		}

		else if (parameterName.equals("speedLimitValues"))
		{
			return arrayToString(dataUnit.getSpeedLimitValues());
		}

		else if (parameterName.equals("intersectionDistance"))
		{
			return Float.toString(dataUnit.getIntersectionDistance());
		}

		else if (parameterName.equals("trafficLightAhead"))
		{
			return Boolean.toString(dataUnit.getTrafficLightAhead());
		}

		else if (parameterName.equals("trafficLightDist"))
		{
			return Float.toString(dataUnit.getTrafficLightDist());
		}

		else if (parameterName.equals("trafficLightStates"))
		{
			return arrayToString(dataUnit.getTrafficLightStates());
		}

		else if (parameterName.equals("trafficLightTimesToChange"))
		{
			return arrayToString(dataUnit.getTrafficLightTimesToChange());
		}

		else if (parameterName.equals("targetDistance"))
		{
			return Float.toString(dataUnit.getTargetDistance());
		}

		else if (parameterName.equals("leadingCarPresent"))
		{
			if(dataUnit.getLeadingCarPresent() == null)
				return "false";
			else
				return Boolean.toString(dataUnit.getLeadingCarPresent());
		}

		else if (parameterName.equals("leadingCarName"))
		{
			if(dataUnit.getLeadingCarName() == null)
				return "";
			else
				return dataUnit.getLeadingCarName();
		}

		else if (parameterName.equals("leadingCarRoadID"))
		{
			if(dataUnit.getLeadingCarRoadID() == null)
				return "";
			else
				return dataUnit.getLeadingCarRoadID();
		}

		else if (parameterName.equals("leadingCarLaneID"))
		{
			if(dataUnit.getLeadingCarLaneID() == null)
				return "0";
			else
				return Integer.toString(dataUnit.getLeadingCarLaneID());
		}

		else if (parameterName.equals("leadingCarS"))
		{
			if(dataUnit.getLeadingCarS() == null)
				return "-1";
			else
				return Float.toString(dataUnit.getLeadingCarS());
		}

		else if (parameterName.equals("leadingCarDist"))
		{
			if(dataUnit.getLeadingCarDist() == null)
				return "-1";
			else
				return Float.toString(dataUnit.getLeadingCarDist());
		}

		else if (parameterName.equals("leadingCarSpeedKmh"))
		{
			if(dataUnit.getLeadingCarSpeedKmh() == null)
				return "-1";
			else
				return Float.toString(dataUnit.getLeadingCarSpeedKmh());
		}
		
		else if (parameterName.equals("eyeTrackerMilliseconds"))
		{
			if(dataUnit.getEyeTrackerDate() != null)
				return Long.toString(dataUnit.getEyeTrackerDate().getTime());
			else
				return null;
		}
		
		else if (parameterName.equals("eyeTrackerTimestamp"))
		{
			return dataUnit.getEyeTrackerTimestamp();
		}
		
		else if (parameterName.equals("isValidLeftEye"))
		{
			return Boolean.toString(dataUnit.isValidLeftEye());
		}
		
		else if (parameterName.equals("isValidRightEye"))
		{
			return Boolean.toString(dataUnit.isValidRightEye());
		}
		
		else if (parameterName.equals("gazeCoordinate"))
		{
			Vector2f gazeCoord = dataUnit.getGazeCoordinate();
			if(gazeCoord != null)
				return "(" + gazeCoord.getX() + "/" + gazeCoord.getY() + ")";
			else
				return "";
		}

		else if (parameterName.equals("gazeCoordinateAvg"))
		{
			Vector2f gazeCoordAvg = dataUnit.getGazeCoordinateAvg();
			if(gazeCoordAvg != null)
				return "(" + gazeCoordAvg.getX() + "/" + gazeCoordAvg.getY() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("gazeRayOrigin"))
		{
			Vector3f gazeRayOrigin = dataUnit.getGazeRayOrigin();
			if(gazeRayOrigin != null)
				return "(" + gazeRayOrigin.getX() + "/" + gazeRayOrigin.getY() + "/" + gazeRayOrigin.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("gazeRayDirection"))
		{
			Vector3f gazeRayDirection = dataUnit.getGazeRayDirection();
			if(gazeRayDirection != null)
				return "(" + gazeRayDirection.getX() + "/" + gazeRayDirection.getY() + "/" + gazeRayDirection.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("closestHitByGazeRay"))
		{
			String closestHit = dataUnit.getClosestHitByGazeRay();
			if(closestHit != null)
				return closestHit;
			else
				return "";
		}
		
		else if (parameterName.equals("closestContactPointByGazeRay"))
		{
			Vector3f closestContactPoint = dataUnit.getClosestContactPointByGazeRay();
			if(closestContactPoint != null)
				return "(" + closestContactPoint.getX() + "/" + closestContactPoint.getY() + "/" + closestContactPoint.getZ() + ")";
			else
				return "";
		}
		
		else if (parameterName.equals("frontPosition"))
		{
			// be aware: for comparison of frontPos with driverPos use the following convention:
			// driverPos = carPos.add(0, 1, 0);
			// since frontPos is already raised by 1 m
			Vector3f frontPos = dataUnit.getFrontPosition();
			return "(" + frontPos.getX() + "/" + frontPos.getY() + "/" + frontPos.getZ() + ")";
		}

		else if (parameterName.equals("referenceObjectData"))
		{
			return dataUnit.getReferenceObjectData();
		}
		
		else if (parameterName.equals("aoi2D"))
		{
			return dataUnit.getAoi2D();
		}
		
		return null;
	}

	
	private String arrayToString(ArrayList<?> array)
	{
		String output = "[";
		
		for(int i=0; i<array.size(); i++)
		{
			output += array.get(i).toString();
			
			if(i<array.size()-1)
				output += ",";
		}

		return output + "]";
	}
	
	
	private String vector3fArrayToString(ArrayList<Vector3f> array)
	{
		String output = "[";
		
		for(int i=0; i<array.size(); i++)
		{
			output += "(" + array.get(i).getX() +  "/" + array.get(i).getY() + "/" + array.get(i).getZ() + ")";
			
			if(i<array.size()-1)
				output += ",";
		}

		return output + "]";
	}
	
	
	private String quaternionArrayToString(ArrayList<Quaternion> array)
	{
		String output = "[";
		
		for(int i=0; i<array.size(); i++)
		{
			output += "(" + array.get(i).getX() +  "/" + array.get(i).getY() + "/" + array.get(i).getZ() + "/" + array.get(i).getW() + ")";
			
			if(i<array.size()-1)
				output += ",";
		}

		return output + "]";
	}


	public String getDataString()
	{
		return stringRepresentation;
	}

}
