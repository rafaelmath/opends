/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.analyzer.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class LogParameters
{
	private LinkedProperties properties = new LinkedProperties();
	private ArrayList<String> selectedParametersList = new ArrayList<String>();
	
	
	public LogParameters(String logParameterFilePath)
	{
		try {
			FileInputStream inputStream = new FileInputStream(logParameterFilePath);
			properties.load(inputStream);
			inputStream.close();
			
			ArrayList<String> allParameterList = getAllParametersList();

			Iterator<Object> elems = properties.orderedKeys().iterator();
			while(elems.hasNext())
			{
				String key = elems.next().toString();
				if(key != null && !key.isEmpty() && allParameterList.contains(key))
				{
					boolean value = getBooleanProperty(key, false);
					if(value)
						selectedParametersList.add(key);
				}
				else
					System.err.println("LogParameter: '" + key + "' is not a valid log parameter");
			}

		} catch (FileNotFoundException e) {
			System.err.println("LogParameter: file '" + logParameterFilePath + "' could not be found");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private boolean getBooleanProperty(String propertyName, boolean defaultValue) 
	{
		Boolean propertyValueBool = null;
		String propertyValue = properties.getProperty(propertyName);
		
		if(propertyValue == null)
			return defaultValue;
		
        try {
        	propertyValueBool = Boolean.parseBoolean(propertyValue);
        	//System.out.println(propertyName + ": " + propertyValueBool);
        	
        } catch (Exception e) {

			System.err.println("LogParameters: " + propertyName + ": '" + propertyValue 
					+ "' is not a valid boolean. Using default: " + defaultValue);
		}
        
        return (propertyValueBool==null?defaultValue:propertyValueBool);
	}

	
	public String getHeaderString(String valueSeparator)
	{
		String returnString = "";
		
		for(int i=0; i<selectedParametersList.size(); i++)
		{
			String parameterName = selectedParametersList.get(i);
			
			if(i == 0)
				returnString = parameterName;
			else
				returnString += valueSeparator + parameterName;
		}

		return returnString;
	}
	
	
	public ArrayList<String> getSelectedParametersList()
	{
		return selectedParametersList;
	}
	
	
	public static ArrayList<String> getAllParametersList()
	{
		ArrayList<String> allParametersList = new ArrayList<String>();
		allParametersList.add("milliseconds");
		allParametersList.add("timestamp");
		allParametersList.add("simulatorState");
		allParametersList.add("position");
		allParametersList.add("rotation");
		allParametersList.add("speedKmh");
		allParametersList.add("steeringWheelPos");
		allParametersList.add("acceleratorPedalPos");
		allParametersList.add("brakePedalPos");
		allParametersList.add("isEngineOn");
		allParametersList.add("clutchPedalPos");
		allParametersList.add("rpm");
		allParametersList.add("isHandBrakeEngaged");
		allParametersList.add("turnSignalState");
		allParametersList.add("isLightOn");
		allParametersList.add("lightIntensity");
		allParametersList.add("isAutoPilotOn");
		allParametersList.add("isBrakeLightOn");
		allParametersList.add("isCruiseControlOn");
		allParametersList.add("cruiseControlTargetSpeedKmh");
		allParametersList.add("isAutomatic");
		allParametersList.add("selectedGear");
		allParametersList.add("milage");
		allParametersList.add("headingDegree");
		allParametersList.add("slopeDegree");
		allParametersList.add("accelerationLgt");
		allParametersList.add("accelerationLat");
		allParametersList.add("snowingPercentage");
		allParametersList.add("rainingPercentage");
		allParametersList.add("fogPercentage");
		allParametersList.add("roadID");
		allParametersList.add("laneID");
		allParametersList.add("s");
		allParametersList.add("hdgLane");
		allParametersList.add("hdgCar");
		allParametersList.add("hdgDiff");
		allParametersList.add("isWrongWay");
		allParametersList.add("laneType");
		allParametersList.add("lanePosition");
		allParametersList.add("nrObjs");
		allParametersList.add("objName");
		allParametersList.add("objClass");
		allParametersList.add("objClassString");
		allParametersList.add("objX");
		allParametersList.add("objY");
		allParametersList.add("objDist");
		allParametersList.add("objDirection");
		allParametersList.add("objVel");
		allParametersList.add("objPos");
		allParametersList.add("objRot");
		allParametersList.add("laneWidth");
		allParametersList.add("latOffsLineR");
		allParametersList.add("latOffsLineL");
		allParametersList.add("laneCrvt");
		allParametersList.add("leftLineType");
		allParametersList.add("rightLineType");
		allParametersList.add("leftLaneInfo");
		allParametersList.add("rightLaneInfo");
		allParametersList.add("sideObstacleLeft");
		allParametersList.add("sideObstacleRight");
		allParametersList.add("blindSpotObstacleLeft");
		allParametersList.add("blindSpotObstacleRight");
		allParametersList.add("nrLanesDrivingDirection");
		allParametersList.add("nrLanesOppositeDirection");
		allParametersList.add("currentSpeedLimit");
		allParametersList.add("nrSpeedLimits");
		allParametersList.add("speedLimitDist");
		allParametersList.add("speedLimitValues");
		allParametersList.add("intersectionDistance");
		allParametersList.add("trafficLightAhead");
		allParametersList.add("trafficLightDist");
		allParametersList.add("trafficLightStates");
		allParametersList.add("trafficLightTimesToChange");
		allParametersList.add("targetDistance");
		allParametersList.add("leadingCarPresent");
		allParametersList.add("leadingCarName");
		allParametersList.add("leadingCarRoadID");
		allParametersList.add("leadingCarLaneID");
		allParametersList.add("leadingCarS");
		allParametersList.add("leadingCarDist");
		allParametersList.add("leadingCarSpeedKmh");
		allParametersList.add("eyeTrackerMilliseconds");
		allParametersList.add("eyeTrackerTimestamp");
		allParametersList.add("isValidLeftEye");
		allParametersList.add("isValidRightEye");
		allParametersList.add("gazeCoordinate");
		allParametersList.add("gazeCoordinateAvg");
		allParametersList.add("gazeRayOrigin");
		allParametersList.add("gazeRayDirection");
		allParametersList.add("closestHitByGazeRay");
		allParametersList.add("closestContactPointByGazeRay");
		allParametersList.add("frontPosition");
		allParametersList.add("referenceObjectData");
		allParametersList.add("aoi2D");
		return allParametersList;
	}

}
