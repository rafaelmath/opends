/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.analyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import com.jme3.math.Vector3f;

import eu.opends.drivingTask.DrivingTask;

/**
 * 
 * @author Rafael Math
 */
public class DataReader 
{
	private File inFile;
	private BufferedReader inputReader;
	private String nameOfDrivingTaskFile;
	private String nameOfDriver;
	private Date fileDate = null;
	private String valueSeparator = ";";
	private String listSeparator = ",";
	private String coordinateSeparator = "/";
	private DataProcessor dataPool;
	
	
	private ArrayList<Vector3f> carPositionList = new ArrayList<Vector3f>();
	private LinkedList<DataUnit> dataUnitList = new LinkedList<DataUnit>();
	
	
	public boolean initReader(String filePath, boolean verbose) 
	{
		String inputLine;
		String[] splittedLineArray;

		inFile = new File(filePath);
		if (!inFile.isFile())
			System.err.println("File " + inFile.toString() + " could not be found.");

		try {
			inputReader = new BufferedReader(new FileReader(inFile));
			
			inputLine = inputReader.readLine();
			splittedLineArray = inputLine.split(": ");
			
			while(splittedLineArray.length == 2)
			{
				if(splittedLineArray[0].equalsIgnoreCase("Driving Task"))
				{
					nameOfDrivingTaskFile = splittedLineArray[1];
					if(verbose)
						System.out.println("Driving Task: " + splittedLineArray[1]);
				}
				else if(splittedLineArray[0].equalsIgnoreCase("Creation Time"))
				{
					try {
						// Save the date
						fileDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(splittedLineArray[1]);
						if(verbose)
							System.out.println("Creation Time: " + fileDate);

					} catch (ParseException e) {
						System.err.println("The date could not be read: " + inputLine + " is no valid date.");
					}
				}
				else if(splittedLineArray[0].equalsIgnoreCase("Driver"))
				{
					// Read in name of the driver
					nameOfDriver = splittedLineArray[1];
					if(verbose)
						System.out.println("Driver: " + nameOfDriver);
				}
				else if(splittedLineArray[0].equalsIgnoreCase("Value Separator"))
				{
					valueSeparator = splittedLineArray[1];
					if(verbose)
						System.out.println("Value Separator: " + valueSeparator);
				}
				else if(splittedLineArray[0].equalsIgnoreCase("List Separator"))
				{
					listSeparator = splittedLineArray[1];
					if(verbose)
						System.out.println("List Separator: " + listSeparator);
				}
				else if(splittedLineArray[0].equalsIgnoreCase("Coordinate Separator"))
				{
					coordinateSeparator = splittedLineArray[1];
					if(verbose)
						System.out.println("Coordinate Separator: " + coordinateSeparator);
				}
				
				inputLine = inputReader.readLine();
				splittedLineArray = inputLine.split(": ");
			}
			
			// Read in the header of the parameters
			dataPool = new DataProcessor(valueSeparator, listSeparator, coordinateSeparator, verbose);
			dataPool.processHeader(inputLine);
			
			
		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	public boolean loadDriveData() 
	{		
		try {
			// get drive data
			String inputLine = inputReader.readLine();
			while (inputLine != null) 
			{
				dataPool.addDataUnit(inputLine);
				inputLine = inputReader.readLine();
			}

		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		}
		
		carPositionList = dataPool.getCarPositionList();
		dataUnitList = dataPool.getDataUnitList();
		
		return true;
	}



	public String getNameOfDriver() 
	{
		return nameOfDriver;
	}


	public Date getFileDate() 
	{
		return fileDate;
	}
	

	public String getNameOfDrivingTaskFile() 
	{
		return nameOfDrivingTaskFile;
	}

	
	public ArrayList<Vector3f> getCarPositionList()
	{
		return carPositionList;
	}
	
	
	public float getTotalDistance()
	{
		return dataPool.getTotalDistance();
	}
	
	
	public LinkedList<DataUnit> getDataUnitList()
	{
		return dataUnitList;
	}
	
	
	public boolean isValidAnalyzerFile(File analyzerFile) 
	{
		String analyzerFilePath = analyzerFile.getPath();
		
		try {
			
			boolean errorOccured = !initReader(analyzerFilePath, false);
			if(errorOccured)
			{
				System.err.println("File is not a valid analyzer file: " + analyzerFilePath);
				return false;
			}
			
		} catch (Exception e) {
			
			System.err.println("File is not a valid analyzer file: " + analyzerFilePath);
			return false;
		}
		
		try {
			
			// check whether specified driving task is valid
			String drivingTaskFileName = getNameOfDrivingTaskFile();
			File drivingTaskFile = new File(drivingTaskFileName);				
			if(!DrivingTask.isValidDrivingTask(drivingTaskFile))
			{
				System.err.println("File '" + analyzerFilePath + 
						"'\npoints to an invalid driving task file : " + drivingTaskFileName);
				return false;
			}
			
		} catch (Exception e) {
			
			System.err.println("File '" + analyzerFilePath + "'\npoints to an invalid driving task file");
			return false;
		}
		
		return true;
	}

}
