/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.analyzer;


import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Spline;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Spline.SplineType;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Curve;
import com.jme3.scene.shape.Sphere;

import eu.opends.basics.SimulationBasics;
import eu.opends.drivingTask.interaction.ActionDescription;
import eu.opends.drivingTask.interaction.InteractionLoader;
import eu.opends.drivingTask.interaction.InteractionMethods;
import eu.opends.main.DriveAnalyzer;
import eu.opends.trigger.TriggerAction;


public class RayAnalyzerCenter
{		
	private boolean debug = true;
	
	private boolean showCrossHairs = false;
	private ColorRGBA crossHairsColor = ColorRGBA.White;
	private float scalingFactor = 2;
	
	private Vector2f screenPos;
	private DriveAnalyzer driveAnalyzer;
	private Node sceneNode;
	private AssetManager assetManager;
	private Material redMaterial, blueMaterial, yellowMaterial, greenMaterial;
	private BitmapText crosshairs;
	
	// using maps to access markers/connectors by ID
	private HashMap<String, Geometry> markerMap = new HashMap<String, Geometry>();
	private HashMap<String, Node> connectorMap = new HashMap<String, Node>();
	
	private String activityIDList = "";
	
	
	public RayAnalyzerCenter(DriveAnalyzer driveAnalyzer)
	{
		this.driveAnalyzer = driveAnalyzer;
		this.sceneNode = driveAnalyzer.getSceneNode();
		this.assetManager = driveAnalyzer.getAssetManager();
		
		// init gaze pos
		screenPos = new Vector2f(driveAnalyzer.getSettings().getWidth() / 2f, driveAnalyzer.getSettings().getHeight() / 2f);
			
		// a "+" in the middle of the screen to help aiming
		BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		crosshairs = new BitmapText(guiFont, false);
		crosshairs.setSize(guiFont.getCharSet().getRenderedSize() * scalingFactor);
		crosshairs.setText("+");
		crosshairs.setColor(crossHairsColor);
			
		if(showCrossHairs)
			driveAnalyzer.getGuiNode().attachChild(crosshairs);

		redMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		redMaterial.getAdditionalRenderState().setWireframe(false);
		redMaterial.setColor("Color", ColorRGBA.Red);
		
		blueMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		blueMaterial.getAdditionalRenderState().setWireframe(false);
		blueMaterial.setColor("Color", ColorRGBA.Blue);
		
		yellowMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		yellowMaterial.getAdditionalRenderState().setWireframe(false);
		yellowMaterial.setColor("Color", ColorRGBA.Yellow);
		
		greenMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		greenMaterial.getAdditionalRenderState().setWireframe(false);
		greenMaterial.setColor("Color", ColorRGBA.Green);
	}


	public void update(float tpf)
	{
		// converting screen pos from 3 screen setup (5760 x 1080) to 1 screen setup (1920 x 1080)
		// only using center screen
		// perfect small resolution: 256x1152 ?
		Vector2f screenPosOrig = driveAnalyzer.getCurrentDataUnit().getGazeCoordinateAvg();
				
		if(screenPosOrig != null)
		{
			float width = driveAnalyzer.getSettings().getWidth() * (screenPosOrig.getX()-1920.0f) / 1920.0f; //5760.0f;
			float height = driveAnalyzer.getSettings().getHeight() * screenPosOrig.getY() / 1080.0f;
			screenPos = new Vector2f(width , height);
		
			//System.err.println("screenPosOrig:" + screenPosOrig + ", screenPos: " + screenPos);

			// set cross hairs
			crosshairs.setLocalTranslation(screenPos.getX() - crosshairs.getLineWidth()/2f,
					screenPos.getY() + crosshairs.getLineHeight()/2f, 0);
		}
		else
			crosshairs.setLocalTranslation(-1,-1,0);
			
		
		// get logged eye gaze data
		Vector3f rayOrigin = driveAnalyzer.getCurrentDataUnit().getGazeRayOrigin();
		Vector3f rayDirection = driveAnalyzer.getCurrentDataUnit().getGazeRayDirection();
		String closestGeometry = driveAnalyzer.getCurrentDataUnit().getClosestHitByGazeRay();
		Vector3f closestContactPoint = driveAnalyzer.getCurrentDataUnit().getClosestContactPointByGazeRay();


		
		// visualize logged gaze ray from Tobii eye tracker
		deleteRay("redRay");

		if(rayOrigin != null && rayDirection != null)
		{
			// find "end of ray" position 1000 meter away from origin in given direction 
			Vector3f endOfRay = rayDirection.mult(1000).add(rayOrigin);
			if(closestContactPoint!=null)
				endOfRay = closestContactPoint;

			drawRay("redRay", endOfRay, rayOrigin, redMaterial, true);
		}
		
/*		
		// visualize recalculated gaze ray from Tobii eye tracker
		deleteRay("redRayReCalc");
		Vector3f endOfRayReCalc = rayDirection.mult(1000).add(rayOrigin);
			
		Ray ray = new Ray(rayOrigin, rayDirection);
		CollisionResults results = new CollisionResults();	
		sceneNode.collideWith(ray, results);
		if (results.size() > 0) 
		{
			// the closest collision point is what was truly hit
			CollisionResult closest = getClosestCollision(results); // remove special objects
			if(closest != null)
			{
				endOfRayReCalc = closest.getContactPoint();		
				
				//if(closest.getGeometry() != null)
					//System.err.println("Closest geometry:" + closest.getGeometry().getName() + ", Closest geometry (log): " + closestGeometry);
				
				//System.err.println("Closest contact point:" + closest.getContactPoint() + ", Closest contact point (log): " + closestContactPoint);
			}
		}
		
		drawRay("redRayReCalc", endOfRayReCalc, rayOrigin, redMaterial, true);
*/		
		
		// update blue ray
		RayDirectionRecord blueRayDirectionRecord = driveAnalyzer.getBlueRayDirectionRecord();		
		updateRay("blueRay", rayOrigin, blueRayDirectionRecord, blueMaterial);
		
		// update yellow ray
		RayDirectionRecord yellowRayDirectionRecord = driveAnalyzer.getYellowRayDirectionRecord();		
		updateRay("yellowRay", rayOrigin, yellowRayDirectionRecord, yellowMaterial);
				
		// update green ray
		RayDirectionRecord greenRayDirectionRecord = driveAnalyzer.getGreenRayDirectionRecord();		
		updateRay("greenRay", rayOrigin, greenRayDirectionRecord, greenMaterial);		
		
		
		// set current state to environment
		
		// read state from log
		String state = driveAnalyzer.getCurrentDataUnit().getSimulatorState();
		
		String nextActivityIDList = state + ";updateScene";
		if(!nextActivityIDList.equals(activityIDList))
		{
			InteractionLoader interactionLoader = SimulationBasics.getDrivingTask().getInteractionLoader();
			HashMap<String, List<ActionDescription>> activityMap = interactionLoader.getActivityMap();
		
			// check for multiple activityIDs separated by ;
			String[] activityIdArray = nextActivityIDList.split(";");
			for(String activityID : activityIdArray)
			{
				if(!activityID.isEmpty() && activityMap.containsKey(activityID))
				{
					System.err.println("Perform Activity ID '" + activityID + "' on arrival");

					List<ActionDescription> actionDescriptionList = activityMap.get(activityID);
					for(ActionDescription actionDescription : actionDescriptionList)
					{
						TriggerAction triggerAction = createTriggerAction(actionDescription);
						if(triggerAction != null)
							triggerAction.performAction();
					}	
				}
				else
					System.err.println("interaction.xml: Activity ID '" + activityID + "' does not exist.");
			}
			activityIDList = nextActivityIDList;
		}
	}


	private void updateRay(String rayName, Vector3f rayOrigin, RayDirectionRecord rayDirectionRecord, Material material)
	{
		// remove ray from camera 
		deleteRay(rayName);

		Date currentDate = driveAnalyzer.getCurrentDataUnit().getDate();
		Vector3f rayDirectionLocal = rayDirectionRecord.lookupRayDirectionByTimestamp(currentDate);
		
		if(rayDirectionLocal != null)
		{
			Vector3f rayDirectionWorld = localToWorld(rayDirectionLocal);
			Vector3f endOfRay = rayDirectionWorld.mult(1000).add(rayOrigin);

			Ray ray = new Ray(rayOrigin, rayDirectionWorld);
			CollisionResults results = new CollisionResults();
			sceneNode.collideWith(ray, results);

			if (results.size() > 0) 
			{
				CollisionResult closest = getClosestCollision(results);
				if(closest != null)
					endOfRay = closest.getContactPoint();
			}
			
			drawRay(rayName, endOfRay, rayOrigin, material, true);
		}
	}
	
	
	private TriggerAction createTriggerAction(ActionDescription actionDescription) 
	{
		String name = actionDescription.getName();
		float delay = actionDescription.getDelay();
		int repeat = actionDescription.getRepeat();
		String executionClass = actionDescription.getExecutionClass();
		Properties parameterList = actionDescription.getParameterList();

		TriggerAction triggerAction = null;
		
		// reflection to corresponding method
		try {

			// argument list with corresponding types
			Object argumentList[] = new Object[] {driveAnalyzer, delay, repeat, executionClass, parameterList};
			Class<?> parameterTypes[] = new Class[] {SimulationBasics.class, Float.TYPE, Integer.TYPE, String.class, Properties.class};
			
			// get method to call
			Class<?> interactionMethodsClass = Class.forName("eu.opends.drivingTask.interaction.InteractionMethods");
			Method method = interactionMethodsClass.getMethod(name, parameterTypes);
			
			// call method and get return value
			triggerAction = (TriggerAction) method.invoke(new InteractionMethods(), argumentList);

		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return triggerAction;
	}

	
	private Vector3f localToWorld(Vector3f directionVectorLocal)
	{
		if(directionVectorLocal != null)
		{
			Vector3f worldPos = driveAnalyzer.getTarget().localToWorld(directionVectorLocal, null);
			Vector3f directionVectorWorld = worldPos.subtract(driveAnalyzer.getCurrentDataUnit().getCarPosition());
			directionVectorWorld.normalizeLocal();
			return directionVectorWorld;
		}
		
		return null;
	}
	
	
	private void drawRay(String ID, Vector3f targetPosition, Vector3f vehiclePosition, Material material, 
			boolean drawConnector)
	{
		if(debug)
		{
			Geometry marker = markerMap.get(ID);
			
			if(marker == null)
			{
				// create new sphere
				marker = new Geometry(ID, new Sphere(30, 30, 0.2f));
				
				driveAnalyzer.getSceneNode().attachChild(marker);
		        markerMap.put(ID, marker);
			}
			
			marker.setLocalTranslation(targetPosition);
			marker.setMaterial(material);
			
			Spatial connector = connectorMap.get(ID + "_connector");
			if(connector != null)
			{
				driveAnalyzer.getSceneNode().detachChild(connector);
				connectorMap.remove(ID + "_connector");
			}
			
			if(drawConnector)
				drawConnector(ID + "_connector", vehiclePosition, targetPosition, material);
		}
	}
	
	
	private void drawConnector(String ID, Vector3f startPos, Vector3f targetPos, Material material)
	{
        Spline spline = new Spline();
        
		// add points
        spline.addControlPoint(startPos);
        spline.addControlPoint(targetPos);
        
		spline.setType(SplineType.Linear);
		
		Node curveGeometry = new Node(ID);
		Geometry curve = new Geometry(ID + "_curve", new Curve(spline, 0));
		curveGeometry.attachChild(curve);
		
		curveGeometry.setMaterial(material);

		driveAnalyzer.getSceneNode().attachChild(curveGeometry);
		connectorMap.put(ID,curveGeometry);
	}

	
	private void deleteRay(String ID)
	{
		if(debug)
		{
			Spatial marker = markerMap.get(ID);
			if(marker != null)
			{
				driveAnalyzer.getSceneNode().detachChild(marker);
				markerMap.remove(ID);
			}
			
			Spatial connector = connectorMap.get(ID + "_connector");
			if(connector != null)
			{
				driveAnalyzer.getSceneNode().detachChild(connector);
				connectorMap.remove(ID + "_connector");
			}
		}
	}
	
	
	private CollisionResult getClosestCollision(CollisionResults results)
	{
		for(int i=0; i<results.size(); i++)
		{
			String geometryName = results.getCollision(i).getGeometry().getName();
		
			if(!geometryName.startsWith("cone_") && !geometryName.startsWith("ray") 
					&& !geometryName.startsWith("x-") && !geometryName.startsWith("y-") 
					&& !geometryName.startsWith("z-") && !geometryName.startsWith("center")
					&& !geometryName.startsWith("Sky") && !geometryName.startsWith("ODarea")
					&& !geometryName.equals("sideObstacleLeftSensor") && !geometryName.equals("sideObstacleRightSensor"))
			{
				return results.getCollision(i);
			}
		}

		return null;
	}
	


}