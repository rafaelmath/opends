/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.movie;

public class MovieData 
{
	private String id;
	private String path;
	private int noOfCycles;
	private boolean ignoreAspectRatio;
	private boolean isAudioMuted;
	private float playbackRate;
	private float volume;
	
	
	public MovieData(String id, String path, int noOfCycles, boolean ignoreAspectRatio, boolean isAudioMuted,
			float playbackRate, float volume)
	{
		this.id = id;
		this.path = path;
		
		if(noOfCycles <= 0)
			this.noOfCycles = Integer.MAX_VALUE;
		else
			this.noOfCycles = noOfCycles;
		
		this.ignoreAspectRatio = ignoreAspectRatio;
		this.isAudioMuted = isAudioMuted;
		this.playbackRate = Math.max(Math.min(playbackRate, 8.0f),0.0f);
		this.volume = Math.max(Math.min(volume, 1.0f),0.0f);
	}


	public String getID()
	{
		return id;
	}
	
	
	public String getPath() 
	{
		return path;
	}


	public int getNoOfCycles()
	{
		return noOfCycles;
	}


	public boolean isIgnoreAspectRatio()
	{
		return ignoreAspectRatio;
	}


	public boolean isAudioMuted()
	{
		return isAudioMuted;
	}


	public float getPlaybackRate()
	{
		return playbackRate;
	}


	public float getVolume()
	{
		return volume;
	}

}
