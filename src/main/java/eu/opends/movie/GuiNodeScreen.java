/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.movie;

import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture2D;
import com.jme3.util.BufferUtils;

import eu.opends.basics.SimulationBasics;


public class GuiNodeScreen implements NodeScreen
{
	private SimulationBasics sim;
	private String screenName;
	private Node rotationNode;
	private Node screen;
	private float aspectRatio;
	
	
	public GuiNodeScreen(SimulationBasics sim, String screenName, Integer quadWidth, Integer quadHeight)
	{
		this.sim = sim;
		this.screenName = screenName;
		
		int screenWidth = sim.getSettings().getWidth();
		int screenHeight = sim.getSettings().getHeight();
		
		if(quadWidth == null)
			quadWidth = screenWidth;
		
		if(quadHeight == null)
			quadHeight = screenHeight;
		
		aspectRatio = (float)quadWidth/quadHeight;
		
		Quad quad = new Quad(quadWidth, quadHeight);
		Vector2f[] texCoords = new Vector2f[]
			{
	            new Vector2f(0, 0),
	            new Vector2f(1, 0),
	            new Vector2f(1, 1),
	            new Vector2f(0, 1)
	        };
		quad.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoords));
		Geometry screenGeometry = new Geometry(screenName + "_Geometry", quad);
		
		// use a node with the screen center in its center --> rotation pivot in center
		rotationNode = new Node(screenName + "_RotationNode");
		rotationNode.attachChild(screenGeometry);
		screenGeometry.setLocalTranslation(-0.5f*quadWidth, -0.5f*quadHeight, 0);
		
		// use a second node to re-translate the geometry --> translation pivot in lower left corner
		screen = new Node(screenName);
		screen.attachChild(rotationNode);
		rotationNode.setLocalTranslation(0.5f*quadWidth, 0.5f*quadHeight, 0);		
		
		// set geometry "screen" to the center of the rendering window
		int offsetX = (screenWidth - quadWidth) / 2;
		int offsetY = (screenHeight - quadHeight) / 2;
		screen.setLocalTranslation(offsetX, offsetY, 0);
	}


	public void setPosition(Vector2f position)
	{
		// set position on 2D screen
		screen.setLocalTranslation(position.x, position.y, 0);	
	}
	
	
	public void setRotation(float degree)
	{
		// set rotation on screen
		// positive: clockwise; negative: counterclockwise
		float radians = FastMath.PI/180f * degree;
		Quaternion rotation = new Quaternion();
		rotation.fromAngles(0, 0, -radians);
		rotationNode.setLocalRotation(rotation);	
	}

	
	public String getName()
	{
		return screenName;
	}
	

	public float getAspectRatio()
	{
		return aspectRatio;
	}
	
	
	public void showTexture(Texture2D texture)
	{
		final Material movieShaderMaterial = new Material(sim.getAssetManager(), 
				"MatDefs/MoviePlayer/MovieShader.j3md");
		movieShaderMaterial.setTexture("ColorMap", texture);
		screen.setMaterial(movieShaderMaterial);
	
		sim.getGuiNode().attachChild(screen);
	}
	

	public void turnOff()
	{
		sim.getGuiNode().detachChild(screen);
	}	

}
