/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.movie;

import java.io.File;
import java.text.SimpleDateFormat;

import com.jme3.math.ColorRGBA;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;
import eu.opends.movie.TextureMovie.LetterboxMode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;


public class MoviePlayer 
{
	private SimulationBasics sim;
	private NodeScreen nodeScreen;
	private TextureMovie textureMovie;
	private MediaPlayer	mediaPlayer = null;
	private String previousMovieID = null;
	private boolean stopRequested = false;
	private MovieData movieData = null;
	
	
	public MoviePlayer(SimulationBasics sim, NodeScreen nodeScreen)
	{
		this.sim = sim;
		this.nodeScreen = nodeScreen;
	}

	
	public void requestStop()
	{
		// queue stop of current playback (execution in update() during next frame)
		stopRequested = true;
	}
	
	
	public void requestPlay(MovieData movieData)
	{
		// queue movie for playback (execution in update() during next frame)
		this.movieData = movieData;
	}
	
	
	public void update(float tpf)
	{
		// play movie in accordance with scene graph update to avoid invalid states
		if(movieData != null)
		{
			play(movieData);
			movieData = null;
		}
		
		// play player in accordance with scene graph update to avoid invalid states
		if(stopRequested)
		{
			stop();
			stopRequested = false;
		}
		
		// check whether simulation is paused --> pause/resume movie
		if(sim.isPause())
			pause();
		else
			resume();
	}
	

	public String getPreviousMovieID()
	{
		return previousMovieID;
	}
	
	
	private void play(MovieData movieData)
	{
		if(movieData != null)
		{
			// note previously played movie (needed if "playNextMovie" action is triggered)
			previousMovieID = movieData.getID();
			
			// stop currently running movie (if applicable)
			stop();
			
			// init media file
			final Media media = new Media(new File(movieData.getPath()).toURI().toASCIIString());
			media.errorProperty().addListener((observable, oldValue, newValue) -> newValue.printStackTrace());
			
			// init media player
			mediaPlayer = new MediaPlayer(media);
			
			// set number of repetitions
			mediaPlayer.setCycleCount(movieData.getNoOfCycles());
			
			// set whether audio will be muted
			mediaPlayer.setMute(movieData.isAudioMuted());
			
			// set playback rate [0.0, 8.0]
			mediaPlayer.setRate(movieData.getPlaybackRate());
			
			// set volume [0.0, 1.0]
			mediaPlayer.setVolume(movieData.getVolume());
			
			// stop player after last cycle has finished
			mediaPlayer.setOnEndOfMedia(new Runnable() {
	            @Override
	            public void run() {
	            	
	            	if(mediaPlayer.getCurrentCount() >= movieData.getNoOfCycles())
	            		requestStop();
	            }
	        });

			LetterboxMode letterboxMode = LetterboxMode.PRESERVE_ASPECT_RATIO;
			
			if(movieData.isIgnoreAspectRatio())
				letterboxMode = LetterboxMode.VALID_SQUARE; // ignore aspect ratio --> fill screen
				
			float screenAspectRatio = nodeScreen.getAspectRatio();
			
			// init texture movie object
			textureMovie = new TextureMovie(sim, mediaPlayer, letterboxMode, screenAspectRatio);
			textureMovie.setLetterboxColor(ColorRGBA.Black);
			
			// attach movie texture to screen object
			nodeScreen.showTexture(textureMovie.getTexture());

			// start playback of movie			
			mediaPlayer.play();

			//log
			/*
			String newLine = System.getProperty("line.separator");
			String time = new SimpleDateFormat("HH:mm:ss.SSS").format(System.currentTimeMillis());
			Simulator.getDrivingTaskLogger().reportText(time + " --> Start movie '" + movieData.getID() 
					+ "'" + newLine);
			*/
			long milliseconds = System.currentTimeMillis();
			String screenName = nodeScreen.getName();
			String movieName = movieData.getID();
			String status = screenName + " (" + movieName + ")";
			Simulator.getDrivingTaskLogger().reportEvent(milliseconds, "startMovie", status, null, null, null, null, 
					null, null, null, null);
		}
		else
			System.err.println("MoviePlayer::play(): Movie could not be found");
	}

	
	private void pause()
	{
		if(mediaPlayer != null && mediaPlayer.getStatus() == Status.PLAYING)
			mediaPlayer.pause();
	}


	private void resume()
	{
		if(mediaPlayer != null && mediaPlayer.getStatus() == Status.PAUSED)
			mediaPlayer.play();
	}
	
	
	private void stop()
	{
		if(mediaPlayer != null)
		{
			// free resources
			mediaPlayer.stop();
			mediaPlayer.dispose();
			
			// turn off screen (e.g. draw black still image)
			nodeScreen.turnOff();
			
			// log
			/*
			String newLine = System.getProperty("line.separator");
			String time = new SimpleDateFormat("HH:mm:ss.SSS").format(System.currentTimeMillis());
			Simulator.getDrivingTaskLogger().reportText(time + " --> Stop movie" + newLine);
			*/
			long milliseconds = System.currentTimeMillis();
			String screenName = nodeScreen.getName();
			Simulator.getDrivingTaskLogger().reportEvent(milliseconds, "stopMovie", screenName, null, null, null, null, 
					null, null, null, null);
		}
	}
}
