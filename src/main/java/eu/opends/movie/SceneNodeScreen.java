/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.movie;

import com.jme3.material.Material;
import com.jme3.material.Materials;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture2D;
import com.jme3.util.BufferUtils;

import eu.opends.basics.SimulationBasics;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.util.ODPositionWithOffsets;
import eu.opends.tools.Util;


public class SceneNodeScreen implements NodeScreen
{
	private SimulationBasics sim;
	private String screenName;
	private Texture2D standbyTexture;
	private Material movieShaderMat;
	private Material backSideMat;
	private Node screen;
	private float aspectRatio;

	
	public SceneNodeScreen(SimulationBasics sim, String screenName, Vector3f innerScreenSize, Float borderWidth, 
			Float borderHeight, ColorRGBA backsideColor, Texture2D texture)
	{
		this.sim = sim;
		this.screenName = screenName;
		
		if(borderWidth == null)
			borderWidth = 0.3f;
		
		if(borderHeight == null)
			borderHeight = 0.3f;
		
		if(backsideColor == null)
			backsideColor = ColorRGBA.Black;
			
		if(texture == null)
			standbyTexture = (Texture2D) sim.getAssetManager().loadTexture("Textures/Misc/darkgray.png");
		else
			standbyTexture = texture;
		
		movieShaderMat = new Material(sim.getAssetManager(), "MatDefs/MoviePlayer/MovieShader.j3md");
		movieShaderMat.setTexture("ColorMap", standbyTexture);
    	
        backSideMat = new Material(sim.getAssetManager(), Materials.LIGHTING);
        backSideMat.setBoolean("UseMaterialColors", true);
        backSideMat.setColor("Diffuse", backsideColor);

        aspectRatio = innerScreenSize.getX()/innerScreenSize.getY();
        
        borderWidth = Math.max(borderWidth, 0);
        borderHeight = Math.max(borderHeight, 0);
        
        float totalWidth = innerScreenSize.getX() + (2 * borderWidth);
        float totalHeight = innerScreenSize.getY() +  + (2 * borderHeight);
        float depth = innerScreenSize.getZ();
        Vector3f totalSize = new Vector3f(totalWidth, totalHeight, depth);
        
		screen = createImageWallNode(screenName, totalSize, borderWidth, borderHeight);
		sim.getSceneNode().attachChild(screen);
	}


	public void setLocalTranslation(Vector3f location)
	{
		screen.setLocalTranslation(location);	
	}
	
	
	public void setLocalRotation(Quaternion rotation)
	{
		screen.setLocalRotation(rotation);	
	}
	
	
	public String getName()
	{
		return screenName;
	}
	
	
	public float getAspectRatio()
	{
		return aspectRatio;
	}

	
	public void showTexture(Texture2D texture)
	{
		movieShaderMat.setTexture("ColorMap", texture);
	}
	

	public void turnOff()
	{
		// set back to standby image
		showTexture(standbyTexture);
	}
	
	
	public void setODPosition(ODPositionWithOffsets odPos) 
	{
		String roadID = odPos.getRoadID();
		float s = odPos.getS();
		float lateralOffset = odPos.getLateralOffset();
		float verticalOffset= odPos.getVerticalOffset();
		
		if(Util.isValidOffroadPosition(sim.getOpenDriveCenter(), screenName + "_screen_node_position", roadID, 
				lateralOffset, verticalOffset, s))
		{
			ODPoint point = sim.getOpenDriveCenter().getRoadMap().get(roadID).getPointOnReferenceLine(s, 
					screenName + "_screen_node_position");
			Vector3f referencePosition = point.getPosition().toVector3f();
			float ortho = (float)point.getOrtho();
		
			float x = referencePosition.getX() + lateralOffset*FastMath.sin(ortho);
			float z = referencePosition.getZ() + lateralOffset*FastMath.cos(ortho);
			float y = Util.getElevationAt(sim,x,z) + verticalOffset;
			screen.setLocalTranslation(new Vector3f(x, y, z));

			// overwrite original rotation with its relative rotation wrt road object
			Quaternion rotation = screen.getLocalRotation();	
			float[] angles = new float[3];
			rotation.toAngles(angles);
			screen.setLocalRotation((new Quaternion()).fromAngles(angles[0], ortho + angles[1] - FastMath.HALF_PI, angles[2]));	

		}
		else
			System.err.println("Could not set position of screen node '" + screenName	+ "' at: " + roadID +
					"/" + s);
	}
	

	private Node createImageWallNode(String nodeName, Vector3f size, float borderWidth, float borderHeight)
    {
    	Node wall = new Node(nodeName);

        // 1. Front
    	Node frontNode = createFrontNode(size, borderWidth, borderHeight);
    	frontNode.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
    	wall.attachChild(frontNode);    	
    	
        // 2. Back
        Quad backQuad = new Quad(size.x, size.y);

        Geometry back = new Geometry("back", backQuad);
        back.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        back.setLocalScale(new Vector3f(-1, 1, 1));
        back.setMaterial(backSideMat);
        wall.attachChild(back);

        // 3. Side (right, left)
        Quad sideQuad = new Quad(size.y, size.z);

        Geometry right = new Geometry("right", sideQuad);
        right.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        right.rotate(0, FastMath.HALF_PI, FastMath.HALF_PI);
        right.setMaterial(backSideMat);
        wall.attachChild(right);

        Geometry left = new Geometry("left", sideQuad);
        left.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
        left.rotate(0, -FastMath.HALF_PI, FastMath.HALF_PI);
        left.setMaterial(backSideMat);
        wall.attachChild(left);

        // 4. Side (top, bottom)
        sideQuad = new Quad(size.x, size.z);

        Geometry top = new Geometry("top", sideQuad);
        top.setLocalTranslation(-size.x / 2, size.y / 2, size.z / 2);
        top.rotate(-FastMath.HALF_PI, 0, 0);
        top.setMaterial(backSideMat);
        wall.attachChild(top);

        Geometry bottom = new Geometry("bottom", sideQuad);
        bottom.setLocalTranslation(-size.x / 2, -size.y / 2, -size.z / 2);
        bottom.rotate(FastMath.HALF_PI, 0, 0);
        bottom.setMaterial(backSideMat);
        wall.attachChild(bottom);

        return wall;
    }
	
	
	private Node createFrontNode(Vector3f size, float borderWidth, float borderHeight)
	{
		// Subdivide front into 3 rows having different numbers of columns:
		//
		// ----------------
		// |--------------|
		// | |          | |
		// | |          | |
		// |--------------|
		// ----------------
		
		// border positions
		float leftMin = 0;
		float leftMax = 0 + borderWidth/size.x;
		float rightMin = 1 - borderWidth/size.x;
		float rightMax = 1;
		
		float bottomMin = 0;
		float bottomMax = 0 + borderHeight/size.y;
		float topMin = 1 - borderHeight/size.y;
		float topMax = 1;
		
		Node frontNode = new Node("front");
		
		Geometry row1Geometry = createGeometry("subGeometry_1", size, leftMin, rightMax, topMin, topMax);
		row1Geometry.setMaterial(backSideMat);
		frontNode.attachChild(row1Geometry);
        
		Geometry row2col1Geometry = createGeometry("subGeometry_2_1", size, leftMin, leftMax, bottomMax, topMin);
		row2col1Geometry.setMaterial(backSideMat);
		frontNode.attachChild(row2col1Geometry);
		
		// center
		Geometry row2col2Geometry = createMovieGeometry("subGeometry_2_2", size, leftMax, rightMin, bottomMax, topMin);
		row2col2Geometry.setMaterial(movieShaderMat);
		frontNode.attachChild(row2col2Geometry);
		
		Geometry row2col3Geometry = createGeometry("subGeometry_2_3", size, rightMin, rightMax, bottomMax, topMin);
		row2col3Geometry.setMaterial(backSideMat);
		frontNode.attachChild(row2col3Geometry);
		
		Geometry row3Geometry = createGeometry("subGeometry_3", size, leftMin, rightMax, bottomMin, bottomMax);
		row3Geometry.setMaterial(backSideMat);
		frontNode.attachChild(row3Geometry);

        return frontNode;
	}

	
	private Geometry createGeometry(String name, Vector3f size, float left, float right, float bottom, float top)
	{
        Quad quad = new Quad((right-left) * size.x, (top-bottom) * size.y);
        Geometry geometry = new Geometry(name, quad);
        geometry.setLocalTranslation(left * size.x, bottom * size.y, 0.0f);
        
        return geometry;
	}
	
	private Geometry createMovieGeometry(String name, Vector3f size, float left, float right, float bottom, float top)
	{
        Quad quad = new Quad((right-left) * size.x, (top-bottom) * size.y);
        
        Vector2f[] texCoords = new Vector2f[]{
                new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
            };
            
        quad.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoords));
            
        Geometry geometry = new Geometry(name, quad);
        geometry.setLocalTranslation(left * size.x, bottom * size.y, 0.0f);
        
        return geometry;
	}
	
	
}
