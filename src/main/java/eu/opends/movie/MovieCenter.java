/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.movie;

import java.util.Map.Entry;
import java.util.TreeMap;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;


public class MovieCenter 
{
	private TreeMap<String, MoviePlayer> playerMap;
	private TreeMap<String, MovieData> movieMap;
	
	
	public MovieCenter(SimulationBasics sim)
	{
		movieMap = Simulator.getDrivingTask().getSceneLoader().getMoviesMap();
		playerMap = Simulator.getDrivingTask().getSceneLoader().getMoviePlayerMap(sim);
	}

    
	// update all movie players
	public void update(float tpf)
	{
		for(MoviePlayer player : playerMap.values())
			player.update(tpf);
	}

	
	// queue the given movie to be played by the given player
	public void play(String playerID, String movieID)
	{
		MoviePlayer player = playerMap.get(playerID);
		if(player != null)
		{
			MovieData movie = movieMap.get(movieID);
			if(movie != null)
			{
				player.requestPlay(movie);
			}
			else
				System.err.println("MovieCenter::play(): Movie '" + movieID + "' could not be found.");
		}
		else
			System.err.println("MovieCenter::play(): Player '" + playerID + "' could not be found.");
	}

	
	// not in use
	public void playNext(String playerID)
	{
		MoviePlayer player = playerMap.get(playerID);
		if(player != null)
		{
			String previousMovieID = player.getPreviousMovieID();
			if(previousMovieID != null)
			{
				Entry<String, MovieData> entry = movieMap.higherEntry(previousMovieID);
				if(entry != null)
				{
					// play next movie (having a strictly greater key)
					player.requestPlay(entry.getValue());
				}
				else
				{
					// if not available: play first movie of the map
					// be aware: map cannot be empty here since previousMovieID is not null
					player.requestPlay(movieMap.firstEntry().getValue());
				}
			}
			else
			{
				if(movieMap.size() > 0)
					player.requestPlay(movieMap.firstEntry().getValue());
				else
					System.err.println("MovieCenter::playNext(): No movie available");
			}
		}
		else
			System.err.println("MovieCenter::playNext(): Player '" + playerID + "' could not be found.");
	}
	
	
	// queue the given player to be stopped
	public void stop(String playerID)
	{
		MoviePlayer player = playerMap.get(playerID);
		if(player != null)
		{
			player.requestStop();
		}
		else
			System.err.println("MovieCenter::stop(): Player '" + playerID + "' could not be found.");
	}
	
	
	// queue all players to be stopped (will be called at shutdown)
	public void close() 
	{
		for(MoviePlayer player : playerMap.values())
			player.requestStop();
	}	

}
