/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.movie;

import java.nio.ByteBuffer;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;

import com.jme3.app.Application;
import com.jme3.texture.Image;
import com.jme3.texture.image.ColorSpace;
import com.jme3.util.BufferUtils;

public class ImageExchanger
{
    private final Semaphore imageExchange = new Semaphore(1);
    private ByteBuffer jmeData;
    private ByteBuffer fxData;
    private final Image jmeImage;
    private boolean fxDataReady;
    private final Application app;

    
    /**
     *
     * @param width
     * @param height
     * @param format
     * @param app
     */
    public ImageExchanger(int width, int height, Image.Format format, Application app)
    {
        this.app = app;
        this.jmeData = BufferUtils.createByteBuffer(width * height * format.getBitsPerPixel() / 8);
        this.fxData = BufferUtils.createByteBuffer(width * height * format.getBitsPerPixel() / 8);
        this.jmeImage = new Image(format, width, height, this.jmeData, ColorSpace.sRGB);
    }

    
    public void startUpdate() throws InterruptedException
    {
        imageExchange.acquire();
    }

    
    public void flushUpdate()
    {
        try {
            fxDataReady = true;
            this.app.enqueue(new Callable<Void>()
            {
                @Override
                public Void call() throws Exception
                {
                    final boolean updateImage = imageExchange.tryAcquire();
                    /**
                     * we update only if we can do that in nonblocking mode if
                     * would need to block, it means that another callable with
                     * newer data will be enqueued soon, so we can just ignore
                     * this repaint
                     */
                    if (updateImage)
                    {
                        try {
                            if (fxDataReady)
                            {
                                fxDataReady = false;
                                final ByteBuffer tmp = jmeData;
                                jmeData = fxData;
                                fxData = tmp;
                            }
                        } finally {
                            imageExchange.release();
                        }
                        jmeImage.setData(jmeData);
                    }
                    return null;
                }
            });

        } finally {
            imageExchange.release();
        }
    }

    
    public void dispose()
    {
        BufferUtils.destroyDirectBuffer(fxData);
        BufferUtils.destroyDirectBuffer(jmeData);
        jmeImage.dispose();
    }

    
    public Image getImage()
    {
        return jmeImage;
    }

    
    public ByteBuffer getFxData()
    {
        return fxData;
    }

    
    public ByteBuffer getJmeData()
    {
        return jmeData;
    }

}
