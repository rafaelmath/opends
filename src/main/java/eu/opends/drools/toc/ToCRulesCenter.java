/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.drools.toc;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import eu.opends.main.Simulator;


public class ToCRulesCenter
{
	private KieSession ksession = null;
	
	private DriverParams driverParams;
	private VehicleParams vehicleParams;
	private EnvironmentParams environmentParams;
	private Actions actions;

	private final FactHandle driverParamsHandle;
	private final FactHandle vehicleParamsHandle;
	private final FactHandle environmentParamsHandle;
	private final FactHandle actionsHandle;
	
	public ToCRulesCenter(Simulator sim)
	{
		driverParams = new DriverParams();
		vehicleParams = new VehicleParams();
		environmentParams = new EnvironmentParams();
		actions = new Actions(sim);
		
		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
		ksession = kc.newKieSession("TransferOfControlDecisionTable");
		
		driverParamsHandle = ksession.insert(driverParams);
		vehicleParamsHandle = ksession.insert(vehicleParams);
		environmentParamsHandle = ksession.insert(environmentParams);
		actionsHandle = ksession.insert(actions);
	}
	
	
	public DriverParams getDriverParams()
	{
		return driverParams;
	}
	
	
	public VehicleParams getVehicleParams()
	{
		return vehicleParams;
	}
	
	
	public EnvironmentParams getEnvironmentParams()
	{
		return environmentParams;
	}
	
	
	public void update(float tpf)
	{
		ksession.update(driverParamsHandle, driverParams);
		ksession.update(vehicleParamsHandle, vehicleParams);
		ksession.update(environmentParamsHandle, environmentParams);
		ksession.update(actionsHandle, actions);
		ksession.fireAllRules();
	}
	
	
	public void close()
	{
		ksession.dispose();
	}

}
