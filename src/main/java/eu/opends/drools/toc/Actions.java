/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.drools.toc;

import java.util.TreeMap;
import java.util.HashMap;
import java.util.Map.Entry;

import com.jme3.audio.AudioNode;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.ui.Picture;

import eu.opends.analyzer.DataWriter;
import eu.opends.audio.AudioCenter;
import eu.opends.effects.EffectCenter;
import eu.opends.main.Simulator;
import eu.opends.opendrive.processed.ODLane.Position;
import eu.opends.tools.PanelCenter;
import eu.opends.traffic.OpenDRIVECar;
import eu.opends.traffic.TrafficObject;


public class Actions
{
	private Simulator sim;
	private HashMap<String, Long> activationMap = new HashMap<String, Long>();
	
	
	public Actions(Simulator sim)
	{
		this.sim = sim;
	}

	
	public void reportActivation(String ruleName)
	{
		//System.err.println("report " + ruleName);
		long currentTime = System.currentTimeMillis();
		activationMap.put(ruleName, currentTime);
	}
	
	
	public void unreportActivation(String ruleName)
	{
		activationMap.remove(ruleName);
	}
	
	
	public long msSinceLastActivation(String ruleName)
	{
		Long activationTime = activationMap.get(ruleName);
		
		if(activationTime == null)
			return Long.MAX_VALUE;
		else
			return System.currentTimeMillis() - activationTime;
	}	
	
	
	public boolean hasBeenActivated(String ruleName)
	{
		return activationMap.containsKey(ruleName);
	}
	
	
	public boolean hasNotBeenActivated(String ruleName)
	{
		return !activationMap.containsKey(ruleName);
	}
	

	public void writeToCommandLine(String message)
	{
		System.out.println(message);
	}
	
	
	public void writeToLogFile(String log)
	{
		Simulator.getDrivingTaskLogger().reportText(log);
	}
	
	
	public void sendMessage(String message, int duration)
	{
		PanelCenter.getMessageBox().addMessage(message,duration);
	}
	
	
	public void changeLaneUserVehicle(String lanePosString)
	{
		Position lanePos = getLanePosFromString(lanePosString);
		sim.getCar().changeLane(lanePos);
	}
	
	
	public void setTargetLaneUserVehicle(int laneID)
	{
		sim.getCar().setTargetLane(laneID);
	}
	
	
	public void changeLaneTrafficVehicle(String trafficObjectName, String lanePosString)
	{
		TrafficObject trafficObject = sim.getPhysicalTraffic().getTrafficObject(trafficObjectName);
		
		if(trafficObject != null && trafficObject instanceof OpenDRIVECar)
		{
			Position lanePos = getLanePosFromString(lanePosString);
			((OpenDRIVECar)trafficObject).changeLane(lanePos);
		}
		else
			System.err.println(getClass().getCanonicalName() + ".changeLaneTrafficVehicle(): '" 
					+ trafficObjectName + "' is not a valid OpenDRIVECar");
	}
	
	
	public void setTargetLaneTrafficVehicle(String trafficObjectName, int laneID)
	{
		TrafficObject trafficObject = sim.getPhysicalTraffic().getTrafficObject(trafficObjectName);
		
		if(trafficObject != null && trafficObject instanceof OpenDRIVECar)
			((OpenDRIVECar)trafficObject).setTargetLane(laneID);
		else
			System.err.println("'" + trafficObjectName + "' is not a valid OpenDRIVECar");
	}	
	
	
	private Position getLanePosFromString(String lanePosString)
	{
		for (Position pos : Position.values())
		{
            if (pos.toString().equalsIgnoreCase(lanePosString.trim()))
                return pos;
		}
		
		System.err.println(getClass().getCanonicalName() + ".getLanePosFromString(): '" 
				+ lanePosString + "' is not a valid lane change position (Left, Right). Using 'Left'");

		return Position.Left;
	}
	
	
	public void manipulatePicture(String pictureID, boolean isVisible)
	{
		CullHint visibility;
		if(isVisible)
			visibility = CullHint.Dynamic;
		else
			visibility = CullHint.Always;
		
		
		// set all pictures to ...
		if(pictureID.equalsIgnoreCase("all"))
		{
			TreeMap<String, Picture> pictureMap = PanelCenter.getPictureMap();
	        for(Entry<String,Picture> entry : pictureMap.entrySet())
	        	entry.getValue().setCullHint(visibility);
		}
		
		// set only given picture to ...
		Spatial spatial = sim.getGuiNode().getChild(pictureID);
		if(spatial instanceof Picture)
		{
			Picture picture = (Picture) spatial;
				picture.setCullHint(visibility);
		}
	}
	
	
	public void playSynthesizedText(String text)
	{
		AudioCenter.playSynthesizedText(text);
	}
	
	
	
	public void playSound(String soundID)
	{
		AudioNode audioNode = AudioCenter.getAudioNode(soundID);
		
		if(audioNode != null)
			AudioCenter.playSound(audioNode);
		else
			System.err.println(getClass().getCanonicalName() + ".playSound(): audio node '" 
						+ soundID + "' does not exist");
	}
	
	
	public void pauseSound(String soundID)
	{
		AudioNode audioNode = AudioCenter.getAudioNode(soundID);
		
		if(audioNode != null)
			AudioCenter.pauseSound(audioNode);
		else
			System.err.println(getClass().getCanonicalName() + ".pauseSound(): audio node '" 
					+ soundID + "' does not exist");
			
	}
	
	
	public void pauseSimulation(int duration)
	{
		sim.setPause(true);
		
		if(duration > 0)
		{
			Thread stopPauseThread = new Thread() {
		        public void run() {
		        	try {Thread.sleep(duration);} 
		        	catch (InterruptedException e){}

		    		sim.setPause(false);
		        }
		    };
		    stopPauseThread.start();
		}
	}
	
	
	public void setAutoPilot(boolean isEnabled)
	{
		sim.getCar().setODAutoPilot(isEnabled);
	}
	
	
	public void setSnowingPercentage(float snowingPercentage)
	{
		EffectCenter.setSnowingPercentage(snowingPercentage);
	}
	
	
	public void setRainingPercentage(float rainingPercentage)
	{
		EffectCenter.setRainingPercentage(rainingPercentage);
	}
	
	
	public void setFogPercentage(float fogPercentage)
	{
		EffectCenter.setFogPercentage(fogPercentage);
	}
	
	
	public void shutDownSimulator()
	{
		sim.stop();
	}
	
	
	public void startRecording(int trackNumber)
	{
		// start recording drive
		DataWriter dataWriter = sim.getDataWriter();

		if (!dataWriter.isRecording()) 
		{
			System.out.println("Start storing Drive-Data");
			dataWriter.startRecording(trackNumber);
			PanelCenter.getStoreText().setText("Recording");
		}
	}
	
	
	public void stopRecording()
	{
		DataWriter dataWriter = sim.getDataWriter();
		
		// stop recording drive	
		if (dataWriter.isRecording()) 
		{
			System.out.println("Stop storing Drive-Data");
			String timestamp = dataWriter.stopRecording();
			PanelCenter.getStoreText().setText(" ");
				
			// print time elapsed since start of recording to screen 
			PanelCenter.getMessageBox().addMessage(timestamp, 3);
		}
	}
	
}
