/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.drools.toc;

import com.jme3.math.Vector3f;

public class DriverParams
{	
	// set in eu.opends.mq3t.callbacks.*
	// ---------------------------------
	
	public int heartRate = 0;
	public float heartRateThreshold = -1;
	public float timeThresholdExceeded = 0;
	public boolean isEnabledHRbasedToC = false;
	public Vector3f wristAcceleration = new Vector3f(0, 0, 0);
	public double bloodVolumePulse = 0;
	public double galvanicSkinResponse = 0;
	public double interBeatInterval = 0;
	public boolean isDeviceOnWrist = false;
	public double wristTemperature = 0;
	public String activity = "driving";
}
