/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.drools.toc;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import eu.opends.car.LightTexturesContainer.TurnSignalState;

public class VehicleParams
{
	// set in eu.opends.car.SteeringCar
	// --------------------------------
	
	public float speedKmh = 0;
	public float rpm = 0;
	public float steeringWheelPos = 0;
	public float acceleratorPedalPos = 0;
	public float brakePedalPos = 0;
	public float clutchPedalPos = 0;
	public boolean isHandBrakeEngaged = false;
	public boolean isEngineOn = true;
	public TurnSignalState turnSignalState = TurnSignalState.OFF;
	public boolean isLightOn = false;
	public float lightIntensity = 0;
	public Boolean isAutoPilotOn = false;
	public boolean isBrakeLightOn = false;
	public boolean isCruiseControlOn = false;
	public float cruiseControlTargetSpeedKmh = 0;
	public boolean isAutomatic = true;
	public int selectedGear = 1;
	public float milage = 0;
	public float headingDegree = 0;
	public Vector3f position = new Vector3f(0, 0, 0);
	public Quaternion rotation = new Quaternion();
	public float slopeDegree = 0;
	
	
	
	// set in eu.opends.codriver.ScenarioMessage
	// -----------------------------------------
	
	public float accelerationLgt = 0;
	public float accelerationLat = 0;

}
