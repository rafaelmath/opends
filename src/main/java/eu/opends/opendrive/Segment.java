package eu.opends.opendrive;


public class Segment 
{
	public float length;
	public float curvature;
	
	
	public Segment(float length, float curvature)
	{
		this.length = length;
		this.curvature = curvature;
	}

	
}
