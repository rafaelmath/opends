//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.08 at 08:54:08 PM CEST 
//


package eu.opends.opendrive.data;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for e_bridgeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="e_bridgeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="concrete"/>
 *     &lt;enumeration value="steel"/>
 *     &lt;enumeration value="brick"/>
 *     &lt;enumeration value="wood"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "e_bridgeType")
@XmlEnum
public enum EBridgeType {

    @XmlEnumValue("concrete")
    CONCRETE("concrete"),
    @XmlEnumValue("steel")
    STEEL("steel"),
    @XmlEnumValue("brick")
    BRICK("brick"),
    @XmlEnumValue("wood")
    WOOD("wood");
    private final String value;

    EBridgeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EBridgeType fromValue(String v) {
        for (EBridgeType c: EBridgeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
