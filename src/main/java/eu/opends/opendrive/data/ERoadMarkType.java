//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.08 at 08:54:08 PM CEST 
//


package eu.opends.opendrive.data;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for e_roadMarkType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="e_roadMarkType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="none"/>
 *     &lt;enumeration value="solid"/>
 *     &lt;enumeration value="broken"/>
 *     &lt;enumeration value="solid solid"/>
 *     &lt;enumeration value="solid broken"/>
 *     &lt;enumeration value="broken solid"/>
 *     &lt;enumeration value="broken broken"/>
 *     &lt;enumeration value="botts dots"/>
 *     &lt;enumeration value="grass"/>
 *     &lt;enumeration value="curb"/>
 *     &lt;enumeration value="custom"/>
 *     &lt;enumeration value="edge"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "e_roadMarkType")
@XmlEnum
public enum ERoadMarkType {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("solid")
    SOLID("solid"),
    @XmlEnumValue("broken")
    BROKEN("broken"),
    @XmlEnumValue("solid solid")
    SOLID_SOLID("solid solid"),
    @XmlEnumValue("solid broken")
    SOLID_BROKEN("solid broken"),
    @XmlEnumValue("broken solid")
    BROKEN_SOLID("broken solid"),
    @XmlEnumValue("broken broken")
    BROKEN_BROKEN("broken broken"),
    @XmlEnumValue("botts dots")
    BOTTS_DOTS("botts dots"),
    @XmlEnumValue("grass")
    GRASS("grass"),
    @XmlEnumValue("curb")
    CURB("curb"),
    @XmlEnumValue("custom")
    CUSTOM("custom"),
    @XmlEnumValue("edge")
    EDGE("edge");
    private final String value;

    ERoadMarkType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ERoadMarkType fromValue(String v) {
        for (ERoadMarkType c: ERoadMarkType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
