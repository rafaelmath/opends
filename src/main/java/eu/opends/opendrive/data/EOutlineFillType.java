//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.08 at 08:54:08 PM CEST 
//


package eu.opends.opendrive.data;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for e_outlineFillType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="e_outlineFillType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="grass"/>
 *     &lt;enumeration value="concrete"/>
 *     &lt;enumeration value="cobble"/>
 *     &lt;enumeration value="asphalt"/>
 *     &lt;enumeration value="pavement"/>
 *     &lt;enumeration value="gravel"/>
 *     &lt;enumeration value="soil"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "e_outlineFillType")
@XmlEnum
public enum EOutlineFillType {

    @XmlEnumValue("grass")
    GRASS("grass"),
    @XmlEnumValue("concrete")
    CONCRETE("concrete"),
    @XmlEnumValue("cobble")
    COBBLE("cobble"),
    @XmlEnumValue("asphalt")
    ASPHALT("asphalt"),
    @XmlEnumValue("pavement")
    PAVEMENT("pavement"),
    @XmlEnumValue("gravel")
    GRAVEL("gravel"),
    @XmlEnumValue("soil")
    SOIL("soil");
    private final String value;

    EOutlineFillType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EOutlineFillType fromValue(String v) {
        for (EOutlineFillType c: EOutlineFillType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
