//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.22 at 03:20:44 PM CET 
//


package eu.opends.opendrive.roadGenerator;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for vehicleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vehicleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="maxSpeed" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="startPosition" type="{http://opends.eu/roadDescription}onroadPositionType"/>
 *         &lt;element name="preferredConnections" type="{http://opends.eu/roadDescription}preferredConnectionsType" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vehicleType", propOrder = {

})
public class VehicleType {

    protected Float maxSpeed;
    @XmlElement(required = true)
    protected OnroadPositionType startPosition;
    protected PreferredConnectionsType preferredConnections;
    @XmlAttribute(name = "id")
    protected String id;

    /**
     * Gets the value of the maxSpeed property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Sets the value of the maxSpeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMaxSpeed(Float value) {
        this.maxSpeed = value;
    }

    /**
     * Gets the value of the startPosition property.
     * 
     * @return
     *     possible object is
     *     {@link OnroadPositionType }
     *     
     */
    public OnroadPositionType getStartPosition() {
        return startPosition;
    }

    /**
     * Sets the value of the startPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnroadPositionType }
     *     
     */
    public void setStartPosition(OnroadPositionType value) {
        this.startPosition = value;
    }

    /**
     * Gets the value of the preferredConnections property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredConnectionsType }
     *     
     */
    public PreferredConnectionsType getPreferredConnections() {
        return preferredConnections;
    }

    /**
     * Sets the value of the preferredConnections property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredConnectionsType }
     *     
     */
    public void setPreferredConnections(PreferredConnectionsType value) {
        this.preferredConnections = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
