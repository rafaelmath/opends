/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.opendrive.util;

public class ODPositionWithOffsets
{
	private String roadID;
	private float s;
	private float lateralOffset;
	private float verticalOffset;

	
	public ODPositionWithOffsets(String roadID, float s, float lateralOffset, float verticalOffset)
	{
		this.roadID = roadID;
		this.s = s;
		this.lateralOffset = lateralOffset;
		this.verticalOffset = verticalOffset;
	}

	
	public String getRoadID()
	{
		return roadID;
	}


	public float getS() 
	{
		return s;
	}
	
	
	public float getLateralOffset()
	{
		return lateralOffset;
	}
	
	
	public float getVerticalOffset()
	{
		return verticalOffset;
	}
	
	
	@Override
	public String toString()
	{
		return roadID + "/" + s + "/lat:" + lateralOffset + "/vert:" + verticalOffset;
	}
}
