/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import eu.opends.main.Simulator;

public class MqttReceiverCallback implements MqttCallback
{
	private Simulator sim;
	
	
	public MqttReceiverCallback(Simulator sim)
	{
		this.sim = sim;
	}
	
	
	public void connectionLost(Throwable cause)
	{
		System.out.println("connection lost: " + cause.getMessage());
	}

	
	public void messageArrived(String topic, MqttMessage message)
	{
		String text = new String(message.getPayload());
		//System.out.println("received: " + text + " (" + topic + ")");
		
		
		if(topic.equalsIgnoreCase(MqttConnection.TOPIC_gazesge))
		{
			float gazeSGE = Float.parseFloat(text);
			sim.getOnScreenVisualizer().setGazeSGE(gazeSGE);
		} 
		else if (topic.equalsIgnoreCase(MqttConnection.TOPIC_mwleeg_display))
		{
			float mwlEEG = Float.parseFloat(text);
			sim.getOnScreenVisualizer().setMwlEEG(mwlEEG);
		} 
		else if (topic.equalsIgnoreCase(MqttConnection.TOPIC_class2sec))
		{
			float class2Sec = Float.parseFloat(text);
			sim.getOnScreenVisualizer().setClass2Sec(class2Sec);
		} 
		else if (topic.equalsIgnoreCase(MqttConnection.TOPIC_class4sec))
		{
			float class4Sec = Float.parseFloat(text);
			sim.getOnScreenVisualizer().setClass4Sec(class4Sec);
		} 
		else if (topic.equalsIgnoreCase(MqttConnection.TOPIC_class8sec))
		{
			float class8Sec = Float.parseFloat(text);
			sim.getOnScreenVisualizer().setClass8Sec(class8Sec);
		} 
	}
	

	public void deliveryComplete(IMqttDeliveryToken token)
	{
		//System.out.println("deliveryComplete---------" + token.isComplete());
	}
}