/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.jme3.math.FastMath;

import eu.opends.main.Simulator;

public class MqttConnection
{
	public static String TOPIC_gazesge = "camelot/data/gazesge";
	public static String TOPIC_mwleeg = "camelot/data/mwleeg";
	public static String TOPIC_mwleeg_display = "camelot/data/mwleeg_display";
	public static String TOPIC_class2sec = "camelot/data/class2sec";
	public static String TOPIC_class4sec = "camelot/data/class4sec";
	public static String TOPIC_class8sec = "camelot/data/class8sec";
	public static String TOPIC_gazex = "camelot/data/gazex";
	public static String TOPIC_gazey = "camelot/data/gazey";
	
	private Simulator sim;
	private MqttClient client;
	private String clientid = "opends";
	private int qos = 0;


	public MqttConnection(Simulator sim, String url)
	{
		this.sim = sim;
		
		try {
			// set up client
			client = new MqttClient(url, clientid, new MemoryPersistence());
           
			// connect options
			MqttConnectOptions options = new MqttConnectOptions();
			options.setConnectionTimeout(10);
			options.setKeepAliveInterval(10);
           
			// setup callback
			MqttReceiverCallback mqttCallback = new MqttReceiverCallback(sim);
			client.setCallback(mqttCallback);
           
			// start connection
			client.connect(options);
           
			// subscribe to viz parameters
			client.subscribe(TOPIC_gazesge, qos);
			client.subscribe(TOPIC_mwleeg_display, qos);
			client.subscribe(TOPIC_class2sec, qos);
			client.subscribe(TOPIC_class4sec, qos);
			client.subscribe(TOPIC_class8sec, qos);
           
			// TODO remove
			//client.subscribe(TOPIC_gazex, qos);
			//client.subscribe(TOPIC_gazey, qos);
           
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void publishGazeCoordinates(Float x, Float y)
	{
		if(client != null && x != null && y != null)
		{
			// create messages and setup QoS
			MqttMessage messageX = new MqttMessage(x.toString().getBytes());
			messageX.setQos(qos);
    
			MqttMessage messageY = new MqttMessage(y.toString().getBytes());
			messageY.setQos(qos);
			
			try
			{
				// publish message
				client.publish(TOPIC_gazex, messageX);
				client.publish(TOPIC_gazey, messageY);
				
			} catch (MqttPersistenceException e)
			{
				e.printStackTrace();
			} catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	float counter = 0;
	public void publishDummyData()
	{
		counter += 0.001f;
		
		if(client != null)
		{
			// create messages and setup QoS
			Float gazeSGE = sim.getCar().getCurrentSpeedKmhRounded()/50f;
			MqttMessage messageGazeSGE = new MqttMessage(gazeSGE.toString().getBytes());
			messageGazeSGE.setQos(qos);

			Float mwleeg = 0.6f - sim.getCar().getCurrentSpeedKmhRounded()/250f;
			MqttMessage messageMwleeg = new MqttMessage(mwleeg.toString().getBytes());
			messageMwleeg.setQos(qos);

			
			Float class2Sec = 0.5f + 0.4f * FastMath.sin(counter); //0.2f + sim.getCar().getCurrentSpeedKmhRounded()/50f;
			MqttMessage messageClass2Sec = new MqttMessage(class2Sec.toString().getBytes());
			messageClass2Sec.setQos(qos);
			
			Float class4Sec = 0.5f + 0.4f * FastMath.sin(counter+2f); //0.4f + sim.getCar().getCurrentSpeedKmhRounded()/50f;
			MqttMessage messageClass4Sec = new MqttMessage(class4Sec.toString().getBytes());
			messageClass4Sec.setQos(qos);
			
			Float class8Sec = 0.5f + 0.4f * FastMath.sin(counter+4f); //0.6f + sim.getCar().getCurrentSpeedKmhRounded()/50f;
			MqttMessage messageClass8Sec = new MqttMessage(class8Sec.toString().getBytes());
			messageClass8Sec.setQos(qos);

			try
			{
				// publish message
				client.publish(TOPIC_gazesge, messageGazeSGE);
				client.publish(TOPIC_mwleeg, messageMwleeg);
				client.publish(TOPIC_mwleeg_display, messageMwleeg);
				client.publish(TOPIC_class2sec, messageClass2Sec);
				client.publish(TOPIC_class4sec, messageClass4Sec);
				client.publish(TOPIC_class8sec, messageClass8Sec);
				
			} catch (MqttPersistenceException e)
			{
				e.printStackTrace();
			} catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	}
	   
	public void close()
	{
		if(client != null)
		{
			try
			{
				client.disconnect();
				client.close();
			
			} catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	}

}
