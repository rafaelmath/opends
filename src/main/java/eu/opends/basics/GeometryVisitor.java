/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.basics;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.jme3.material.MatParamTexture;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapAxis;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.util.BufferUtils;

import eu.opends.opendrive.data.ELaneType;
import eu.opends.opendrive.data.ERoadMarkType;
import eu.opends.opendrive.data.TRoadLanesLaneSectionLcrLaneRoadMark;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.processed.ODLane.LaneSide;
import eu.opends.tools.Vector3d;


public class GeometryVisitor implements SceneGraphVisitor
{
	private SimulationBasics sim;
	private Node mapObjectNode = null;
	
	
	// textures containing any pixel with alpha value below the given threshold (where
	// 0 = transparent and 255 = opaque) will be treated as (semi-)transparent images.
	// Others will be treated as opaque images.
	private int transparencyThreshold = 20;
	
	
	public GeometryVisitor(SimulationBasics sim, MapObject mapObject)
	{
		this.sim = sim;
		
		if(mapObject != null)
		{
			Spatial spatial = mapObject.getSpatial();
			if(spatial instanceof Node)
				mapObjectNode = (Node) spatial;
		}
	}
	
	
	@Override
	public void visit(Spatial spatial)
	{
		if (spatial instanceof Geometry)
		{
			Geometry geometry = (Geometry) spatial;
			MatParamTexture diffuseMap = geometry.getMaterial().getTextureParam("DiffuseMap");
			if (diffuseMap != null)
			{
				Texture texture = diffuseMap.getTextureValue();
				String texturePath = texture.getKey().getName();
				
				if (hasTransparentPixel(texture))
				{
					// Activate blend mode "alpha" and discard (semi-)transparent pixels 
					// below 20% threshold (= pixels with alpha value below 51).
					// Furthermore, add object to the "Transparent" render queue bucket
					geometry.getMaterial().setFloat("AlphaDiscardThreshold", 0.2f);
					geometry.getMaterial().getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
					geometry.setQueueBucket(Bucket.Transparent);
					//System.err.println("Activated: " + geometry.getName());
				}
				
				
				// Move the back side quad of each road sign (detected by filename) 3cm along the 
				// normals to prevent overwriting of the front side texture (z-fighting).
				// By default, back and front side are exactly at the same position.
				// match ...sign-{...}back.png  -->  e.g. Scenes/testSchild/sign-{368-d8}back.png
				// match ...Sign_...-Back.png   -->  e.g. Scenes/testSchild/Sign_Yield-Back.png
				if(//texturePath.matches("^\\S+sign\\-\\{\\S+\\}back.png$")
					//|| texturePath.matches("^\\S+\\-Back.png$"))
					/*||*/ texturePath.matches("^\\S+Sign_\\S+\\-Back.png$")
					/*|| texturePath.matches("^A_Roundabout\\-Back.png$") */ )
				{

					// Usually each back side quad has 4 corner points,
					// which will result in 12 float values per sign (4 points * 3 coordinates)
					// Be aware: there can be more than one set of corner points in the buffer
					// if there is more than one instance of the geometry
					VertexBuffer positionVertexBuffer = geometry.getMesh().getBuffer(Type.Position);
					FloatBuffer positionBuffer = (FloatBuffer) positionVertexBuffer.getData();
					
					// for each corner point there will be a normal vector
					// this will result in 12 float values per sign (4 points * 3 coordinates)
					VertexBuffer normalVertexBuffer = geometry.getMesh().getBuffer(Type.Normal);
					FloatBuffer normalBuffer = (FloatBuffer) normalVertexBuffer.getDataReadOnly();

					for(int i = 0; i<normalBuffer.limit(); i+=3)
					{
						// shift each corner point by 3cm along the corresponding normal
						positionBuffer.put(i, positionBuffer.get(i) + 0.03f * normalBuffer.get(i));
						positionBuffer.put(i+1, positionBuffer.get(i+1) + 0.03f * normalBuffer.get(i+1));
						positionBuffer.put(i+2, positionBuffer.get(i+2) + 0.03f * normalBuffer.get(i+2));
					}
					
					
					//geometry.setCullHint(CullHint.Always);
					//clear(texture);
					//geometry.getMaterial().getAdditionalRenderState().setFaceCullMode(FaceCullMode.Back);
					//System.err.println("Make texture invisible: " + texturePath);
				}
				
				
				// Move the front side quad of each road sign (detected by filename) 3 cm along the 
				// normals to prevent overwriting of the back side texture (z-fighting).
				// By default, back and front side are exactly at the same position.
				// match ...sign-{...}front.png  -->  e.g. Scenes/testSchild/sign-{368-d8}front.png
				if(texturePath.matches("^\\S+sign\\-\\{\\S+\\}front.png$"))
				{
					// Usually each front side quad has 4 corner points,
					// which will result in 12 float values per sign (4 points * 3 coordinates)
					// Be aware: there can be more than one set of corner points in the buffer
					// if there is more than one instance of the geometry
					VertexBuffer positionVertexBuffer = geometry.getMesh().getBuffer(Type.Position);
					FloatBuffer positionBuffer = (FloatBuffer) positionVertexBuffer.getData();
					
					// for each corner point there will be a normal vector
					// this will result in 12 float values per sign (4 points * 3 coordinates)
					VertexBuffer normalVertexBuffer = geometry.getMesh().getBuffer(Type.Normal);
					FloatBuffer normalBuffer = (FloatBuffer) normalVertexBuffer.getDataReadOnly();

					for(int i = 0; i<normalBuffer.limit(); i+=3)
					{
						// shift each corner point by 3cm along the corresponding normal
						positionBuffer.put(i, positionBuffer.get(i) + 0.05f * normalBuffer.get(i));
						positionBuffer.put(i+1, positionBuffer.get(i+1) + 0.05f * normalBuffer.get(i+1));
						positionBuffer.put(i+2, positionBuffer.get(i+2) + 0.05f * normalBuffer.get(i+2));
					}
				}

				
				// Put an invisible quad in front of each road sign (detected by filename) shifted 
				// 3 cm along the normals to prevent z-fighting with the front texture.
				// This can be used to catch the gaze ray.
				// Works only with valid mapObject, as the resulting quad will be attached to the 
				// mapObject in order to preserve scale, translation, and rotation.
				// match ...sign-{...}front.png  -->  e.g. Scenes/test/sign-{368-d8}front.png
				// match ...Sign_....png         -->  e.g. Scenes/test/Sign_NoEntry.png
				// match ...Sign_...-Back.png    -->  e.g. Scenes/test/Sign_NoEntry-Back.png
				if(mapObjectNode != null && (
						texturePath.matches("^\\S+sign\\-\\{\\S+\\}front.png$") ||
						(
							texturePath.matches("^\\S+Sign\\_\\S+.png$") && 
							!texturePath.matches("^\\S+Sign\\_\\S+-Back.png$")
						)
					))
				{
					// Usually each front side quad has 4 corner points,
					// which will result in 12 float values per sign (4 points * 3 coordinates)
					// Be aware: there can be more than one set of corner points in the buffer
					// if there is more than one instance of the geometry
					VertexBuffer positionVertexBuffer = geometry.getMesh().getBuffer(Type.Position);
					FloatBuffer positionBuffer = (FloatBuffer) positionVertexBuffer.getData();
					
					// for each corner point there will be a normal vector
					// this will result in 12 float values per sign (4 points * 3 coordinates)
					VertexBuffer normalVertexBuffer = geometry.getMesh().getBuffer(Type.Normal);
					FloatBuffer normalBuffer = (FloatBuffer) normalVertexBuffer.getDataReadOnly();

					ArrayList<Vector3f> cornerPointPositionList = new ArrayList<Vector3f>();
					for(int i = 0; i<normalBuffer.limit(); i+=3)
					{
						Vector3f cornerPointPosition = new Vector3f();
						cornerPointPosition.setX(positionBuffer.get(i) + 0.05f * normalBuffer.get(i));
						cornerPointPosition.setY(positionBuffer.get(i+1) + 0.05f * normalBuffer.get(i+1));
						cornerPointPosition.setZ(positionBuffer.get(i+2) + 0.05f * normalBuffer.get(i+2));
						cornerPointPositionList.add(cornerPointPosition);
						
						// if 4 corner points collected --> build quad and reset list
						if(cornerPointPositionList.size() == 4)
						{
							//scaleCornerPoints(cornerPointPositionList, 1.5f);
							addRoadSignQuad(texturePath, cornerPointPositionList, false);
							cornerPointPositionList.clear();
						}
					}
				}
				
				
				/*
				// add shininess
				geometry.getMaterial().setFloat("Shininess", 5f);
				geometry.getMaterial().setBoolean("UseMaterialColors",true);
				geometry.getMaterial().setColor("Specular",ColorRGBA.White);
				geometry.getMaterial().setColor("Diffuse",ColorRGBA.White);
				*/
				
				
				// apply anisotropic filter to streets (used for "BigCity" model only)
				if (spatial.getName().startsWith("street_material_"))
					texture.setAnisotropicFilter(32);
				
				// apply anisotropic filter to buildings (used for "Gesture Task" model only)
				if (spatial.getName().equals("groundFloor") || spatial.getName().equals("upperFloors")
						|| spatial.getName().equals("logoGeometry_1"))
				{
					texture.setAnisotropicFilter(32);
				}
				
				// apply anisotropic filter to parking space (used for "NorthB1" and "SouthB1" model only)
				if (spatial.getName().startsWith("parking"))
				{
					texture.setAnisotropicFilter(32);
				}
				
				// remove trees and bushes from scene
				if(texturePath.matches("^\\S+EucalyptusLeaves_Diff.png$")
						|| texturePath.matches("^\\S+CoulterPineBark_Diff.png$")
						|| texturePath.matches("^\\S+CoulterPineLeaves_Diff.png$")
						|| texturePath.matches("^\\S+EucalyptusTrunk_Diff.png$"))
				{
					//spatial.removeFromParent();
				}
				
				//System.err.println(texturePath);
			}
		}
	}
	
	/*
	private void scaleCornerPoints(ArrayList<Vector3f> pointList, float scale)
	{		
		// Idea: make the resulting quad cover more of the underlying sign than just the original extent
		// Move the positions of the corner points further to the outside
		//
		//   Original           Step 1                Step 2
		//                                        x-----------x
		//    UL   UR                             |           |
		//    x-----x        x--x-----x--x        |  x-----x  |
		//    |     |        |  |     |  |        |  |     |  |
		//    |     |        |  |     |  |        |  |     |  |
		//    x-----x        x--x-----x--x        |  x-----x  |
		//    LL   LR                             |           |
		//                                        x-----------x
		
		
		// original corner points
		Vector3f cornerLL = pointList.get(0);
		Vector3f cornerLR = pointList.get(1);
		Vector3f cornerUR = pointList.get(2);
		Vector3f cornerUL = pointList.get(3);
		
		
		// Step 1: horizontal extension
		// ----------------------------
		
		// move corner points LL and LR along line LL-LR
		Vector3f line_LL_LR = cornerLR.subtract(cornerLL);
		Vector3f horCornerLL = cornerLR.add(line_LL_LR.mult(-scale));
		Vector3f horCornerLR = cornerLL.add(line_LL_LR.mult(scale));
		
		// move corner points UR and UL along line UL-UR
		Vector3f line_UL_UR = cornerUR.subtract(cornerUL);
		Vector3f horCornerUR = cornerUL.add(line_UL_UR.mult(scale));
		Vector3f horCornerUL = cornerUR.add(line_UL_UR.mult(-scale));
		
		
		// Step 2: vertical extension
		// --------------------------
		
		// move corner points LL and UL along line LL-UL
		Vector3f line_LL_UL = horCornerUL.subtract(horCornerLL);
		Vector3f vertCornerLL = horCornerUL.add(line_LL_UL.mult(-scale));
		Vector3f vertCornerUL = horCornerLL.add(line_LL_UL.mult(scale));
		
		// move corner points LR and UR along line LR-UR
		Vector3f line_LR_UR = horCornerUR.subtract(horCornerLR);
		Vector3f vertCornerUR = horCornerLR.add(line_LR_UR.mult(scale));
		Vector3f vertCornerLR = horCornerUR.add(line_LR_UR.mult(-scale));
		
		
		// overwrite original corner points
		pointList.get(0).set(vertCornerLL);
		pointList.get(1).set(vertCornerLR);
		pointList.get(2).set(vertCornerUR);
		pointList.get(3).set(vertCornerUL);
	}
	*/

	private void addRoadSignQuad(String texturePath, ArrayList<Vector3f> pointList, boolean isViz)
	{
		Path directoryPath = Paths.get(texturePath);
		String geoID = directoryPath.getFileName().toString();
		
		Vector3f[] vertices = new Vector3f[4];
		for(int i=0; i<pointList.size(); i++)
		{
			vertices[i] = pointList.get(i);
		
			if(isViz)
				addBox(pointList.get(i));
		}
			
		Vector2f[] texCoord = new Vector2f[]
		{
			new Vector2f(0,0),
			new Vector2f(1,0),
			new Vector2f(1,1),
			new Vector2f(0,1)
		};
		
		int [] indexes = new int[]
		{
			0, 1, 2, 2, 3, 0
		};

		Mesh mesh = new Mesh();
		mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
		mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
		mesh.updateBound();
	
		Geometry geometry = new Geometry(geoID, mesh);
		
		if(!isViz)
			geometry.setCullHint(CullHint.Always);
		
		Material material = createTexturedMaterial("Textures/Road/shoulder.jpg", WrapAxis.S, WrapMode.Repeat);
		geometry.setMaterial(material);
		
		mapObjectNode.attachChild(geometry);
	}


	private void addBox(Vector3f position)
	{
		Box box = new Box(0.1f, 0.1f, 0.1f);
		Geometry geometry = new Geometry("box", box);
		geometry.setLocalTranslation(position);
		Material material = new Material(sim.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		material.setColor("Color", ColorRGBA.Red);
		geometry.setMaterial(material);
		mapObjectNode.attachChild(geometry);
	}

	
    private Material createTexturedMaterial(String texturePath, WrapAxis wrapAxis, WrapMode wrapMode)
    {
	    Material material = new Material(sim.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
	    Texture texture = sim.getAssetManager().loadTexture(texturePath);
	    texture.setAnisotropicFilter(32);
	    
	    if(wrapAxis != null && wrapMode != null)
	    	texture.setWrap(wrapAxis, wrapMode);

	    material.setTexture("ColorMap", texture);
	    return material;
	}
    
	
	private boolean hasTransparentPixel(Texture texture)
	{
		for(ByteBuffer buf : texture.getImage().getData())
		{
			for(int i=0; i<buf.limit(); i+=4)
			{
				int i_a = Byte.toUnsignedInt(buf.get(i));   // alpha channel
				//int i_b = Byte.toUnsignedInt(buf.get(i+1)); // blue channel
				//int i_g = Byte.toUnsignedInt(buf.get(i+2)); // green channel
				//int i_r = Byte.toUnsignedInt(buf.get(i+3)); // red channel
				//System.err.println(i_a + " " + i_b + " " + i_g + " " + i_r);
					
				if(i_a < transparencyThreshold)
					return true;
			}
		}

		return false;
	}


}
