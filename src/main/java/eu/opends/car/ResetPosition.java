/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.car;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import eu.opends.main.Simulator;
import eu.opends.opendrive.processed.ODLane;
import eu.opends.opendrive.processed.ODLaneSection;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.processed.ODRoad;
import eu.opends.opendrive.processed.PreferredConnections;
import eu.opends.opendrive.util.ODPosition;


/**
 * This class represents a single reset position. Reset positions can be 
 * placed in the map model. Their location and rotation will be stored
 * and can be requested by these methods.
 * 
 * @author Rafael Math
 */
public class ResetPosition 
{
	private String name;
	private Vector3f location;
	private Quaternion rotation;
	private ODPosition odPos;
	
	
	/**
	 * Creates a new reset position containing location and rotation.
	 * 
	 * @param location
	 * 			Location of the reset point, where the car will be placed.
	 * 
	 * @param rotation
	 * 			Rotation of the reset point, which will be applied to the car.
	 */
	public ResetPosition(Vector3f location, Quaternion rotation) 
	{
		this.name = "";
		this.location = location;
		this.rotation = rotation;
		this.odPos = null;
	}

	
	/**
	 * Creates a new reset position containing name, location and rotation.
	 * 
	 * @param name
	 * 			Name of the reset point. Used to retrieve a certain point.
	 * 
	 * @param location
	 * 			Location of the reset point, where the car will be placed.
	 * 
	 * @param rotation
	 * 			Rotation of the reset point, which will be applied to the car.
	 */
	public ResetPosition(String name, Vector3f location, Quaternion rotation) 
	{
		this.name = name;
		this.location = location;
		this.rotation = rotation;
		this.odPos = null;
	}


	/**
	 * Creates a new reset position containing name and OpenDRIVE position.
	 * 
	 * @param name
	 * 			Name of the reset point. Used to retrieve a certain point.
	 * 
	 * @param odPos
	 * 			OpenDRIVE position to calculate location and rotation.
	 */
	public ResetPosition(String name, ODPosition odPos)
	{
		this.name = name;
		this.location = null;
		this.rotation = null;
		this.odPos = odPos;
	}


	/**
	 * Returns the name of this reset point.
	 * 
	 * @return
	 * 			Name of the reset point.
	 */
	public String getName()
	{
		return name;
	}
	
	
	/**
	 * Returns the location of this reset point as vector.
	 * 
	 * @return
	 * 			Location of the reset point.
	 */
	public Vector3f getLocation(Simulator sim)
	{
		if(location == null)
			initResetPosition(sim);
		
		return location;
	}


	/**
	 * Returns the rotation of this reset point as quaternion.
	 * 
	 * @return
	 * 			Rotation of the reset point.
	 */
	public Quaternion getRotation(Simulator sim)
	{
		if(rotation == null)
			initResetPosition(sim);
		
		return rotation;
	}

	
	private void initResetPosition(Simulator sim)
	{
		String roadID = odPos.getRoadID();
		int laneNumber = odPos.getLane();
		double s = odPos.getS();
		
		if(roadID != null && !roadID.isEmpty())
		{
			if(s < 0)
				s = 0;
			
			ODRoad road = sim.getOpenDriveCenter().getRoadMap().get(roadID);
			if(road != null)
			{
				for(ODLaneSection laneSection : road.getLaneSectionList())
				{
					if(laneSection.getS() <= s && s <= laneSection.getEndS())
					{
						ODLane lane = laneSection.getLaneMap().get(laneNumber);
						PreferredConnections preferredConnections = new PreferredConnections(); 
						ODPoint point = lane.getLaneCenterPointAhead(false, s, 0, preferredConnections, null);
						ODPoint targetPoint = lane.getLaneCenterPointAhead(false, s, 1, preferredConnections, null);
						
						Vector3f position = point.getPosition().toVector3f();
						Vector3f targetPosition = targetPoint.getPosition().toVector3f();
						
						Vector3f posDiff = targetPosition.subtract(position);
						float rot = FastMath.atan2(posDiff.x,-posDiff.z);
	
						location = point.getPosition().toVector3f();
						rotation = new Quaternion().fromAngles(0, rot, 0);
					}
				}
			}
			else
				System.err.println("ResetPosition '" + name + "' has invalid roadID: '" + roadID + "'");
		}
	}
	
	
	/**
	 * String representation, containing location and rotation
	 */
	public String toString()
	{
		return "loc: " + location + ", rot: " + rotation;
	}
}
