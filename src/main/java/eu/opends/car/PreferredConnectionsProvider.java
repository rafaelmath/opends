/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.car;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import eu.opends.basics.SimulationBasics;
import eu.opends.drivingTask.interaction.ActionDescription;
import eu.opends.drivingTask.interaction.InteractionLoader;
import eu.opends.drivingTask.interaction.InteractionMethods;
import eu.opends.drivingTask.scenario.ScenarioLoader;
import eu.opends.main.Simulator;
import eu.opends.opendrive.processed.ODLane;
import eu.opends.opendrive.processed.PreferredConnections;
import eu.opends.opendrive.roadGraph.RoadGraph;
import eu.opends.opendrive.util.ODPosition;
import eu.opends.tools.PanelCenter;
import eu.opends.trigger.TriggerAction;
import javafx.util.Pair;

public class PreferredConnectionsProvider
{
	private float minDist = 3.0f;
	private Simulator sim;
	private boolean isUserMode = false;
	private PreferredConnections preferredConnections;
	private ArrayList<Pair<ODPosition, PreferredConnections>> preferredConnectionsList;
	private int indexCurrentPC = -1;
	private String targetPosString = "(no target)";
	
	
	public PreferredConnectionsProvider(Simulator sim)
	{
		this.sim = sim;
		
		ScenarioLoader scenarioLoader = SimulationBasics.getDrivingTask().getScenarioLoader();
		
		// lookup whether user-defined connections exist
		preferredConnections = scenarioLoader.getPreferredConnectionsList();
		if(!preferredConnections.isEmpty())
			isUserMode = true;
		
		// lookup start, via, and target positions
		ODPosition startPos = scenarioLoader.getOpenDriveStartPosition(sim);
		ArrayList<ODPosition> viaPosList = scenarioLoader.getOpenDriveViaPositions(sim);
		ODPosition targetPos = scenarioLoader.getOpenDriveTargetPosition(sim);
		preferredConnectionsList = extractPreferredConnectionLists(startPos, viaPosList, targetPos);
		
		if(isUserMode && targetPos != null)
			System.err.println("Warning: Preferred Connections have been defined and will overwrite"
				+ " the way finding towards the given target point! (PreferredConnectionsProvider.java)");
		
		if(viaPosList.size() > 0 && targetPos == null)
			System.err.println("Warning: Via positions have been defined but no target position!"
				+ " (PreferredConnectionsProvider.java)");
	}


	private boolean hasReachedFinalTarget = false;
	public boolean isPCUpdateAvailable(ODLane lane, double s, boolean isWrongWay)
	{
		if(!isUserMode && lane != null)
		{
			ODPosition currentTargetPos = getCurrentTargetPos();
			double distToCurrentTarget = lane.getDistanceToTargetAhead(isWrongWay, s, preferredConnections, currentTargetPos);
			//System.out.println("dist == " + distToCurrentTarget);
			
			int distInt = (int) distToCurrentTarget;
			PanelCenter.getInfoText().setText(targetPosString + ": " + distInt);
			
			if(0 < distToCurrentTarget && distToCurrentTarget < minDist && !hasReachedFinalTarget)
			{
				// log arrival at current target position
				long milliseconds = System.currentTimeMillis();
				Simulator.getDrivingTaskLogger().reportEvent(milliseconds, "arrivalAtPosition", currentTargetPos.toString(), 
						null, null, null, null, null, null, null, null);
				
				// check whether activity will be triggered at current target position
				ODPosition targetPos = preferredConnectionsList.get(indexCurrentPC).getKey();
				checkActivityOnArrival(targetPos);
				
				if(indexCurrentPC < preferredConnectionsList.size()-1)
					return true;
				else
					hasReachedFinalTarget = true;
			}
		}
		
		return false;
	}


	private void checkActivityOnArrival(ODPosition position)
	{
		String userData = position.getUserData();
		
		if(userData != null && userData.startsWith("activityID:"))
		{
			String activityIDList = userData.replace("activityID:", "");
			InteractionLoader interactionLoader = SimulationBasics.getDrivingTask().getInteractionLoader();
			HashMap<String, List<ActionDescription>> activityMap = interactionLoader.getActivityMap();
			
			// check for multiple activityIDs separated by ;
			String[] activityIdArray = activityIDList.split(";");
			for(String activityID : activityIdArray)
			{
				if(!activityID.isEmpty() && activityMap.containsKey(activityID))
				{
					System.err.println("Perform Activity ID '" + activityID + "' on arrival");
				
					long milliseconds = System.currentTimeMillis();
					Simulator.getDrivingTaskLogger().reportEvent(milliseconds, "activityOnArrival", activityID, null, 
						null, null, null, null, null, null, null);

					List<ActionDescription> actionDescriptionList = activityMap.get(activityID);
					for(ActionDescription actionDescription : actionDescriptionList)
					{
						TriggerAction triggerAction = createTriggerAction(actionDescription);
						if(triggerAction != null)
							triggerAction.performAction();
					}	
				}
				else
					System.err.println("interaction.xml: Activity ID '" + activityID + "' does not exist.");
			}
		}
	}

	
	private TriggerAction createTriggerAction(ActionDescription actionDescription) 
	{
		String name = actionDescription.getName();
		float delay = actionDescription.getDelay();
		int repeat = actionDescription.getRepeat();
		String executionClass = actionDescription.getExecutionClass();
		Properties parameterList = actionDescription.getParameterList();

		TriggerAction triggerAction = null;
		
		// reflection to corresponding method
		try {

			// argument list with corresponding types
			Object argumentList[] = new Object[] {sim, delay, repeat, executionClass, parameterList};
			Class<?> parameterTypes[] = new Class[] {SimulationBasics.class, Float.TYPE, Integer.TYPE, String.class, Properties.class};
			
			// get method to call
			Class<?> interactionMethodsClass = Class.forName("eu.opends.drivingTask.interaction.InteractionMethods");
			Method method = interactionMethodsClass.getMethod(name, parameterTypes);
			
			// call method and get return value
			triggerAction = (TriggerAction) method.invoke(new InteractionMethods(), argumentList);

		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return triggerAction;
	}
	

	public PreferredConnections getPreferredConnections()
	{
		if(!isUserMode)
			preferredConnections = getNextPC();
		
		//System.err.println(preferredConnections.toString());
		
		return preferredConnections;
	}
	

	private ArrayList<Pair<ODPosition,PreferredConnections>> extractPreferredConnectionLists(ODPosition startPos, 
			ArrayList<ODPosition> viaPosList, ODPosition targetPos)
	{
		ArrayList<Pair<ODPosition, PreferredConnections>> list = 
				new ArrayList<Pair<ODPosition, PreferredConnections>>();
		
		if(startPos != null && targetPos != null)
		{
			ODPosition fromPos = startPos;
			ODPosition toPos;
			
			for(int i=0; i<viaPosList.size(); i++)
			{
				toPos = viaPosList.get(i);
				
				Pair<ODPosition, PreferredConnections> pair = getPreferredConnections(fromPos, toPos);
				
				// if way finding is broken --> do not add further PreferredConnections to the list
				if(pair == null)
					return list;
				
				list.add(pair);
				
				fromPos = toPos;
			}
			
			toPos = targetPos;
			
			Pair<ODPosition, PreferredConnections> pair = getPreferredConnections(fromPos, toPos);
			if(pair != null)
				list.add(pair);
		}
		
		return list;		
	}
	
	
	private Pair<ODPosition, PreferredConnections> getPreferredConnections(ODPosition fromPos, ODPosition toPos)
	{
		RoadGraph roadGraph = sim.getOpenDriveCenter().getRoadGraph();
		if(roadGraph != null)
		{
			PreferredConnections pc = roadGraph.getShortestPath(fromPos, toPos);
			if(pc != null)
			{
				//System.err.println("PreferredConnections of steering car: \n" + pc);
				return new Pair<ODPosition, PreferredConnections>(toPos, pc);
			}
			else
				System.err.println("No route from " + fromPos + " to " + toPos 
					+ " could be found for steering car! No navigation beyond " 
					+ fromPos + "! (PreferredConnectionsProvider.java)");
		}
		else
			System.err.println("Road graph not available");
		
		return null; 
	}
	
	
	private PreferredConnections getNextPC()
	{
		indexCurrentPC++;
		
		if(indexCurrentPC < preferredConnectionsList.size())
		{
			Pair<ODPosition, PreferredConnections> p = preferredConnectionsList.get(indexCurrentPC);
			
			targetPosString = p.getKey().getRoadID() + "/" + p.getKey().getLane() + "/" + p.getKey().getS();
			//System.err.println("Going to: " + targetPosString);
			
			// log next target position
			long milliseconds = System.currentTimeMillis();
			Simulator.getDrivingTaskLogger().reportEvent(milliseconds, "goingToPosition", targetPosString, 
					null, null, null, null, null, null, null, null);
			
			return p.getValue();
		}
		else
			return new PreferredConnections();
	}

	
	private ODPosition getCurrentTargetPos()
	{
		if(indexCurrentPC < preferredConnectionsList.size())
			return preferredConnectionsList.get(indexCurrentPC).getKey();
		else
			return null;
	}
}
