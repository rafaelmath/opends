/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects;

import java.util.TreeMap;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;


public class DynamicObjectsCenter
{
	private SimulationBasics sim;
	private TreeMap<String, DynamicObject> dynamicObjectsMap;
	
	
	public DynamicObjectsCenter(SimulationBasics sim)
	{
		this.sim = sim;
		dynamicObjectsMap = Simulator.getDrivingTask().getSceneLoader().getDynamicObjectsMap(sim);
	}
	
	
	public void update(float tpf)
	{
		if(!sim.isPause())
		{
			for(DynamicObject dynamicObject : dynamicObjectsMap.values())
				dynamicObject.update(tpf);
		}
	}


	public TreeMap<String, DynamicObject> getDynamicObjectsMap()
	{
		return dynamicObjectsMap;
	}
	
	
}
