/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.variableSign;

import com.jme3.material.Material;
import com.jme3.material.Materials;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Quad;
import com.jme3.util.BufferUtils;

import eu.opends.basics.SimulationBasics;
import eu.opends.dynamicObjects.DynamicObject;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.util.ODPositionWithOffsets;
import eu.opends.tools.Util;


public class VariableSign implements DynamicObject
{
	public enum TransitionType
	{
		SlideUp, SlideDown, SlideLeft, SlideRight, None
	}
	
	private enum QuadPos
	{
		Top, Bottom, Left, Right
	}

	private SimulationBasics sim;
	private String name;
	private Vector3f displaySize;
	private Vector3f totalSize;
	private float borderWidth = 0.3f;
	private float borderHeight = 0.3f;
	private VariableSignImageSequence imageSequence;
	private Material matBackSide, matOff;
	private Node frontNode;
	private Spatial display;
	private Node signNode;
	private boolean isInitialImage = true;
	private Material currentMaterial;
	private Material nextMaterial;
	private TransitionType transitionType = TransitionType.None;
	private float slidingUpdateTimer = 0;
	private float slidingDuration = 1f;
	private float slidingStepSize = 0.02f;
	private float percentage = -1f;
	
	
	public VariableSign(SimulationBasics sim, String name, Vector3f displaySize, TransitionType transitionType,
			Float slidingDuration, VariableSignImageSequence imageSequence, ColorRGBA backsideColor, 
			Float borderWidth, Float borderHeight, Boolean addPole)
	{
		this.sim = sim;
		this.name = name;
		this.displaySize = displaySize;
		
		if(transitionType != null)
			this.transitionType = transitionType;
		
		if(slidingDuration != null)
			this.slidingDuration = slidingDuration;

		this.imageSequence = imageSequence;
		
		initMaterials(backsideColor);
		
        // create materials for each image in sequence
        imageSequence.init(sim);  	

		if(borderWidth != null)
			this.borderWidth = Math.max(0, borderWidth);
		
		if(borderHeight != null)
			this.borderHeight = Math.max(0, borderHeight);
        
		// compute total size (= display size + borders)
        float totalWidth = displaySize.getX() + (2 * this.borderWidth);
        float totalHeight = displaySize.getY() +  + (2 * this.borderHeight);
        float depth = displaySize.getZ();
        totalSize = new Vector3f(totalWidth, totalHeight, depth);
        
		// create sign
		signNode = createSignNode(name, this.borderWidth, this.borderHeight);
		
		if(addPole != null && addPole)
		{
			// create pole
			Spatial pole = sim.getAssetManager().loadModel("Models/RoadSigns/pole/pole.scene");
			pole.setLocalScale(1.8f);
			pole.setLocalTranslation(0.0f, -2.8f, -0.05f);
			signNode.attachChild(pole);
		}
		
		sim.getSceneNode().attachChild(signNode);
		
		// set first image (may be null)
		imageSequence.setImageByIndex(this, 0);
	}


	public void setLocalTranslation(Vector3f location)
	{
		signNode.setLocalTranslation(location);	
	}
	
	
	public void setLocalRotation(Quaternion rotation)
	{
		signNode.setLocalRotation(rotation);	
	}

	
	public void setODPosition(ODPositionWithOffsets odPos) 
	{
		String roadID = odPos.getRoadID();
		float s = odPos.getS();
		float lateralOffset = odPos.getLateralOffset();
		float verticalOffset= odPos.getVerticalOffset();
		
		if(Util.isValidOffroadPosition(sim.getOpenDriveCenter(), name + "_screen_node_position", roadID, 
				lateralOffset, verticalOffset, s))
		{
			ODPoint point = sim.getOpenDriveCenter().getRoadMap().get(roadID).getPointOnReferenceLine(s, 
					name + "_screen_node_position");
			Vector3f referencePosition = point.getPosition().toVector3f();
			float ortho = (float)point.getOrtho();
		
			float x = referencePosition.getX() + lateralOffset*FastMath.sin(ortho);
			float z = referencePosition.getZ() + lateralOffset*FastMath.cos(ortho);
			float y = Util.getElevationAt(sim,x,z) + verticalOffset;
			signNode.setLocalTranslation(new Vector3f(x, y, z));

			// overwrite original rotation with its relative rotation wrt road object
			Quaternion rotation = signNode.getLocalRotation();	
			float[] angles = new float[3];
			rotation.toAngles(angles);
			signNode.setLocalRotation((new Quaternion()).fromAngles(angles[0], ortho + angles[1] - FastMath.HALF_PI,
					angles[2]));
		}
		else
			System.err.println("Could not set position of screen node '" + name	+ "' at: " + roadID + "/" + s);
	}
	
	
	public void setLocalScale(float scale)
	{
		signNode.setLocalScale(scale);		
	}
	
	
	public void update(float tpf)
	{
		imageSequence.update(this);
		
		slidingUpdateTimer += tpf;
		
		// allow max. 1 update per sliding interval
		// default: sliding sign image by 2 percent (slidingStepSize = 0.02) 
		//          every 20 ms (slidingDuration = 1.0 seconds) --> 1 second to complete
		if(slidingUpdateTimer > slidingStepSize * slidingDuration)
		{
			updateImageSliding();
			slidingUpdateTimer = 0;
		}
	}
	
	
	private void updateImageSliding()
	{
		if(0.0f <= percentage && percentage <= 1.0f && transitionType != TransitionType.None)
		{
			frontNode.detachChild(display);
			display = createDisplayNode(percentage);
			frontNode.attachChild(display);
			
			if(transitionType == TransitionType.SlideUp || transitionType == TransitionType.SlideRight)
				percentage += slidingStepSize;
			else // if(transitionType == TransitionType.SlideDown || transitionType == TransitionType.SlideLeft)
				percentage -= slidingStepSize;
		}
	}
	
	
	public void setImage(VariableSignImage image)
	{
		if(image != null)
		{
			//System.err.println("Set image: " + image.getID());
			startTransition(image.getMaterial());
		}
		else
			startTransition(matOff);
	}
	
	
	public void setImageById(String imageID)
	{
		imageSequence.setImageById(this, imageID);
	}
	

	private void startTransition(Material material)
	{
		if(isInitialImage)
		{
			display.setMaterial(material);
			isInitialImage = false;
		}
		else if(transitionType == TransitionType.SlideUp || transitionType == TransitionType.SlideRight)
			percentage = 0;
		else if(transitionType == TransitionType.SlideDown || transitionType == TransitionType.SlideLeft)
			percentage = 1;
		else
			display.setMaterial(material); //transitionType == Transition.None
		
		currentMaterial = nextMaterial;
		nextMaterial = material;
	}


	private Node createDisplayNode(float percentage)
	{
		// Subdivide display into 2 rows (slide up/down) or 2 columns (slide left/right), respectively:
		//
		// ---------------  1.0                  ---------------  1.0
		// |             |                       |      |      |
		// |  geometry1  |                       |      |      |
		// |-------------|  perc                 | geo1 | geo2 |
		// |             |                       |      |      |
		// |  geometry2  |                       |      |      |
		// ---------------  0.0                  ---------------  0.0
		// 0.0         1.0                       0.0  perc   1.0
		
		Node display = new Node("display");
		
		// if(transitionType == TransitionType.SlideUp || transitionType == TransitionType.SlideLeft)
		Material geo1Mat = currentMaterial;
		Material geo2Mat = nextMaterial;
		
		if(transitionType == TransitionType.SlideDown || transitionType == TransitionType.SlideRight)
		{
			geo1Mat = nextMaterial;
			geo2Mat = currentMaterial;
		}
		
		Geometry geometry1;
		Geometry geometry2;
		
		if(transitionType == TransitionType.SlideUp || transitionType == TransitionType.SlideDown)
		{
			geometry1 = createDisplayGeometry("subGeometry_2_2_A", percentage, QuadPos.Top, geo1Mat);
			geometry2 = createDisplayGeometry("subGeometry_2_2_B", percentage, QuadPos.Bottom, geo2Mat);
		}
		else
		{
			geometry1 = createDisplayGeometry("subGeometry_2_2_A", percentage, QuadPos.Left, geo1Mat);
			geometry2 = createDisplayGeometry("subGeometry_2_2_B", percentage, QuadPos.Right, geo2Mat);
		}
		

		if(geometry1 != null)
			display.attachChild(geometry1);
		
		if(geometry2 != null)
			display.attachChild(geometry2);
		
    	display.setLocalTranslation(borderWidth, borderHeight, 0.0f);
		
        return display;
	}
        

	private Geometry createDisplayGeometry(String name, float percentage, QuadPos pos, Material mat)
	{
        float left = 0.0f;
        float right = 1.0f;
        float bottom = 0.0f;
        float top = 1.0f;
        
        if(pos == QuadPos.Top)
        	top = 1.0f - percentage;
        else if(pos == QuadPos.Bottom)
        	bottom = 1.0f - percentage;
        else if(pos == QuadPos.Left)
        	left = 1.0f - percentage;
        else // pos == QuadPos.Right
        	right = 1.0f - percentage;
        
        float width = right - left;
		float height = top - bottom;
			
		// if geometry is too small
        if(width <= slidingStepSize || height <= slidingStepSize)
        	return null;

        Quad quad = new Quad(width * displaySize.x, height * displaySize.y);

        Vector2f[] texCoords = new Vector2f[]
        {
           	new Vector2f(left, bottom),
           	new Vector2f(right, bottom),
           	new Vector2f(right, top),
           	new Vector2f(left, top)
        };
        
        quad.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoords));
        Geometry geometry = new Geometry(name, quad);
        geometry.setMaterial(mat);
        
        float x = (1.0f - right) * displaySize.x;
        float y = (1.0f - top) * displaySize.y;
        geometry.setLocalTranslation(x, y, 0.0f);
        
        return geometry;
	}
	

	private Node createSignNode(String nodeName, float borderWidth, float borderHeight)
    {
		Node screen = new Node(nodeName);

    	// 1. Front
    	frontNode = createFrontNode(totalSize, borderWidth, borderHeight);
    	frontNode.setLocalTranslation(-totalSize.x / 2, -totalSize.y / 2, totalSize.z / 2);
		screen.attachChild(frontNode);
    	
    	
        // 2. Back
        Quad backQuad = new Quad(totalSize.x, totalSize.y);
        
        Geometry back = new Geometry("back", backQuad);
        back.setLocalTranslation(totalSize.x / 2, -totalSize.y / 2, -totalSize.z / 2);
        back.setLocalScale(new Vector3f(-1, 1, 1));
        back.setMaterial(matBackSide);
        screen.attachChild(back);

        
        // 3. Side (right, left)
        Quad sideQuad = new Quad(totalSize.y, totalSize.z);
        
        Geometry right = new Geometry("right", sideQuad);
        right.setLocalTranslation(totalSize.x / 2, -totalSize.y / 2, -totalSize.z / 2);
        right.rotate(0, FastMath.HALF_PI, FastMath.HALF_PI);
        right.setMaterial(matBackSide);
        screen.attachChild(right);

        Geometry left = new Geometry("left", sideQuad);
        left.setLocalTranslation(-totalSize.x / 2, -totalSize.y / 2, totalSize.z / 2);
        left.rotate(0, -FastMath.HALF_PI, FastMath.HALF_PI);
        left.setMaterial(matBackSide);
        screen.attachChild(left);

        
        // 4. Side (top, bottom)
        sideQuad = new Quad(totalSize.x, totalSize.z);

        Geometry top = new Geometry("top", sideQuad);
        top.setLocalTranslation(-totalSize.x / 2, totalSize.y / 2, totalSize.z / 2);
        top.rotate(-FastMath.HALF_PI, 0, 0);
        top.setMaterial(matBackSide);
        screen.attachChild(top);

        Geometry bottom = new Geometry("bottom", sideQuad);
        bottom.setLocalTranslation(-totalSize.x / 2, -totalSize.y / 2, -totalSize.z / 2);
        bottom.rotate(FastMath.HALF_PI, 0, 0);
        bottom.setMaterial(matBackSide);
        screen.attachChild(bottom);

        return screen;
    }


	private Node createFrontNode(Vector3f size, float borderWidth, float borderHeight)
	{
		// Subdivide front into 3 rows having different numbers of columns:
		//
		// ----------------
		// |--------------|
		// | |          | |
		// | |          | |
		// |--------------|
		// ----------------
		
		// border positions
		float leftMin = 0;
		float leftMax = 0 + borderWidth/size.x;
		float rightMin = 1 - borderWidth/size.x;
		float rightMax = 1;
		
		float bottomMin = 0;
		float bottomMax = 0 + borderHeight/size.y;
		float topMin = 1 - borderHeight/size.y;
		float topMax = 1;
		
		Node frontNode = new Node("front");
		
		Geometry row1Geometry = createGeometry("subGeometry_1", size, leftMin, rightMax, topMin, topMax);
		row1Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row1Geometry);
        
		Geometry row2col1Geometry = createGeometry("subGeometry_2_1", size, leftMin, leftMax, bottomMax, topMin);
		row2col1Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row2col1Geometry);
		
		// center
		display = createGeometry("subGeometry_2_2", size, leftMax, rightMin, bottomMax, topMin);
		display.setMaterial(matOff);
		frontNode.attachChild(display);
		
		Geometry row2col3Geometry = createGeometry("subGeometry_2_3", size, rightMin, rightMax, bottomMax, topMin);
		row2col3Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row2col3Geometry);
		
		Geometry row3Geometry = createGeometry("subGeometry_3", size, leftMin, rightMax, bottomMin, bottomMax);
		row3Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row3Geometry);

        return frontNode;
	}
	
	
	private Geometry createGeometry(String name, Vector3f size, float left, float right, float bottom, float top)
	{
        Quad quad = new Quad((right-left) * size.x, (top-bottom) * size.y);
        Geometry geometry = new Geometry(name, quad);
        geometry.setLocalTranslation(left * size.x, bottom * size.y, 0.0f);
        
        return geometry;
	}
	

	private void initMaterials(ColorRGBA backsideColor)
	{
		if(backsideColor == null)
			backsideColor = ColorRGBA.Black;
		
		matBackSide = new Material(sim.getAssetManager(), Materials.LIGHTING);
        matBackSide.setBoolean("UseMaterialColors", true);
        matBackSide.setColor("Diffuse", backsideColor);
        
        matOff = new Material(sim.getAssetManager(), Materials.LIGHTING);
        matOff.setBoolean("UseMaterialColors", true);
        matOff.setColor("Diffuse", ColorRGBA.DarkGray);
        
        currentMaterial = matOff;
        nextMaterial = matOff;
	}


	
}
