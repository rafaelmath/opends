/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.variableSign;

import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.texture.Texture;

import eu.opends.basics.SimulationBasics;

public class VariableSignImage
{
	private String id;
	private String texturePath;
	private float duration; // in seconds
	private long startTime = Long.MAX_VALUE;
	private Material material;
	private boolean isInitialized = false;
	
	
	public VariableSignImage(String id, String texturePath, Float duration)
	{
		this.id = id;
		this.texturePath = texturePath;
		
		if(duration != null)
			this.duration = duration;
		else
			this.duration = 0; // infinite duration
	}


	public void init(SimulationBasics sim)
	{
    	material = loadMaterial(sim, texturePath);
    	isInitialized = true;
	}
	
	
	public String getID()
	{
		return id;
	}
	
	
	public Material getMaterial()
	{
		if(!isInitialized)
			System.err.println("Warning: VariableSignImage '" + id + "' has not been initialized!");
			
		return material;
	}

	
	public void setStarted()
	{
		startTime = System.currentTimeMillis();
	}
	

	public boolean hasElapsed()
	{
		long now = System.currentTimeMillis();
		return (duration > 0 && (now - startTime > (duration*1000)));
	}
	
	
	private Material loadMaterial(SimulationBasics sim, String texturePath)
	{
		Material material = null;
		
		try{
			TextureKey textureKey = new TextureKey(texturePath, true);
			Texture texture = sim.getAssetManager().loadTexture(textureKey);
			material = new Material(sim.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
			material.setTexture("ColorMap",texture);
			
		} catch (Exception e){
			e.printStackTrace();
			System.err.println("Error loading texture file " + texturePath);
		}
		
	    return material;
	}
	
}
