/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.variableSign;

import java.util.ArrayList;

import eu.opends.basics.SimulationBasics;

public class VariableSignImageSequence
{
	// after last image: repeat from beginning or stop with last image?
	private boolean isRepeat = true;
	private ArrayList<VariableSignImage> imageList = new ArrayList<VariableSignImage>();
	private VariableSignImage currentImage = null;
	

	public VariableSignImageSequence()
	{
	}
	
	
	public VariableSignImageSequence(boolean isRepeat)
	{
		this.isRepeat = isRepeat;
	}

	
	public void setRepeat(boolean isRepeat)
	{
		this.isRepeat = isRepeat;
	}

	
	public void add(VariableSignImage variableSignImage)
	{
		imageList.add(variableSignImage);		
	}
	

	public void init(SimulationBasics sim)
	{
		for(VariableSignImage image : imageList)
        	image.init(sim);  
	}

	
	public void update(VariableSign variableSign)
	{
		// if current image has elapsed
		if(currentImage != null && currentImage.hasElapsed())
		{
			// get index of next image
			int nextIndex = imageList.indexOf(currentImage) + 1;
			
			// if reached end of sequence and in REPEAT mode, start from beginning
			if(nextIndex >= imageList.size() && isRepeat)
				nextIndex = 0;
			
			// set next/first image (if available)
			setImageByIndex(variableSign, nextIndex);
		}
	}
	

	public void setImageByIndex(VariableSign variableSign, int index)
	{
		if(imageList.size() > index)
		{
			VariableSignImage image = imageList.get(index);
			variableSign.setImage(image);
			
			currentImage = image;
			image.setStarted();
		}
		else
			currentImage = null;
	}
	
	
	public void setImageById(VariableSign variableSign, String imageID)
	{
		for(VariableSignImage image : imageList)
		{
			if(image.getID().equals(imageID))
			{
				variableSign.setImage(image);
				currentImage = image;
				image.setStarted();
				return;
			}
		}
		
		currentImage = null;
	}

	
	public VariableSignImage getImageByIndex(int index)
	{
		if(imageList.size() > index)
			return imageList.get(index);
		
		return null;
	}


}
