/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package  eu.opends.dynamicObjects.messageSign;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.Materials;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;

import eu.opends.basics.SimulationBasics;
import eu.opends.dynamicObjects.DynamicObject;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.util.ODPositionWithOffsets;
import eu.opends.tools.Util;


public class MessageSign implements DynamicObject
{
	private SimulationBasics sim;
	private String name;
	private Vector3f size;
	private Node screenNode, textNode;
	private Material matFrontSide, matBackSide;
	private float fontSize;
	private BitmapFont font;
	private MessageSignText currentText = null;
	
	private float textOffset = 0.03f; //position of text in front of sign (to prevent z-fighting)
	private int noOfLines = 3;
	private int charsPerLine = 20;
	private float charHeight = 0.5f;
	private float signDepth = 0.1f;
	private float borderWidth = 0.1f;
	private float borderHeight = 0.1f;
	private ColorRGBA defaultFontColor = ColorRGBA.Orange;

	
	public MessageSign(SimulationBasics sim, String name, Integer noOfLines, Integer charsPerLine, Float charHeight,
			 Float signDepth, Float borderWidth, Float borderHeight, ColorRGBA defaultFontColor, 
			 ColorRGBA frontsideColor, ColorRGBA backsideColor)
	{
		this.sim = sim;		
		this.name = name;
		
		if(noOfLines != null)
			this.noOfLines = Math.max(1, noOfLines);
		
		if(charsPerLine != null)
			this.charsPerLine = Math.max(1, charsPerLine);
		
		if(charHeight != null)
			this.charHeight = Math.max(0, charHeight);
		
		if(signDepth != null)
			this.signDepth = Math.max(0, signDepth);
		
		if(borderWidth != null)
			this.borderWidth = Math.max(0, borderWidth);
		
		if(borderHeight != null)
			this.borderHeight = Math.max(0, borderHeight);
		
		if(defaultFontColor != null)
			this.defaultFontColor = defaultFontColor;
		
		initMaterials(frontsideColor, backsideColor);

		// calculate size of message sign object
		size = calculateSize(this.signDepth);
		
		screenNode = createScreenNode();
		textNode = new Node(name + "_textNode");
		screenNode.attachChild(textNode);
		sim.getSceneNode().attachChild(screenNode);
		
		font = sim.getAssetManager().loadFont("Interface/Fonts/5x7DOTMatrix/5x7DOTMatrixBold.fnt");
	}


	private Vector3f calculateSize(float totalDepth)
	{
		fontSize = (5f/7f) * charHeight; // fontSize: 1.0 --> charHeight: 1.4
		float displayHeight = noOfLines * charHeight;
		float totalHeight = displayHeight + (2 * borderHeight);
		
		float charWidth = 0.792f * fontSize; // fontSize: 1.0 --> charWidth: 0.792
		float displayWidth = charsPerLine * charWidth;
		float totalWidth = displayWidth + (2 * borderWidth);
		
		return new Vector3f(totalWidth, totalHeight, totalDepth);
	}
	
	
	public void setLocalTranslation(Vector3f location)
	{
		screenNode.setLocalTranslation(location);	
	}
	
	
	public void setLocalRotation(Quaternion rotation)
	{
		screenNode.setLocalRotation(rotation);	
	}

	
	public void setODPosition(ODPositionWithOffsets odPos) 
	{
		String roadID = odPos.getRoadID();
		float s = odPos.getS();
		float lateralOffset = odPos.getLateralOffset();
		float verticalOffset= odPos.getVerticalOffset();
		
		if(Util.isValidOffroadPosition(sim.getOpenDriveCenter(), name + "_screen_node_position", roadID, 
				lateralOffset, verticalOffset, s))
		{
			ODPoint point = sim.getOpenDriveCenter().getRoadMap().get(roadID).getPointOnReferenceLine(s, 
					name + "_screen_node_position");
			Vector3f referencePosition = point.getPosition().toVector3f();
			float ortho = (float)point.getOrtho();
		
			float x = referencePosition.getX() + lateralOffset*FastMath.sin(ortho);
			float z = referencePosition.getZ() + lateralOffset*FastMath.cos(ortho);
			float y = Util.getElevationAt(sim,x,z) + verticalOffset;
			screenNode.setLocalTranslation(new Vector3f(x, y, z));

			// overwrite original rotation with its relative rotation wrt road object
			Quaternion rotation = screenNode.getLocalRotation();	
			float[] angles = new float[3];
			rotation.toAngles(angles);
			screenNode.setLocalRotation((new Quaternion()).fromAngles(angles[0], ortho + angles[1] - FastMath.HALF_PI,
					angles[2]));
		}
		else
			System.err.println("Could not set position of screen node '" + name	+ "' at: " + roadID + "/" + s);
	}
	
	
	public void showText(MessageSignText text)
	{
		// clear any previous text
		clearText();

		float left  = -0.5f * size.x + borderWidth;
		float top   =  0.5f * size.y - borderHeight;
		float depth =  0.5f * size.z + textOffset;

		for(int i=0; i<noOfLines; i++)
		{
			BitmapText bmpTxt = text.getBitmapText(i, charsPerLine, font, fontSize, defaultFontColor);
			if(bmpTxt != null)
			{
				float y = top - (i * charHeight);
				Vector3f position = new Vector3f(left, y, depth);
				bmpTxt.setLocalTranslation(position);
				
				textNode.attachChild(bmpTxt);
			}
		}
		
		text.setStarted();
		
		currentText = text;
	}

	
	public void clearText()
	{
		textNode.detachAllChildren();
		currentText = null;
	}
	

	@Override
	public void update(float tpf)
	{
		// clear text if it has elapsed
		if(currentText != null && currentText.hasElapsed())
			clearText();

		if(currentText != null)
			currentText.update(tpf);
	}


	private void initMaterials(ColorRGBA frontsideColor, ColorRGBA backsideColor)
	{
		if(frontsideColor == null)
			frontsideColor = ColorRGBA.Black;
		
		if(backsideColor == null)
			backsideColor = ColorRGBA.White;
		
		matFrontSide = new Material(sim.getAssetManager(), Materials.LIGHTING);
		matFrontSide.setBoolean("UseMaterialColors", true);
		matFrontSide.setColor("Diffuse", frontsideColor);
        
		matBackSide = new Material(sim.getAssetManager(), Materials.LIGHTING);
        matBackSide.setBoolean("UseMaterialColors", true);
        matBackSide.setColor("Diffuse", backsideColor);
	}
	
	
	private Node createScreenNode()
    {
    	Node screen = new Node(name + "_screenNode");

    	// 1. Front
    	Node frontNode = createFrontNode();
    	frontNode.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
    	screen.attachChild(frontNode);

    	
        // 2. Back
        Quad backQuad = new Quad(size.x, size.y);
        
        Geometry back = new Geometry("back", backQuad);
        back.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        back.setLocalScale(new Vector3f(-1, 1, 1));
        back.setMaterial(matBackSide);
        screen.attachChild(back);

        
        // 3. Side (right, left)
        Quad sideQuad = new Quad(size.y, size.z);
        
        Geometry right = new Geometry("right", sideQuad);
        right.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        right.rotate(0, FastMath.HALF_PI, FastMath.HALF_PI);
        right.setMaterial(matBackSide);
        screen.attachChild(right);

        Geometry left = new Geometry("left", sideQuad);
        left.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
        left.rotate(0, -FastMath.HALF_PI, FastMath.HALF_PI);
        left.setMaterial(matBackSide);
        screen.attachChild(left);

        
        // 4. Side (top, bottom)
        sideQuad = new Quad(size.x, size.z);

        Geometry top = new Geometry("top", sideQuad);
        top.setLocalTranslation(-size.x / 2, size.y / 2, size.z / 2);
        top.rotate(-FastMath.HALF_PI, 0, 0);
        top.setMaterial(matBackSide);
        screen.attachChild(top);

        Geometry bottom = new Geometry("bottom", sideQuad);
        bottom.setLocalTranslation(-size.x / 2, -size.y / 2, -size.z / 2);
        bottom.rotate(FastMath.HALF_PI, 0, 0);
        bottom.setMaterial(matBackSide);
        screen.attachChild(bottom);

        return screen;
    }
	
	
	private Node createFrontNode()
	{
		// Subdivide front into 3 rows having different numbers of columns:
		//
		// ----------------
		// |--------------|
		// | |          | |
		// | |          | |
		// |--------------|
		// ----------------
		
		// border positions
		float leftMin = 0;
		float leftMax = 0 + borderWidth/size.x;
		float rightMin = 1 - borderWidth/size.x;
		float rightMax = 1;
		
		float bottomMin = 0;
		float bottomMax = 0 + borderHeight/size.y;
		float topMin = 1 - borderHeight/size.y;
		float topMax = 1;
		
		Node frontNode = new Node("front");
		
		Geometry row1Geometry = createGeometry("subGeometry_1", leftMin, rightMax, topMin, topMax);
		row1Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row1Geometry);
        
		Geometry row2col1Geometry = createGeometry("subGeometry_2_1", leftMin, leftMax, bottomMax, topMin);
		row2col1Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row2col1Geometry);
		
		// center
		Geometry row2col2Geometry = createGeometry("subGeometry_2_2", leftMax, rightMin, bottomMax, topMin);
		row2col2Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row2col2Geometry);
		
		Geometry row2col3Geometry = createGeometry("subGeometry_2_3", rightMin, rightMax, bottomMax, topMin);
		row2col3Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row2col3Geometry);
		
		Geometry row3Geometry = createGeometry("subGeometry_3", leftMin, rightMax, bottomMin, bottomMax);
		row3Geometry.setMaterial(matBackSide);
		frontNode.attachChild(row3Geometry);

        return frontNode;
	}

	
	private Geometry createGeometry(String name, float left, float right, float bottom, float top)
	{
        Quad quad = new Quad((right-left) * size.x, (top-bottom) * size.y);
        Geometry geometry = new Geometry(name, quad);
        geometry.setLocalTranslation(left * size.x, bottom * size.y, 0.0f);
        
        return geometry;
	}
}
