/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.messageSign;

import java.util.ArrayList;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;

import eu.opends.dynamicObjects.messageSign.MessageSignTextLine.TextAlignment;

public class MessageSignText
{
	private ArrayList<MessageSignTextLine> lineList = new ArrayList<MessageSignTextLine>();
	private int duration = 0;
	private long startTime = Long.MAX_VALUE;
	
	
	public MessageSignText()
	{
	}
	
	
	public MessageSignText(int duration)
	{
		this.duration = Math.max(0, duration);
	}
	
	
	public void addLine(MessageSignTextLine messageSignTextLine)
	{
		lineList.add(messageSignTextLine);
	}
	
	
	public void addPlainText(String text, int charsPerLine, ColorRGBA color, TextAlignment alignment)
	{
		String line = "";
		
		String[] stringArray = text.split(" ");
		
		for(int i=0; i<stringArray.length; i++)
		{
			line += stringArray[i];
			
			if(i<stringArray.length-1)
			{
				if(line.length() + 1 + stringArray[i+1].length() >= charsPerLine)
				{
					lineList.add(new MessageSignTextLine(line, color, alignment));
					line = "";
				}
				else
					line += " ";
			}
			else
				lineList.add(new MessageSignTextLine(line, color, alignment));
		}
	}
	
	
	public void addSampleText(int noOfLines, int charsPerLine, ColorRGBA color, TextAlignment alignment)
	{
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		for(int i=0; i<noOfLines; i++)
		{
			String text = "";
			for(int j=0; j<charsPerLine; j++)
			{
				int pointer = ((i+j)%(alphabet.length()-1));
				text += alphabet.charAt(pointer);
			}
			lineList.add(new MessageSignTextLine(text, color, alignment));
		}
	}


	public void update(float tpf)
	{
		for(MessageSignTextLine line : lineList)
			line.update(tpf);
	}

	
	public String getText(int index, int maxCharLength)
	{
		if(index < lineList.size())
			return lineList.get(index).getAlignedText(maxCharLength, 0);
		else
			return "";
	}
	
	
	public BitmapText getBitmapText(int index, int maxCharLength, BitmapFont font, 
			float fontSize, ColorRGBA fontColor)
	{
		if(index < lineList.size())
			return lineList.get(index).getBitmapText(maxCharLength, font, fontSize, fontColor);
		else
			return null;
	}


	public ColorRGBA getColor(int index)
	{
		if(index < lineList.size())
			return lineList.get(index).getColor();
		else
			return null;
	}

	
	public void setStarted()
	{
		startTime = System.currentTimeMillis();
	}
	

	public boolean hasElapsed()
	{
		long now = System.currentTimeMillis();
		return (duration > 0 && (now - startTime > duration));
	}



}
