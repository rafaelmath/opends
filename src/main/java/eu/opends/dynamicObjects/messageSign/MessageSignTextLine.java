/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.messageSign;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Spatial.CullHint;


public class MessageSignTextLine
{
	public enum TextAlignment
	{
		Left, Center, Right, Sliding
	}

	private String text = "";
	private ColorRGBA color;
	private TextAlignment alignment;
	private BitmapText bmpText = null;
	private float shiftingUpdateTimer = 0;
	private float flashingUpdateTimer = 0;
	private float shiftingInterval = 0.2f;
	private float flashingInterval = Float.MAX_VALUE;
	
	
	public MessageSignTextLine(String text, ColorRGBA color, TextAlignment alignment)
	{
		if(text != null)
			this.text = text;
		
		this.color = color; // may be null
		
		if(alignment == null)
			this.alignment = TextAlignment.Center;
		else
			this.alignment = alignment;
	}

	
	public void setSlidingSpeed(float lettersPerSecond)
	{
		lettersPerSecond = Math.max(lettersPerSecond, 0.1f);
		shiftingInterval = 1.0f / lettersPerSecond;
	}
	
	
	public void setFlashingInterval(float flashingInterval)
	{
		this.flashingInterval = flashingInterval;
	}

	
	public String getText()
	{
		return text;
	}

	
	public String getAlignedText(int maxCharLength, int shiftedPositions)
	{
		int actualLength = text.length();
		
		if(alignment == TextAlignment.Sliding)
		{
			// slide text from right to left (beginning with empty screen: shiftedPositions == 0)
			int padLeft = maxCharLength - shiftedPositions;
			int stringStart = Math.min(Math.max(0, shiftedPositions-maxCharLength), text.length());
			int stringEnd = Math.min(shiftedPositions, text.length());
			return padWS(padLeft) + text.substring(stringStart, stringEnd);
		}
		else
		{
			if(actualLength > maxCharLength)
			{
				int diff = actualLength - maxCharLength;
				if(alignment == TextAlignment.Left)
				{
					// crop text after last visible position
					return text.substring(0, maxCharLength);
				}
				else if(alignment == TextAlignment.Right)
				{
					// crop text before first visible position
					return text.substring(diff, actualLength);
				}
				else //alignment == TextAlignment.Center
				{
					// crop text at both ends
					// in case of unequal distribution, crop larger part at end
					int frontCropping = diff/2;
					int endCropping = diff - frontCropping;
					return text.substring(frontCropping, actualLength-endCropping);
				}
			}
			else if (actualLength < maxCharLength)
			{
				int diff = maxCharLength - actualLength;
				if(alignment == TextAlignment.Left)
				{
					// insert whitespace after last character
					return text + padWS(diff);
				}
				else if(alignment == TextAlignment.Right)
				{
					// insert whitespace before first character
					return padWS(diff) + text;
				}
				else //alignment == TextAlignment.Center
				{
					// insert whitespace at both ends
					// in case of unequal distribution, insert larger part at end
					int frontPadding = diff/2;
					int endPadding = diff - frontPadding;
					return padWS(frontPadding) + text + padWS(endPadding);
				}
			}
			else
				return text;
		}
	}
	

	public BitmapText getBitmapText(int maxCharLength, BitmapFont font, float fontSize, ColorRGBA defaultColor)
	{
		bmpText = new BitmapText(font);
		bmpText.setUserData("maxCharLength", maxCharLength);
		bmpText.setUserData("shiftedPositions", 0);
		bmpText.setUserData("isVisible", true);
		bmpText.setSize(fontSize);
		bmpText.setText(getAlignedText(maxCharLength, 0));
		
		if(color != null)
			bmpText.setColor(color);
		else
			bmpText.setColor(defaultColor);
		
		return bmpText;
	}
	
	
	public void update(float tpf)
	{
		shiftingUpdateTimer += tpf;
		flashingUpdateTimer += tpf;
		
		// allow max. 1 update per shifting interval
		if(shiftingUpdateTimer > shiftingInterval)
		{
			if(bmpText != null && alignment == TextAlignment.Sliding)
			{
				int maxCharLength = bmpText.getUserData("maxCharLength");
				int shiftedPositions = bmpText.getUserData("shiftedPositions");
				
				// reset slide after one full passage
				if(shiftedPositions > maxCharLength + text.length())
					shiftedPositions = 0;
				else
					shiftedPositions++;
				
				bmpText.setText(getAlignedText(maxCharLength, shiftedPositions));
				bmpText.setUserData("shiftedPositions", shiftedPositions);
			}
			shiftingUpdateTimer = 0;
		}
		
		// allow max. 1 update per flashing interval
		if(flashingUpdateTimer > flashingInterval)
		{
			if(bmpText != null)
			{
				boolean isVisible = bmpText.getUserData("isVisible");
				
				isVisible = !isVisible;
				
				if(isVisible)
					bmpText.setCullHint(CullHint.Inherit);
				else
					bmpText.setCullHint(CullHint.Always);
				
				bmpText.setUserData("isVisible", isVisible);
			}
			flashingUpdateTimer = 0;
		}
	}
	

	private String padWS(int noOfWhiteSpaces)
	{
		String returnString = "";
		
		for(int i=0; i<noOfWhiteSpaces; i++)
		{
			returnString += " ";
		}
		
		return returnString;
	}


	public ColorRGBA getColor()
	{
		return color;
	}


	public TextAlignment getAlignment()
	{
		return alignment;
	}


	
}
