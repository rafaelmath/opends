/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.dynamicObjects.speedIndicator;

import java.util.ArrayList;

import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.material.Materials;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;
import com.jme3.util.BufferUtils;

import eu.opends.basics.SimulationBasics;
import eu.opends.car.Car;
import eu.opends.dynamicObjects.DynamicObject;
import eu.opends.main.Simulator;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.opendrive.util.ODPositionWithOffsets;
import eu.opends.tools.Util;
import eu.opends.traffic.PhysicalTraffic;
import eu.opends.traffic.TrafficObject;


public class SpeedIndicator implements DynamicObject
{
	private SimulationBasics sim;
	private String name;
	private Material[] matDigitRed = new Material[10];
	private Material[] matDigitGreen = new Material[10];
	private Material matSmileyRed, matSmileyGreen;
	private Material matFrontSide, matBackSide, matOff;
	private Geometry digit1Geometry;
	private Geometry digit2Geometry;
	private Geometry smileyGeometry;
	private Node screenNode;
	private Node frontNode;
	private Node centerNode;
	private int maxSpeed;
	private float updateTimer = 0;
	private float maxAngle = 45;
	private float minDist = 2;
	private float maxDist = 80;
	private float minDetectionSpeed = 2;
	
	
	public SpeedIndicator(SimulationBasics sim, String name, int maxSpeed, ColorRGBA backsideColor,
			String language, Boolean addPole, Float maxAngle, Float minDist, Float maxDist, 
			Float minDetectionSpeed)
	{
		this.sim = sim;
		this.name = name;
		this.maxSpeed = maxSpeed;
		
		if(maxAngle != null)
			this.maxAngle = Math.max(0, Math.min(90, maxAngle));
		
		if(minDist != null)
			this.minDist = Math.max(0, minDist);
		
		if(maxDist != null)
			this.maxDist = Math.max(0, maxDist);
		
		if(minDetectionSpeed != null)
			this.minDetectionSpeed = Math.max(0, minDetectionSpeed);
		
		initMaterials(backsideColor, language);

		// create display (aspect ratio given by texture)
		Vector3f size = new Vector3f(1.4f, 1.8f, 0.1f);
		screenNode = createScreenNode(name, size);
		
		if(addPole != null && addPole)
		{
			// create pole
			Spatial pole = sim.getAssetManager().loadModel("Models/RoadSigns/pole/pole.scene");
			pole.setLocalScale(1.8f);
			pole.setLocalTranslation(0.0f, -2.8f, -0.05f);
			screenNode.attachChild(pole);
		}
		
		setupReferencePoints();
		
		sim.getSceneNode().attachChild(screenNode);
	}


	public void setLocalTranslation(Vector3f location)
	{
		screenNode.setLocalTranslation(location);	
	}
	
	
	public void setLocalRotation(Quaternion rotation)
	{
		screenNode.setLocalRotation(rotation);	
	}
	
	
	public void setLocalScale(float scale)
	{
		screenNode.setLocalScale(scale);	
	}

	
	public void setODPosition(ODPositionWithOffsets odPos) 
	{
		String roadID = odPos.getRoadID();
		float s = odPos.getS();
		float lateralOffset = odPos.getLateralOffset();
		float verticalOffset= odPos.getVerticalOffset();
		
		if(Util.isValidOffroadPosition(sim.getOpenDriveCenter(), name + "_screen_node_position", roadID, 
				lateralOffset, verticalOffset, s))
		{
			ODPoint point = sim.getOpenDriveCenter().getRoadMap().get(roadID).getPointOnReferenceLine(s, 
					name + "_screen_node_position");
			Vector3f referencePosition = point.getPosition().toVector3f();
			float ortho = (float)point.getOrtho();
		
			float x = referencePosition.getX() + lateralOffset*FastMath.sin(ortho);
			float z = referencePosition.getZ() + lateralOffset*FastMath.cos(ortho);
			float y = Util.getElevationAt(sim,x,z) + verticalOffset;
			screenNode.setLocalTranslation(new Vector3f(x, y, z));

			// overwrite original rotation with its relative rotation wrt road object
			Quaternion rotation = screenNode.getLocalRotation();	
			float[] angles = new float[3];
			rotation.toAngles(angles);
			screenNode.setLocalRotation((new Quaternion()).fromAngles(angles[0], ortho + angles[1] - FastMath.HALF_PI,
					angles[2]));
		}
		else
			System.err.println("Could not set position of screen node '" + name	+ "' at: " + roadID +
					"/" + s);
	}
	

	public void update(float tpf)
	{
		updateTimer += tpf;
		
		// allow max. 1 update per 750ms
		if(updateTimer > 0.75f)
		{
			// check which car's speed is measured
			// measure only vehicle's speed that is in detection range AND closest
			Car car = getClosestCarInDetectionRange();
			if(car != null)
			{
				int measuredSpeed = (int) car.getCurrentSpeedKmh();

				if(measuredSpeed > maxSpeed)
				{
					setDigits(measuredSpeed, true);
					smileyGeometry.setMaterial(matSmileyRed);
				}
				else
				{
					setDigits(measuredSpeed, false);
					smileyGeometry.setMaterial(matSmileyGreen);
				}
			}
			else
			{
				setDigits(null, false);
				smileyGeometry.setMaterial(matOff);
			}
				
			// reset timer
			updateTimer = 0;
		}
	}
	

	private void setupReferencePoints() 
	{
		// add node in direction of speed measurement
		frontNode = new Node();
		frontNode.setLocalTranslation(0, 0, 1);
		screenNode.attachChild(frontNode);
		
		// add node in origin of speed measurement
		centerNode = new Node();
		centerNode.setLocalTranslation(0, 0, 0);
		screenNode.attachChild(centerNode);
	}
	
	
	private Car getClosestCarInDetectionRange()
	{
		ArrayList<Car> carsInDetectionRangeList = new ArrayList<Car>();
		
		// add all traffic vehicles within the detection range to the list
		for(TrafficObject trafficObject : PhysicalTraffic.getTrafficObjectList())
		{
			if(trafficObject instanceof Car)
			{
				Car car = (Car) trafficObject;
				if(isWithinDetectionRange(car))
					carsInDetectionRangeList.add(car);
			}
		}
		
		// add user controlled vehicle to list if it is within the detection range
		if((sim instanceof Simulator) && isWithinDetectionRange(((Simulator)sim).getCar()))
			carsInDetectionRangeList.add(((Simulator)sim).getCar());
		
		
		Car closestCar = null;
		float distance = Float.MAX_VALUE;
		
		// get closest car
		for(Car car : carsInDetectionRangeList)
		{
			// get distance
			Vector3f centerPos = centerNode.getWorldTranslation();
			Vector3f carPos = car.getPosition();
			
			float currentDist = centerPos.distance(carPos);
			if(currentDist < distance)
			{
				distance = currentDist;
				closestCar = car;
			}
		}
		
		// reset speed indicator if car in focus has changed or if
		// car is moving away from speed indicator
		if(isReset(closestCar, distance))
			closestCar = null;

		return closestCar;
	}


	private Car prevClosestCar = null;
	private float prevDistance = Float.MAX_VALUE;
	private boolean isReset(Car car, float distance)
	{
		boolean isReset = false;
		
		if(car != prevClosestCar)
		{
			// different car has been detected --> reset
			isReset = true;
		}
		else
		{
			// if still same car, check if it has moved away from speed indicator --> reset
			if(distance >= prevDistance)
				isReset = true;
		}
			
		prevClosestCar = car;
		prevDistance = distance;
		
		return isReset;
	}


	private boolean isWithinDetectionRange(Car car)
	{
		// get position of vehicle
		Vector3f carPos = car.getPosition();		
		
		// get angle between measuring direction and direction towards car
		// angle must lie within detection angle to return true
		// only consider 2D space (projection of all three positions to xz-plane)
		Vector3f frontPos = frontNode.getWorldTranslation();
		Vector3f centerPos = centerNode.getWorldTranslation();
		float angle = Util.getAngleBetweenPoints(frontPos, centerPos, carPos, true);
		//System.err.println("ANGLE: " + angle * FastMath.RAD_TO_DEG);
		
		// distance between car and speed indicator
		float distance = centerPos.distance(carPos);
		//System.err.println("DIST: " +  distance);
		
		// speed of car (must be at least minDetectionSpeed, otherwise no detection)
		float speed = car.getCurrentSpeedKmh();
				
		return ((angle < maxAngle * FastMath.DEG_TO_RAD) && (minDist <= distance) && (distance <= maxDist) && 
				(minDetectionSpeed <= speed));
	}


	private void initMaterials(ColorRGBA backsideColor, String language)
	{
		for(int i=0; i<=9; i++)
		{
			matDigitRed[i] = loadMaterial("Textures/SpeedIndicator/" + i + "_red.png");
			matDigitGreen[i] = loadMaterial("Textures/SpeedIndicator/" + i + "_green.png");
		}

		matSmileyRed = loadMaterial("Textures/SpeedIndicator/smiley_red.png");
		matSmileyGreen = loadMaterial("Textures/SpeedIndicator/smiley_green.png");
		
		if(language != null && language.equalsIgnoreCase("DE"))
			matFrontSide = loadMaterial("Textures/SpeedIndicator/main_DE.png");
		else
			matFrontSide = loadMaterial("Textures/SpeedIndicator/main_EN.png");
		
		if(backsideColor == null)
			backsideColor = ColorRGBA.Black;
		
		matBackSide = new Material(sim.getAssetManager(), Materials.LIGHTING);
        matBackSide.setBoolean("UseMaterialColors", true);
        matBackSide.setColor("Diffuse", backsideColor);
        
        matOff = new Material(sim.getAssetManager(), Materials.LIGHTING);
        matOff.setBoolean("UseMaterialColors", true);
        matOff.setColor("Diffuse", ColorRGBA.Black);
	}
	
	
	private void setDigits(Integer measuredSpeed, boolean useRedColor)
	{
		Material digit1mat = matOff;
		Material digit2mat = matOff;
		
		if(measuredSpeed != null)
		{
			// indicator can only display numbers between 0 .. 99
			measuredSpeed = Math.max(Math.min(measuredSpeed, 99),0);
		
			int digit1 = measuredSpeed/10;
			int digit2 = measuredSpeed%10;

			if(useRedColor)
			{
				digit1mat = matDigitRed[digit1];
				digit2mat = matDigitRed[digit2];
			}
			else
			{
				digit1mat = matDigitGreen[digit1];
				digit2mat = matDigitGreen[digit2];
			}
		
			if(digit1 == 0)
				digit1mat = matOff;
		}
		
		digit1Geometry.setMaterial(digit1mat);
		digit2Geometry.setMaterial(digit2mat);
	}


	private Node createScreenNode(String nodeName, Vector3f size)
    {
    	Node screen = new Node(nodeName);

    	// 1. Front
    	Node frontNode = createFrontNode(size);
    	frontNode.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
    	screen.attachChild(frontNode);
    	
    	
        // 2. Back
        Quad backQuad = new Quad(size.x, size.y);
        
        Geometry back = new Geometry("back", backQuad);
        back.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        back.setLocalScale(new Vector3f(-1, 1, 1));
        back.setMaterial(matBackSide);
        screen.attachChild(back);

        
        // 3. Side (right, left)
        Quad sideQuad = new Quad(size.y, size.z);
        
        Geometry right = new Geometry("right", sideQuad);
        right.setLocalTranslation(size.x / 2, -size.y / 2, -size.z / 2);
        right.rotate(0, FastMath.HALF_PI, FastMath.HALF_PI);
        right.setMaterial(matBackSide);
        screen.attachChild(right);

        Geometry left = new Geometry("left", sideQuad);
        left.setLocalTranslation(-size.x / 2, -size.y / 2, size.z / 2);
        left.rotate(0, -FastMath.HALF_PI, FastMath.HALF_PI);
        left.setMaterial(matBackSide);
        screen.attachChild(left);

        
        // 4. Side (top, bottom)
        sideQuad = new Quad(size.x, size.z);

        Geometry top = new Geometry("top", sideQuad);
        top.setLocalTranslation(-size.x / 2, size.y / 2, size.z / 2);
        top.rotate(-FastMath.HALF_PI, 0, 0);
        top.setMaterial(matBackSide);
        screen.attachChild(top);

        Geometry bottom = new Geometry("bottom", sideQuad);
        bottom.setLocalTranslation(-size.x / 2, -size.y / 2, -size.z / 2);
        bottom.rotate(FastMath.HALF_PI, 0, 0);
        bottom.setMaterial(matBackSide);
        screen.attachChild(bottom);

        return screen;
    }


	private Node createFrontNode(Vector3f size)
	{
		// Subdivide front into 5 rows having different numbers of columns:
		//
		// ----------------  1.0
		// |              |
		// |--------------|  0.8
		// |   |  ||  |   |
		// |   |  ||  |   |
		// |--------------|  0.5
		// |--------------|  0.4
		// |    |    |    |
		// |    |    |    |
		// |--------------|  0.1
		// ----------------  0.0
		// 0.0          1.0
		
		Node frontNode = new Node("front");
		
		Geometry row1Geometry = createGeometry("subGeometry_1", size, 0.0f, 1.0f, 0.8f, 1.0f, false);
		row1Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row1Geometry);
        
		Geometry row2col1Geometry = createGeometry("subGeometry_2_1", size, 0.0f, 0.24f, 0.45f, 0.8f, false);
		row2col1Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row2col1Geometry);
		
		// speed indicator (1st digit) geometry
		Geometry row2col2Geometry = createGeometry("subGeometry_2_2", size, 0.24f, 0.49f, 0.45f, 0.8f, true);
		digit1Geometry = row2col2Geometry;
		digit1Geometry.setMaterial(matOff);
		frontNode.attachChild(row2col2Geometry);
		
		Geometry row2col3Geometry = createGeometry("subGeometry_2_3", size, 0.49f, 0.51f, 0.45f, 0.8f, false);
		row2col3Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row2col3Geometry);
		
		// speed indicator (2nd digit) geometry
		Geometry row2col4Geometry = createGeometry("subGeometry_2_4", size, 0.51f, 0.76f, 0.45f, 0.8f, true);
		digit2Geometry = row2col4Geometry;
		digit2Geometry.setMaterial(matOff);
		frontNode.attachChild(row2col4Geometry);
		
		Geometry row2col5Geometry = createGeometry("subGeometry_2_5", size, 0.76f, 1.0f, 0.45f, 0.8f, false);
		row2col5Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row2col5Geometry);
		
		Geometry row3Geometry = createGeometry("subGeometry_3", size, 0.0f, 1.0f, 0.32f, 0.45f, false);
		row3Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row3Geometry);
		
		Geometry row4col1Geometry = createGeometry("subGeometry_4_1", size, 0.0f, 0.37f, 0.1f, 0.32f, false);
		row4col1Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row4col1Geometry);
		
		// smiley geometry
		Geometry row4col2Geometry = createGeometry("subGeometry_4_2", size, 0.37f, 0.63f, 0.1f, 0.32f, true);
		smileyGeometry = row4col2Geometry;
		smileyGeometry.setMaterial(matOff);
		frontNode.attachChild(row4col2Geometry);
		
		Geometry row4col3Geometry = createGeometry("subGeometry_4_3", size, 0.63f, 1.0f, 0.1f, 0.32f, false);
		row4col3Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row4col3Geometry);
		
		Geometry row5Geometry = createGeometry("subGeometry_5", size, 0.0f, 1.0f, 0.0f, 0.1f, false);
		row5Geometry.setMaterial(matFrontSide);
		frontNode.attachChild(row5Geometry);
		
        return frontNode;
	}
        
	
	private Geometry createGeometry(String name, Vector3f size, float left, float right, float bottom, 
			float top, boolean fullTexture)
	{
        Quad quad = new Quad((right-left) * size.x, (top-bottom) * size.y);
        
        Vector2f[] texCoords;
        if(fullTexture)
        {
        	texCoords = new Vector2f[]
        	{
        		new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
        	};
        }
        else
        {
        	texCoords = new Vector2f[]
        	{
        		new Vector2f(left, bottom),
        		new Vector2f(right, bottom),
        		new Vector2f(right, top),
        		new Vector2f(left, top)
        	};
        }
        
        quad.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoords));
        Geometry geometry = new Geometry(name, quad);
        geometry.setLocalTranslation(left * size.x, bottom * size.y, 0.0f);
        
        return geometry;
	}
	
	
	private Material loadMaterial(String texturePath)
	{
		Material material = null;
		
		try{
			TextureKey textureKey = new TextureKey(texturePath, true);
			Texture texture = sim.getAssetManager().loadTexture(textureKey);
			material = new Material(sim.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
			material.setTexture("ColorMap",texture);
			
		} catch (Exception e){
			e.printStackTrace();
			System.err.println("Error loading texture file " + texturePath);
		}
		
	    return material;
	}
}
