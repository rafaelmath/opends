/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.tools.visualOutput;

import java.util.ArrayList;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;

import eu.opends.main.Simulator;

public class OnScreenVisualizer
{
	private Simulator sim;
	//private MotorCortexVisualizer motorCortexVisualizer;
	//private ParameterVisualizer parameterVisualizer1;
	//private ParameterVisualizer parameterVisualizer2;
	
	private ParameterVisualizer gazeSGEVisualizer;
	public ParameterVisualizer getGazeSGEVisualizer()
	{
		return gazeSGEVisualizer;
	}
	
	private ParameterVisualizer mwlEEGVisualizer;
	public ParameterVisualizer getMwlEEGVisualizer()
	{
		return mwlEEGVisualizer;
	}
	
	private ParameterVisualizer class2SecVisualizer;
	public ParameterVisualizer getClass2SecVisualizer()
	{
		return class2SecVisualizer;
	}
	
	private ParameterVisualizer class4SecVisualizer;
	private ParameterVisualizer class8SecVisualizer;
	private int counter = 0;

	private ParameterVisualizer gazeSGEResultVisualizer1, gazeSGEResultVisualizer2, mwlEEGResultVisualizer1, 
		mwlEEGResultVisualizer2, class2SecResultVisualizer1, class2SecResultVisualizer2,
		class4SecResultVisualizer1, class4SecResultVisualizer2, class8SecResultVisualizer1, class8SecResultVisualizer2;
	
	
	public OnScreenVisualizer(Simulator sim)
	{
		this.sim = sim;
		//motorCortexVisualizer = new MotorCortexVisualizer(sim);
		
		ColorRGBA semiTransparent = new ColorRGBA(0, 0, 0, 0.2f);
		
		/*
		parameterVisualizer1 = new ParameterVisualizer(sim, 0, 150, 500, 100, 700, 600, ColorRGBA.Blue, KeyInput.KEY_N, 1, semiTransparent);
		
		// add grid lines
		ArrayList<GridLine> horizontalLinesList1 = new ArrayList<GridLine>();
        horizontalLinesList1.add(new GridLine(0f, ColorRGBA.Black, 1));
        horizontalLinesList1.add(new GridLine(20f, ColorRGBA.Black, 1));
        horizontalLinesList1.add(new GridLine(40f, ColorRGBA.Black, 1));
        horizontalLinesList1.add(new GridLine(60f, ColorRGBA.Red, 1));
        horizontalLinesList1.add(new GridLine(80f, ColorRGBA.Black, 1));
        horizontalLinesList1.add(new GridLine(99f, ColorRGBA.Black, 1));
        parameterVisualizer1.addHorizontalGridLines(horizontalLinesList1);
        //parameterVisualizer1.addHorizontalGridLines(10);
		parameterVisualizer1.addVerticalGridLines(10);
		
		ArrayList<ColorChange> colorChangeList1 = new ArrayList<ColorChange>();
		//colorChangeList1.add(new ColorChange(0f, ColorRGBA.Blue));
		//colorChangeList1.add(new ColorChange(40f, ColorRGBA.White));
		colorChangeList1.add(new ColorChange(60f, ColorRGBA.Red));
		parameterVisualizer1.addColorChange(colorChangeList1);
		*/
		
		
		/*
		parameterVisualizer2 = new ParameterVisualizer(sim, 0, 150, 500, 100, 700, 600, ColorRGBA.Yellow, KeyInput.KEY_N, 1, semiTransparent);
		
		// add grid lines
		ArrayList<GridLine> horizontalLinesList2 = new ArrayList<GridLine>();
        horizontalLinesList2.add(new GridLine(0f, ColorRGBA.Black, 1));
        horizontalLinesList2.add(new GridLine(20f, ColorRGBA.Black, 1));
        horizontalLinesList2.add(new GridLine(40f, ColorRGBA.Black, 1));
        horizontalLinesList2.add(new GridLine(60f, ColorRGBA.Red, 1));
        horizontalLinesList2.add(new GridLine(80f, ColorRGBA.Black, 1));
        horizontalLinesList2.add(new GridLine(99f, ColorRGBA.Black, 1));
        parameterVisualizer2.addHorizontalGridLines(horizontalLinesList2);
        //parameterVisualizer2.addHorizontalGridLines(10);
		parameterVisualizer2.addVerticalGridLines(10);
		
		ArrayList<ColorChange> colorChangeList2 = new ArrayList<ColorChange>();
		//colorChangeList2.add(new ColorChange(0f, ColorRGBA.Blue));
		//colorChangeList2.add(new ColorChange(40f, ColorRGBA.White));
		colorChangeList2.add(new ColorChange(60f, ColorRGBA.Red));
		parameterVisualizer2.addColorChange(colorChangeList2);
		*/
		
		
		
		gazeSGEVisualizer = new ParameterVisualizer("gazeSGEVisualizer", sim, 0.0f, 5.0f, 500, 100, 100, 600, ColorRGBA.Cyan, KeyInput.KEY_N, 1, semiTransparent, 350);
		gazeSGEVisualizer.setLabel("SGE gaze", 15, 662, ColorRGBA.Cyan);
		gazeSGEVisualizer.addHorizontalGridLines(6);
		gazeSGEVisualizer.addVerticalGridLines(2);
		
		mwlEEGVisualizer = new ParameterVisualizer("mwlEEGVisualizer", sim, 0.0f, 1.0f, 500, 100, 100, 400, ColorRGBA.Cyan, KeyInput.KEY_N, 1, semiTransparent, 350);
		mwlEEGVisualizer.setLabel("MWL EEG", 15, 462, ColorRGBA.Cyan);
		mwlEEGVisualizer.addHorizontalGridLines(6);
		mwlEEGVisualizer.addVerticalGridLines(2);
		
		class8SecVisualizer = new ParameterVisualizer("class8SecVisualizer", sim, 0.0f, 1.0f, 500, 100, 100, 200, ColorRGBA.Blue, KeyInput.KEY_N, 1, ColorRGBA.BlackNoAlpha, 350);
		class8SecVisualizer.setLabel("8 seconds", 15, 292, ColorRGBA.Blue);

		class4SecVisualizer = new ParameterVisualizer("class4SecVisualizer", sim, 0.0f, 1.0f, 500, 100, 100, 200, ColorRGBA.Green, KeyInput.KEY_N, 1, ColorRGBA.BlackNoAlpha, 350);
		class4SecVisualizer.setLabel("4 seconds", 15, 262, ColorRGBA.Green);
		
		class2SecVisualizer = new ParameterVisualizer("class2SecVisualizer", sim, 0.0f, 1.0f, 500, 100, 100, 200, ColorRGBA.Yellow, KeyInput.KEY_N, 1, semiTransparent, 350);
		class2SecVisualizer.setLabel("2 seconds", 15, 232, ColorRGBA.Yellow);
		class2SecVisualizer.addHorizontalGridLines(6);
		class2SecVisualizer.addVerticalGridLines(2);
		
		
		// Result visualizers (1st column)
		gazeSGEResultVisualizer1 = new ParameterVisualizer("gazeSGEResultVisualizer1", sim, 0.0f, 5.0f, 250, 100, 700, 600, ColorRGBA.Cyan, KeyInput.KEY_M, 1, semiTransparent, 250);
		gazeSGEResultVisualizer1.setPaused(true);
		gazeSGEResultVisualizer1.addHorizontalGridLines(6);
		gazeSGEResultVisualizer1.addVerticalGridLines(2);
		
		mwlEEGResultVisualizer1 = new ParameterVisualizer("mwlEEGResultVisualizer1", sim, 0.0f, 1.0f, 250, 100, 700, 400, ColorRGBA.Cyan, KeyInput.KEY_M, 1, semiTransparent, 250);
		mwlEEGResultVisualizer1.setPaused(true);
		mwlEEGResultVisualizer1.addHorizontalGridLines(6);
		mwlEEGResultVisualizer1.addVerticalGridLines(2);
		
		class8SecResultVisualizer1 = new ParameterVisualizer("class8SecResultVisualizer1", sim, 0.0f, 1.0f, 250, 100, 700, 200, ColorRGBA.Blue, KeyInput.KEY_M, 1, ColorRGBA.BlackNoAlpha, 250);
		class8SecResultVisualizer1.setPaused(true);
		
		class4SecResultVisualizer1 = new ParameterVisualizer("class4SecResultVisualizer1", sim, 0.0f, 1.0f, 250, 100, 700, 200, ColorRGBA.Green, KeyInput.KEY_M, 1, ColorRGBA.BlackNoAlpha, 250);
		class4SecResultVisualizer1.setPaused(true);
		
		class2SecResultVisualizer1 = new ParameterVisualizer("class2SecResultVisualizer1", sim, 0.0f, 1.0f, 250, 100, 700, 200, ColorRGBA.Yellow, KeyInput.KEY_M, 1, semiTransparent, 250);
		class2SecResultVisualizer1.setPaused(true);
		class2SecResultVisualizer1.addHorizontalGridLines(6);
		class2SecResultVisualizer1.addVerticalGridLines(2);
		
		
		// Result visualizers (2nd column)
		gazeSGEResultVisualizer2 = new ParameterVisualizer("gazeSGEResultVisualizer2", sim, 0.0f, 5.0f, 250, 100, 1000, 600, ColorRGBA.Cyan, KeyInput.KEY_M, 1, semiTransparent, 250);
		gazeSGEResultVisualizer2.setPaused(true);
		gazeSGEResultVisualizer2.addHorizontalGridLines(6);
		gazeSGEResultVisualizer2.addVerticalGridLines(2);
		
		mwlEEGResultVisualizer2 = new ParameterVisualizer("mwlEEGResultVisualizer2", sim, 0.0f, 1.0f, 250, 100, 1000, 400, ColorRGBA.Cyan, KeyInput.KEY_M, 1, semiTransparent, 250);
		mwlEEGResultVisualizer2.setPaused(true);
		mwlEEGResultVisualizer2.addHorizontalGridLines(6);
		mwlEEGResultVisualizer2.addVerticalGridLines(2);
		
		class8SecResultVisualizer2 = new ParameterVisualizer("class8SecResultVisualizer2", sim, 0.0f, 1.0f, 250, 100, 1000, 200, ColorRGBA.Blue, KeyInput.KEY_M, 1, ColorRGBA.BlackNoAlpha, 250);
		class8SecResultVisualizer2.setPaused(true);
		
		class4SecResultVisualizer2 = new ParameterVisualizer("class4SecResultVisualizer2", sim, 0.0f, 1.0f, 250, 100, 1000, 200, ColorRGBA.Green, KeyInput.KEY_M, 1, ColorRGBA.BlackNoAlpha, 250);
		class4SecResultVisualizer2.setPaused(true);
		
		class2SecResultVisualizer2 = new ParameterVisualizer("class2SecResultVisualizer2", sim, 0.0f, 1.0f, 250, 100, 1000, 200, ColorRGBA.Yellow, KeyInput.KEY_M, 1, semiTransparent, 250);
		class2SecResultVisualizer2.setPaused(true);
		class2SecResultVisualizer2.addHorizontalGridLines(6);
		class2SecResultVisualizer2.addVerticalGridLines(2);
	}

	public float[]  myColorMap(double sal){

		float[] RGB = new float[3];
		float r = 0, g = 0, b = 0;

		r = (float)Math.min(1,1.5*Math.sqrt(1-sal));
		g = (float)Math.min(1,1.5*Math.sqrt(sal));

		RGB[0]=r;
		RGB[1]=g;
		RGB[2]=b;

		return RGB;
	}

	
	private float updateTimer = 0;
	private float updateInterval = 0.1f;
	public void update(float tpf)
	{
		updateTimer += tpf;
		
		
        float[][][] array = new float[41][41][3];
        float[] colors;
        double[] motorCortexFlat = new double[1681];
        int[] index = new int[2];

        // get flatten motor cortex
		sim.getCodriverConnector().getMotorCortex(motorCortexFlat);

		// get winning index
		sim.getCodriverConnector().getWinningIndex(index);

		// normalize motorCortexFlat for displaying taking care for sign
		double maxMC = -10., minMC = 10.;
		for(int i=0; i<motorCortexFlat.length; i++){
			if(motorCortexFlat[i] < minMC) minMC = motorCortexFlat[i];
			if(motorCortexFlat[i] > maxMC) maxMC = motorCortexFlat[i];
		}

		if(minMC < 0.) {
			for (int i = 0; i < motorCortexFlat.length; i++) {
				motorCortexFlat[i] = 1 - (motorCortexFlat[i] - minMC) / (maxMC - minMC);
			}
		}
		else {
			for (int i = 0; i < motorCortexFlat.length; i++) {
				motorCortexFlat[i] = (motorCortexFlat[i] - minMC) / (maxMC - minMC);
			}
		}


        for(int row = 0; row < 41; row++)
        	for(int col = 0; col < 41; col++){
				colors = myColorMap(motorCortexFlat[row*41+col]);
				array[row][40-col][0] = colors[0];
				array[row][40-col][1] = colors[1];
				array[row][40-col][2] = colors[2];
			}

		array[index[0]][40-index[1]][0] = 0;
		array[index[0]][40-index[1]][1] = 0;
		array[index[0]][40-index[1]][2] = 0;

        // motorCortexVisualizer.setColorBuffer(array, 0.8f);
        
        // allow 1 update per update interval
        if(updateTimer > updateInterval)
        {
        	while(updateTimer > updateInterval)
        		updateTimer -= updateInterval;
        	
        	counter++;
        	
			//motorCortexVisualizer.setColorBuffer(array, 0.75f);
        	//parameterVisualizer1.addValue(sim.getCar().getCurrentSpeedKmhRounded());
        	//parameterVisualizer2.addValue(sim.getCar().getCurrentSpeedKmhRounded()+10);
        	gazeSGEVisualizer.addValue(gazeSGE);
        	mwlEEGVisualizer.addValue(mwlEEG);
        	class2SecVisualizer.addValue(class2Sec);
        	class4SecVisualizer.addValue(class4Sec);
        	class8SecVisualizer.addValue(class8Sec);
        	
        	/*
        	if(counter == 10) {
        		parameterVisualizer1.addVerticalTarget(30, ColorRGBA.Green);
        		parameterVisualizer2.addVerticalTarget(45, ColorRGBA.Red);
        		System.err.println("DONE");
        	}
        	
        	if(counter == 40) {
        		parameterVisualizer1.addVerticalTarget(300, ColorRGBA.Green);
        		parameterVisualizer2.addVerticalTarget(450, ColorRGBA.Red);
        		System.err.println("DONE2");
        	}
        	
        	if(counter == 80) {
        		parameterVisualizer1.addVerticalTarget(500, ColorRGBA.Green);
        		parameterVisualizer2.addVerticalTarget(600, ColorRGBA.Red);
        		System.err.println("DONE");
        	}
        	 */
        	
        	/*
        	if(counter == 300)
        	{
        		System.err.println("300: " + System.currentTimeMillis());
        		gazeSGEVisualizer.addVerticalTarget(50, ColorRGBA.Green);
        	}
        	
        	if(counter == 350)
        	{
        		System.err.println("350: " + System.currentTimeMillis());
        	}
        	*/
        	
        	/*
        	if(counter == 100)
        	{
        		parameterVisualizer1.addVerticalTarget(0, ColorRGBA.Red);
        	}
        	
        	if(counter == 200)
        	{
        		parameterVisualizer1.addVerticalTarget(0, ColorRGBA.Green);
        	}
        	
        	if(counter == 300)
        	{
        		parameterVisualizer1.addVerticalTarget(0, ColorRGBA.Blue);
        	}
        	
        	if(counter == 310)
        	{
        		System.err.println("EXPORT 1");
        		snapshotGraphs();
        	}
        	
        	if(counter == 400)
        	{
        		parameterVisualizer1.addVerticalTarget(0, ColorRGBA.White);
        	}
        	
        	if(counter == 500)
        	{
        		parameterVisualizer1.addVerticalTarget(0, ColorRGBA.Brown);
        	}
        	
        	if(counter == 610)
        	{
        		System.err.println("EXPORT 2");
        		snapshotGraphs();
        	}
        	
        	if(counter == 700)
        	{
        		System.err.println("IMPORT");
        		showSnapshots();
        	}
        
        	
        	if(counter == 700)
        	{
        		System.err.println("IMPORT");
        		showSnapshots();
        	}
        	*/
        	
        	showSnapshots();
        }
	}
	
	
	private ArrayList<Graph> gazeSGEVisualizerGraphList = new ArrayList<Graph>();
	private ArrayList<Graph> mwlEEGVisualizerGraphList = new ArrayList<Graph>();
	private ArrayList<Graph> class2SecVisualizerGraphList = new ArrayList<Graph>();
	private ArrayList<Graph> class4SecVisualizerGraphList = new ArrayList<Graph>();
	private ArrayList<Graph> class8SecVisualizerGraphList = new ArrayList<Graph>();
	
	
	public void snapshotGraphs()
	{
		gazeSGEVisualizerGraphList.add(gazeSGEVisualizer.exportGraph());
		mwlEEGVisualizerGraphList.add(mwlEEGVisualizer.exportGraph());
		class2SecVisualizerGraphList.add(class2SecVisualizer.exportGraph());
		class4SecVisualizerGraphList.add(class4SecVisualizer.exportGraph());
		class8SecVisualizerGraphList.add(class8SecVisualizer.exportGraph());
	}
	
	
	public void showSnapshots()
	{
		if(gazeSGEVisualizerGraphList.size()>=1)
			gazeSGEResultVisualizer1.importGraph(gazeSGEVisualizerGraphList.get(0));
		
		if(gazeSGEVisualizerGraphList.size()>=2)
			gazeSGEResultVisualizer2.importGraph(gazeSGEVisualizerGraphList.get(1));
		
		if(mwlEEGVisualizerGraphList.size()>=1)
			mwlEEGResultVisualizer1.importGraph(mwlEEGVisualizerGraphList.get(0));
		
		if(mwlEEGVisualizerGraphList.size()>=2)
			mwlEEGResultVisualizer2.importGraph(mwlEEGVisualizerGraphList.get(1));
		
		if(class2SecVisualizerGraphList.size()>=1)
			class2SecResultVisualizer1.importGraph(class2SecVisualizerGraphList.get(0));
		
		if(class2SecVisualizerGraphList.size()>=2)
			class2SecResultVisualizer2.importGraph(class2SecVisualizerGraphList.get(1));
		
		if(class4SecVisualizerGraphList.size()>=1)
			class4SecResultVisualizer1.importGraph(class4SecVisualizerGraphList.get(0));
		
		if(class4SecVisualizerGraphList.size()>=2)
			class4SecResultVisualizer2.importGraph(class4SecVisualizerGraphList.get(1));
		
		if(class8SecVisualizerGraphList.size()>=1)
			class8SecResultVisualizer1.importGraph(class8SecVisualizerGraphList.get(0));
		
		if(class8SecVisualizerGraphList.size()>=2)
			class8SecResultVisualizer2.importGraph(class8SecVisualizerGraphList.get(1));
	}
	
	private float gazeSGE = 0;
	private float mwlEEG = 0;
	private float class2Sec = 0.9f;
	private float class4Sec = 0.2f;
	private float class8Sec = 0.5f;
	
	
	public void setGazeSGE(float gazeSGE)
	{
		this.gazeSGE = gazeSGE;
	}

	public void setMwlEEG(float mwlEEG)
	{
		this.mwlEEG = mwlEEG;
	}

	public void setClass2Sec(float class2Sec)
	{
		this.class2Sec = class2Sec;
	}
	
	public float getClass2Sec()
	{
		return class2Sec;
	}
	
	public void setClass4Sec(float class4Sec)
	{
		this.class4Sec = class4Sec;
	}
	
	public float getClass4Sec()
	{
		return class4Sec;
	}
	
	public void setClass8Sec(float class8Sec)
	{
		this.class8Sec = class8Sec;
	}

	public float getClass8Sec()
	{
		return class8Sec;
	}

}
