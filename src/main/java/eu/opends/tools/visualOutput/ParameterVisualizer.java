/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.tools.visualOutput;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;

import eu.opends.main.Simulator;
import eu.opends.tools.Util;

public class ParameterVisualizer
{
	public static final String PARAMETER_VISUALIZER_TOGGLE = "ParameterVisualizerToggle";
    public static final int DEFAULT_KEY = KeyInput.KEY_N;
    public static final int DEFAULT_WIDTH = 500;
    public static final int DEFAULT_HEIGHT = 100;
    public static final int DEFAULT_POS_X = 700;
    public static final int DEFAULT_POS_Y = 500;
    public static final ColorRGBA DEFAULT_COLOR = ColorRGBA.Blue;
    public static final float DEFAULT_HORIZONTAL_SCALE = 1.0f;
    public static final ColorRGBA DEFAULT_BACKGROUND_COLOR = new ColorRGBA(0,0,0, 0.2f);
    public static final int DEFAULT_ACTIVE_WIDTH = 350;

    private final KeyListener keyListener = new KeyListener();
    private String name;
    private Simulator sim;
	private float[] array;
	private ColorRGBA[] verticalTargetsArray;
	private int index = 0;
	private Mesh backgroundMesh = new Mesh();
	private Geometry backgroundGeometry;
    private Mesh horizontalGridLinesMesh = new Mesh();
    private Geometry horizontalGridLinesGeometry;
    private Mesh verticalGridLinesMesh = new Mesh();
    private Geometry verticalGridLinesGeometry;
    private Mesh verticalTargetsMesh = new Mesh();
    private Geometry verticalTargetsGeometry;
    private Mesh graphMesh = new Mesh();
    private Geometry graphGeometry;
    private float minValue;
    private float maxValue;
    private int width;
    private int height;
    private ColorRGBA graphColor;
    private int rows;
    private int cols;
    private int activeCols;
    private ArrayList<ColorChange> colorChangeList = null;
    private ColorRGBA forceColor = null;
    private boolean isVisible = false;
    private BitmapText label = null;
    private boolean isPause = false;
    
    
	public ParameterVisualizer(String name, Simulator sim, float minValue, float maxValue)
	{
		this(name, sim, minValue, maxValue, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
		
	
	public ParameterVisualizer(String name, Simulator sim, float minValue, float maxValue, int width, int height)
	{
		this(name, sim, minValue, maxValue, width, height, DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_COLOR, 
				DEFAULT_KEY, DEFAULT_HORIZONTAL_SCALE, DEFAULT_BACKGROUND_COLOR, DEFAULT_ACTIVE_WIDTH);
	}
	
	
	public ParameterVisualizer(String name, Simulator sim, float minValue, float maxValue, int width, int height, 
			float posX, float posY, ColorRGBA graphColor, Integer key, float horizontalScale,
			ColorRGBA backgroundColor, int activeWidth)
	{
		this.name = name;
		this.sim = sim;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.width = width;
		this.height = height;
		this.graphColor = graphColor;
		
        this.rows = 3;
        this.cols = width;
        this.activeCols = Math.min(width, activeWidth); //width-150;
        
        Material mat = new Material(sim.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setBoolean("VertexColor", true);
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        
        // prepare geometry to visualize background
        backgroundGeometry = new Geometry("backgroundGeometry", backgroundMesh);
        backgroundGeometry.setMaterial(mat);
        backgroundGeometry.setLocalTranslation(posX, posY, 0);
        backgroundGeometry.setLocalScale(horizontalScale, 1, 1);
        initBackgroundMesh(backgroundColor);
        
        // prepare geometry to visualize horizontal grid lines
        horizontalGridLinesGeometry = new Geometry("horizontalGridLinesGeometry", horizontalGridLinesMesh);
        horizontalGridLinesGeometry.setMaterial(mat);
        horizontalGridLinesGeometry.setLocalTranslation(posX, posY, 0);
        horizontalGridLinesGeometry.setLocalScale(horizontalScale, 1, 1);
        
        // prepare geometry to visualize vertical grid lines
        verticalGridLinesGeometry = new Geometry("verticalGridLinesGeometry", verticalGridLinesMesh);
        verticalGridLinesGeometry.setMaterial(mat);
        verticalGridLinesGeometry.setLocalTranslation(posX, posY, 0);
        verticalGridLinesGeometry.setLocalScale(horizontalScale, 1, 1);
        
        // prepare geometry to visualize vertical targets
        verticalTargetsGeometry = new Geometry("verticalTargetsGeometry", verticalTargetsMesh);
        verticalTargetsGeometry.setMaterial(mat);
        verticalTargetsGeometry.setLocalTranslation(posX, posY, 0);
        verticalTargetsGeometry.setLocalScale(horizontalScale, 1, 1);
        initVerticalTargetsMesh();
        
        graphGeometry = new Geometry("graphGeometry", graphMesh);
        graphGeometry.setMaterial(mat);
        graphGeometry.setLocalTranslation(posX, posY, 0);
        graphGeometry.setLocalScale(horizontalScale, 1, 1);
        initGraphMesh();
        
        InputManager inputManager = sim.getInputManager();        
        if(inputManager != null && key != null)
        { 
            inputManager.addMapping(PARAMETER_VISUALIZER_TOGGLE + "_" + name, new KeyTrigger(key));
            inputManager.addListener(keyListener, PARAMETER_VISUALIZER_TOGGLE + "_" + name); 
        }      
	}
    
	
    private void initBackgroundMesh(ColorRGBA bgColor)
    {
    	// init position buffer
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(4 * 3);
        positionBuf.put(0).put(0).put(0);
        positionBuf.put(width).put(0).put(0);
        positionBuf.put(width).put(height).put(0);
        positionBuf.put(0).put(height).put(0);      
        backgroundMesh.setBuffer(Type.Position, 3, positionBuf);
        
        // init color buffer        
        FloatBuffer colorBuf = BufferUtils.createFloatBuffer(4 * 4);
		for(int i = 0; i < 4; i++)
			colorBuf.put(bgColor.r).put(bgColor.g).put(bgColor.b).put(bgColor.a);
		backgroundMesh.setBuffer(Type.Color, 4, colorBuf);
        
        // init index buffer
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(2 * 3);
        indexBuf.put((short) 0).put((short) 1).put((short) 2);
        indexBuf.put((short) 0).put((short) 2).put((short) 3);
        backgroundMesh.setBuffer(Type.Index, 3, indexBuf);
    }
    
    
    public void addHorizontalGridLines(ArrayList<GridLine> linesList)
    {
    	ArrayList<GridLine> filteredLinesList = new ArrayList<GridLine>();
    	for(GridLine line : linesList) 
        {
    		if(minValue <= line.getPosition() && line.getPosition() <= maxValue)
    			filteredLinesList.add(line);
        }
    	
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(filteredLinesList.size() * 4 * 3);
        FloatBuffer colorBuf = BufferUtils.createFloatBuffer(filteredLinesList.size() * 4 * 4);
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(filteredLinesList.size() * 4 * 3);
        
        for(int i = 0; i < filteredLinesList.size(); i++) 
        {
        	float val = filteredLinesList.get(i).getPosition();
        	ColorRGBA color = filteredLinesList.get(i).getColor();
        	int strength = filteredLinesList.get(i).getStrength();

       		int h = (int) Util.map(val, minValue, maxValue, 0, height);
        	
       		// init position buffer
       		positionBuf.put(0).put(h).put(0);
       		positionBuf.put(width).put(h).put(0);
       		positionBuf.put(width).put(h+strength).put(0);
       		positionBuf.put(0).put(h+strength).put(0);
        	
       		// init color buffer
       		for(int j = 0; j < 4; j++)
       			colorBuf.put(color.r).put(color.g).put(color.b).put(0.3f); 
			
       		// init index buffer
       		indexBuf.put((short) (i*4)).put((short) (i*4+1)).put((short) (i*4+2));
       		indexBuf.put((short) (i*4)).put((short) (i*4+2)).put((short) (i*4+3));
        } 
        
        horizontalGridLinesMesh.setBuffer(Type.Position, 3, positionBuf);
		horizontalGridLinesMesh.setBuffer(Type.Color, 4, colorBuf);
        horizontalGridLinesMesh.setBuffer(Type.Index, 3, indexBuf);
    }
    
    
    public void addHorizontalGridLines(int noOfHorizontalGridLines)
    {
    	int h = height / (noOfHorizontalGridLines-1);
    	
    	// init position buffer
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(noOfHorizontalGridLines * 4 * 3);
        for(int i = 0; i < noOfHorizontalGridLines; i++) 
        {
        	positionBuf.put(0).put(i*h).put(0);
        	positionBuf.put(width).put(i*h).put(0);
        	positionBuf.put(width).put(i*h+1).put(0);
        	positionBuf.put(0).put(i*h+1).put(0);
        }
        horizontalGridLinesMesh.setBuffer(Type.Position, 3, positionBuf);

        
        // init color buffer
		FloatBuffer colorBuf = BufferUtils.createFloatBuffer(noOfHorizontalGridLines * 4 * 4);
		for(int col = 0; col < noOfHorizontalGridLines; col++)
			for(int i = 0; i < 4; i++)
				colorBuf.put(0).put(0).put(0).put(0.3f);        
		horizontalGridLinesMesh.setBuffer(Type.Color, 4, colorBuf);
        
        
        // init index buffer
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(noOfHorizontalGridLines * 4 * 3);
        for(int i = 0; i < noOfHorizontalGridLines; i++) 
        {
        	indexBuf.put((short) (i*4)).put((short) (i*4+1)).put((short) (i*4+2));
        	indexBuf.put((short) (i*4)).put((short) (i*4+2)).put((short) (i*4+3));
        }         
        horizontalGridLinesMesh.setBuffer(Type.Index, 3, indexBuf);
    }

    
    public void addVerticalGridLines(int noOfVerticalGridLines)
    {
    	int w = width / (noOfVerticalGridLines-1);
    	
    	// init position buffer
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(noOfVerticalGridLines * 4 * 3);
        for(int i = 0; i < noOfVerticalGridLines; i++) 
        {
        	positionBuf.put(i*w).put(0).put(0);
        	positionBuf.put(i*w+1).put(0).put(0);
        	positionBuf.put(i*w+1).put(height).put(0);
        	positionBuf.put(i*w).put(height).put(0);
        }
        verticalGridLinesMesh.setBuffer(Type.Position, 3, positionBuf);

        
        // init color buffer
		FloatBuffer colorBuf = BufferUtils.createFloatBuffer(noOfVerticalGridLines * 4 * 4);
		for(int col = 0; col < noOfVerticalGridLines; col++)
			for(int i = 0; i < 4; i++)
				colorBuf.put(0).put(0).put(0).put(0.3f);        
		verticalGridLinesMesh.setBuffer(Type.Color, 4, colorBuf);
        
        
        // init index buffer
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(noOfVerticalGridLines * 4 * 3);
        for(int i = 0; i < noOfVerticalGridLines; i++) 
        {
        	indexBuf.put((short) (i*4)).put((short) (i*4+1)).put((short) (i*4+2));
        	indexBuf.put((short) (i*4)).put((short) (i*4+2)).put((short) (i*4+3));
        }         
        verticalGridLinesMesh.setBuffer(Type.Index, 3, indexBuf);
    }
    

	public void addColorChange(ArrayList<ColorChange> colorChangeList)
	{
		this.colorChangeList = colorChangeList;
	}
	
	
	private int startForceColorColumn = 0;
	public void forceColor(ColorRGBA color)
	{
		forceColor = color;
		
		if(forceColor != null)
			startForceColorColumn = index;
	}
	
	
    private void initVerticalTargetsMesh()
    {
    	// init position buffer
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(cols * 4 * 3);
        for(int j = 0; j < cols-1; j++) 
        {
        	positionBuf.put(j).put(0).put(0);
        	positionBuf.put(j+1).put(0).put(0);
        	positionBuf.put(j+1).put(height).put(0);
        	positionBuf.put(j).put(height).put(0);
        }         
        verticalTargetsMesh.setBuffer(Type.Position, 3, positionBuf);
        
        // init color buffer
    	verticalTargetsArray = new ColorRGBA[cols];
		Arrays.fill(verticalTargetsArray, null);              
        setVerticalTargetsColorBuffer(verticalTargetsArray);
        
        // init index buffer
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(cols * 4 * 3);
        for(int i = 0; i < cols; i++) 
        {
        	indexBuf.put((short) (i*4)).put((short) (i*4+1)).put((short) (i*4+2));
        	indexBuf.put((short) (i*4)).put((short) (i*4+2)).put((short) (i*4+3));
        }         
        verticalTargetsMesh.setBuffer(Type.Index, 3, indexBuf);
    }
    
    
	private void setVerticalTargetsColorBuffer(ColorRGBA[] array)
	{
		FloatBuffer colorBuf = BufferUtils.createFloatBuffer(cols * 4 * 4);
		for(int col = 0; col < cols-1; col++)
		{
			ColorRGBA color = array[col];
					
			for(int i = 0; i < 4; i++)
			{
				if(color != null)
					colorBuf.put(color.r).put(color.g).put(color.b).put(color.a);
				else
					colorBuf.put(0).put(0).put(0).put(0);
			}
		}
		verticalTargetsMesh.setBuffer(Type.Color, 4, colorBuf);
	}
	
	
    private void initGraphMesh()
    {
    	// init position buffer
    	array = new float[cols];
		Arrays.fill(array, Float.NaN); 
        setPositionBuffer(array);
        
        // init color buffer
        setColorBuffer(array);
        
        // init index buffer
        ShortBuffer indexBuf = BufferUtils.createShortBuffer(rows * cols * 4 * 3);
        for(int i = 0; i < rows*cols; i++) 
        {
        	indexBuf.put((short) (i*4)).put((short) (i*4+1)).put((short) (i*4+2));
        	indexBuf.put((short) (i*4)).put((short) (i*4+2)).put((short) (i*4+3));
        }         
        graphMesh.setBuffer(Type.Index, 3, indexBuf);
    }

    
	private void setPositionBuffer(float[] array)
	{
        FloatBuffer positionBuf = BufferUtils.createFloatBuffer(rows * cols * 4 * 3);
        for(int j = 0; j < cols; j++) 
        {
        	if(Float.isNaN(array[j]))
        	{
        		// area below graph (0px strength)
        		positionBuf.put(j).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put(j).put(0).put(0);
        		
        		// graph (0px strength)
        		positionBuf.put(j).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put(j).put(0).put(0);

        		// area above graph
        		positionBuf.put(j).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put((j+1)).put(height).put(0);
        		positionBuf.put(j).put(height).put(0);
        	}
        	else
        	{
        		// area below graph
        		positionBuf.put(j).put(0).put(0);
        		positionBuf.put((j+1)).put(0).put(0);
        		positionBuf.put((j+1)).put(array[j] - 1).put(0);
        		positionBuf.put(j).put(array[j] - 1).put(0);
        		
        		// graph (2px strength)
        		positionBuf.put(j).put(array[j] - 1).put(0);
        		positionBuf.put((j+1)).put(array[j] - 1).put(0);
        		positionBuf.put((j+1)).put(array[j] + 1).put(0);
        		positionBuf.put(j).put(array[j] + 1).put(0);

        		// area above graph
        		positionBuf.put(j).put(array[j] + 1).put(0);
        		positionBuf.put((j+1)).put(array[j] + 1).put(0);
        		positionBuf.put((j+1)).put(height).put(0);
        		positionBuf.put(j).put(height).put(0);
        	}
        }         
        graphMesh.setBuffer(Type.Position, 3, positionBuf);
	}
	

	private void setColorBuffer(float[] array)
	{
		// init color buffer
		FloatBuffer colorBuf = BufferUtils.createFloatBuffer(rows * cols * 4 * 4);
		for(int j = 0; j < cols; j++)
		{
			
			// area below graph
			for(int i = 0; i < 4; i++)
				colorBuf.put(0).put(0).put(0).put(0);
			
			// graph
			for(int i = 0; i < 4; i++)
			{
				ColorRGBA color = getParameterDependentGraphColor(j, array[j]);
				colorBuf.put(color.r).put(color.g).put(color.b).put(color.a);
			}
			
			// area above graph
			for(int i = 0; i < 4; i++)
				colorBuf.put(0).put(0).put(0).put(0);
		}         
		graphMesh.setBuffer(Type.Color, 4, colorBuf);
	}
	

	private ColorRGBA getParameterDependentGraphColor(int currentColumn, float value)
	{
		if(forceColor != null && (currentColumn >= startForceColorColumn))
			return forceColor;
		
		ColorRGBA returnColor = graphColor;

		if(colorChangeList != null && !Float.isNaN(value))
		{
			for(ColorChange entry : colorChangeList)
			{
				int h = (int) Util.map(entry.getPosition(), minValue, maxValue, 0, height);
				if(value > h)
					returnColor = entry.getColor();
			}
		}
		
		return returnColor;
	}
	

	public void addValue(float value)
	{
		if(!isPause)
		{
			float normalizedValue = height * (value - minValue) / (maxValue - minValue);

			if(normalizedValue < 0 || normalizedValue > height)
				normalizedValue = Float.NaN;
		
			if(index < activeCols)
			{
				// continue filling array
				array[index] = normalizedValue;
				index++;
			}
			else
			{
				// shift values one position to the left
				System.arraycopy(array, 1, array, 0, activeCols-1);
				array[activeCols-1] = normalizedValue;
				startForceColorColumn = Math.max(0, startForceColorColumn-1);
    		
				// shift values one position to the left
				System.arraycopy(verticalTargetsArray, 1, verticalTargetsArray, 0, cols-1);
				verticalTargetsArray[cols-1] = null;
				setVerticalTargetsColorBuffer(verticalTargetsArray);
			}
    	
			setPositionBuffer(array);
			setColorBuffer(array);	
		}
	}
	
	
	public void addVerticalTarget(int relPosition, ColorRGBA color)
	{
		if(!isPause)
		{
			int absPosition = index + relPosition;
		
			absPosition = Math.max(0, Math.min(cols-1, absPosition));
        
			// set color buffer
			verticalTargetsArray[absPosition] = color;
			setVerticalTargetsColorBuffer(verticalTargetsArray);
		}
	}
	
    
    private class KeyListener implements ActionListener
    {
        public void onAction(String name, boolean value, float tpf)
        {
            if(value)
            	toggleVisibility();
        }
    }
    
    
    public void toggleVisibility()
    {
    	if(!isVisible)
    		show();
    	else
    		hide();
    }
    

	public void show()
	{
		sim.getGuiNode().attachChild(backgroundGeometry);
		sim.getGuiNode().attachChild(horizontalGridLinesGeometry);
		sim.getGuiNode().attachChild(verticalGridLinesGeometry);
		sim.getGuiNode().attachChild(verticalTargetsGeometry);
		sim.getGuiNode().attachChild(graphGeometry);
		
		if(label != null)
			sim.getGuiNode().attachChild(label);
		
		isVisible = true;
	}
	
	
	public void hide()
	{
		backgroundGeometry.removeFromParent();
		horizontalGridLinesGeometry.removeFromParent();
		verticalGridLinesGeometry.removeFromParent();
		verticalTargetsGeometry.removeFromParent();
		graphGeometry.removeFromParent();
		
		if(label != null)
			label.removeFromParent();
		
		isVisible = false;
	}


	public void setLabel(String text, float posX, float posY, ColorRGBA color)
	{
		BitmapFont guiFont = sim.getAssetManager().loadFont("Interface/Fonts/Default.fnt");        
		label = new BitmapText(guiFont, false);
		label.setText(text);
		label.setLocalTranslation(posX, posY, 0);
		label.setSize(guiFont.getCharSet().getRenderedSize());
		label.setColor(color);
	}

	
	public Graph exportGraph()
	{
		return new Graph(array.clone(), verticalTargetsArray.clone());
	}

	
	public void importGraph(Graph graph)
	{
		float[] importedArray = graph.getValues();
		array = importedArray;
		
		ColorRGBA[] importedVerticalTargetsArray = graph.getVerticalTargets();
		verticalTargetsArray = importedVerticalTargetsArray;
		
		setVerticalTargetsColorBuffer(verticalTargetsArray);
		setPositionBuffer(array);
		setColorBuffer(array);
	}
	
	
	public void setPaused(boolean isPause)
	{
		this.isPause = isPause;
	}
}
