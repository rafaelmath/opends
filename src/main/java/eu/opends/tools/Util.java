/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.tools;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.jme3.collision.CollisionResults;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;

import eu.opends.basics.SimulationBasics;
import eu.opends.opendrive.OpenDriveCenter;

/**
 * 
 * @author Rafael Math
 */
public class Util 
{
    public static Geometry findGeom(Spatial spatial, String name) 
    {
        if (spatial instanceof Node) 
        {
            Node node = (Node) spatial;
            for (int i = 0; i < node.getQuantity(); i++) 
            {
                Spatial child = node.getChild(i);
                Geometry result = findGeom(child, name);
                if (result != null)
                    return result;
            }
        } else if (spatial instanceof Geometry) 
        {
            if (spatial.getName().startsWith(name))
                return (Geometry) spatial;
        }
        return null;
    }
    
    
    public static Node findNode(Spatial spatial, String name) 
    {
        if (spatial != null && spatial instanceof Node) 
        {
            Node node = (Node) spatial;
            
            if (node.getName() != null && node.getName().startsWith(name))
            	return (Node) spatial;
            
            for (int i = 0; i < node.getQuantity(); i++) 
            {
                Spatial child = node.getChild(i);
                Node result = findNode(child, name);
                if (result != null)
                    return result;
            }
        }
        return null;
    }
    
    
    public static String printTree(Spatial spatial) 
    {
    	return printTree(spatial, 0);
    }
    
    
    private static String printTree(Spatial spatial, int level) 
    {
    	String padding = "";
    	for(int k=0; k<level; k++)
    		padding += "-";
    	
        if (spatial instanceof Node) 
        {
            Node node = (Node) spatial;
            String resultList = padding + "node:" + spatial.getName() + "\n";
            for (int i = 0; i < node.getQuantity(); i++)
            {
                Spatial child = node.getChild(i);
                resultList += printTree(child, level+1);
            }
            return resultList;
            
        } else if (spatial instanceof Geometry) 
        {
        	return padding + "geometry:"+spatial.getName() + "\n";
        }
        return null;
    }
    
    
    public static String getPath(Spatial spatial) 
    {
        if (spatial != null) 
        	return getPath(spatial.getParent()) + "/" + spatial.getName();                
        else
        	return "";
    }


    private static List<Geometry> resultListGeometries = new ArrayList<Geometry>();
    
	private static void collectGeometries(Spatial spatial) 
	{
        if (spatial instanceof Node) 
        {
            for (Spatial child: ((Node) spatial).getChildren()) 
                collectGeometries(child);
        } 
        else if (spatial instanceof Geometry) 
        {
        	resultListGeometries.add((Geometry) spatial);
        }
	}
	
	public static List<Geometry> getAllGeometries(Spatial spatial)
	{
		resultListGeometries.clear();
		collectGeometries(spatial);
		return resultListGeometries;
	}
	
	
	/*
	//---------------------------------------------
    private static List<Spatial> resultListSpatials = new ArrayList<Spatial>();
    
	private static void collectSpatials(Spatial spatial) 
	{
		resultListSpatials.add(spatial);
		
        if (spatial instanceof Node) 
        {
            for (Spatial child: ((Node) spatial).getChildren()) 
                collectSpatials(child);
        }
	}
	
	public static List<Spatial> getAllSpatials(Spatial spatial) 
	{
		resultListSpatials.clear();
		collectSpatials(spatial);
		return resultListSpatials;
	}
	//---------------------------------------------
	*/
	
	
	public static void setFaceCullMode(Spatial spatial, FaceCullMode mode)
	{
		List<Geometry> geometryList = getAllGeometries(spatial);
    	for(Geometry geometry : geometryList)
    		geometry.getMaterial().getAdditionalRenderState().setFaceCullMode(mode);
	}
	
	public static void setWireFrame(Spatial spatial, boolean enable)
	{
		List<Geometry> geometryList = getAllGeometries(spatial);
    	for(Geometry geometry : geometryList)
    		geometry.getMaterial().getAdditionalRenderState().setWireframe(enable);
	}
	
	public static void setCullHint(Spatial spatial, CullHint cullHint)
	{
		List<Geometry> geometryList = getAllGeometries(spatial);
    	for(Geometry geometry : geometryList)
    		geometry.setCullHint(cullHint);
	}
    
	public static void open(String fileName)
	{  	  
	  	  try {
	   
	  		File file = new File(fileName);
	  		if (file.exists())
	  		{
	  			if (Desktop.isDesktopSupported()) 
	  			{
	  				Desktop.getDesktop().open(file);
	  			} else 
	  			{
	  				System.err.println("Awt Desktop is not supported!");
	  			}
	   
	  		} else {
	  			System.err.println("File does not exist!");
	  		}
	   
	  	  } catch (Exception ex) {
	  		  
	  		ex.printStackTrace();
	  	  }
	}


	public static String getDateTimeString() 
	{
		Calendar cal = Calendar.getInstance();
		DecimalFormat form = new DecimalFormat("00");
		String str = Integer.toString(cal.get(Calendar.YEAR)) + "_"
				+ form.format(cal.get(Calendar.MONTH) + 1) + "_"
				+ form.format(cal.get(Calendar.DAY_OF_MONTH)) + "-"
				+ form.format(cal.get(Calendar.HOUR_OF_DAY)) + "_"
				+ form.format(cal.get(Calendar.MINUTE)) + "_"
				+ form.format(cal.get(Calendar.SECOND));
		return str;
	}


	/**
	 * Makes the given directory if it not yet exists.
	 *
	 * @param directory
	 * 			Directory to make.
	 */
	public static void makeDirectory(String directory) 
	{
		directory = directory.replace("\\", "/");
		String[] array = directory.split("/");
		
		String currentLevel = "";
		
		for(int i=0; i<array.length; i++)
		{
			if(i==0)
				currentLevel = array[i];
			else
				currentLevel += "/" + array[i];
			
			File dir = new File(currentLevel);
		
			if (!dir.exists())
				dir.mkdir();
			else if (!dir.isDirectory())
				System.err.println("'" + currentLevel + "' exists but is not a directory");
		}
	}
	
	
	public static boolean isValidFilename(String filename) 
	{
		File f = new File(filename);
		
		try {
			f.getCanonicalPath();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	
	/**
	 * Computes the angle between three given points A, B, and C at point B. 
	 * 
	 * @param pointA
	 * 		3-dimensional point A
	 * 
	 * @param pointB
	 * 		3-dimensional point B where angle is measured
	 * 
	 * @param pointC
	 * 		3-dimensional point C
	 * 
	 * @param is2DSpace
	 * 		Compute angle in 2D-space (y=0 in points A, B, C)
	 * 
	 * @return
	 * 		Angle between vector BA and vector BC
	 */
	public static float getAngleBetweenPoints(Vector3f pointA, Vector3f pointB, Vector3f pointC, boolean is2DSpace) 
	{
		// vector pointing from vehicle's center towards vehicle's front
		Vector3f vectorBA = pointA.subtract(pointB);
		if(is2DSpace)
			vectorBA.setY(0);
		vectorBA.normalizeLocal();
		
		// vector pointing from vehicle's center towards obstacle
		Vector3f vectorBC = pointC.subtract(pointB);
		if(is2DSpace)
			vectorBC.setY(0);
		vectorBC.normalizeLocal();
		
		// angle between both vectors
		return vectorBA.angleBetween(vectorBC);
	}


	public static float map(float input, float input_start, float input_end, float output_start, float output_end)
	{
		float input_clamped = clamp(input, input_start, input_end);
		float slope = (output_end - output_start) / (input_end - input_start);
		float output = output_start + slope * (input_clamped - input_start);
		return clamp(output, output_start, output_end);
	}
	
	
	private static float clamp(float input, float limit1, float limit2)
	{
		// output value must lie within limit1 and limit2
		if(limit1 < limit2)
			return Math.min(Math.max(input, limit1), limit2);
		else
			return Math.min(Math.max(input, limit2), limit1);
	}

	
	public static boolean isValidOffroadPosition(OpenDriveCenter openDriveCenter, String elementID, 
			String roadID, Float lateralOffset, Float verticalOffset, Float s)
	{
		if(roadID == null)
		{
			System.err.println(elementID + ": RoadID attribute is missing.");
			return false;
		}
		
		if(roadID.isEmpty() || !openDriveCenter.getRoadMap().containsKey(roadID))
		{
			System.err.println(elementID + ": RoadID '" + roadID + "' is invalid.");
			return false;
		}
		
		if(lateralOffset == null)
		{
			System.err.println(elementID + ": Lateral offset attribute is missing.");
			return false;
		}
		
		if(verticalOffset == null)
		{
			System.err.println(elementID + ": Vertical offset attribute is missing.");
			return false;
		}
		
		if(s == null)
		{
			System.err.println(elementID + ": s attribute is missing.");
			return false;
		}
		
		if(s<0)
		{
			System.err.println(elementID + ": s '" + s + "' is smaller than minimum (0).");
			return false;
		}
		
		double endS = openDriveCenter.getRoadMap().get(roadID).getEndS();
		if(endS<s)
		{
			System.err.println(elementID + ": s '" + s + "' is greater than maximum (" + endS + ").");
			return false;
		}
		
		return true;
	}
	
	
	public static float getElevationAt(SimulationBasics sim, float x, float z)
	{
		Vector3f origin = new Vector3f(x, 10000, z);
		
		// reset collision results list
		CollisionResults results = new CollisionResults();
				
		// downward direction
		Vector3f direction = new Vector3f(0,-1,0);
				
		// aim a ray from the car's position towards the target
		Ray ray = new Ray(origin, direction);

		// collect intersections between ray and scene elements in results list.
		sim.getSceneNode().collideWith(ray, results);				

		for(int i=0; i<results.size(); i++)
		{
			String geometryName = results.getCollision(i).getGeometry().getName();
		
			if(!geometryName.startsWith("x-") && !geometryName.startsWith("y-") 
					&& !geometryName.startsWith("z-") && !geometryName.startsWith("center")
					&& !geometryName.startsWith("Sky") && !geometryName.startsWith("ODarea")
					&& !geometryName.startsWith("pedestrian") 
					&& (sim.getVegetationGenerator()==null || !sim.getVegetationGenerator().isVegetation(geometryName)))
			{
				return results.getCollision(i).getContactPoint().getY();
			}
		}

		return 0;
	}
}
