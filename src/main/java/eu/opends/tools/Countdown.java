/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.tools;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.ui.Picture;

import eu.opends.main.Simulator;

public class Countdown
{
	private Simulator sim;
	Node countdownNode = new Node("countdownNode");
	private int number = -1;
	private int maxNumber = 12;
	private boolean isRunning = false;
	private float updateTimer = 0;
	private float updateInterval = 1.0f;

	
	public Countdown(Simulator sim)
	{
		this.sim = sim;
		
		for(int i=0; i<=maxNumber; i++)
		{
			Picture picture = loadNumber(i);
			picture.setCullHint(CullHint.Always);
			countdownNode.attachChild(picture);
		}
		
		sim.getGuiNode().attachChild(countdownNode);
	}

	
	public void startCountdown(int startNumber)
	{
		number = Math.min(Math.max(0, startNumber), maxNumber);
		isRunning = true;
	}
	
	
	public void stopCountdown()
	{
		for(Spatial spatial : countdownNode.getChildren())
			spatial.setCullHint(CullHint.Always);
		
		isRunning = false;
	}
	
	
	public void update(float tpf)
	{
		updateTimer += tpf;
		
		// allow max. 1 update per refresh interval
		if(isRunning && updateTimer > updateInterval)
		{
			for(Spatial spatial : countdownNode.getChildren())
				spatial.setCullHint(CullHint.Always);
				
			if(number > 0)
			{
				Spatial spatial = countdownNode.getChild("number_" + number);
				spatial.setCullHint(CullHint.Dynamic);
				
				number--;
			}
			else
				isRunning = false;
			
			updateTimer = 0;
		}
	}
	
	
	private Picture loadNumber(int i)
	{
		Picture number = new Picture("number_" + i);
        number.setImage(sim.getAssetManager(), "Textures/Countdown/" + i + ".png", true);
        number.setWidth(128);
        number.setHeight(128);
        //number.setPosition(1940, 20);
		number.setPosition(2816, 650);
        
		return number;
	}

}
