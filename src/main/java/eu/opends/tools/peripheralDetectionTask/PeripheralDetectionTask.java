/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.tools.peripheralDetectionTask;

import java.lang.reflect.Field;
import java.util.Random;

import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.JoyButtonTrigger;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.ui.Picture;

import eu.opends.main.Simulator;

public class PeripheralDetectionTask
{
	private Simulator sim;
	private Random random;
	private Picture pdtMarker;
	private int markerWidth = 33;
	private int markerHeight = 33;
	private int screenWidth;
	private int screenHeight;
	private boolean isRunning = false;
	private Long onsetTimestamp = null;
	
	private float markerUpdateTimer = 0;
	private float markerOnsetInterval = 0.0f;
	private float markerMinOnsetInterval = 3.0f;
	private float markerMaxOnsetInterval = 5.0f;
	private float markerDisplayDuration = 1.0f;
	private float markerMaxResponseDuration = 2.0f;
	
	//private String correctReaction = "JOY_0_1";
	private String correctReaction = "KEY_PGUP";
	
	
	public PeripheralDetectionTask(Simulator sim)
	{
		this.sim = sim;
		random = new Random();
		
		// init marker picture
		pdtMarker = new Picture("PDT_Marker");
	    pdtMarker.setImage(sim.getAssetManager(), "Textures/Misc/redDot_33px.png", true);
	    pdtMarker.setWidth(markerWidth);
	    pdtMarker.setHeight(markerHeight);
	    pdtMarker.setCullHint(CullHint.Always);
	    sim.getGuiNode().attachChild(pdtMarker);
	    
	    // init display range
	    screenWidth = sim.getSettings().getWidth();
	    screenHeight = sim.getSettings().getHeight();
	}
	
	
	public void start()
	{
		if(!isRunning)
		{
			addMapping("PDT_button_press", correctReaction);
			
			ActionListener al = new ActionListener()
			{
				@Override
				public void onAction(String binding, boolean value, float tpf) 
				{
					if(binding.equals("PDT_button_press") && value)
						reportButtonPress();
				}
			};
			sim.getInputManager().addListener(al, "PDT_button_press");

			long now = System.currentTimeMillis();
			Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "started", null, null,
					null, null, null, null, null, null);
			isRunning = true;
		}
	}
	
	
	public void stop()
	{
		if(isRunning)
		{
			sim.getInputManager().deleteMapping("PDT_button_press");
			
			long now = System.currentTimeMillis();
			Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "stopped", null, null,
					null, null, null, null, null, null);
			isRunning = false;
		}
	}
	

	public void update(float tpf)
	{
		markerUpdateTimer += tpf;
		
		if(isRunning)
		{
			// allow max. 1 marker display per marker onset interval
			if(markerUpdateTimer > markerOnsetInterval)
			{
				// randomly update the duration (between min and max value) for displaying the next marker
				float extraTime = random.nextFloat() * (markerMaxOnsetInterval - markerMinOnsetInterval);
				markerOnsetInterval = markerMinOnsetInterval + extraTime;
				
				/*
				// randomly pick an x coordinate from the first third of the screen width
				// subtract width of marker to avoid marker being positioned out of the third screen (right)
				int x = random.nextInt(screenWidth/3 - markerWidth);
				
				// randomly add 2/3 of the screen width  (right periphery) or do nothing (left periphery)
				if(random.nextBoolean())
					x += (2*screenWidth)/3;
				
				// randomly pick a y coordinate from the total screen height
				// subtract height of marker to avoid marker being positioned out of screen (above top)
				int y = random.nextInt(screenHeight - markerHeight);
				*/
				
				
				// Goal: randomly pick an x coordinate between 35 and 45 percent of the screen width
				// Goal(alternative): randomly pick an x coordinate between 35 and 45 percent OR 55 and 
				//                    65 percent of the screen width
				
				// Step1: randomly pick an x coordinate between 0 and 10 percent of screen width
				// subtract width of marker to prevent it from being positioned right of this range 
				int x = random.nextInt(Math.round(0.1f * screenWidth) - markerWidth);
				
				// Step2: add 35 percent of the screen width to the previous x coordinate
				x += 0.35f*screenWidth;
				
				// Step2a: randomly add 20 % of the screen width  (right periphery) or do nothing (left periphery)
				if(random.nextBoolean())
					x += 0.2f*screenWidth;
				
				// Goal: randomly pick a y coordinate between 45 and 65 percent of the screen height
				
				// Step1: randomly pick an y coordinate between 0 and 20 percent of screen height
				// subtract height of marker to prevent it from being positioned above of this range 
				int y = random.nextInt(Math.round(0.2f * screenHeight) - markerHeight);
				
				// Step2: add 45 percent of the screen height to the previous y coordinate
				y += 0.45f*screenHeight;
				
				// set marker position
				showMarker(x, y);
				
				markerUpdateTimer = 0;
			}
		}
		
		// hiding the marker can happen before the reaction time measurement will be closed
		if(isShowingMarker() && markerUpdateTimer > markerDisplayDuration)
		{
			hideMarker();
		}
		
		// reaction time measurement will be closed (independent from visibility of marker)
		if(onsetTimestamp != null && markerUpdateTimer > markerMaxResponseDuration)
		{
			// log missed reaction
			long now = System.currentTimeMillis();
			long milliseconds = now - onsetTimestamp;
			long maxDuration = (long) (markerMaxResponseDuration * 1000);
			Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "missedReaction", 
					milliseconds, maxDuration, null, null, null, null, null, null);
			
			// close reaction time measurement
			onsetTimestamp = null;
		}
	}
	
	
	public void reportButtonPress()
	{
		if(isRunning)
		{
			if(onsetTimestamp != null)
			{
				// log correct reaction
				long now = System.currentTimeMillis();
				long milliseconds = now - onsetTimestamp;
				long maxDuration = (long) (markerDisplayDuration * 1000);
				Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "correctReaction", 
						milliseconds, maxDuration, null, null, null, null, null, null);
					
				hideMarker();
					
				// close reaction time measurement
				onsetTimestamp = null;
			}
			else
			{
				// log false reaction
				long now = System.currentTimeMillis();
				Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "falseReaction", 
						null, null, null, null,	null, null, null, null);
			}
		}
	}
	
	
	private void showMarker(int x, int y)
	{
		if(!isShowingMarker())
		{
			pdtMarker.setPosition(x, y);
			pdtMarker.setCullHint(CullHint.Dynamic);
			
			// set start point for reaction measurement
			long now = System.currentTimeMillis();
			onsetTimestamp = now;
			
			// log showing marker
			long maxDuration = (long) (markerDisplayDuration * 1000);
			Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "showMarker", null,
					maxDuration, x, y, null, null, null, null);
		}
	}
	
	
	private void hideMarker()
	{
		pdtMarker.setCullHint(CullHint.Always);
		
		// log hiding marker
		long now = System.currentTimeMillis();
		long maxDuration = (long) (markerDisplayDuration * 1000);
		Simulator.getDrivingTaskLogger().reportEvent(now, "peripheralDetectionTask", "hideMarker", null,
				maxDuration, (int)pdtMarker.getLocalTranslation().getX(), (int)pdtMarker.getLocalTranslation().getY(),
				null, null, null, null);
	}


	private boolean isShowingMarker()
	{
		return pdtMarker.getCullHint() != CullHint.Always;
	}
	
	
	private void addMapping(String mappingID, String buttonString) 
	{
		String[] buttonArray = buttonString.split(",");
		for(String button : buttonArray)
		{
			button = button.toUpperCase().trim();
			Trigger trigger = getTrigger(button);
			if(trigger != null)
				sim.getInputManager().addMapping(mappingID, trigger);
		}
	}
	

	private Trigger getTrigger(String buttonName)
	{
		try {
			
			if(buttonName.startsWith("KEY_"))
			{
				// prefix "KEY_"
				Field field = KeyInput.class.getField(buttonName);
				int keyNumber = field.getInt(KeyInput.class);
				return new KeyTrigger(keyNumber);
				
			}
			else if(buttonName.startsWith("JOY_"))
			{
				// prefix "JOY_" (e.g. JOY_3 or JOY_1_5)
				//String suffix = buttonName.replace("JOY_", "");
				String[] numberArray = buttonName.split("_");
				if(numberArray.length == 2)
				{
					int buttonNumber = Integer.parseInt(numberArray[1])-1;
					return new JoyButtonTrigger(0, buttonNumber);
				}
				else if(numberArray.length == 3)
				{
					int deviceNumber = Integer.parseInt(numberArray[1]);
					int buttonNumber = Integer.parseInt(numberArray[2])-1;
					return new JoyButtonTrigger(deviceNumber, buttonNumber);
				}
				
				System.err.println("PeripheralDetectionTask: '" + buttonName + "' is an invalid button name");
				return null;
			}
			else
			{
				// no prefix
				String keyString = "KEY_" + buttonName;
				Field field = KeyInput.class.getField(keyString);
				int keyNumber = field.getInt(KeyInput.class);
				return new KeyTrigger(keyNumber);
			}
			
		} catch (Exception e) {
			
			if(!buttonName.isEmpty())
				System.err.println("Invalid key '" + buttonName + "'! Use prefix 'KEY_' or 'JOY_'");
			return null;
		}
	}
	
}
