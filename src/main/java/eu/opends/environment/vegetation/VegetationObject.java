/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.environment.vegetation;

import java.util.Random;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;

import eu.opends.basics.SimulationBasics;
import eu.opends.opendrive.OpenDriveCenter;
import eu.opends.opendrive.processed.ODPoint;
import eu.opends.tools.Util;

public class VegetationObject
{
	private OpenDriveCenter openDriveCenter;
	private Spatial vegetationNodeClone = null;
	private Vector3f position;
	private Quaternion rotation;
	private Vector3f scale;
	
	
	public VegetationObject(SimulationBasics sim, Spatial vegetationNode, String roadID, float s, float lateralOffset, 
			float verticalOffset, Random random)
	{
		this.openDriveCenter = sim.getOpenDriveCenter();
		
		String name = "road_" + roadID + "/" + s + "_vegetation";
		
		if(Util.isValidOffroadPosition(openDriveCenter, name, roadID, lateralOffset, verticalOffset, s))
		{
			// clone spatial
			vegetationNodeClone = vegetationNode.clone();
			
			ODPoint point = openDriveCenter.getRoadMap().get(roadID).getPointOnReferenceLine(s, name+"_point");
			Vector3f referencePosition = point.getPosition().toVector3f();
			float ortho = (float)point.getOrtho();
			
			// get absolute position according to relative position
			float x = referencePosition.getX() + lateralOffset*FastMath.sin(ortho);
			float z = referencePosition.getZ() + lateralOffset*FastMath.cos(ortho);
			float y = Util.getElevationAt(sim,x,z) + verticalOffset;
			position = vegetationNodeClone.getLocalTranslation().add(x, y, z);
			vegetationNodeClone.setLocalTranslation(position);
			
			// value between 0.0 .. 6.283  (--> 0.0 .. 360 degrees)
			float orientation = FastMath.TWO_PI * random.nextFloat();
			float[] angles = new float[3];
			vegetationNodeClone.getLocalRotation().toAngles(angles);
			float xRot = angles[0];
			float yRot = ortho + angles[1] + orientation;
			float zRot = angles[2];
			rotation = (new Quaternion()).fromAngles(xRot, yRot, zRot);
			vegetationNodeClone.setLocalRotation(rotation);
			
			// value between 0.75 .. 1.25
			float randomFactor = 0.75f + (0.5f * random.nextFloat());
			scale = vegetationNodeClone.getLocalScale().mult(randomFactor);
			vegetationNodeClone.setLocalScale(scale);
			
			// add spatial to the scene node
			sim.getSceneNode().attachChild(vegetationNodeClone);
		}
		else
			System.err.println("Could not init vegetation '" + vegetationNode.getName() 
				+ "' at: " + roadID + "/" + s);
	}

	
	public Vector3f getPosition()
	{
		return position;
	}


	public Quaternion getRotation()
	{
		return rotation;
	}


	public Vector3f getScale()
	{
		return scale;
	}


	public void updateLod(Vector3f cameraPos)
	{
		if(vegetationNodeClone != null)
		{
			for(Geometry vegetationGeometry : Util.getAllGeometries(vegetationNodeClone))
			{
				// get current and maximum LOD level of this vegetation geometry
				int currentLodLevel = vegetationGeometry.getLodLevel();
				int maxLodLevel = vegetationGeometry.getMesh().getNumLodLevels()-1;
				
				// calculate distance between vegetation geometry and camera 
				float dist = cameraPos.distance(vegetationGeometry.getWorldTranslation());
				
				// increase LOD level (= reduce triangles of vegetation geometry) with increasing distance from camera 
				int targetLodLevel = 0;
				if(dist < 200)
					targetLodLevel = 0;
				else if(200 <= dist && dist < 500)
					targetLodLevel = 1;
				else if(500 <= dist && dist < 800)
					targetLodLevel = 2;
				else if(800 <= dist)
					targetLodLevel = 3;
				
				// not all geometries consist of 4 LOD levels --> cap at maximum LOD level of this vegetation geometry
				if(targetLodLevel > maxLodLevel)
					targetLodLevel = maxLodLevel;
				
				// if LOD level has changed --> update vegetation geometry
				if(currentLodLevel != targetLodLevel)
					vegetationGeometry.setLodLevel(targetLodLevel);
			}
		}
	}


	public void setVisibility(boolean isVisible)
	{
		if(isVisible)
			vegetationNodeClone.setCullHint(CullHint.Dynamic);
		else
			vegetationNodeClone.setCullHint(CullHint.Always);
	}
}
