/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.opends.profiler.BasicProfilerState;
import com.jme3.app.StatsAppState;
//import com.jme3.app.state.VideoRecorderAppState;
import com.jme3.input.Joystick;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.system.JmeContext.Type;
import com.sun.javafx.application.PlatformImpl;

import de.lessvoid.nifty.Nifty;
import eu.opends.analyzer.DataWriter;
import eu.opends.analyzer.DrivingTaskLogger;
import eu.opends.audio.AudioCenter;
import eu.opends.basics.InternalMapProcessing;
import eu.opends.basics.MapObjectOD;
import eu.opends.basics.SimulationBasics;
import eu.opends.camera.SimulatorCam;
import eu.opends.cameraFlight.CameraFlight;
import eu.opends.cameraFlight.NotEnoughWaypointsException;
import eu.opends.canbus.CANClient;
import eu.opends.car.Car;
import eu.opends.car.ResetPosition;
import eu.opends.car.SteeringCar;
import eu.opends.chrono.ChronoPhysicsSpace;
import eu.opends.chrono.ChronoVehicleControl;
import eu.opends.codriver.CodriverConnector;
import eu.opends.dashboard.OpenDSGaugeCenter;
import eu.opends.drivingTask.DrivingTask;
import eu.opends.drivingTask.settings.SettingsLoader.Setting;
import eu.opends.drools.toc.ToCRulesCenter;
import eu.opends.dynamicObjects.DynamicObjectsCenter;
import eu.opends.effects.EffectCenter;
import eu.opends.environment.TrafficLightCenter;
import eu.opends.environment.vegetation.VegetationGenerator;
import eu.opends.events.EventCenter;
import eu.opends.eyetracker.EyetrackerCenter;
import eu.opends.hmi.HMICenter;
import eu.opends.infrastructure.RoadNetwork;
import eu.opends.input.KeyBindingCenter;
import eu.opends.knowledgeBase.KnowledgeBase;
import eu.opends.logitechsdk.ForceFeedbackController;
import eu.opends.movie.MovieCenter;
import eu.opends.mqtt.MqttConnection;
import eu.opends.multiDriver.MultiDriverClient;
import eu.opends.niftyGui.DrivingTaskSelectionGUIController;
import eu.opends.oculusRift.OculusRift;
import eu.opends.opendrive.OpenDriveCenter;
import eu.opends.reactionCenter.ReactionCenter;
import eu.opends.settingsController.SettingsControllerServer;
import eu.opends.taskDescription.contreTask.SteeringTask;
import eu.opends.taskDescription.tvpTask.MotorwayTask;
import eu.opends.taskDescription.tvpTask.ThreeVehiclePlatoonTask;
import eu.opends.tools.CollisionListener;
import eu.opends.tools.ObjectManipulationCenter;
import eu.opends.tools.PanelCenter;
import eu.opends.tools.SpeedControlCenter;
import eu.opends.tools.Util;
import eu.opends.tools.peripheralDetectionTask.PeripheralDetectionTask;
import eu.opends.tools.visualOutput.OnScreenVisualizer;
import eu.opends.traffic.PhysicalTraffic;
import eu.opends.trigger.TriggerCenter;
import eu.opends.visualization.LightningClient;


/**
 * 
 * @author Rafael Math
 */
public class Simulator extends SimulationBasics
{
	private static boolean isHeadLess = false;
	private static final Logger logger = LoggerFactory.getLogger(Simulator.class);
	
    private Nifty nifty;
    private int frameCounter = 0;
    private boolean drivingTaskGiven = false;
    private boolean initializationFinished = false;
    
    private static long simulationStartTime;
	public static long getSimulationStartTime()
	{
		return simulationStartTime;
	}
    
    private static Float gravityConstant;
	public static Float getGravityConstant()
	{
		return gravityConstant;
	}
	
	private RoadNetwork roadNetwork;
    public RoadNetwork getRoadNetwork()
    {
    	return roadNetwork;
    }
    
	private SteeringCar car;
    public SteeringCar getCar()
    {
    	return car;
    }
    
    private PhysicalTraffic physicalTraffic;
    public PhysicalTraffic getPhysicalTraffic()
    {
    	return physicalTraffic;
    }
	
	private static DrivingTaskLogger drivingTaskLogger;
	public static DrivingTaskLogger getDrivingTaskLogger()
	{
		return drivingTaskLogger;
	}
	
	private DataWriter dataWriter;
	public DataWriter getDataWriter() 
	{
		return dataWriter;
	}
	
	private PeripheralDetectionTask peripheralDetectionTask;
	public PeripheralDetectionTask getPeripheralDetectionTask()
	{
		return peripheralDetectionTask;
	}
	
	private LightningClient lightningClient;
	public LightningClient getLightningClient() 
	{
		return lightningClient;
	}
	
	private static CANClient canClient;
	public static CANClient getCanClient() 
	{
		return canClient;
	}
	
	private MultiDriverClient multiDriverClient;
	public MultiDriverClient getMultiDriverClient() 
	{
		return multiDriverClient;
	}
	
	private TriggerCenter triggerCenter = new TriggerCenter(this);
	public TriggerCenter getTriggerCenter()
	{
		return triggerCenter;
	}

	private static List<ResetPosition> resetPositionList = new LinkedList<ResetPosition>();
	public static List<ResetPosition> getResetPositionList() 
	{
		return resetPositionList;
	}

	private boolean showStats = false;	
	public void showStats(boolean show)
	{
		showStats = show;
		setDisplayFps(show);
    	setDisplayStatView(show);
    	
    	if(show)
    		getCoordinateSystem().setCullHint(CullHint.Dynamic);
    	else
    		getCoordinateSystem().setCullHint(CullHint.Always);
	}
	
	public void toggleStats()
	{
		showStats = !showStats;
		showStats(showStats);
	}
	
	private CameraFlight cameraFlight;
	public CameraFlight getCameraFlight()
	{
		return cameraFlight;
	}
	
	private SteeringTask steeringTask;
	public SteeringTask getSteeringTask()
	{
		return steeringTask;
	}
	
	private ThreeVehiclePlatoonTask threeVehiclePlatoonTask;
	public ThreeVehiclePlatoonTask getThreeVehiclePlatoonTask()
	{
		return threeVehiclePlatoonTask;
	}
	
	private MotorwayTask motorwayTask;
	public MotorwayTask getMotorwayTask()
	{
		return motorwayTask;
	}
	
	private ReactionCenter reactionCenter;
	public ReactionCenter getReactionCenter()
	{
		return reactionCenter;
	}
	
	private EffectCenter effectCenter;
	public EffectCenter getEffectCenter()
	{
		return effectCenter;
	}
	
	private ObjectManipulationCenter objectManipulationCenter;
	public ObjectManipulationCenter getObjectManipulationCenter()
	{
		return objectManipulationCenter;
	}
	
	private String instructionScreenID = null;
	public void setInstructionScreen(String ID)
	{
		instructionScreenID = ID;
	}
	
	private SettingsControllerServer settingsControllerServer;
	public SettingsControllerServer getSettingsControllerServer()
	{
		return settingsControllerServer;
	}	
	
	private EyetrackerCenter eyetrackerCenter;
	public EyetrackerCenter getEyetrackerCenter()
	{
		return eyetrackerCenter;
	}
	
	
	private static String outputFolder;
	public static String getOutputFolder()
	{
		return outputFolder;
	}
	
	public static boolean oculusRiftAttached = false;/*
    private static OculusRift oculusRift;
	public static OculusRift getOculusRift()
	{
		return oculusRift;
	}
	*/
	
	private ChronoPhysicsSpace chronoPhysicsSpace;
	public ChronoPhysicsSpace getChronoPhysicsSpace()
	{
		return chronoPhysicsSpace;
	}
	
	private CodriverConnector codriverConnector;
	public CodriverConnector getCodriverConnector() 
	{
		return codriverConnector;
	}
	

	private OnScreenVisualizer onScreenVisualizer;
	public OnScreenVisualizer getOnScreenVisualizer() 
	{
		return onScreenVisualizer;
	}
	
	
	private ForceFeedbackController ffbController;
	public ForceFeedbackController getForceFeedbackController() 
	{
		return ffbController;
	}
	
	
	private OpenDSGaugeCenter openDSGaugeCenter;
	public OpenDSGaugeCenter getOpenDSGaugeCenter() 
	{
		return openDSGaugeCenter;
	}
	
	
	private EventCenter eventCenter;
	public EventCenter getEventCenter() 
	{
		return eventCenter;
	}
	
	
	private ToCRulesCenter toCRulesCenter;
	public ToCRulesCenter getToCRulesCenter() 
	{
		return toCRulesCenter;
	}
	
	private MqttConnection mqttConnection;
	public MqttConnection getMqttConnection() 
	{
		return mqttConnection;
	}
	

	
    @Override
    public void simpleInitApp()
    {
    	showStats(false);
    	
    	if(drivingTaskGiven)
    		simpleInitDrivingTask(SimulationDefaults.drivingTaskFileName, SimulationDefaults.driverName);
    	else
    		initDrivingTaskSelectionGUI();
    }
    
    
	private void initDrivingTaskSelectionGUI() 
	{
		NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
    	
    	// Create a new NiftyGUI object
    	nifty = niftyDisplay.getNifty();
    		
    	String xmlPath = "Interface/DrivingTaskSelectionGUI.xml";
    	
    	// Read XML and initialize custom ScreenController
    	nifty.fromXml(xmlPath, "start", new DrivingTaskSelectionGUIController(this, nifty));
    		
    	// attach the Nifty display to the gui view port as a processor
    	guiViewPort.addProcessor(niftyDisplay);
    	
    	// disable fly cam
    	flyCam.setEnabled(false);
	}
	
	
	public void closeDrivingTaskSelectionGUI() 
	{
		nifty.exit();
        inputManager.setCursorVisible(false);
        flyCam.setEnabled(true);
	}


    public void simpleInitDrivingTask(String drivingTaskFileName, String driverName)
    {
		chronoPhysicsSpace = new ChronoPhysicsSpace();
		
    	stateManager.attach(new BasicProfilerState(false));
    	
    	SimulationDefaults.drivingTaskFileName = drivingTaskFileName;
    	
    	Util.makeDirectory("analyzerData");
    	outputFolder = "analyzerData/" + Util.getDateTimeString();
    	
    	initDrivingTaskLayers();
    	
    	// show stats if set in driving task
    	showStats(settingsLoader.getSetting(Setting.General_showStats, false));
    	
    	// check Oculus Rift mode: auto, enabled, disabled
    	String oculusAttachedString = settingsLoader.getSetting(Setting.OculusRift_isAttached, 
    			SimulationDefaults.OculusRift_isAttached);
		if(oculusAttachedString.equalsIgnoreCase("enabled"))
			oculusRiftAttached = true;
		else if(oculusAttachedString.equalsIgnoreCase("disabled"))
			oculusRiftAttached = false;
		
    	// sets up physics, camera, light, shadows and sky
    	super.simpleInitApp();
		
    	// set gravity
    	gravityConstant = drivingTask.getSceneLoader().getGravity(SimulationDefaults.gravity);
    	getBulletPhysicsSpace().setGravity(new Vector3f(0, -gravityConstant, 0));	
    	getBulletPhysicsSpace().setAccuracy(0.01f);
    	//getBulletPhysicsSpace().setAccuracy(0.008f); //TODO comment to set accuracy to 0.0166666 ?
    	//getBulletPhysicsSpace().setAccuracy(0.011f); // new try
    	
    	PanelCenter.init(this);
	
        Joystick[] joysticks = inputManager.getJoysticks();
        if(joysticks != null)
        	for (Joystick joy : joysticks)
        		System.out.println("Connected joystick: " + joy.toString());
        
    	//load map model
        InternalMapProcessing internalMapProcessing = new InternalMapProcessing(this);

		roadNetwork = new RoadNetwork(this);
		
		// create and place steering car
		car = new SteeringCar(this);
		
		// open TCP connection to KAPcom (knowledge component) [affects the driver name, see below]
		if(settingsLoader.getSetting(Setting.KnowledgeManager_enableConnection, SimulationDefaults.KnowledgeManager_enableConnection))
		{
			String ip = settingsLoader.getSetting(Setting.KnowledgeManager_ip, SimulationDefaults.KnowledgeManager_ip);
			if(ip == null || ip.isEmpty())
				ip = "127.0.0.1";
			int port = settingsLoader.getSetting(Setting.KnowledgeManager_port, SimulationDefaults.KnowledgeManager_port);
					
			//KnowledgeBase.KB.setConnect(true);
			KnowledgeBase.KB.setCulture("en-US");
			KnowledgeBase.KB.Initialize(this, ip, port);
			KnowledgeBase.KB.start();
		}
		
		// sync driver name with KAPcom. May provide suggestion for driver name if NULL.
		//driverName = KnowledgeBase.User().initUserName(driverName);  
		
		if(driverName == null || driverName.isEmpty())
			driverName = settingsLoader.getSetting(Setting.General_driverName, SimulationDefaults.driverName);
    	SimulationDefaults.driverName = driverName;
		
        // setup key binding
		keyBindingCenter = new KeyBindingCenter(this);

        // setup camera settings
        cameraFactory = new SimulatorCam(this, car);
		
		// init HMICenter
		HMICenter.init(this);


		// open TCP connection to Lightning
		if(settingsLoader.getSetting(Setting.ExternalVisualization_enableConnection, SimulationDefaults.Lightning_enableConnection))
		{
			lightningClient = new LightningClient();
		}
		
		// open TCP connection to CAN-bus
		if(settingsLoader.getSetting(Setting.CANInterface_enableConnection, SimulationDefaults.CANInterface_enableConnection))
		{
			canClient = new CANClient(this);
			canClient.start();
		}
		
		if(settingsLoader.getSetting(Setting.MultiDriver_enableConnection, SimulationDefaults.MultiDriver_enableConnection))
		{
			multiDriverClient = new MultiDriverClient(this, driverName);
			multiDriverClient.start();
		}
		
		drivingTaskLogger = new DrivingTaskLogger(outputFolder, driverName, drivingTask.getFileName());
		
		SpeedControlCenter.init(this);
		
		try {
			
			// attach camera to camera flight
			cameraFlight = new CameraFlight(this);
			
		} catch (NotEnoughWaypointsException e) {

			// if not enough way points available, attach camera to driving car
			car.getCarNode().attachChild(cameraFactory.getMainCameraNode());
		}
		
		reactionCenter = new ReactionCenter(this);
		
		steeringTask = new SteeringTask(this, driverName);
		
		threeVehiclePlatoonTask = new ThreeVehiclePlatoonTask(this, driverName);
		
		motorwayTask = new MotorwayTask(this);
		
		// start effect center
		effectCenter = new EffectCenter(this);
		
		objectManipulationCenter = new ObjectManipulationCenter(this);
		
		if(settingsLoader.getSetting(Setting.SettingsControllerServer_startServer, SimulationDefaults.SettingsControllerServer_startServer))
		{
			settingsControllerServer = new SettingsControllerServer(this);
			settingsControllerServer.start();
		}
		
		StatsAppState statsAppState = stateManager.getState(StatsAppState.class);
    	if (statsAppState != null && statsAppState.getFpsText() != null && statsAppState.getStatsView() != null) 
    	{
    		statsAppState.getFpsText().setLocalTranslation(3, getSettings().getHeight()-145, 0);
    		statsAppState.getStatsView().setLocalTranslation(3, getSettings().getHeight()-145, 0);
    		statsAppState.setDarkenBehind(false);
        }
    	
    	// add physics collision listener
    	CollisionListener collisionListener = new CollisionListener();
        getBulletPhysicsSpace().addCollisionListener(collisionListener);
        
        String videoPath = settingsLoader.getSetting(Setting.General_captureVideo, "");
        if((videoPath != null) && (!videoPath.isEmpty()) && (Util.isValidFilename(videoPath)))
        {
        	System.err.println("videoPath: " + videoPath);
        	File videoFile = new File(videoPath);
        	stateManager.attach(new VideoRecorderAppState(videoFile));
        }
        
		if(settingsLoader.getSetting(Setting.Eyetracker_enableConnection, SimulationDefaults.Eyetracker_enableConnection))
		{
			eyetrackerCenter = new EyetrackerCenter(this);
		}
        
        openDriveCenter = new OpenDriveCenter(this);
        String openDrivePath = Simulator.drivingTask.getOpenDrivePath();
        if(openDrivePath != null)
        	openDriveCenter.processOpenDrive(openDrivePath);
		
        // initialization of relative map objects (after OpenDRIVE)
        for(MapObjectOD dependentMapObject : drivingTask.getSceneLoader().getDependentMapObjects())
        {
        	if(dependentMapObject.initLocation(this, openDriveCenter))
        		internalMapProcessing.addMapObjectToScene(dependentMapObject);
        }

		// init collision triggers
        internalMapProcessing.initializationFinished();
		triggerCenter.setup();
        
		// start trafficLightCenter
		trafficLightCenter = new TrafficLightCenter(this);
        
		// initialize physical vehicles
		physicalTraffic = new PhysicalTraffic(this);
		//physicalTraffic.start(); //TODO
		
        if(!isHeadLess)
        	AudioCenter.init(this);
        
        codriverConnector = new CodriverConnector(this);
        
        onScreenVisualizer = new OnScreenVisualizer(this);
        
		ffbController = new ForceFeedbackController(this);
		
		openDSGaugeCenter = new OpenDSGaugeCenter();
		
		eventCenter = new EventCenter(this);

		vegetationGenerator = new VegetationGenerator(this);
		vegetationGenerator.init();
		
		movieCenter = new MovieCenter(this);
		
		toCRulesCenter = new ToCRulesCenter(this);
		
		dynamicObjectsCenter = new DynamicObjectsCenter(this);

		dataWriter = new DataWriter(this);
		
		peripheralDetectionTask = new PeripheralDetectionTask(this);
		
		mqttConnection = new MqttConnection(this, "tcp://localhost:1883");

		initializationFinished = true;
    }

    
	public boolean isInitializationFinished()
	{
		return initializationFinished;
	}
	
	
	private void initDrivingTaskLayers()
	{
		String drivingTaskFileName = SimulationDefaults.drivingTaskFileName;
		File drivingTaskFile = new File(drivingTaskFileName);
		drivingTask = new DrivingTask(this, drivingTaskFile);

		sceneLoader = drivingTask.getSceneLoader();
		scenarioLoader = drivingTask.getScenarioLoader();
		interactionLoader = drivingTask.getInteractionLoader();
		settingsLoader = drivingTask.getSettingsLoader();
	}
	
	
    @Override
    public void simpleUpdate(float tpf) 
    {
    	if(initializationFinished)
    	{
			super.simpleUpdate(tpf);

			// updates camera
			cameraFactory.updateCamera(tpf);
		
			if(!isPause())
				car.getCarControl().updateRPM(tpf);
		
			PanelCenter.update(tpf);
		
			triggerCenter.doTriggerChecks();
			
			// send camera data via TCP to Lightning
			if(lightningClient != null)
				lightningClient.sendCameraData(cam);
			
			// send car data via TCP to CAN-bus
			if(canClient != null)
				canClient.sendCarData();
				
			if(multiDriverClient != null)
				multiDriverClient.update();
			
			if(!isPause())
				car.update(tpf, PhysicalTraffic.getTrafficObjectList());
			
			chronoPhysicsSpace.update(tpf);
			
			// TODO start thread in init-method to update traffic
			physicalTraffic.update(tpf); 
			
			SpeedControlCenter.update();
			
			// update necessary even in pause
			if(!isHeadLess)
				AudioCenter.update(tpf, cam);
			
			if(!isPause())
				steeringTask.update(tpf);
			
			//if(!isPause())
				//getCameraFlight().play();
			
			threeVehiclePlatoonTask.update(tpf);
			
			motorwayTask.update(tpf);
			
			movieCenter.update(tpf);
			
			if(cameraFlight != null)
				cameraFlight.update();
			
			reactionCenter.update();
			
			// update effects
			effectCenter.update(tpf);
			
			// forward instruction screen if available
			if(instructionScreenID != null)
			{
				instructionScreenGUI.showDialog(instructionScreenID);
				instructionScreenID = null;
			}

    		if(frameCounter == 5)
    		{
    			if(settingsLoader.getSetting(Setting.General_pauseAfterStartup, SimulationDefaults.General_pauseAfterStartup))
    				setPause(true);
    		}
    		frameCounter++;
    		
    		updateCoordinateSystem();
    		
    		openDriveCenter.update(tpf);
    		
    		onScreenVisualizer.update(tpf);
    		
    		ffbController.update();
    		
    		eventCenter.update();
    		
    		vegetationGenerator.update(tpf);
    		
    		Vector3f driversPos = car.getPosition().add(0, 1, 0);
    		Quaternion rotation = car.getRotation();
    		Vector3f frontPos = car.getFrontGeometry().getWorldTranslation().add(0, 1, 0);
    		Vector3f headGazeDirection = null; // only used by DriveAnalyzer
    		Vector3f pointingDirection = null; // only used by DriveAnalyzer
    		Boolean isNoise = null;            // only used by DriveAnalyzer
    		//gestureAnalyzer.updateRays(driversPos, rotation, frontPos, headGazeDirection, pointingDirection, isNoise);//FIXME
			
			toCRulesCenter.update(tpf);
			
			dynamicObjectsCenter.update(tpf);
			
			if(eyetrackerCenter != null)
				eyetrackerCenter.update();
			
			dataWriter.update(tpf);
			
			peripheralDetectionTask.update(tpf);
    	}
    }

    
	private void updateCoordinateSystem()
	{
		getCoordinateSystem().getChild("x-cone").setLocalTranslation(car.getPosition().getX(), 0, 0);
		getCoordinateSystem().getChild("y-cone").setLocalTranslation(0, car.getPosition().getY(), 0);
		getCoordinateSystem().getChild("z-cone").setLocalTranslation(0, 0, car.getPosition().getZ());
	}
	
	
	/**
	 * Cleanup after game loop was left.
	 * Will be called when pressing any close-button.
	 * destroy() will be called subsequently.
	 */
	/*
	@Override
    public void stop()
    {
		logger.info("started stop()");		
		super.stop();
		logger.info("finished stop()");
    }
	*/
	
	
	/**
	 * Cleanup after game loop was left
	 * Will be called whenever application is closed.
	 */
	
	@Override
	public void destroy()
    {
		logger.info("started destroy()");

		if(initializationFinished)
		{
			chronoPhysicsSpace.destroy();
			
			if(lightningClient != null)
				lightningClient.close();
			
			if(canClient != null)
				canClient.requestStop();
				
			if(multiDriverClient != null)
				multiDriverClient.close();
			
			trafficLightCenter.close();
			
			steeringTask.close();
			
			threeVehiclePlatoonTask.close();
			
			movieCenter.close();
			
			reactionCenter.close();
			
			HMICenter.close();
			
			KnowledgeBase.KB.disconnect();
			
			car.close();
			
			physicalTraffic.close();
			
			if(settingsControllerServer != null)
				settingsControllerServer.close();
			
			if(eyetrackerCenter != null)
				eyetrackerCenter.close();
			
			//initDrivingTaskSelectionGUI();
			
			codriverConnector.close();
			
    		ffbController.close();
    		
    		toCRulesCenter.close();
    		
    		mqttConnection.close();
    		
    		dataWriter.close();
		}

		super.destroy();
		logger.info("finished destroy()");
		
		PlatformImpl.exit();
		//System.exit(0);
    }
	

    public static void main(String[] args) 
    {
		simulationStartTime = System.currentTimeMillis();
		
    	try
    	{
    		/*
    		logger.debug("Sample debug message");
    		logger.info("Sample info message");
    		logger.warn("Sample warn message");
    		logger.error("Sample error message");
    		logger.fatal("Sample fatal message");
    		*/
    		
    		oculusRiftAttached = OculusRift.initialize();
    		
    		// only show severe jme3-logs
    		java.util.logging.Logger.getLogger("").setLevel(java.util.logging.Level.SEVERE);
    		
    		PlatformImpl.startup(() -> {});
    		
	    	Simulator sim = new Simulator();
    		
	    	StartPropertiesReader startPropertiesReader = new StartPropertiesReader();

			sim.setSettings(startPropertiesReader.getSettings());

			// show/hide settings screen
			sim.setShowSettings(startPropertiesReader.showSettingsScreen());
			
			if(startPropertiesReader.showBorderlessWindow())
				System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
			
			if(!startPropertiesReader.getDrivingTaskPath().isEmpty() &&
					DrivingTask.isValidDrivingTask(new File(startPropertiesReader.getDrivingTaskPath())))
    		{
    			SimulationDefaults.drivingTaskFileName = startPropertiesReader.getDrivingTaskPath();
    			sim.drivingTaskGiven = true;
    		}
			
			if(!startPropertiesReader.getDriverName().isEmpty())
				SimulationDefaults.driverName = startPropertiesReader.getDriverName();
			
			isHeadLess = startPropertiesReader.getIsHeadLess();
			
	    	if(args.length >= 1)
	    	{
	    		if(DrivingTask.isValidDrivingTask(new File(args[0])))
	    		{
	    			SimulationDefaults.drivingTaskFileName = args[0];
	    			sim.drivingTaskGiven = true;
	    		}
	    	}
	
	    	if(args.length >= 2)
	    	{
	    		SimulationDefaults.driverName = args[1];
	    	}
			
	    	sim.setPauseOnLostFocus(false);
	    	
	    	if(isHeadLess)
	    		sim.start(Type.Headless);
	    	else
	    		sim.start();
    	}
    	catch(Exception e1)
    	{
    		logger.error("Could not run main method:", e1);
    	}
    }

    
	private static void copyFile(String sourceString, String targetString) 
	{
		try {
			
			Path source = Paths.get(sourceString);
			Path target = Paths.get(targetString);
			
			if(Files.exists(source, LinkOption.NOFOLLOW_LINKS))
				Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
			else
				System.err.println("ERROR: '" + sourceString + "' does not exist.");
		
		} catch (IOException e) {

			e.printStackTrace();
		}
	}



}
