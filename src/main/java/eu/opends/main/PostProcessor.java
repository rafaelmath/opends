/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext.Type;

import eu.opends.analyzer.DataUnit;
import eu.opends.analyzer.DataUnitPostProcessor;
import eu.opends.analyzer.DataWriterForAnalyzer;
import eu.opends.analyzer.DataWriterPostProcessor;
import eu.opends.analyzer.NoiseRecord;
import eu.opends.analyzer.RayAnalyzerCenter;
import eu.opends.analyzer.RayDirectionRecord;
import eu.opends.analyzer.DataReader;
import eu.opends.basics.InternalMapProcessing;
import eu.opends.basics.MapObject;
import eu.opends.basics.MapObjectOD;
import eu.opends.basics.SimulationBasics;
import eu.opends.drivingTask.DrivingTask;
import eu.opends.dynamicObjects.DynamicObjectsCenter;
import eu.opends.environment.vegetation.VegetationGenerator;
import eu.opends.gesture.RecordedReferenceObject;
import eu.opends.gesture.SceneRay;
import eu.opends.movie.MovieCenter;
import eu.opends.opendrive.OpenDriveCenter;
import eu.opends.opendrive.processed.ODLane;
import eu.opends.opendrive.processed.PreferredConnections;
import eu.opends.opendrive.util.ODPosition;
import eu.opends.traffic.StaticOpenDRIVECar;
import eu.opends.traffic.StaticTraffic;
import eu.opends.traffic.StaticTrafficObject;
import eu.opends.trigger.PlaySoundTriggerAction;
import eu.opends.trigger.Trigger;
import eu.opends.trigger.TriggerAction;

/**
 * 
 * @author Rafael Math
 */
public class PostProcessor extends SimulationBasics 
{
	private float totalDistance = 0;
	private boolean rewriteOutputFile = true;
	
    public String analyzerFilePath = "";
    private boolean initializationFinished = false;
    private long initialTimestamp;
	private Node target = new Node();
	private int targetIndex = 0;
	private LinkedList<DataUnit> dataUnitList = new LinkedList<DataUnit>();
	
	private RayDirectionRecord blueRayDirectionRecord;
	public RayDirectionRecord getBlueRayDirectionRecord()
	{
		return blueRayDirectionRecord;
	}
	
	private RayDirectionRecord yellowRayDirectionRecord;
	public RayDirectionRecord getYellowRayDirectionRecord()
	{
		return yellowRayDirectionRecord;
	}
	
	private RayDirectionRecord greenRayDirectionRecord;
	public RayDirectionRecord getGreenRayDirectionRecord()
	{
		return greenRayDirectionRecord;
	}
	
	private RayDirectionRecord headGazeDirectionRecord;
	private RayDirectionRecord pointingDirectionRecord;
	private NoiseRecord noiseRecord;
	private DataReader dataReader = new DataReader();
	private DataUnit currentDataUnit;
	private DataWriterForAnalyzer dataWriter;
	
	private StaticTraffic staticTraffic;
	
	private Vector3f headGazeDirectionLocal = null;
	private Vector3f pointingDirectionLocal = null;
	private Boolean isNoise = null;
	
	
	public boolean isValidAnalyzerFile(File analyzerFile) 
	{
		return dataReader.isValidAnalyzerFile(analyzerFile);
	}
	
	
	@Override
	public void simpleInitApp() 
	{
		setDisplayFps(false);
		setDisplayStatView(false);
		
		assetManager.registerLocator("assets", FileLocator.class);
		 
		loadDrivingTask();
		
		loadData();
		
		String blueRayDirectionFileName = "blueRay.txt";
		blueRayDirectionRecord = new RayDirectionRecord(analyzerFilePath, blueRayDirectionFileName, false);
		
		String yellowRayDirectionFileName = "yellowRay.txt";
		yellowRayDirectionRecord = new RayDirectionRecord(analyzerFilePath, yellowRayDirectionFileName, false);
		
		String greenRayDirectionFileName = "greenRay.txt";
		greenRayDirectionRecord = new RayDirectionRecord(analyzerFilePath, greenRayDirectionFileName, false);
		
		/*
		String headGazeDirectionFileName = "headgaze_smooth.txt";
		headGazeDirectionRecord = new RayDirectionRecord(analyzerFilePath, headGazeDirectionFileName, false);
		
		String pointingDirectionFileName = "gesture_aptive_smooth.txt";
		pointingDirectionRecord = new RayDirectionRecord(analyzerFilePath, pointingDirectionFileName, true);
		
		String noiseFileName = "audio.txt";
		noiseRecord = new NoiseRecord(analyzerFilePath, noiseFileName);
		*/
		
		super.simpleInitApp();	

    	//load map model
		InternalMapProcessing internalMapProcessing = new InternalMapProcessing(this);

		// OpenDRIVE road visualization
        openDriveCenter = new OpenDriveCenter(this);
        String openDrivePath = Simulator.drivingTask.getOpenDrivePath();
        if(openDrivePath != null)
        	openDriveCenter.processOpenDrive(openDrivePath);
        
        // initialization of relative map objects (after OpenDRIVE)
        for(MapObjectOD dependentMapObject : drivingTask.getSceneLoader().getDependentMapObjects())
        {
        	if(dependentMapObject.initLocation(this, openDriveCenter))
        		internalMapProcessing.addMapObjectToScene(dependentMapObject);
        }

        internalMapProcessing.initializationFinished();
		
        initialTimestamp = System.currentTimeMillis();
		
		vegetationGenerator = new VegetationGenerator(this);
		vegetationGenerator.init();
		
		movieCenter = new MovieCenter(this);
		
		dynamicObjectsCenter = new DynamicObjectsCenter(this);
		
		staticTraffic = new StaticTraffic(this);
		
		
        File f = new File(analyzerFilePath);
        if(f.getParent() != null && rewriteOutputFile)
        {
        	String outputFolder = f.getParent();
        	dataWriter = new DataWriterForAnalyzer(outputFolder, dataReader.getNameOfDriver(), 
        			dataReader.getNameOfDrivingTaskFile(), dataReader.getFileDate(), -1);
        }
		
        initializationFinished = true;
	}
	

	private void loadData() 
	{
		dataReader.initReader(analyzerFilePath, true);
		dataReader.loadDriveData();		
		totalDistance = dataReader.getTotalDistance();
		dataUnitList = dataReader.getDataUnitList();
	}
	
    
	private void loadDrivingTask() 
	{
		String drivingTaskName = dataReader.getNameOfDrivingTaskFile();
		File drivingTaskFile = new File(drivingTaskName);
		drivingTask = new DrivingTask(this,drivingTaskFile);
		
		sceneLoader = drivingTask.getSceneLoader();
		scenarioLoader = drivingTask.getScenarioLoader();
		interactionLoader = drivingTask.getInteractionLoader();
		settingsLoader = drivingTask.getSettingsLoader();
	}
		

    @Override
    public void simpleUpdate(float tpf) 
    {
    	if(initializationFinished)
    	{
			if((targetIndex) < dataUnitList.size())
			{
				// process each data record in a separate iteration of the game loop to allow the 
				// scene graph (which contains the node called "target") being properly updated
				updateView(dataUnitList.get(targetIndex));
				targetIndex++;
			}
			else
			{
				// end the processing
				long now = System.currentTimeMillis();
				long timeDiff = now-initialTimestamp;
				System.out.println("finished after: " + timeDiff);
				stop();
			}
    	}
    }


	private void updateView(DataUnit dataUnit)
	{
		currentDataUnit = dataUnit;
		
		target.setLocalTranslation(currentDataUnit.getCarPosition());
		target.setLocalRotation(currentDataUnit.getCarRotation());
		
		updateStaticTraffic();
		
		//boolean isTriggerPosition = doTriggerCheck();
		
		//TreeMap<String, RecordedReferenceObject> logList = updateReferenceObjects();
		
		updateDataWriter();
	}

	
	private void updateStaticTraffic()
	{
		if(staticTraffic != null)
		{
			ArrayList<String> ObjNameList = currentDataUnit.getObjName();
			ArrayList<Integer> ObjClassList = currentDataUnit.getObjClass();
			ArrayList<Vector3f> ObjPosList = currentDataUnit.getObjPos();
			ArrayList<Quaternion> ObjRotList = currentDataUnit.getObjRot();
			
			if(ObjNameList != null && ObjClassList != null && ObjPosList != null && ObjRotList != null)
			{
				for(int i=0; i<ObjNameList.size(); i++)
				{
					//if(ObjClassList.get(i) == 1)
					{
						StaticTrafficObject sto = staticTraffic.getStaticTrafficObject(ObjNameList.get(i));
						sto.setPosition(ObjPosList.get(i));
						sto.setRotation(ObjRotList.get(i));
					}
				}
			}
		}
	}


	private boolean doTriggerCheck()
	{
		return false;
	}


	private TreeMap<String, RecordedReferenceObject> updateReferenceObjects()
	{
		String activeReferenceObjectName = null;
		ArrayList<String> visibleObjects = new ArrayList<String>();
		
		// walk through all logged reference objects at the current position
		ArrayList<RecordedReferenceObject> recRefObjList = currentDataUnit.getReferenceObjectList();
		for(RecordedReferenceObject recRefObj : recRefObjList)
		{
			// collect the name of the current active reference object
			if(recRefObj.isActive())
				activeReferenceObjectName = recRefObj.getName();
			
			// collect the IDs of all buildings visible at the current position
			visibleObjects.add(recRefObj.getName());
		}
		
		// walk through ALL registered reference objects (type: MapObject)
		for(MapObject mapObject : gestureAnalyzer.getReferenceObjectList())
		{
			// make object visible if contained in list and invisible if not
			if(visibleObjects.contains(mapObject.getName()))
				mapObject.getSpatial().getParent().setCullHint(CullHint.Inherit);
			else
				mapObject.getSpatial().getParent().setCullHint(CullHint.Always);
		}

		// set active reference object
		gestureAnalyzer.setActiveReferenceObject(activeReferenceObjectName);
		
		// draw rays
		Vector3f origin = currentDataUnit.getCarPosition().add(0, 1f, 0);
		Quaternion rotation = currentDataUnit.getCarRotation();
		Vector3f frontPos = currentDataUnit.getFrontPosition().add(0, 1f, 0);  // original frontPos of Simulator		
		
		// lookup local headpose+gaze direction (relative to vehicle coordinate system)
		headGazeDirectionLocal = headGazeDirectionRecord.lookupRayDirectionByTimestamp(currentDataUnit.getDate());
		Vector3f headGazeDirectionWorld = localToWorld(headGazeDirectionLocal);
				
		// lookup local pointing direction (relative to vehicle coordinate system)
		pointingDirectionLocal = pointingDirectionRecord.lookupRayDirectionByTimestamp(currentDataUnit.getDate());
		Vector3f pointingDirectionWorld = localToWorld(pointingDirectionLocal);
		
		// lookup noise
		isNoise = noiseRecord.lookupNoiseByTimestamp(currentDataUnit.getDate());
				
		return gestureAnalyzer.updateRays(origin, rotation, frontPos, headGazeDirectionWorld, pointingDirectionWorld, isNoise);
	}

	
	private void updateDataWriter() 
	{
		// get most probable lane from result list according to expected lane list (and 
		// highest score concerning least heading deviation and least elevation difference)
		Vector3f carPos = target.getWorldTranslation();
		HashSet<ODLane> emptySet = new HashSet<ODLane>();
		ODLane lane = openDriveCenter.getMostProbableLane(carPos, emptySet, emptySet);
		if(lane != null)
		{
			String roadID_car = lane.getODRoad().getID();
			int lane_car = lane.getID();
			double s_car = lane.getCurrentInnerBorderPoint().getS();
			
			
			// <isWrongWay>------------------------------------------------------------------------
			
			if(currentDataUnit.getIsWrongWay() == null)
			{
				float hdgDiff = lane.getHeadingDiff(getTargetHeadingDegree());
				boolean isWrongWay = (FastMath.abs(hdgDiff) > 90);
				//System.out.println("isWrongWay: " + isWrongWay);
			
				//boolean isWrongWay_log = false;
				//if(currentDataUnit.getIsWrongWay() != null)
					//isWrongWay_log = currentDataUnit.getIsWrongWay();
			
				//if(isWrongWay != isWrongWay_log)
				//{
					//System.err.println("getHeadingDegree(): " + getHeadingDegree());
					//System.err.println("lane.getHeadingDiff(): " + hdgDiff);
				//}

				currentDataUnit.setIsWrongWay(isWrongWay);
			}
			
			// </isWrongWay>------------------------------------------------------------------------
			
			
			// <leadingCar>------------------------------------------------------------------------
			
			if(currentDataUnit.getLeadingCarPresent() == null)
			{
				StaticOpenDRIVECar closestLeadingCar = null;
				double closestLeadingCarDistance = Double.MAX_VALUE;
			
				ArrayList<StaticTrafficObject> staticTrafficObjectList = StaticTraffic.getStaticTrafficObjectList();
				for(StaticTrafficObject staticTrafficObject : staticTrafficObjectList)
				{
					if(staticTrafficObject instanceof StaticOpenDRIVECar)
					{
						StaticOpenDRIVECar leadingCar = ((StaticOpenDRIVECar) staticTrafficObject);
						if(leadingCar.getCurrentLane() != null)
						{	
							String leadingCarRoadID = leadingCar.getCurrentLane().getODRoad().getID();
							int leadingCarLaneID = leadingCar.getCurrentLane().getID();
							double leadingCarS = leadingCar.getCurrentS();
							ODPosition leadingCarPosition = new ODPosition(leadingCarRoadID, leadingCarLaneID, leadingCarS);

							if(leadingCarPosition != null)
							{
								PreferredConnections pc = new PreferredConnections();
								double distToLeadingCar = lane.getDistanceToTargetAhead(currentDataUnit.getIsWrongWay(), s_car, pc, leadingCarPosition);

								if(0 <= distToLeadingCar && distToLeadingCar < closestLeadingCarDistance)
								{
									closestLeadingCar = leadingCar;
									closestLeadingCarDistance = distToLeadingCar;
								}
							}
						}
					}
				}
			
				if(closestLeadingCar != null)
				{
					//System.err.println("Closest leading car " + closestLeadingCar.getName() + " in " 
						//+ closestLeadingCarDistance + " meters");
					//System.err.println("Closest leading car (log) " + currentDataUnit.getLeadingCarName() + " in " 
						//+ currentDataUnit.getLeadingCarDist() + " meters");
				
				
					currentDataUnit.setLeadingCarPresent(true);
					currentDataUnit.setLeadingCarName(closestLeadingCar.getName());
					currentDataUnit.setLeadingCarRoadID(closestLeadingCar.getCurrentLane().getODRoad().getID());
					currentDataUnit.setLeadingCarLaneID(closestLeadingCar.getCurrentLane().getID());
					currentDataUnit.setLeadingCarS((float)closestLeadingCar.getCurrentS());
					currentDataUnit.setLeadingCarDist((float)closestLeadingCarDistance);
				
					// speed (from position change)
					float carSpeedKmh = 0;
					if(targetIndex-50 >= 0)
					{
						DataUnit previousDataUnit = dataUnitList.get(targetIndex-50);
					
						long prevMillis = previousDataUnit.getDate().getTime();
						long currentMillis = currentDataUnit.getDate().getTime();
						float timeChange = (currentMillis - prevMillis)/1000f;
					
						int prevIndex = previousDataUnit.getObjName().indexOf(closestLeadingCar.getName());
						int currentIndex = currentDataUnit.getObjName().indexOf(closestLeadingCar.getName());
						if(prevIndex >= 0 && currentIndex >= 0)
						{
							Vector3f prevPos = previousDataUnit.getObjPos().get(prevIndex);
							Vector3f currentPos = currentDataUnit.getObjPos().get(currentIndex);
							float posChange = currentPos.distance(prevPos);
					
							if(timeChange != 0)
								carSpeedKmh = 3.6f * posChange/timeChange;
					
							//System.err.println("Prev: " + prevMillis + "; " + prevPos);
							//System.err.println("Curr: " + currentMillis + "; " + currentPos);
							//System.err.println("Diff: " + timeChange + "; " + posChange + "; " + carSpeedKmh);
						}
					}
				
					currentDataUnit.setLeadingCarSpeedKmh(carSpeedKmh);
				}
				else
				{
					currentDataUnit.setLeadingCarPresent(false);
					currentDataUnit.setLeadingCarName("");
					currentDataUnit.setLeadingCarRoadID("");
					currentDataUnit.setLeadingCarLaneID(0);
					currentDataUnit.setLeadingCarS(-1f);
					currentDataUnit.setLeadingCarDist(-1f);
					currentDataUnit.setLeadingCarSpeedKmh(-1f);

				}
			}
			// </leadingCar>-----------------------------------------------------------------------
		}
			
		
		// <delete eye tracker data where content is the same as in the previous log line >--------------------
		if(targetIndex-1 >= 0)
		{
			DataUnit previousDataUnit = dataUnitList.get(targetIndex-1);
			
			if(currentDataUnit.getEyeTrackerDate().getTime() == previousDataUnit.getEyeTrackerDate().getTime())
			{
				currentDataUnit.setValidLeftEye(false);
				currentDataUnit.setValidRightEye(false);
				currentDataUnit.setGazeCoordinate(null);
				currentDataUnit.setGazeCoordinateAvg(null);
				currentDataUnit.setGazeRayOrigin(null);
				currentDataUnit.setGazeRayDirection(null);
				currentDataUnit.setClosestHit(null);
				currentDataUnit.setClosestContactPointByGazeRay(null);
			}
		}
		// </delete eye tracker data where content is the same as in the previous log line >--------------------
			
			
		// <add a parameter "aoi2D" to log file. put in the name of the GUInode-related AOI hit by the gaze ray >
		if(currentDataUnit.getAoi2D() == null)
		{
			String aoiName = "";
				
			Vector2f gazeCoordinateAvg = currentDataUnit.getGazeCoordinateAvg();
			if(gazeCoordinateAvg != null)
			{
				float x = gazeCoordinateAvg.getX();
				float y = gazeCoordinateAvg.getY();
				
				if(2816 <= x && x <= 2944 && 650 <= y && y <= 778)
					aoiName = "Countdown";
				else if(2100 <= x && x <= 2456 && 50 <= y && y <= 128)
					aoiName = "Autopilot_Indicator";
				else if(3252 <= x && x <= 3536 && 15 <= y && y <= 199)
					aoiName = "Speedometer";
				else if(1440 <= x && x <= 1901 && 54 <= y && y <= 216)
					aoiName = "Left_Mirror";
				else if(2419 <= x && x <= 3341 && 864 <= y && y <= 1026)
					aoiName = "Center_Mirror";
				else if(3859 <= x && x <= 4320 && 54 <= y && y <= 216)
					aoiName = "Right_Mirror";
			}
				
			currentDataUnit.setAoi2D(aoiName);
		}
		// </add a parameter "aoi2D" to log file. put in the name of the GUInode-related AOI hit by the gaze ray >
		
		
		if (dataWriter != null) 
		{
			dataWriter.writeln(currentDataUnit);
		} 
		
		
		float percentage = 100 * currentDataUnit.getTraveledDistance()/totalDistance;
		if(percentage > progress+1)
		{
			progress++;
			System.err.println(progress + "%");
		}
	}
	private int progress = 0;

    
	private Vector3f localToWorld(Vector3f gazeDirectionLocal)
	{
		if(gazeDirectionLocal != null)
		{
			Vector3f worldPos = target.localToWorld(gazeDirectionLocal, null);
			Vector3f gazeDirectionWorld = worldPos.subtract(currentDataUnit.getCarPosition());
			gazeDirectionWorld.normalizeLocal();
			return gazeDirectionWorld;
		}
		
		return null;
	}	
	
	/**
	 * Cleanup after game loop was left
	 * Will be called whenever application is closed.
	 */
	@Override
	public void destroy()
    {
		if(dataWriter != null)
			dataWriter.close();
    }
	
	
	public static void main(String[] args) 
	{
		Logger.getLogger("").setLevel(Level.SEVERE);
		PostProcessor analyzer = new PostProcessor();

    	if(args.length >= 1)
    	{
    		analyzer.analyzerFilePath = args[0];
    		
    		if(analyzer.isValidAnalyzerFile(new File(args[0])))
    		{
    			AppSettings settings = new AppSettings(false);
    	        settings.setUseJoysticks(true);
    	        settings.setSettingsDialogImage("OpenDS.png");
    	        settings.setTitle("OpenDS Analyzer");
    	        settings.setFrameRate(6000); // internal maximum is 330 fps
    	        
    			analyzer.setSettings(settings);
    			
    			analyzer.setPauseOnLostFocus(false);
    			analyzer.start(Type.Headless);	
    		}
    	}    	
	}
	
	
	public float getTargetHeading() 
	{
		// get Euler angles from rotation quaternion
		float[] angles = target.getWorldRotation().toAngles(null);
		
		// heading in radians
		float heading = -angles[1];
		
		// normalize radian angle
		float fullAngle = 2*FastMath.PI;
		float angle_rad = (heading + fullAngle) % fullAngle;
		
		return angle_rad;
	}
	
	
	public float getTargetHeadingDegree()
	{
		return getTargetHeading()  * FastMath.RAD_TO_DEG;
	}

}
