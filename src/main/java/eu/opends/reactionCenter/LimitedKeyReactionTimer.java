/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.reactionCenter;

import java.lang.reflect.Field;
//import java.util.Date;
import java.util.GregorianCalendar;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.JoyButtonTrigger;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.Trigger;

import eu.opends.jasperReport.ReactionLogger;
import eu.opends.main.Simulator;

/**
 * 
 * @author Rafael Math
 */
public class LimitedKeyReactionTimer extends ReactionTimer
{
	private InputManager inputManager;
	private ReactionListener reactionListener;
	private Long maxDuration;
	
	
	public LimitedKeyReactionTimer(Simulator sim, InputManager inputManager, ReactionListener reactionListener, 
			ReactionLogger reactionlogger, long experimentStartTime, String timerID, int index)
	{
		super(sim, null, experimentStartTime, timerID, index); // reactionlogger set to null (not used)
		
		this.inputManager = inputManager;
		this.reactionListener = reactionListener;
	}

	
	public void setup(String nextReactionGroupID, String correctReaction, 
			String failureReaction, Long nextMaxDuration, String nextComment)
	{		
		long currentTime = System.currentTimeMillis();
		
		if(timerIsActive)
		{
			// aborted reaction
			long elapsedTime = currentTime - reactionTimer.getTimeInMillis();
			stopTimer();
			//Simulator.getDrivingTaskLogger().reportText("RTM: aborted reaction measurement (" + elapsedTime + " ms)", new Date(currentTime)); 
			Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "aborted", elapsedTime, 
					maxDuration, null, null, null, null, null, null);
			//System.err.println("Aborted: " + elapsedTime);
		}
		
		reactionGroupID = nextReactionGroupID;
		maxDuration = nextMaxDuration;
		comment = nextComment;

		timerIsActive = true;
		reactionTimer = new GregorianCalendar();
		correctReactionReported = false;
		failureReactionReported = false;
		
		addMapping("reaction_group_" + index, correctReaction);
		addMapping("failure_group_" + index, failureReaction);			
		inputManager.addListener(reactionListener, "reaction_group_" + index, "failure_group_" + index);
		
		/*
		String maxDurationString = "unlimited";
		if(maxDuration != null)
			maxDurationString = maxDuration + " ms";
		
		Simulator.getDrivingTaskLogger().reportText("RTM: setup reaction measurement (max. duration: " 
				+ maxDurationString + ")", new Date(currentTime));
		*/
		Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "setup", null, 
				maxDuration, null, null, null, null, null, null);
		//System.err.println("Setup reaction timer '" + timerID + "' (reaction group: '" + reactionGroupID + "')");
	}

	
	@Override
	public void update()
	{
		if(timerIsActive)
		{
			long currentTime = System.currentTimeMillis();
			long elapsedTime = currentTime - reactionTimer.getTimeInMillis();
			
			if(maxDuration != null && elapsedTime >= maxDuration)
			{
				// missed reaction
				stopTimer();
				//Simulator.getDrivingTaskLogger().reportText("RTM: missed reaction (" + maxDuration + " ms)", new Date(currentTime)); 
				Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "missed", elapsedTime, 
						maxDuration, null, null, null, null, null, null);
				//System.err.println("Missed: " + maxDuration);
			}
			else if(correctReactionReported)
			{
				// correct reaction
				stopTimer();
				//Simulator.getDrivingTaskLogger().reportText("RTM: correct reaction (" + elapsedTime + " ms)", new Date(currentTime)); 
				Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "correct", elapsedTime, 
						maxDuration, null, null, null, null, null, null);
				//System.err.println("Correct: " + elapsedTime);
			}
			else if(failureReactionReported)
			{
				// failure reaction
				stopTimer();
				//Simulator.getDrivingTaskLogger().reportText("RTM: failure reaction (" + elapsedTime + " ms)", new Date(currentTime)); 
				Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "false", elapsedTime, 
						maxDuration, null, null, null, null, null, null);
				//System.err.println("Failure: " + elapsedTime);
			}
		}
	}
	
	
	@Override
	public void close()
	{
		if(timerIsActive)
		{
			// aborted reaction
			long currentTime = System.currentTimeMillis();
			long elapsedTime = currentTime - reactionTimer.getTimeInMillis();
			stopTimer();
			//Simulator.getDrivingTaskLogger().reportText("RTM: aborted reaction measurement (" + elapsedTime + " ms)", new Date(currentTime)); 
			Simulator.getDrivingTaskLogger().reportEvent(currentTime, "reactionTimeMeasurement", "aborted", elapsedTime, 
					maxDuration, null, null, null, null, null, null);
			//System.err.println("Aborted: " + elapsedTime);
		}
	}
	
	
	private void stopTimer()
	{
		reactionTimer = null;
		timerIsActive = false;
		
		inputManager.deleteMapping("reaction_group_" + index);
		inputManager.deleteMapping("failure_group_" + index);
	}
	
	
	private void addMapping(String mappingID, String buttonString) 
	{
		String[] buttonArray = buttonString.split(",");
		for(String button : buttonArray)
		{
			button = button.toUpperCase().trim();
			Trigger trigger = getTrigger(button);
			if(trigger != null)
				inputManager.addMapping(mappingID, trigger);
		}
	}
	
	
	private Trigger getTrigger(String buttonName)
	{
		try {
			
			if(buttonName.startsWith("KEY_"))
			{
				// prefix "KEY_"
				Field field = KeyInput.class.getField(buttonName);
				int keyNumber = field.getInt(KeyInput.class);
				return new KeyTrigger(keyNumber);
				
			}
			else if(buttonName.startsWith("JOY_"))
			{
				// prefix "JOY_" (e.g. JOY_3 or JOY_1_5)
				//String suffix = buttonName.replace("JOY_", "");
				String[] numberArray = buttonName.split("_");
				if(numberArray.length == 2)
				{
					int buttonNumber = Integer.parseInt(numberArray[1])-1;
					return new JoyButtonTrigger(0, buttonNumber);
				}
				else if(numberArray.length == 3)
				{
					int deviceNumber = Integer.parseInt(numberArray[1]);
					int buttonNumber = Integer.parseInt(numberArray[2])-1;
					return new JoyButtonTrigger(deviceNumber, buttonNumber);
				}
				
				System.err.println("LimitedKeyReactionTimer: '" + buttonName + "' is an invalid button name");
				return null;
			}
			else
			{
				// no prefix
				String keyString = "KEY_" + buttonName;
				Field field = KeyInput.class.getField(keyString);
				int keyNumber = field.getInt(KeyInput.class);
				return new KeyTrigger(keyNumber);
			}
			
		} catch (Exception e) {
			
			if(!buttonName.isEmpty())
				System.err.println("Invalid key '" + buttonName + "'! Use prefix 'KEY_' or 'JOY_'");
			return null;
		}
	}
	
}
