/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.drivingTask.scene;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.xpath.XPathConstants;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.amazonaws.services.polly.model.OutputFormat;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioData.DataType;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.Light;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Sphere;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Image;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

import eu.opends.audio.AmazonPollyTTS;
import eu.opends.basics.MapObject;
import eu.opends.basics.MapObjectOD;
import eu.opends.basics.SimulationBasics;
import eu.opends.car.ResetPosition;
import eu.opends.drivingTask.DrivingTaskDataQuery;
import eu.opends.drivingTask.DrivingTaskDataQuery.Layer;
import eu.opends.dynamicObjects.DynamicObject;
import eu.opends.dynamicObjects.messageSign.MessageSign;
import eu.opends.dynamicObjects.messageSign.MessageSignText;
import eu.opends.dynamicObjects.messageSign.MessageSignTextLine;
import eu.opends.dynamicObjects.messageSign.MessageSignTextLine.TextAlignment;
import eu.opends.dynamicObjects.speedIndicator.SpeedIndicator;
import eu.opends.dynamicObjects.variableSign.VariableSign;
import eu.opends.dynamicObjects.variableSign.VariableSign.TransitionType;
import eu.opends.dynamicObjects.variableSign.VariableSignImage;
import eu.opends.dynamicObjects.variableSign.VariableSignImageSequence;
import eu.opends.environment.vegetation.RoadSideVegetation;
import eu.opends.environment.vegetation.RoadSideVegetation.SideOfRoad;
import eu.opends.gesture.ReferenceObjectParams;
import eu.opends.main.Simulator;
import eu.opends.movie.GuiNodeScreen;
import eu.opends.movie.MovieData;
import eu.opends.movie.MoviePlayer;
import eu.opends.movie.SceneNodeScreen;
import eu.opends.opendrive.util.ODPosition;
import eu.opends.opendrive.util.ODPositionWithOffsets;
import eu.opends.tools.Util;
import eu.opends.traffic.OpenDRIVECar;
import eu.opends.traffic.Pedestrian;
import eu.opends.traffic.TrafficCar;
import eu.opends.traffic.TrafficObject;

/**
 * 
 * @author Rafael Math
 */
public class SceneLoader 
{
	private DrivingTaskDataQuery dtData;
	private SimulationBasics sim;
	private AssetManager assetManager;
	private Map<String, Spatial> geometryMap = new HashMap<String, Spatial>();
	private Map<String, Vector3f> pointMap = new HashMap<String, Vector3f>();
	private Map<String, ResetPosition> resetPositionMap = new HashMap<String, ResetPosition>();
	private List<MapObject> mapObjectsList = new ArrayList<MapObject>();
	private List<MapObjectOD> dependentMapObjectsList = new ArrayList<MapObjectOD>();
	private String pathToVegetationObjects = "assets/Models/Vegetation/vegetationObjects.txt";
	private HashMap<String, ArrayList<String>> vegetationGroupMap = new HashMap<String, ArrayList<String>>();
	private ArrayList<RoadSideVegetation> roadSideVegetationList = new ArrayList<RoadSideVegetation>();
	
	
	public SceneLoader(DrivingTaskDataQuery dtData, SimulationBasics sim) 
	{
		this.dtData = dtData;
		this.sim = sim;
		this.assetManager = sim.getAssetManager();
		assetManager.registerLocator("assets", FileLocator.class);
		getGeometries(new String[]{"box", "sphere", "cylinder", "terrain"});
		getPoints();
		getResetPoints();
		createMapObjects();
		extractVegetationData();
	}


	public Map<String,AudioNode> getAudioNodes(AmazonPollyTTS pollyTTS)
	{
		Map<String,AudioNode> audioNodeList = new HashMap<String,AudioNode>();
		
		try {
			
			NodeList audioNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:sounds/scene:sound", XPathConstants.NODESET);

			for (int k = 1; k <= audioNodes.getLength(); k++) 
			{
				Node currentNode = audioNodes.item(k-1);
				
				// get ID of audio node
				//String audioNodeID = dtData.getValue(Layer.SCENE,
				//		"/scene:scene/scene:sounds/scene:sound["+k+"]/@id", String.class);
				String audioNodeID = currentNode.getAttributes().getNamedItem("id").getNodeValue();
				
				// get URL of audio node
				//String audioNodeURL = dtData.getValue(Layer.SCENE,
				//		"/scene:scene/scene:sounds/scene:sound["+k+"]/@key", String.class);
				String audioNodeURL = currentNode.getAttributes().getNamedItem("key").getNodeValue();
			
				if((audioNodeURL != null) && (!audioNodeURL.equals("")))
				{
					// If the element <fromAmazonPolly> exists and the given <text> is not empty,
					// create a new ogg sound file using Amazon Polly TTS.
					// The resulting sound file will be saved under the location specified in
					// "key" and will be loaded afterwards if the speech synthesis was successful.
					String text = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:fromAmazonPolly/scene:text", String.class);
					if(text != null && !text.isEmpty())
					{
						boolean success = pollyTTS.synthesize(text, OutputFormat.Ogg_vorbis, audioNodeURL);
		        
						if(!success)
						{
							// error message
							System.err.println("SceneLoader: unable to generate audio file '" + audioNodeURL 
									+ "' from text (found in soundID '" + audioNodeID + "').");
							
							// show warning message if <amazonPolly> has been disabled in settings.xml
							if(!pollyTTS.isEnabled())
								System.err.println("Amazon Polly must be enabled in settings.xml");
								
							// go to next audio node
							continue;
						}
					}
					
					AudioNode audioNode = new AudioNode(assetManager, audioNodeURL, DataType.Buffer);
					
					// set positional
					Boolean isPositional = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:positional/@value", Boolean.class);
					if(isPositional == null)
						isPositional = false;
					audioNode.setPositional(isPositional);
					if(isPositional)
					{
						Node parentNodeNode = (Node) dtData.xPathQuery(Layer.SCENE, 
								"/scene:scene/scene:sounds/scene:sound[\"+k+\"]/scene:positional/scene:parentNode", XPathConstants.NODE);
						
						boolean attachedToParent = false;
						if(parentNodeNode != null && sim instanceof Simulator)
						{
							NodeList childNodeList = parentNodeNode.getChildNodes();
							
							for (int l = 1; l <= childNodeList.getLength(); l++) 
							{
								Node childNode = childNodeList.item(l-1);

								if (childNode.getNodeName().equals("modelNode"))
								{
									String modelID = childNode.getTextContent();
									
									Spatial modelNode = sim.getRootNode().getChild(modelID);
									
									if(modelNode != null && modelNode instanceof com.jme3.scene.Node)
									{
										((com.jme3.scene.Node) modelNode).attachChild(audioNode);
										
										attachedToParent = true;
										
										System.err.println("attached to model node '" + modelID + "'");
									}
									else
										System.err.println("SceneLoader: '"	+ modelID 
												+ "' is not a valid modelID (found in soundID '" + audioNodeID + "')");
								}

								else if (childNode.getNodeName().equals("trafficNode"))
								{
									String trafficID = childNode.getTextContent();
									TrafficObject trafficObject = ((Simulator) sim).getPhysicalTraffic().getTrafficObject(trafficID);
									
									if(trafficObject instanceof OpenDRIVECar)
										((OpenDRIVECar) trafficObject).getCarNode().attachChild(audioNode);
									else if(trafficObject instanceof TrafficCar)
										((TrafficCar) trafficObject).getCarNode().attachChild(audioNode);
									else if(trafficObject instanceof Pedestrian)
										((Pedestrian) trafficObject).getPedestrianNode().attachChild(audioNode);
								    
									attachedToParent = true;
									
									System.err.println("attached to traffic node '" + trafficID + "'");
								}

								else if (childNode.getNodeName().equals("driverNode"))
								{
									((Simulator) sim).getCar().getCarNode().attachChild(audioNode);
									
									attachedToParent = true;
									
									System.err.println("attached to steering car node");
								}
							}
						}
						
						if(!attachedToParent)
						{
							sim.getRootNode().attachChild(audioNode);

							System.err.println("attached to root node");
						}

						
						Vector3f translation = dtData.getVector3f(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:positional/scene:translation");
						if(translation != null)
							audioNode.setLocalTranslation(translation);
						
						
						// set reference distance of attenuation (volume will halve after x meters)
						Float refDistance = dtData.getValue(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:positional/scene:refDistance", Float.class);
						if(refDistance == null)
							refDistance = 5.0f; //10.0f;
						audioNode.setRefDistance(refDistance);
						
						
						// set max distance of attenuation (no further volume decrease after x meters)
						Float maxDistance = dtData.getValue(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:positional/scene:maxDistance", Float.class);
						if(maxDistance == null)
							maxDistance = 2000.0f; //200.0f;
						audioNode.setMaxDistance(maxDistance);
						

						// set reverberation status
						Boolean isReverbEnabled = dtData.getValue(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:positional/scene:reverb", Boolean.class);
						if(isReverbEnabled == null)
							isReverbEnabled = false;
						audioNode.setReverbEnabled(isReverbEnabled);
						
						//audioNode.setVelocity(new Vector3f(0,0,300));
					}
					
					
					// set directional
					Boolean isDirectional = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:directional/@value", Boolean.class);
					if(isDirectional == null || isPositional)
						isDirectional = false;
					audioNode.setDirectional(isDirectional);
					if(isDirectional)
					{
						Vector3f direction = dtData.getVector3f(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:directional/scene:direction");
						if(direction != null)
							audioNode.setDirection(direction);
						
						Float innerAngle = dtData.getValue(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:directional/scene:innerAngle", 
								Float.class);
						if(innerAngle != null)
							audioNode.setInnerAngle(dtData.degToRad(innerAngle));
						
						Float outerAngle = dtData.getValue(Layer.SCENE,
								"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:directional/scene:outerAngle", 
								Float.class);
						if(outerAngle != null)
							audioNode.setOuterAngle(dtData.degToRad(outerAngle));
					}
					
					
					// set looping
					Boolean isLooping = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:loop", Boolean.class);
					if(isLooping == null)
						isLooping = false;
					audioNode.setLooping(isLooping);
					
					
					// set volume
					Float volume = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:volume", Float.class);
					if(volume == null)
						volume = 0.5f;
					audioNode.setVolume(volume);
					
					
					// set pitch
					Float pitch = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:sounds/scene:sound["+k+"]/scene:pitch", Float.class);
					if(pitch == null)
						pitch = 1.0f;
					audioNode.setPitch(pitch);

					
					audioNodeList.put(audioNodeID, audioNode);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return audioNodeList;		
	}
	
	
	public TreeMap<String, MovieData> getMoviesMap() 
	{
		TreeMap<String,MovieData> movieMap = new TreeMap<String,MovieData>();
		
		try {
			
			NodeList movieNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:movies/scene:movie", XPathConstants.NODESET);

			for (int k = 1; k <= movieNodes.getLength(); k++) 
			{
				Node currentNode = movieNodes.item(k-1);
				
				// get ID of movie node
				String movieNodeID = currentNode.getAttributes().getNamedItem("id").getNodeValue();
				
				// get URL of movie node
				String movieNodeURL = currentNode.getAttributes().getNamedItem("key").getNodeValue();
				
				if(!movieNodeURL.startsWith("assets"))
					movieNodeURL = "assets/" + movieNodeURL;
				
				File movieFile = new File(movieNodeURL);

				if((movieNodeURL != null) && (!movieNodeURL.equals("")) && movieFile.exists() && !movieFile.isDirectory())
				{
					// noOfCycles
					Integer noOfCycles = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:movies/scene:movie["+k+"]/scene:noOfCycles", Integer.class);
					if(noOfCycles == null)
						noOfCycles = 1;
					
					// ignoreAspectRatio
					Boolean ignoreAspectRatio = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:movies/scene:movie["+k+"]/scene:ignoreAspectRatio", Boolean.class);
					if(ignoreAspectRatio == null)
						ignoreAspectRatio = false;
					
					// muteAudio
					Boolean muteAudio = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:movies/scene:movie["+k+"]/scene:muteAudio", Boolean.class);
					if(muteAudio == null)
						muteAudio = false;
					
					// playbackRate
					Float playbackRate = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:movies/scene:movie["+k+"]/scene:playbackRate", Float.class);
					if(playbackRate == null)
						playbackRate = 1.0f;
					
					// volume
					Float volume = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:movies/scene:movie["+k+"]/scene:volume", Float.class);
					if(volume == null)
						volume = 1.0f;
					
					movieMap.put(movieNodeID, new MovieData(movieNodeID, movieNodeURL, noOfCycles, 
							ignoreAspectRatio, muteAudio, playbackRate, volume));
				}
				else
					System.err.println("SceneLoader: movie '" + movieNodeID + "' could not be found (URL:"
							+ movieNodeURL + ")");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return movieMap;
	}
	
	
	public TreeMap<String, Picture> getPictures()
	{
		TreeMap<String,Picture> pictureList = new TreeMap<String,Picture>();
		
		try {
			
			NodeList pictureNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:pictures/scene:picture", XPathConstants.NODESET);

			for (int k = 1; k <= pictureNodes.getLength(); k++) 
			{
				Node currentNode = pictureNodes.item(k-1);
				
				// get ID of picture node
				//String pictureID = dtData.getValue(Layer.SCENE,
				//		"/scene:scene/scene:pictures/scene:picture["+k+"]/@id", String.class);
				String pictureID = currentNode.getAttributes().getNamedItem("id").getNodeValue();
				
				// get URL of picture node
				//String pictureURL = dtData.getValue(Layer.SCENE,
				//		"/scene:scene/scene:pictures/scene:picture["+k+"]/@key", String.class);
				String pictureURL = currentNode.getAttributes().getNamedItem("key").getNodeValue();
				
				String pictureLevel = currentNode.getAttributes().getNamedItem("level").getNodeValue();
							
				if((pictureURL != null) && (!pictureURL.equals("")))
				{
					Picture picture = new Picture(pictureID);
					
					// set useAlpha
					Boolean useAlpha = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:pictures/scene:picture["+k+"]/scene:useAlpha", Boolean.class);
					if(useAlpha == null)
						useAlpha = false;
					picture.setImage(sim.getAssetManager(), pictureURL, useAlpha);
					

					// set width
					Integer width = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:pictures/scene:picture["+k+"]/scene:width", Integer.class);
					if(width == null)
						width = 100;
					picture.setWidth(width);
					
					
					// set height
					Integer height = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:pictures/scene:picture["+k+"]/scene:height", Integer.class);
					if(height == null)
						height = 100;
					picture.setHeight(height);

					
					// set isVisible
					Boolean isVisible = dtData.getValue(Layer.SCENE,
							"/scene:scene/scene:pictures/scene:picture["+k+"]/scene:visible", Boolean.class);
					if(isVisible == null)
						isVisible = false;
					picture.setCullHint(isVisible?CullHint.Dynamic:CullHint.Always);
					
					
					// set position
					NodeList pictureChildren = currentNode.getChildNodes();
					int vPosition = 10;
					int hPosition = 10;
					
					for (int l = 1; l <= pictureChildren.getLength(); l++) 
					{
						Node currentPictureChild = pictureChildren.item(l-1);
						
						if(currentPictureChild.getNodeName().equals("vPosition"))
						{
							NodeList vPositionChildren = currentPictureChild.getChildNodes();
							for (int m = 1; m <= vPositionChildren.getLength(); m++) 
							{
								Node currentVPositionChild = vPositionChildren.item(m-1);
								int maxHeight = sim.getSettings().getHeight();
								
								if(currentVPositionChild.getNodeName().equals("center"))
								{
									vPosition = (int) ((maxHeight-height)/2.0f);
									//System.out.println("center: " + vPosition);
								}
								else if(currentVPositionChild.getNodeName().equals("fromTop") || 
										currentVPositionChild.getNodeName().equals("fromBottom"))
								{
									float valueFloat = 10;
									
									String value = currentVPositionChild.getAttributes().getNamedItem("value").getNodeValue();
									if(value != null && !value.isEmpty())
										valueFloat = Float.parseFloat(value);
									
									String unit = currentVPositionChild.getAttributes().getNamedItem("unit").getNodeValue();
									
									if((value != null && !value.isEmpty()) && (unit.equals("%") || unit.equals("percent")))
										vPosition = (int)((maxHeight*valueFloat)/100.0f);
									else
										vPosition = ((int) valueFloat);
									
									if(currentVPositionChild.getNodeName().equals("fromTop"))
										vPosition = maxHeight - (vPosition + height);
									
									vPosition = Math.max(Math.min(vPosition,maxHeight),0);
									
									//System.out.println("vpos: " + vPosition);
								}
							}
						}
						else if(currentPictureChild.getNodeName().equals("hPosition"))
						{
							NodeList hPositionChildren = currentPictureChild.getChildNodes();
							for (int m = 1; m <= hPositionChildren.getLength(); m++) 
							{
								Node currentHPositionChild = hPositionChildren.item(m-1);
								int maxWidth = sim.getSettings().getWidth();
																
								if(currentHPositionChild.getNodeName().equals("center"))
								{
									hPosition = (int) ((maxWidth-width)/2.0f);
									//System.out.println("hpos center: " + hPosition);
								}
								else if(currentHPositionChild.getNodeName().equals("fromLeft") || 
										currentHPositionChild.getNodeName().equals("fromRight"))
								{
									float valueFloat = 10;
									
									String value = currentHPositionChild.getAttributes().getNamedItem("value").getNodeValue();
									if(value != null && !value.isEmpty())
										valueFloat = Float.parseFloat(value);
									
									String unit = currentHPositionChild.getAttributes().getNamedItem("unit").getNodeValue();
									
									if((value != null && !value.isEmpty()) && (unit.equals("%") || unit.equals("percent")))
										hPosition = (int)((maxWidth*valueFloat)/100.0f);
									else
										hPosition = ((int) valueFloat);
									
									if(currentHPositionChild.getNodeName().equals("fromRight"))
										hPosition = maxWidth - (hPosition + width);
									
									hPosition = Math.max(Math.min(hPosition,maxWidth),0);
									
									//System.out.println("hpos: " + hPosition);
								}
							}
						}
					}
					
					picture.setPosition(hPosition, vPosition);				
					
					if(pictureList.containsKey(pictureLevel))
					{
						Picture previousPicture = pictureList.get(pictureLevel);
						System.err.println("Caution: Picture '" + previousPicture.getName() + 
								"' will be overwritten by picture '" + picture.getName() + "'. Same level conflict." );
					}
						
					pictureList.put(pictureLevel, picture);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pictureList;		
	}
	

	public List<MapObject> getMapObjects()
	{
		return mapObjectsList;
	}
	
	
	public List<MapObjectOD> getDependentMapObjects()
	{
		return dependentMapObjectsList;
	}
	
	
	/**
	 * Returns a list of all map objects (dynamic, static spatial objects) which
	 * were defined in the driving task. Those objects will be added to the
	 * objects loaded within the map model. 
	 */
	public void createMapObjects() 
	{	
		// Structure:
		// <scene>
		//     <models>
		//          <model>
		//				<mass>0</mass>
		//				<visible>true</visible>
		//				<addToMapNode>false<addToMapNode>
		//				<collidable>true</collidable>
		//				<scale>
		//					<vector jtype="java_lang_Float" size="3">
		//						<entry>1</entry>
		//						<entry>1</entry>
		//						<entry>1</entry>
		//					</vector>
		//				</scale>
		//
		//				<rotation quaternion="false">
		//					<vector jtype="java_lang_Float" size="3">
		//						<entry>0</entry>
		//						<entry>0</entry>
		//						<entry>0</entry>
		//					</vector>
		//				</rotation>
		//
		//				<translation>
		//					<vector jtype="java_lang_Float" size="3">
		//						<entry>0</entry>
		//						<entry>0</entry>
		//						<entry>0</entry>
		//					</vector>
		//				</translation>

		
		
		try {
			
			NodeList modelNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:models/scene:model", XPathConstants.NODESET);

			for (int k = 1; k <= modelNodes.getLength(); k++) 
			{
				Node currentNode = modelNodes.item(k-1);
				
				// get name
				//String name = dtData.getValue(Layer.SCENE, 
				//		"/scene:scene/scene:models/scene:model["+k+"]/@id", String.class);
				String name = currentNode.getAttributes().getNamedItem("id").getNodeValue();
				
				// get spatial model
				Spatial spatial = null;
				//String spatialURL = dtData.getValue(Layer.SCENE, 
				//		"/scene:scene/scene:models/scene:model["+k+"]/@key", String.class);
				String spatialURL = currentNode.getAttributes().getNamedItem("key").getNodeValue();

				if((spatialURL != null) && (!spatialURL.equals("")))
				{
					spatial = assetManager.loadModel(spatialURL);
				}
				else
				{
					//String geometryRef = dtData.getValue(Layer.SCENE, 
					//		"/scene:scene/scene:models/scene:model["+k+"]/@ref", String.class);
					String geometryRef = currentNode.getAttributes().getNamedItem("ref").getNodeValue();

					if((geometryRef != null) && (geometryMap.containsKey(geometryRef)))
					{
						// get pre-defined shape (!!! clone() causes errors with multiple terrains !!!)
						spatial = geometryMap.get(geometryRef).deepClone();
					}
					else
						throw new Exception("No spatial available for model '" + name + "'");
				}
				


				NodeList childnodes = currentNode.getChildNodes();
				
				Float mass = null;
				Boolean visible = null;
				Boolean addToMapNode = null;
				String collisionShape = null;
				Vector3f scale = null;
				Quaternion rotation = null;
				Vector3f translation = null;
				ODPositionWithOffsets odPos = null;
				String collisionSound = null;
				ReferenceObjectParams referenceObjectParams = null;
				
				for (int j = 1; j <= childnodes.getLength(); j++) 
				{
					Node currentChild = childnodes.item(j-1);
					
					// get material + color
					if(currentChild.getNodeName().equals("material"))
					{
						// get material
						//String materialFile = dtData.getValue(Layer.SCENE,
						//		"/scene:scene/scene:models/scene:model["+k+"]/scene:material/@key", String.class);
						
						Node materialNode = currentChild.getAttributes().getNamedItem("key");
						String matInstance = null;
						if(materialNode != null)
							matInstance = materialNode.getNodeValue();

						if(matInstance != null && !matInstance.equalsIgnoreCase(""))
						{
							Material material = assetManager.loadMaterial(matInstance);
							spatial.setMaterial(material);
						}
						else
						{
							NodeList materialChildren = currentChild.getChildNodes();
							for (int l = 1; l <= materialChildren.getLength(); l++) 
							{
								Node currentMaterialChild = materialChildren.item(l-1);
								
								if(currentMaterialChild.getNodeName().equals("color"))
								{
									// get color
									ColorRGBA color = getColorRGBA(currentMaterialChild);
	
									if(color != null && spatial != null)
									{
										String matDefinition = "Common/MatDefs/Misc/Unshaded.j3md";
										Material material = new Material(sim.getAssetManager(), matDefinition);
										material.setColor("Color", color);
										spatial.setMaterial(material);
									}
								}
							}
						}
					}
					
					else if(currentChild.getNodeName().equals("mass"))
					{
						mass = Float.parseFloat(currentChild.getTextContent());
					}
					
					else if(currentChild.getNodeName().equals("visible"))
					{
						visible = Boolean.parseBoolean(currentChild.getTextContent());
					}
					
					else if(currentChild.getNodeName().equals("addToMapNode"))
					{
						addToMapNode = Boolean.parseBoolean(currentChild.getTextContent());
					}
					
					else if(currentChild.getNodeName().equals("collisionShape"))
					{
						collisionShape = currentChild.getTextContent();
					}
					
					else if(currentChild.getNodeName().equals("textureScale"))
					{
						Vector2f textureScale = getVector2f(currentChild);
						
						for(Geometry g : Util.getAllGeometries(spatial))
						{
							g.getMesh().scaleTextureCoordinates(textureScale);
							break;
						}
					}
					
					else if(currentChild.getNodeName().equals("scale"))
					{
						scale = getVector3f(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("rotation"))
					{
						rotation = getQuaternion(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("translation"))
					{
						translation = getVector3f(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("openDrivePosition"))
					{
						odPos = getOpenDrivePosWithOffsets(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("collisionSound"))
					{
						collisionSound = currentChild.getAttributes().getNamedItem("ref").getNodeValue();
					}
					
					else if(currentChild.getNodeName().equals("referenceObject"))
					{
						referenceObjectParams = extractReferenceObjectParams(currentChild, spatial);
					}
					
					else if(currentChild.getNodeName().equals("ambientLight"))
					{
						// add ambient light to current spatial
						if(spatial != null)
						{
							NodeList lightnodes = currentChild.getChildNodes();
							for (int z = 1; z <= lightnodes.getLength(); z++) 
							{
								Node lightChild = lightnodes.item(z-1);
								if(lightChild.getNodeName().equals("color"))
								{
									AmbientLight ambientLight = new AmbientLight();
							        ColorRGBA color = getColorRGBA(lightChild);
							        ambientLight.setColor(color);
							        spatial.addLight(ambientLight);
								}
							}
						}
					}
					
					else if(currentChild.getNodeName().equals("shadowMode"))
					{
						// add shadow mode to current spatial
						if(spatial != null)
						{
							ShadowMode shadowMode = getShadowMode(currentChild);
							spatial.setShadowMode(shadowMode);							
						}
					}
				}

				if(mass == null)
					mass = 0f;
				
				if(visible == null)
					visible = true;
				
				if(addToMapNode == null)
					addToMapNode = false;
				
				if(scale == null)
					scale = new Vector3f(1,1,1);
				
				// build map object
				if((name != null) && (spatial != null) && (translation != null) && (rotation != null) && 
						(collisionShape != null))
				{
					MapObject mapObject = new MapObject(name, spatial, translation, rotation, scale,
							visible, addToMapNode, collisionShape, mass, spatialURL, collisionSound, 
							referenceObjectParams);
					mapObjectsList.add(mapObject);
				}
				else if((name != null) && (spatial != null) && (odPos != null) && (rotation != null) 
						&& (collisionShape != null))
				{
					MapObjectOD mapObjectOD = new MapObjectOD(name, spatial, odPos, rotation, scale, 
							visible, addToMapNode, collisionShape, mass, spatialURL, collisionSound,
							referenceObjectParams);
					dependentMapObjectsList.add(mapObjectOD);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Extracts the path to a text file where all available vegetation objects will be defined,
	 * a list of vegetation groups (= objects that may be used randomly together along a road),
	 * and a list of OpenDRIVE roads having vegetation aside. For each road, parameters like,
	 * range, distance from road reference line and distance between vegetation objects may be
	 * specified (optional).
	 */
	public void extractVegetationData() 
	{	
		// Structure:
		// <scene>
		//     <vegetation>
		//			<pathToVegetationObjects>Models/Vegetation/vegetationObjects.txt</pathToVegetationObjects>
		//          <vegetationGroups>
		//				<vegetationGroup id="pineTrees">
		//					<vegetationObject>pineTree01</vegetationObject>
		//					<vegetationObject>pineTree02</vegetationObject>
		//					<vegetationObject>pineTree03</vegetationObject>
		//				</vegetationGroup>
		//				<vegetationGroup id="bushes">
		//					<vegetationObject>bush01</vegetationObject>
		//					<vegetationObject>bush02</vegetationObject>
		//					<vegetationObject>bush03</vegetationObject>
		//				</vegetationGroup>
		//			</vegetationGroups>
		//			<roadSideVegetation>
		//				<road>
		//					<openDriveId>road01</openDriveId>
		//					<sideOfRoad>Left</sideOfRoad>
		//					<range minS="100" maxS="200" />
		//					<spacing min="10" max="15" />
		//					<distanceFromRoad min="15" max="16" />
		//					<vegetationGroup>pineTrees</vegetationGroup>
		//				</road>
		//				<road>
		//					<openDriveId>road02</openDriveId>
		//					<sideOfRoad>Both</sideOfRoad>
		//					<range minS="10" maxS="500" />
		//					<spacing min="5" max="7" />
		//					<distanceFromRoad min="10" max="12" />
		//					<vegetationGroup>bushes</vegetationGroup>
		//				</road>
		//			</roadSideVegetation>
		try {
			
			String path = dtData.getValue(Layer.SCENE, 
					"/scene:scene/scene:vegetation/scene:pathToVegetationObjects", String.class);
			if(path != null && !path.isEmpty())
			{
				if(!path.startsWith("assets"))
					path = "assets/" + path;
				
				pathToVegetationObjects = path;
			}

			
			NodeList vegetationGroupNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:vegetation/scene:vegetationGroups/scene:vegetationGroup", XPathConstants.NODESET);
			
			for (int k = 1; k <= vegetationGroupNodes.getLength(); k++) 
			{
				Node currentvegetationGroupNode = vegetationGroupNodes.item(k-1);
				
				// get id
				String id = null;
				Node idNode = currentvegetationGroupNode.getAttributes().getNamedItem("id");
				if(idNode != null)
					id = idNode.getNodeValue();
				
				
				if(id != null && !id.isEmpty())
				{
					NodeList childnodes = currentvegetationGroupNode.getChildNodes();
					ArrayList<String> vegetationObjectList = new ArrayList<String>();
					for (int j = 1; j <= childnodes.getLength(); j++) 
					{
						Node currentChild = childnodes.item(j-1);
					
						// get material + color
						if(currentChild.getNodeName().equals("vegetationObject"))
						{
							String vegetationObjectID = currentChild.getTextContent();
							vegetationObjectList.add(vegetationObjectID);
						}
					}
					
					if(!vegetationObjectList.isEmpty())
						vegetationGroupMap.put(id,vegetationObjectList);
					else
						System.err.println("Vegetation group '" + id + "' has no vegetation objects");
				}
				else
					System.err.println("Each vegetation group must have a unique ID");
			}
			
			
			
			NodeList roadNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:vegetation/scene:roadSideVegetation/scene:road", XPathConstants.NODESET);
			
			for (int k = 1; k <= roadNodes.getLength(); k++) 
			{
				Node currentRoadNode = roadNodes.item(k-1);
				
				String referenceID = null;
				String roadID = null;
				SideOfRoad sideOfRoad = null;
				Float minS = null;
				Float maxS = null;
				Float minSpacing = null; 
				Float maxSpacing = null;
				Float minDistFromRoad = null; 
				Float maxDistFromRoad = null;
				ArrayList<String> vegetationNameList = null;
				
				Node referenceIdNode = currentRoadNode.getAttributes().getNamedItem("referenceId");
				if(referenceIdNode != null)
					referenceID = referenceIdNode.getNodeValue();
				
				NodeList childnodes = currentRoadNode.getChildNodes();
				for (int j = 1; j <= childnodes.getLength(); j++) 
				{
					Node currentChild = childnodes.item(j-1);
					
					if(currentChild.getNodeName().equals("openDriveId"))
					{
						roadID = currentChild.getTextContent();
					}
					else if(currentChild.getNodeName().equals("sideOfRoad"))
					{
						String sideOfRoadString = currentChild.getTextContent();
						sideOfRoad = SideOfRoad.valueOf(sideOfRoadString);
					}
					else if(currentChild.getNodeName().equals("range"))
					{
						Node minSNode = currentChild.getAttributes().getNamedItem("minS");
						if(minSNode != null)
							minS = Float.parseFloat(minSNode.getNodeValue());
						
						Node maxSNode = currentChild.getAttributes().getNamedItem("maxS");
						if(maxSNode != null)
							maxS = Float.parseFloat(maxSNode.getNodeValue());
					}
					else if(currentChild.getNodeName().equals("spacing"))
					{
						Node minSpacingNode = currentChild.getAttributes().getNamedItem("min");
						if(minSpacingNode != null)
							minSpacing = Float.parseFloat(minSpacingNode.getNodeValue());
						
						Node maxSpacingNode = currentChild.getAttributes().getNamedItem("max");
						if(maxSpacingNode != null)
							maxSpacing = Float.parseFloat(maxSpacingNode.getNodeValue());
					}
					else if(currentChild.getNodeName().equals("distanceFromRoad"))
					{
						Node minDistFromRoadNode = currentChild.getAttributes().getNamedItem("min");
						if(minDistFromRoadNode != null)
							minDistFromRoad = Float.parseFloat(minDistFromRoadNode.getNodeValue());
						
						Node maxDistFromRoadNode = currentChild.getAttributes().getNamedItem("max");
						if(maxDistFromRoadNode != null)
							maxDistFromRoad = Float.parseFloat(maxDistFromRoadNode.getNodeValue());
					}
					else if(currentChild.getNodeName().equals("vegetationGroup"))
					{
						String vegetationGroupString = currentChild.getTextContent();
						if(vegetationGroupMap.containsKey(vegetationGroupString))
							vegetationNameList = vegetationGroupMap.get(vegetationGroupString);
						else
							System.err.println("There is no vegetation group named '" + vegetationGroupString + "'.");
					}
				}
				
				if(roadID != null && vegetationNameList != null)
				{
					RoadSideVegetation roadSideVegetation = new RoadSideVegetation(referenceID, roadID, sideOfRoad, 
						minS, maxS, minSpacing, maxSpacing, minDistFromRoad, maxDistFromRoad, vegetationNameList);
					roadSideVegetationList.add(roadSideVegetation);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ReferenceObjectParams extractReferenceObjectParams(Node refObjNode, Spatial spatial)
	{
		// set defaults
		boolean isAlignSpatial = false;
		boolean isFrontLogoEnabled = false;
		boolean isBackLogoEnabled = false;
		boolean isLeftLogoEnabled = false;
		boolean isRightLogoEnabled = false;
		float logoXPos = -2.5f;
		float logoYPos = -0.5f;
		float logoWidth = 5;
		float logoHeight = -1;
		String logoTexturePath = "Textures/Logo/DFKI.jpg";
		HashMap<String, String> textureMap = new HashMap<String, String>();
		
		// fill variables with actual values (if given)
		NodeList childnodes = refObjNode.getChildNodes();
		for (int i = 0; i < childnodes.getLength(); i++) 
		{
			Node childNode = childnodes.item(i);
			if(childNode.getNodeName().equals("alignToGrid"))
			{
				// alignToGrid
				isAlignSpatial = Boolean.parseBoolean(childNode.getTextContent());
			}
			else if(childNode.getNodeName().equals("logo"))
			{
				// logo
				NodeList logoNodes = childNode.getChildNodes();
				for (int j = 0; j < logoNodes.getLength(); j++) 
				{
					Node logoNode = logoNodes.item(j);
					if(logoNode.getNodeName().equals("enableLogoSigns"))
					{
						// enableLogoSigns
						NodeList logoSignNodes = logoNode.getChildNodes();
						for (int k = 0; k < logoSignNodes.getLength(); k++) 
						{
							Node logoSignNode = logoSignNodes.item(k);
							if(logoSignNode.getNodeName().equals("front"))
							{
								// front
								isFrontLogoEnabled = Boolean.parseBoolean(logoSignNode.getTextContent());
							}
							else if(logoSignNode.getNodeName().equals("back"))
							{
								// back
								isBackLogoEnabled = Boolean.parseBoolean(logoSignNode.getTextContent());
							}
							else if(logoSignNode.getNodeName().equals("left"))
							{
								// left
								isLeftLogoEnabled = Boolean.parseBoolean(logoSignNode.getTextContent());
							}
							else if(logoSignNode.getNodeName().equals("right"))
							{
								// right
								isRightLogoEnabled = Boolean.parseBoolean(logoSignNode.getTextContent());
							}
						}
					}
					else if(logoNode.getNodeName().equals("height"))
					{
						// height
						logoHeight = Float.parseFloat(logoNode.getTextContent());
					}
					else if(logoNode.getNodeName().equals("width"))
					{
						// width
						logoWidth = Float.parseFloat(logoNode.getTextContent());
					}
					else if(logoNode.getNodeName().equals("xPos"))
					{
						// xPos
						logoXPos = Float.parseFloat(logoNode.getTextContent());
					}
					else if(logoNode.getNodeName().equals("yPos"))
					{
						// yPos
						logoYPos = Float.parseFloat(logoNode.getTextContent());					
					}
					else if(logoNode.getNodeName().equals("texturePath"))
					{
						// texturePath
						logoTexturePath = logoNode.getTextContent();
					}
				}
			} 
			else if(childNode.getNodeName().equals("textureMap"))
			{
				// textureMap
				NodeList textureNodes = childNode.getChildNodes();
				for (int j = 0; j < textureNodes.getLength(); j++) 
				{
					Node textureNode = textureNodes.item(j);
					if(textureNode.getNodeName().equals("texture"))
					{
						// texture
						String geometryName = textureNode.getAttributes().getNamedItem("geometryName").getNodeValue();
						
						String folderPath = spatial.getKey().getFolder();
						String textureFile = textureNode.getAttributes().getNamedItem("textureFile").getNodeValue();
						String texturePath = folderPath + textureFile;

						textureMap.put(geometryName, texturePath);
					}
				}
			}
		}
		
		return new ReferenceObjectParams(isAlignSpatial, isFrontLogoEnabled, isBackLogoEnabled, isLeftLogoEnabled, 
				isRightLogoEnabled, logoXPos, logoYPos, logoWidth, logoHeight, logoTexturePath, textureMap);
	}


	private ShadowMode getShadowMode(Node currentChild) 
	{
		String shadowModeString = currentChild.getTextContent();
		ShadowMode shadowMode = ShadowMode.Off;
		
		if(shadowModeString.equalsIgnoreCase("CastAndReceive"))
			shadowMode = ShadowMode.CastAndReceive;
		else if(shadowModeString.equalsIgnoreCase("Cast"))
			shadowMode = ShadowMode.Cast;
		else if(shadowModeString.equalsIgnoreCase("Receive"))
			shadowMode = ShadowMode.Receive;
		return shadowMode;
	}

	
	/**
	 * Returns a list of all lights. 
	 * 
	 * @return
	 * 			List of lights
	 */
	public List<Light> getLightList() 
	{
		List<Light> lightList = new ArrayList<Light>();

		try {
			
			NodeList lightNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:lights", XPathConstants.NODESET);

			for (int i = 1; i <= lightNodes.getLength(); i++) 
			{
				Node lightNode = lightNodes.item(i-1);
				NodeList lightTypeNodes = lightNode.getChildNodes();
				
				for (int k = 1; k <= lightTypeNodes.getLength(); k++) 
				{
					Node lightTypeNode = lightTypeNodes.item(k-1);
					NodeList childnodes = lightTypeNode.getChildNodes();
					
					if(lightTypeNode.getNodeName().equals("directionalLight"))
					{						
						Vector3f direction = null;
						ColorRGBA color = null;
						for (int j = 1; j <= childnodes.getLength(); j++) 
						{
							Node currentChild = childnodes.item(j-1);
								
							if(currentChild.getNodeName().equals("direction"))
								direction = getVector3f(currentChild);
							else if(currentChild.getNodeName().equals("color"))
								color = getColorRGBA(currentChild);
						}
						
						if(direction != null)
						{
							DirectionalLight directionalLight = new DirectionalLight();
					        directionalLight.setDirection(direction.normalizeLocal());
					        
					        if(color != null)
					        	directionalLight.setColor(color);
					        
					        lightList.add(directionalLight);
						}
					}
					
					if(lightTypeNode.getNodeName().equals("pointLight"))
					{
						Vector3f position = null;
						Float radius = null;
						ColorRGBA color = null;
						for (int j = 1; j <= childnodes.getLength(); j++) 
						{
							Node currentChild = childnodes.item(j-1);
								
							if(currentChild.getNodeName().equals("position"))
								position = getVector3f(currentChild);
							else if(currentChild.getNodeName().equals("radius"))
								radius = Float.parseFloat(currentChild.getChildNodes().item(0).getTextContent());
							else if(currentChild.getNodeName().equals("color"))
								color = getColorRGBA(currentChild);
						}
						
						if(position != null)
						{
							PointLight pointLight = new PointLight();
							pointLight.setPosition(position);
								
							if(radius != null)
								pointLight.setRadius(radius);
					        
							if(color!= null)
								pointLight.setColor(color);
							
					        lightList.add(pointLight);
						}
					}
					
					if(lightTypeNode.getNodeName().equals("ambientLight"))
					{						
						ColorRGBA color = null;
						for (int j = 1; j <= childnodes.getLength(); j++) 
						{
							Node currentChild = childnodes.item(j-1);

							if(currentChild.getNodeName().equals("color"))
								color = getColorRGBA(currentChild);
						}
						
						if(color != null)
						{
							AmbientLight ambientLight = new AmbientLight();
					        ambientLight.setColor(color);
					        
					        lightList.add(ambientLight);
						}
					}					
				}
			}

		} catch (Exception e) {
			System.err.println("SceneLoader: Error parsing light information");
			e.printStackTrace();
		}
		
		return lightList;
	}
	
	
	public String getSkyTexture(String defaultValue)
	{
		String skyTexture = dtData.getValue(Layer.SCENE, "/scene:scene/scene:skyTexture", String.class);
		if(skyTexture != null && !skyTexture.isEmpty())
			return skyTexture;
		else
			return defaultValue;
	}


	private Vector3f getVector3f(Node node) 
	{
		ArrayList<Float> array = new ArrayList<Float>();
		
		NodeList childnodes = node.getChildNodes();

		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("vector"))
			{
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("entry"))
					{
						array.add(Float.parseFloat(childChildNode.getChildNodes().item(0).getTextContent()));
					}
				}
			}
		}
		
		if(array.size() == 3)
			return new Vector3f(array.get(0),array.get(1),array.get(2));
		else
			return null;
	}
	
	private Vector2f getVector2f(Node node) 
	{
		ArrayList<Float> array = new ArrayList<Float>();
		
		NodeList childnodes = node.getChildNodes();

		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("vector"))
			{
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("entry"))
					{
						array.add(Float.parseFloat(childChildNode.getChildNodes().item(0).getTextContent()));
					}
				}
			}
		}
		
		if(array.size() == 2)
			return new Vector2f(array.get(0),array.get(1));
		else
			return null;
	}

	private Quaternion getQuaternion(Node node) 
	{
		ArrayList<Float> array = new ArrayList<Float>();
		
		NodeList childnodes = node.getChildNodes();

		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("vector"))
			{
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("entry"))
					{
						array.add(Float.parseFloat(childChildNode.getChildNodes().item(0).getTextContent()));
					}
				}
			}
		}
		
		if(array.size() == 3)
		{
			float yaw = degToRad(array.get(0));
			float roll = degToRad(array.get(1));
			float pitch = degToRad(array.get(2));
			return new Quaternion().fromAngles(yaw, roll, pitch);
		}
		else if(array.size() == 4)
			return new Quaternion(array.get(0),array.get(1),array.get(2),array.get(3));
		else
			return null;
	}
	
	private ColorRGBA getColorRGBA(Node node) 
	{
		NodeList childnodes = node.getChildNodes();

		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("vector"))
			{
				ArrayList<Float> array = new ArrayList<Float>();
				
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("entry"))
						array.add(Float.parseFloat(childChildNode.getChildNodes().item(0).getTextContent()));
				}
				
				if(array.size() == 4)
					return new ColorRGBA(array.get(0),array.get(1),array.get(2),array.get(3));
			}
			else if(childNode.getNodeName().equals("name"))
			{
				String colorRGBAString = childNode.getTextContent();				
				
				try {

				    Field f = ColorRGBA.class.getField(colorRGBAString);
				    Class<?> t = f.getType();
				    if(t == ColorRGBA.class)
				    	return (ColorRGBA) f.get(null);
				  
				} catch (NoSuchFieldException x) {
				    x.printStackTrace();
				} catch (IllegalAccessException x) {
				    x.printStackTrace();
				}
			}
		}

		return null;
	}
	
	
	private ODPosition getOpenDrivePos(Node node)
	{
		NamedNodeMap attributes = node.getAttributes();
		
		if(attributes != null)
		{
			String roadID = null;
			Integer lane = null;
			Double s = null;			
			
			Node roadIdNode = attributes.getNamedItem("roadID");
			if(roadIdNode != null)
			{
				String roadID_str = roadIdNode.getNodeValue();
				if(roadID_str != null && !roadID_str.isEmpty())
					roadID = roadID_str;
			}
			
			Node laneNode = attributes.getNamedItem("lane");
			if(laneNode != null)
			{
				String lane_str = laneNode.getNodeValue();
				if(lane_str != null && !lane_str.isEmpty())
					lane = Integer.parseInt(lane_str);
			}
		
			Node sNode = attributes.getNamedItem("s");
			if(sNode != null)
			{
				String s_str = sNode.getNodeValue();
				if(s_str != null && !s_str.isEmpty())
					s = Double.parseDouble(s_str);
			}
			
			if(lane != null && lane.equals(0))
			{
				System.err.println("SceneLoader::getOpenDrivePos(): invalid <openDrivePos> element given. " 
						+ "'lane' must not be 0.");
				return null;
			}
			
			if(s == null)
				s = 0.0;

			if((roadID != null) && (lane != null))
				return new ODPosition(roadID, lane, s);
			else
				System.err.println("SceneLoader::getOpenDrivePos(): invalid <openDrivePos> element given. " 
						+ "At least attributes roadID ('" + roadID + "') and lane ('" + lane + "') must be provided.");
		}
		
		return null;
	}
	
	
	private ODPositionWithOffsets getOpenDrivePosWithOffsets(Node node)
	{
		NamedNodeMap attributes = node.getAttributes();
		
		if(attributes != null)
		{
			String roadID = null;
			Float s = null;
			float lateralOffset = 0f;
			float verticalOffset = 0f;
			
			
			Node roadIdNode = attributes.getNamedItem("roadID");
			if(roadIdNode != null)
			{
				String roadID_str = roadIdNode.getNodeValue();
				if(roadID_str != null && !roadID_str.isEmpty())
					roadID = roadID_str;
			}
		
			Node sNode = attributes.getNamedItem("s");
			if(sNode != null)
			{
				String s_str = sNode.getNodeValue();
				if(s_str != null && !s_str.isEmpty())
					s = Float.parseFloat(s_str);
			}
		
			Node lateralOffsetNode = attributes.getNamedItem("lateralOffset");
			if(lateralOffsetNode != null)
			{
				String lateralOffset_str = lateralOffsetNode.getNodeValue();
				if(lateralOffset_str != null && !lateralOffset_str.isEmpty())
					lateralOffset = Float.parseFloat(lateralOffset_str);
			}
		
			Node verticalOffsetNode = attributes.getNamedItem("verticalOffset");
			if(verticalOffsetNode != null)
			{
				String verticalOffset_str = verticalOffsetNode.getNodeValue();
				if(verticalOffset_str != null && !verticalOffset_str.isEmpty())
					verticalOffset = Float.parseFloat(verticalOffset_str);
			}
		
			if((roadID != null) && (s != null))
				return new ODPositionWithOffsets(roadID, s, lateralOffset, verticalOffset);
			else
				System.err.println("SceneLoader::getOpenDrivePosWithOffsets(): invalid <openDrivePos> element given. " 
						+ "At least attributes roadID ('" + roadID + "') and s ('" + s + "') must be provided.");
		}
		
		return null;
	}
	
	
	public float degToRad(float degree) 
	{
		return degree * (FastMath.PI/180);
	}

	
	public void getGeometries(String[] typeList)
	{
		for(String type : typeList)
		{
			NodeList geometryNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:geometries/scene:" + type, XPathConstants.NODESET);
	
			for (int k = 1; k <= geometryNodes.getLength(); k++) 
			{
				Spatial geometry = null;
				String geometryID = dtData.getValue(Layer.SCENE, 
						"/scene:scene/scene:geometries/scene:" + type + "["+k+"]/@id", String.class);
		
				if(geometryID != null)
				{
					String path = "/scene:scene/scene:geometries/scene:" + type + "["+k+"]";
					
					if(type.equals("box"))
						geometry = createBox(path, geometryID);
					else if(type.equals("sphere"))
						geometry = createSphere(path, geometryID);
					else if(type.equals("cylinder"))
						geometry = createCylinder(path, geometryID);
					else if(type.equals("terrain"))
						geometry = createTerrain(path, geometryID);
				}
				
				if(geometry != null)
					geometryMap.put(geometryID,geometry);
			}
		}
	}


	private Geometry createBox(String path, String name) 
	{
		Geometry geometry = null;
		
		Float width = dtData.getValue(Layer.SCENE, path + "/scene:width", Float.class);		
		Float depth = dtData.getValue(Layer.SCENE, path + "/scene:depth", Float.class);		
		Float height = dtData.getValue(Layer.SCENE, path + "/scene:height", Float.class);

		if((width != null) && (depth != null) && (height != null))
		{
			// create new box
			Box box = new Box(width, depth, height);
			geometry = new Geometry(name + "_box", box);
		}
		
		return geometry;
	}
	
	
	private Geometry createSphere(String path, String name) 
	{
		Geometry geometry = null;
		
		Integer axisSamples = dtData.getValue(Layer.SCENE, path + "/scene:samples/@axis", Integer.class);		
		Integer radialSamples = dtData.getValue(Layer.SCENE, path + "/scene:samples/@radial", Integer.class);		
		Float radius = dtData.getValue(Layer.SCENE, path + "/scene:radius", Float.class);

		if((axisSamples != null) && (radialSamples != null) && (radius != null))
		{
			// create new sphere
			Sphere sphere = new Sphere(axisSamples, radialSamples, radius);
			geometry = new Geometry(name + "_sphere", sphere);
		}
		
		return geometry;
	}
	
	
	private Geometry createCylinder(String path, String name) 
	{
		Geometry geometry = null;
	
		Integer axisSamples = dtData.getValue(Layer.SCENE, path + "/scene:samples/@axis", Integer.class);		
		Integer radialSamples = dtData.getValue(Layer.SCENE, path + "/scene:samples/@radial", Integer.class);		
		Float radius = dtData.getValue(Layer.SCENE, path + "/scene:radius", Float.class);
		Float height = dtData.getValue(Layer.SCENE, path + "/scene:height", Float.class);
		Boolean closed = dtData.getValue(Layer.SCENE, path + "/scene:closed", Boolean.class);

		if((axisSamples != null) && (radialSamples != null) && (radius != null) && 
				(height != null) && (closed != null))
		{
			// create new cylinder
			Cylinder cylinder = new Cylinder(axisSamples, radialSamples, radius, height, closed);
			geometry = new Geometry(name + "_cylinder", cylinder);
		}
		
		return geometry;
	}

	
	private Spatial createTerrain(String path, String name) 
	{
		/*
			<terrain id="myTerrain">
	            <imageBasedHeightMap key="Textures/Terrain/splat/mountains512.png" heightScale="0.2" />
	            <smoothing percentage="0.9" radius="1" />
	            <lod patchSize="65" totalSize="513" lodFactor="2.7" />
	        </terrain>
		 */
		
		TerrainQuad terrain = null;
		
		String heightMapImagePath = dtData.getValue(Layer.SCENE, path + "/scene:imageBasedHeightMap/@key", String.class);
		Float heightScale = dtData.getValue(Layer.SCENE, path + "/scene:imageBasedHeightMap/@heightScale", Float.class);
		
		Float smoothPercentage = dtData.getValue(Layer.SCENE, path + "/scene:smoothing/@percentage", Float.class);
		Integer smoothRadius = dtData.getValue(Layer.SCENE, path + "/scene:smoothing/@radius", Integer.class);		
		
		Integer patchSize = dtData.getValue(Layer.SCENE, path + "/scene:lod/@patchSize", Integer.class);		
		Integer totalSize = dtData.getValue(Layer.SCENE, path + "/scene:lod/@totalSize", Integer.class);	
		Float lodFactor = dtData.getValue(Layer.SCENE, path + "/scene:lod/@distanceFactor", Float.class);

		if(heightMapImagePath != null)
		{
			TextureKey textureKey = new TextureKey(heightMapImagePath, false);
			Image heightMapImage = assetManager.loadTexture(textureKey).getImage();
			
			if(heightScale == null)
				heightScale = 0.5f;
			
			if(smoothPercentage == null)
				smoothPercentage = 0.9f;
			
			if(smoothRadius == null)
				smoothRadius = 1;
			
			if(patchSize == null)
				patchSize = 65;
			
			if(totalSize == null)
				totalSize = heightMapImage.getWidth()+1;
			
			if(lodFactor == null)
				lodFactor = 2.7f;
			
			// create heightmap
			AbstractHeightMap heightmap = new ImageBasedHeightMap(heightMapImage, heightScale);
			heightmap.load();
			heightmap.smooth(smoothPercentage, smoothRadius);
			
			// create terrain
			// The tiles will be 65x65, and the total size of the terrain will be 513x513.
			// Optimal terrain patch size is 65 (64x64). The total size is up to you. At 1025 
			// it ran fine for me (200+FPS), however at size=2049, it got really slow. But that 
			// is a jump from 2 million to 8 million triangles...
			terrain = new TerrainQuad(name + "_terrainQuad", patchSize, totalSize, heightmap.getHeightMap());
			TerrainLodControl control = new TerrainLodControl(terrain, sim.getCamera());
			control.setLodCalculator(new DistanceLodCalculator(patchSize, lodFactor) );
			terrain.addControl(control);
		}
		
		return terrain;
	}
	

	public void getPoints()
	{	
		NodeList pointNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:geometries/scene:point", XPathConstants.NODESET);

		for (int k = 1; k <= pointNodes.getLength(); k++) 
		{
			String pointID = dtData.getValue(Layer.SCENE, 
					"/scene:scene/scene:geometries/scene:point" + "["+k+"]/@id", String.class);

			if(pointID != null)
				addPoint("/scene:scene/scene:geometries/scene:point" + "["+k+"]");
		}
	}
	
	
	private void addPoint(String path) 
	{
		String id = dtData.getValue(Layer.SCENE, path + "/@id", String.class);
		Vector3f translation = dtData.getVector3f(Layer.SCENE, path + "/scene:translation");

		if((id != null) && (translation != null))
			pointMap.put(id, translation);
	}
	
	
	public void getResetPoints() 
	{
		try {
			
			NodeList pointNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
					"/scene:scene/scene:resetPoints/scene:resetPoint", XPathConstants.NODESET);

			for (int k = 1; k <= pointNodes.getLength(); k++) 
			{
				Node currentNode = pointNodes.item(k-1);
				
				// get pointID
				String id = currentNode.getAttributes().getNamedItem("id").getNodeValue();

				NodeList childnodes = currentNode.getChildNodes();
				ODPosition odPos = null;
				Vector3f translation = null;
				Quaternion rotation = null;

				for (int j = 1; j <= childnodes.getLength(); j++) 
				{
					Node currentChild = childnodes.item(j-1);
					
					if(currentChild.getNodeName().equals("openDrivePosition"))
					{
						odPos = getOpenDrivePos(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("translation"))
					{
						translation = getVector3f(currentChild);
					}
					
					else if(currentChild.getNodeName().equals("rotation"))
					{
						rotation = getQuaternion(currentChild);
					}
				}

				// build map object
				if(odPos != null)
				{
					resetPositionMap.put(id, new ResetPosition(id, odPos));
				}
				else if((translation != null) && (rotation != null))
				{
					resetPositionMap.put(id, new ResetPosition(id, translation, rotation));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public float getGravity(float defaultValue)
	{
		Float gravity = dtData.getValue(Layer.SCENE, "/scene:scene/scene:gravity", Float.class);
		if(gravity != null)
			return gravity;
		else
			return defaultValue;
	}


	public Map<String, ResetPosition> getResetPositionMap() 
	{
		return resetPositionMap;
	}


	public Map<String, Vector3f> getPointMap() 
	{
		return pointMap;
	}


	public String getPathToVegetationObjects()
	{
		return pathToVegetationObjects;
	}
	
	
	public ArrayList<RoadSideVegetation> getRoadSideVegetationList()
	{
		return roadSideVegetationList;
	}


	/**
	 * Returns a map of all movie player objects (GUI node screens and scene node screens) which
	 * were defined in the driving task.
	 */
	public TreeMap<String, MoviePlayer> getMoviePlayerMap(SimulationBasics sim) 
	{	
		TreeMap<String, MoviePlayer> playerMap = new TreeMap<String, MoviePlayer>();
		getGuiNodePlayers(sim, playerMap);
		getSceneNodePlayers(sim, playerMap);
		return playerMap;
	}


	private void getGuiNodePlayers(SimulationBasics sim, TreeMap<String, MoviePlayer> playerMap)
	{
		// Structure:
		// 	<moviePlayers>
		//    <guiNodeScreens>
		//      <guiNodeScreen>
		//        <id>player01</id>
		//        <resolution>
		//          <width>640</width>
		//          <height>360</height>
		//        </resolution>
		//        <position>
		//          <vector jtype="java_lang_Float" size="2">
		//            <entry>0</entry>
		//            <entry>0.0</entry>
		//          </vector>
		//        </position>
		//        <rotation>0.0</rotation>
		//      </guiNodeScreen>
		//    </guiNodeScreens>
		//  </moviePlayers>
		
		NodeList gnsNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:moviePlayers/scene:guiNodeScreens/scene:guiNodeScreen", 
				XPathConstants.NODESET);

		for (int k = 1; k <= gnsNodes.getLength(); k++) 
		{
			Node currentNode = gnsNodes.item(k-1);

			NodeList childnodes = currentNode.getChildNodes();
				
			String id = null;
			Integer width = null;
			Integer height = null;
			Vector2f position = null;
			Float rotation = null;
				
			for (int j = 1; j <= childnodes.getLength(); j++) 
			{
				Node currentChild = childnodes.item(j-1);
					
				if(currentChild.getNodeName().equals("id"))
				{
					id = currentChild.getTextContent();
				}
				
				else if(currentChild.getNodeName().equals("resolution"))
				{
					NodeList resolutionChildren = currentChild.getChildNodes();
					for (int l = 1; l <= resolutionChildren.getLength(); l++) 
					{
						Node currentResolutionChild = resolutionChildren.item(l-1);
							
						if(currentResolutionChild.getNodeName().equals("width"))
						{
							width = Integer.parseInt(currentResolutionChild.getTextContent());
						}
						else if (currentResolutionChild.getNodeName().equals("height"))
						{
							height = Integer.parseInt(currentResolutionChild.getTextContent());
						}
					}
				}
					
				else if(currentChild.getNodeName().equals("position"))
				{
					position = getVector2f(currentChild);
				}
					
				else if(currentChild.getNodeName().equals("rotation"))
				{
					rotation = Float.parseFloat(currentChild.getTextContent());
				}
			}

			
			// build MoviePlayer with GuiNodeScreen
			if(id != null && !playerMap.containsKey(id))
			{
				GuiNodeScreen guiNodeScreen = new GuiNodeScreen(sim, "ScreenNode_" + id, width, height);
				
				if(position != null)
					guiNodeScreen.setPosition(position);
					
				if(rotation != null)
					guiNodeScreen.setRotation(rotation);
				
				playerMap.put(id, new MoviePlayer(sim, guiNodeScreen));
			}
			else
				System.err.println("SceneLoader::getGuiNodePlayers(): Player ID '" + id + "' is invalid or"
						+ " already in use by an existing player");
		}
	}
	
	
	private void getSceneNodePlayers(SimulationBasics sim, TreeMap<String, MoviePlayer> playerMap)
	{
		// Structure:
		// 	<moviePlayers>
		//    <sceneNodeScreens>
		//      <sceneNodeScreen>
		//        <id>player02</id>
		//        <size>
		//          <vector jtype="java_lang_Float" size="3">
		//            <entry>10.0</entry>
		//            <entry>5.0</entry>
		//            <entry>0.25</entry>
		//          </vector>
		//        </size>
		//        <borderWidth>0.03</borderWidth>
		//        <borderHeight>0.03</borderHeight>
		//        <backsideColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>1</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </backsideColor>
		//        <standbyTexture>OpenDS.png</standbyTexture>
		//        <position>
		//          <openDrivePos roadID="road01" s="50" lateralOffset="10" verticalOffset="2.5" />
		//        </position>
		//        <rotation quaternion="false">
        //          <vector jtype="java_lang_Float" size="3">
        //            <entry>0</entry>
        //            <entry>0</entry>
        //            <entry>0</entry>
        //          </vector>
        //        </rotation>
		//      </sceneNodeScreen>
		//    </sceneNodeScreens>
		//  </moviePlayers>

		NodeList snsNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:moviePlayers/scene:sceneNodeScreens/scene:sceneNodeScreen", 
				XPathConstants.NODESET);

		for (int k = 1; k <= snsNodes.getLength(); k++) 
		{
			Node currentNode = snsNodes.item(k-1);

			NodeList childnodes = currentNode.getChildNodes();
				
			String id = null;
			Vector3f size = null;
			Float borderWidth = null;
			Float borderHeight = null;
			ColorRGBA backsideColor = null;
			Texture2D standbyTexture = null;
			Quaternion rotation = null;
			Vector3f translation = null;
			ODPositionWithOffsets odPos = null;
								
			for (int j = 1; j <= childnodes.getLength(); j++) 
			{
				Node currentChild = childnodes.item(j-1);
				
				if(currentChild.getNodeName().equals("id"))
				{
					id = currentChild.getTextContent();
				}
					
				else if(currentChild.getNodeName().equals("size"))
				{
					size = getVector3f(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("borderWidth"))
				{
					borderWidth = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("borderHeight"))
				{
					borderHeight = Float.parseFloat(currentChild.getTextContent());
				}
					
				else if(currentChild.getNodeName().equals("backsideColor"))
				{
					backsideColor = getColorRGBA(currentChild);
				}

				else if(currentChild.getNodeName().equals("standbyTexture"))
				{
					String path = currentChild.getTextContent();
					
					try {
						standbyTexture = (Texture2D) sim.getAssetManager().loadTexture(path);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
					
				else if(currentChild.getNodeName().equals("rotation"))
				{
					rotation = getQuaternion(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("position"))
				{
					NodeList positionChildren = currentChild.getChildNodes();
					for (int l = 1; l <= positionChildren.getLength(); l++) 
					{
						Node currentPositionChild = positionChildren.item(l-1);
							
						if(currentPositionChild.getNodeName().equals("translation"))
						{
							translation = getVector3f(currentPositionChild);
						}
						else if (currentPositionChild.getNodeName().equals("openDrivePos"))
						{
							odPos = getOpenDrivePosWithOffsets(currentPositionChild);
						}
					}
				}
			}

			
			// build MoviePlayer with SceneNodeScreen
			if((id != null) && (size != null))
			{
				if(!playerMap.containsKey(id))
				{
					SceneNodeScreen screenNodeScreen = new SceneNodeScreen(sim, id, size, borderWidth, 
							borderHeight, backsideColor, standbyTexture);
				
					if(rotation != null)
						screenNodeScreen.setLocalRotation(rotation);
				
					if(translation != null)
						screenNodeScreen.setLocalTranslation(translation);
					
					if(odPos != null)
						screenNodeScreen.setODPosition(odPos);
				
					playerMap.put(id, new MoviePlayer(sim, screenNodeScreen));
				}
				else
					System.err.println("SceneLoader::getSceneNodePlayers(): Player ID '" + id + "' is invalid or"
						+ " already in use by an existing player");
			}
		}
	}


	public TreeMap<String, DynamicObject> getDynamicObjectsMap(SimulationBasics sim)
	{
		TreeMap<String, DynamicObject> dynamicObjectsMap = new TreeMap<String, DynamicObject>();
		dynamicObjectsMap = extractSpeedIndicators(sim, dynamicObjectsMap);
		dynamicObjectsMap = extractMessageSigns(sim, dynamicObjectsMap);
		dynamicObjectsMap = extractVariableSigns(sim, dynamicObjectsMap);
		return dynamicObjectsMap;
	}
	
	
	private TreeMap<String, DynamicObject> extractSpeedIndicators(SimulationBasics sim, 
			TreeMap<String, DynamicObject> dynamicObjectsMap)
	{
		// Structure:
		// 	<dynamicObjects>
		//    <speedIndicators>
		//      <speedIndicator>
		//        <id>speedIndicator01</id>
		//        <maxSpeed>25</maxSpeed>
		//        <backsideColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>1</entry>
		//            <entry>1</entry>
		//            <entry>1</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </backsideColor>
		//        <language>DE</language>
		//        <addPole>true</addPole>
		//        <maxAngle>45</maxAngle>
		//        <minDist>2</minDist>
		//        <maxDist>80</maxDist>
		//        <minDetectionSpeed>2</minDetectionSpeed>
		//        <position>
		//          <openDrivePos roadID="road01" s="500" lateralOffset="-5" verticalOffset="2" />
		//        </position>
		//        <rotation quaternion="false">
		//          <vector jtype="java_lang_Float" size="3">
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//          </vector>
		//        </rotation>
		//        <scale>1.0</scale>
		//      </speedIndicator>
		//    </speedIndicators>
		//  </dynamicObjects>
		
		
		NodeList siNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:dynamicObjects/scene:speedIndicators/scene:speedIndicator", 
				XPathConstants.NODESET);

		for (int k = 1; k <= siNodes.getLength(); k++) 
		{
			Node currentNode = siNodes.item(k-1);

			NodeList childnodes = currentNode.getChildNodes();
				
			String id = null;
			Integer maxSpeed = null;
			ColorRGBA backsideColor = null;
			String language = null;
			Boolean addPole = null;
			Float maxAngle = null;
			Float minDist = null;
			Float maxDist = null;
			Float minDetectionSpeed = null;
			Quaternion rotation = null;
			Vector3f translation = null;
			ODPositionWithOffsets odPos = null;
			Float scale = null;
								
			for (int j = 1; j <= childnodes.getLength(); j++) 
			{
				Node currentChild = childnodes.item(j-1);
				
				if(currentChild.getNodeName().equals("id"))
				{
					id = currentChild.getTextContent();
				}
					
				else if(currentChild.getNodeName().equals("maxSpeed"))
				{
					maxSpeed = Integer.parseInt(currentChild.getTextContent());
				}
					
				else if(currentChild.getNodeName().equals("backsideColor"))
				{
					backsideColor = getColorRGBA(currentChild);
				}

				else if(currentChild.getNodeName().equals("language"))
				{
					language = currentChild.getTextContent();
				}
				
				else if(currentChild.getNodeName().equals("addPole"))
				{
					addPole = Boolean.parseBoolean(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("maxAngle"))
				{
					maxAngle = Float.parseFloat(currentChild.getTextContent());
				}
					
				else if(currentChild.getNodeName().equals("minDist"))
				{
					minDist = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("maxDist"))
				{
					maxDist = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("minDetectionSpeed"))
				{
					minDetectionSpeed = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("rotation"))
				{
					rotation = getQuaternion(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("position"))
				{
					NodeList positionChildren = currentChild.getChildNodes();
					for (int l = 1; l <= positionChildren.getLength(); l++) 
					{
						Node currentPositionChild = positionChildren.item(l-1);
							
						if(currentPositionChild.getNodeName().equals("translation"))
						{
							translation = getVector3f(currentPositionChild);
						}
						else if (currentPositionChild.getNodeName().equals("openDrivePos"))
						{
							odPos = getOpenDrivePosWithOffsets(currentPositionChild);
						}
					}
				}
				
				else if(currentChild.getNodeName().equals("scale"))
				{
					scale = Float.parseFloat(currentChild.getTextContent());
				}
			}
			
			// build SpeedIndicator
			if((id != null) && (maxSpeed != null))
			{
				SpeedIndicator speedIndicator = new SpeedIndicator(sim, id, maxSpeed, backsideColor, 
						language, addPole, maxAngle, minDist, maxDist, minDetectionSpeed);				
				
				if(rotation != null)
					speedIndicator.setLocalRotation(rotation);
				
				if(translation != null)
					speedIndicator.setLocalTranslation(translation);
					
				if(odPos != null)
					speedIndicator.setODPosition(odPos);
				
				if(scale != null)
					speedIndicator.setLocalScale(scale);
				
				dynamicObjectsMap.put(id, speedIndicator);
			}
		}
		
		return dynamicObjectsMap;
	}
		
	
	private TreeMap<String, DynamicObject> extractMessageSigns(SimulationBasics sim, 
			TreeMap<String, DynamicObject> dynamicObjectsMap)
	{
		// Structure:
		// 	<dynamicObjects>
		//    <messageSigns>
		//      <messageSign>
		//        <id>messageSign01</id>
		//        <noOfLines>3</noOfLines>
		//        <charsPerLine>20</charsPerLine>
		//        <charHeight>0.5</charHeight>
		//        <signDepth>0.1</signDepth>
		//        <borderWidth>0.1</borderWidth>
		//        <borderHeight>0.1</borderHeight>
		//        <frontsideColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </frontsideColor>
		//        <backsideColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>1</entry>
		//            <entry>1</entry>
		//            <entry>1</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </backsideColor>
		//        <defaultFontColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>0.9843</entry>
		//            <entry>0.5098</entry>
		//            <entry>0</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </defaultFontColor>
		//        <position>
		//          <openDrivePos roadID="road01" s="500" lateralOffset="-5" verticalOffset="2" />
		//        </position>
		//        <rotation quaternion="false">
		//          <vector jtype="java_lang_Float" size="3">
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//          </vector>
		//        </rotation>
		//        <message>
		//          ... (c.f. getMessage())
		//        </message>
		//      </messageSign>
		//    </messageSigns>
		//  </dynamicObjects>
		
		NodeList msNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:dynamicObjects/scene:messageSigns/scene:messageSign", 
				XPathConstants.NODESET);

		for (int k = 1; k <= msNodes.getLength(); k++) 
		{
			Node currentNode = msNodes.item(k-1);

			NodeList childnodes = currentNode.getChildNodes();
				
			String id = null;
			Integer noOfLines = null;
			Integer charsPerLine = null;
			Float charHeight = null;
			Float signDepth =  null;
			Float borderWidth = null;
			Float borderHeight = null;
			ColorRGBA frontsideColor = null;
			ColorRGBA backsideColor = null;
			ColorRGBA defaultFontColor = null;
			Quaternion rotation = null;
			Vector3f translation = null;
			ODPositionWithOffsets odPos = null;
			MessageSignText message = null;
			
								
			for (int j = 1; j <= childnodes.getLength(); j++) 
			{
				Node currentChild = childnodes.item(j-1);
				
				if(currentChild.getNodeName().equals("id"))
				{
					id = currentChild.getTextContent();
				}
					
				else if(currentChild.getNodeName().equals("noOfLines"))
				{
					noOfLines = Integer.parseInt(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("charsPerLine"))
				{
					charsPerLine = Integer.parseInt(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("charHeight"))
				{
					charHeight = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("signDepth"))
				{
					signDepth = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("borderWidth"))
				{
					borderWidth = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("borderHeight"))
				{
					borderHeight = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("frontsideColor"))
				{
					frontsideColor = getColorRGBA(currentChild);
				}
					
				else if(currentChild.getNodeName().equals("backsideColor"))
				{
					backsideColor = getColorRGBA(currentChild);
				}

				else if(currentChild.getNodeName().equals("defaultFontColor"))
				{
					defaultFontColor = getColorRGBA(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("rotation"))
				{
					rotation = getQuaternion(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("position"))
				{
					NodeList positionChildren = currentChild.getChildNodes();
					for (int l = 1; l <= positionChildren.getLength(); l++) 
					{
						Node currentPositionChild = positionChildren.item(l-1);
							
						if(currentPositionChild.getNodeName().equals("translation"))
						{
							translation = getVector3f(currentPositionChild);
						}
						else if (currentPositionChild.getNodeName().equals("openDrivePos"))
						{
							odPos = getOpenDrivePosWithOffsets(currentPositionChild);
						}
					}
				}
				
				else if(currentChild.getNodeName().equals("message"))
				{
					message = getMessage(currentChild);
				}
			}
			
			// build MessageSign
			if(id != null)
			{
				MessageSign messageSign = new MessageSign(sim, id, noOfLines, charsPerLine, charHeight, 
					signDepth, borderWidth, borderHeight, defaultFontColor, frontsideColor, backsideColor);
				
				if(rotation != null)
					messageSign.setLocalRotation(rotation);
				
				if(translation != null)
					messageSign.setLocalTranslation(translation);
					
				if(odPos != null)
					messageSign.setODPosition(odPos);

				if(message != null)
					messageSign.showText(message);
				
				dynamicObjectsMap.put(id, messageSign);
			}
		}
		
		return dynamicObjectsMap;
	}
	
	
	private MessageSignText getMessage(Node node)
	{
		//    <message>  
		//      <line>
		//        <text>Put Your Text Here</text>
		//        <fontColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>0.9843</entry>
		//            <entry>0.5098</entry>
		//            <entry>0</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </fontColor>
		//        <alignment>Center</alignment>
		//        <slidingSpeed>10</slidingSpeed>
		//        <flashingInterval>0.5</flashingInterval>
		//      </line>
		//    </message>  
		
		MessageSignText message = new MessageSignText();
		//message.addPlainText("Bla Bla", 20, ColorRGBA.White, TextAlignment.Center);
		//message.addSampleText(3, 20, ColorRGBA.Blue, TextAlignment.Center);
		
		NodeList childnodes = node.getChildNodes();
		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("line"))
			{
				String text = null;
				ColorRGBA fontColor = null;
				TextAlignment alignment = null;
				Float slidingSpeed = null;
				Float flashingInterval = null;
				
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("text"))
					{
						text = childChildNode.getTextContent();
					}
					else if(childChildNode.getNodeName().equals("fontColor"))
					{
						fontColor = getColorRGBA(childChildNode);
					}
					else if(childChildNode.getNodeName().equals("alignment"))
					{
						String alignmentString = childChildNode.getTextContent();
						alignment = TextAlignment.valueOf(alignmentString);
					}
					else if(childChildNode.getNodeName().equals("slidingSpeed"))
					{
						slidingSpeed = Float.parseFloat(childChildNode.getTextContent());
					}
					else if(childChildNode.getNodeName().equals("flashingInterval"))
					{
						flashingInterval = Float.parseFloat(childChildNode.getTextContent());
					}
				}
				
				MessageSignTextLine line = new MessageSignTextLine(text, fontColor, alignment);
				
				if(slidingSpeed != null)
					line.setSlidingSpeed(slidingSpeed);
				
				if(flashingInterval != null)
					line.setFlashingInterval(flashingInterval);
				
				message.addLine(line);
			}
		}
		
		return message;
	}


	private TreeMap<String, DynamicObject> extractVariableSigns(SimulationBasics sim, 
			TreeMap<String, DynamicObject> dynamicObjectsMap)
	{
		// Structure:
		// 	<dynamicObjects>
		//    <variableSigns>
		//      <variableSign>
		//        <id>variableSign01</id>
		//        <size>
		//          <vector jtype="java_lang_Float" size="3">
		//            <entry>2.6</entry>
		//            <entry>2.2</entry>
		//            <entry>0.1</entry>
		//          </vector>
		//        </size>
		//        <borderWidth>0.1</borderWidth>
		//        <borderHeight>0.1</borderHeight>
		//        <transitionType>SlideLeft</transitionType>
		//        <slidingDuration>1.0</slidingDuration>
		//        <backsideColor>
		//          <vector jtype="java_lang_Float" size="4">
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>1</entry>
		//          </vector>
		//        </backsideColor>
		//        <addPole>true</addPole>
		//        <position>
		//          <openDrivePos roadID="road01" s="40" lateralOffset="-5" verticalOffset="2" />
		//        </position>
		//        <rotation quaternion="false">
		//          <vector jtype="java_lang_Float" size="3">
		//            <entry>0</entry>
		//            <entry>0</entry>
		//            <entry>0</entry>
		//          </vector>
		//        </rotation>
		//        <scale>1.0</scale>
		//        <imageSequence>
		//          ... (c.f. getImageSequence())
		//        </imageSequence>
		//      </variableSign>
		//    </variableSigns>
		//  </dynamicObjects>
		
		NodeList vsNodes = (NodeList) dtData.xPathQuery(Layer.SCENE, 
				"/scene:scene/scene:dynamicObjects/scene:variableSigns/scene:variableSign", 
				XPathConstants.NODESET);

		for (int k = 1; k <= vsNodes.getLength(); k++) 
		{
			Node currentNode = vsNodes.item(k-1);

			NodeList childnodes = currentNode.getChildNodes();
			
			String id = null;
			Vector3f size = null;
			Float borderWidth = null;
			Float borderHeight = null;
			TransitionType transitionType = null;
			Float slidingDuration = null;
			ColorRGBA backsideColor = null;
			Boolean addPole = null;
			Vector3f translation = null;
			ODPositionWithOffsets odPos = null;
			Quaternion rotation = null;
			Float scale = null;
			VariableSignImageSequence imageSequence = new VariableSignImageSequence();
			
			
			for (int j = 1; j <= childnodes.getLength(); j++) 
			{
				Node currentChild = childnodes.item(j-1);
				
				if(currentChild.getNodeName().equals("id"))
				{
					id = currentChild.getTextContent();
				}
					
				else if(currentChild.getNodeName().equals("size"))
				{
					size = getVector3f(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("borderWidth"))
				{
					borderWidth = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("borderHeight"))
				{
					borderHeight = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("transitionType"))
				{
					String transitionTypeString = currentChild.getTextContent();
					transitionType = TransitionType.valueOf(transitionTypeString);
				}
					
				else if(currentChild.getNodeName().equals("slidingDuration"))
				{
					slidingDuration = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("repeatSequence"))
				{
					Boolean repeatSequence = Boolean.parseBoolean(currentChild.getTextContent());
					if(repeatSequence != null)
						imageSequence.setRepeat(repeatSequence);
				}
				
				else if(currentChild.getNodeName().equals("backsideColor"))
				{
					backsideColor = getColorRGBA(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("addPole"))
				{
					addPole = Boolean.parseBoolean(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("position"))
				{
					NodeList positionChildren = currentChild.getChildNodes();
					for (int l = 1; l <= positionChildren.getLength(); l++) 
					{
						Node currentPositionChild = positionChildren.item(l-1);
							
						if(currentPositionChild.getNodeName().equals("translation"))
						{
							translation = getVector3f(currentPositionChild);
						}
						else if (currentPositionChild.getNodeName().equals("openDrivePos"))
						{
							odPos = getOpenDrivePosWithOffsets(currentPositionChild);
						}
					}
				}
				
				else if(currentChild.getNodeName().equals("rotation"))
				{
					rotation = getQuaternion(currentChild);
				}
				
				else if(currentChild.getNodeName().equals("scale"))
				{
					scale = Float.parseFloat(currentChild.getTextContent());
				}
				
				else if(currentChild.getNodeName().equals("imageSequence"))
				{
					imageSequence = getImageSequence(currentChild, imageSequence);
				}
			}
			
			// build VariableSign
			if(id != null && size != null)
			{
				VariableSign variableSign = new VariableSign(sim, id, size, transitionType, 
						slidingDuration, imageSequence, backsideColor, borderWidth, borderHeight, addPole);

				if(rotation != null)
					variableSign.setLocalRotation(rotation);
				
				if(translation != null)
					variableSign.setLocalTranslation(translation);
					
				if(odPos != null)
					variableSign.setODPosition(odPos);
				
				if(scale != null)
					variableSign.setLocalScale(scale);

				dynamicObjectsMap.put(id, variableSign);
			}
		}

		return dynamicObjectsMap;
	}


	private VariableSignImageSequence getImageSequence(Node node, VariableSignImageSequence imageSequence)
	{
		//    <imageSequence>  
		//      <image>
		//        <id>test01</id>
		//        <texturePath>Textures/Misc/n1.png</texturePath>
		//        <duration>3.0</duration>
		//      </image>
		//      ...
		//    </imageSequence>  
		
		NodeList childnodes = node.getChildNodes();
		for(int a = 1; a<= childnodes.getLength(); a++)
		{
			Node childNode = childnodes.item(a-1);
			if(childNode.getNodeName().equals("image"))
			{
				String id = null;
				String texturePath = null;
				Float duration = null;
				
				NodeList childChildNodes = childNode.getChildNodes();
				for(int b = 1; b<= childChildNodes.getLength(); b++)
				{
					Node childChildNode = childChildNodes.item(b-1);
					if(childChildNode.getNodeName().equals("id"))
					{
						id = childChildNode.getTextContent();
					}
					else if(childChildNode.getNodeName().equals("texturePath"))
					{
						texturePath = childChildNode.getTextContent();
					}
					else if(childChildNode.getNodeName().equals("duration"))
					{
						duration = Float.parseFloat(childChildNode.getTextContent());
					}
				}
				
				if((id != null) && (texturePath != null))
				{
					VariableSignImage image = new VariableSignImage(id, texturePath, duration);
					imageSequence.add(image);
				}
			}
		}
		
		return imageSequence;
	}
}
