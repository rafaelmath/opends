/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.eyetracker;

import java.net.*;

import org.apache.commons.codec.binary.Base64;

import eu.opends.basics.SimulationBasics;

/**
 * "UDPThread" provides a connection via UDP. DatagramPackets will be 
 * received from the given port. This class extends the class "Thread".
 */
public class UDPClientThread extends Thread 
{
	private SimulationBasics sim;
	private boolean stoprequested;
	private DatagramPacket packet;
	private DatagramSocket incomingSocket;
	private EyeTrackerData logdata;
	private String activity = null;
	

	public UDPClientThread(SimulationBasics sim, int port, int packetSize)
	{
		super();
		this.sim = sim;
		stoprequested = false;

		try {
			
			// open socket connection and listen for incoming packets
			incomingSocket = new DatagramSocket(port);
			byte data[] = new byte[packetSize];
			packet = new DatagramPacket(data, packetSize);
			
			// set time to wait after an unsuccessful receive attempt
			incomingSocket.setSoTimeout(300);
			
			System.out.println("Eye tracker: UDP connection established");

		} catch (SocketException e) {
			System.err.println("UDPThread_Constructor: " + e.toString());
		}
	}

	
	public EyeTrackerData getLogdata()
	{
		return logdata;
	}
	
	
	public String getActivity()
	{
		return activity;
	}
	

	public void setActivity(String activity)
	{
		this.activity = activity;
	}
	
	
	public synchronized void requestStop() 
	{
		stoprequested = true;
	}

	
	@Override
	public void run() 
	{
		int packetsize;
		byte[] packetdata;
		String datastring;

		while (!stoprequested) 
		{
			try {
				// read data and get length
				incomingSocket.receive(packet);
				packetsize = packet.getLength();
				packetdata = packet.getData();
				
			} catch (SocketTimeoutException e) {
				// suppress error output if no data available at incomingSocket
				// wait 300ms as defined in SoTimeout and try again
				continue;
			} catch(NullPointerException e){
				// suppress error output if no data available at incomingSocket
				continue;
			} catch (Exception e) {
				System.err.println("UDPThread_run(): " + e.toString());
				continue;
			}
			
			// transform incoming data to string of variable length
			datastring = new String(packetdata, 0, packetsize);
			//System.out.println(datastring);
			
			// decode packets
			String decodedString = new String(Base64.decodeBase64(datastring.getBytes()));		

			// parse XML
			XMLParser parser = new XMLParser(sim, decodedString);
			logdata = parser.getEyeData();
			
			if(logdata.getActivity() != null && !logdata.getActivity().isEmpty())
				activity = logdata.getActivity();
		}
		
		// close socket connection
		try {
			if (null != incomingSocket)
				incomingSocket.close();
		} catch (Exception ex) {
		}

		System.out.println("Eye tracker: UDP connection closed");
	}


}