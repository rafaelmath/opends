/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.eyetracker;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;

import eu.opends.main.Simulator;
import eu.opends.tools.Util;

public class Fixation
{
	private long minDuration = 150;
	
	private Geometry geometry;
	private String path;
	private Vector3f contactPoint;
	private Vector2f screenCoordinate;
	private long startTimestamp;
	
	
	public Fixation(Geometry geometry, Vector3f contactPoint, Vector2f screenCoordinate) 
	{
		this.geometry = geometry;
		this.path = Util.getPath(geometry);
		this.contactPoint = contactPoint;
		this.screenCoordinate = screenCoordinate;
		this.startTimestamp = System.currentTimeMillis();
	}
	
	
	public Geometry getGeometry()
	{
		return geometry;
	}
	
	
	public void writeToLog()
	{
		long duration = System.currentTimeMillis() - startTimestamp;
		
		if(geometry != null && duration > minDuration)
		{
			Simulator.getDrivingTaskLogger().reportEvent(startTimestamp, "fixation", path, duration, null, (int)screenCoordinate.getX(), 
					(int)screenCoordinate.getY(), contactPoint.getX(), contactPoint.getY(), contactPoint.getZ(), null);
		}
	}


	public String getPath()
	{
		return path;
	}
}
