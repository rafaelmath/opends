/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.eyetracker;

import org.lwjgl.opengl.Display;

import com.jme3.math.Vector2f;

import eu.opends.basics.SimulationBasics;


/**
 * This class contains parameters of an EyeTracker log, which contains time stamp, gaze 
 * position, eye position, eye distance, pupil diameter and validity assumptions for
 * both eyes.
 */
public class EyeTrackerData 
{	
	// parameters provided by the EyeTracker
	private String datastring;
	private String timestamp;
	private float x_gazepos_lefteye;
	private float y_gazepos_lefteye;
	private float x_gazepos_righteye;
	private float y_gazepos_righteye;
	private float x_eyepos_lefteye;
	private float y_eyepos_lefteye;
	private float x_eyepos_righteye;
	private float y_eyepos_righteye;
	private float diameter_pupil_lefteye;
	private float diameter_pupil_righteye;
	private float distance_lefteye;
	private float distance_righteye;
	private long validity_lefteye;
	private long validity_righteye;
	private String activity;
	
	private String output;	
	
	
	public EyeTrackerData(SimulationBasics sim, String datastring, String timestamp, float x_gazepos_lefteye, float y_gazepos_lefteye,
			float x_gazepos_righteye, float y_gazepos_righteye,	float x_eyepos_lefteye, float y_eyepos_lefteye,
			float x_eyepos_righteye, float y_eyepos_righteye, float diameter_pupil_lefteye, float diameter_pupil_righteye,
			float distance_lefteye, float distance_righteye, long validity_lefteye, long validity_righteye, String activity) 
	{
		this.datastring = datastring;
		this.timestamp = timestamp;
		this.x_gazepos_lefteye = x_gazepos_lefteye - Display.getX();
		this.y_gazepos_lefteye = sim.getSettings().getHeight() - y_gazepos_lefteye + Display.getY();
		this.x_gazepos_righteye = x_gazepos_righteye - Display.getX();
		this.y_gazepos_righteye = sim.getSettings().getHeight() - y_gazepos_righteye + Display.getY();
		this.x_eyepos_lefteye = x_eyepos_lefteye;
		this.y_eyepos_lefteye = y_eyepos_lefteye;
		this.x_eyepos_righteye = x_eyepos_righteye;
		this.y_eyepos_righteye = y_eyepos_righteye;
		this.diameter_pupil_lefteye = diameter_pupil_lefteye;
		this.diameter_pupil_righteye = diameter_pupil_righteye;
		this.distance_lefteye = distance_lefteye;
		this.distance_righteye = distance_righteye;
		this.validity_lefteye = validity_lefteye;
		this.validity_righteye = validity_righteye;
		this.activity = activity;
	}

	
	public Vector2f getGazePos() 
	{
		if(validity_lefteye == 0 && validity_righteye == 0)
		{
			float x_gazepos = (x_gazepos_lefteye + x_gazepos_righteye) / 2.0f;
			float y_gazepos = (y_gazepos_lefteye + y_gazepos_righteye) / 2.0f;
			return new Vector2f(x_gazepos, y_gazepos);
		}
		else if(validity_lefteye == 0 || validity_lefteye == 1)
			return new Vector2f(x_gazepos_lefteye, y_gazepos_lefteye);
		else if(validity_righteye == 0 || validity_righteye == 1)
			return new Vector2f(x_gazepos_righteye, y_gazepos_righteye);
		else
			return null;
	}
	
	
	public boolean isValidLeftEye()
	{
		return (validity_lefteye == 0);
	}
	
	
	public boolean isValidRightEye()
	{
		return (validity_righteye == 0);
	}
	
	
	public String getDataString() 
	{
		return datastring;
	}	
	
	
	public String getActivity() 
	{
		return activity;
	}

	
	/**
	 * generates an output string containing all parameters provided by the EyeTracker
	 */
	public String toString()
	{	
		output = "";
		addToOutput("timestamp", timestamp);
		addToOutput("x_gazepos_lefteye", x_gazepos_lefteye);
		addToOutput("y_gazepos_lefteye", y_gazepos_lefteye);
		addToOutput("x_gazepos_righteye", x_gazepos_righteye);
		addToOutput("y_gazepos_righteye", y_gazepos_righteye);
		addToOutput("x_eyepos_lefteye", x_eyepos_lefteye);
		addToOutput("y_eyepos_lefteye", y_eyepos_lefteye);
		addToOutput("x_eyepos_righteye", x_eyepos_righteye);
		addToOutput("y_eyepos_righteye", y_eyepos_righteye);
		addToOutput("diameter_pupil_lefteye", diameter_pupil_lefteye);
		addToOutput("diameter_pupil_righteye", diameter_pupil_righteye);
		addToOutput("distance_lefteye", distance_lefteye);
		addToOutput("distance_righteye", distance_righteye);
		addToOutput("validity_lefteye", validity_lefteye);
		addToOutput("validity_righteye", validity_righteye);
		addToOutput("activity", activity);
		
		return output;
	}
	

	private void addToOutput(String name, String value)
	{
		output += name + ": " + value + "\n";
	}
	
	
	private void addToOutput(String name, float value)
	{
		output += name + ": " + value + "\n";
	}


	public String getTimestamp() 
	{
		return timestamp;
	}

}
