/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.eyetracker;


import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import eu.opends.basics.SimulationBasics;


public class XMLParser
{
	private SimulationBasics sim;
	private Document doc;
	private String datastring;
	
	
	public XMLParser(SimulationBasics sim, String xmlstring) 
	{
		this.sim = sim;
		this.datastring = xmlstring;
		
		try {
			
	        InputSource xmlsource = new InputSource();
	        xmlsource.setCharacterStream(new StringReader(xmlstring));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			doc = db.parse(xmlsource);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public EyeTrackerData getEyeData()
	{
		String timestamp = getnode("timestamp");
		float x_gazepos_lefteye = Float.parseFloat(getnode("x_gazepos_lefteye").replace(",","."));
		float y_gazepos_lefteye = Float.parseFloat(getnode("y_gazepos_lefteye").replace(",","."));
		float x_gazepos_righteye = Float.parseFloat(getnode("x_gazepos_righteye").replace(",","."));
		float y_gazepos_righteye = Float.parseFloat(getnode("y_gazepos_righteye").replace(",","."));
		float x_eyepos_lefteye = Float.parseFloat(getnode("x_eyepos_lefteye").replace(",","."));
		float y_eyepos_lefteye = Float.parseFloat(getnode("y_eyepos_lefteye").replace(",","."));
		float x_eyepos_righteye = Float.parseFloat(getnode("x_eyepos_righteye").replace(",","."));
		float y_eyepos_righteye = Float.parseFloat(getnode("y_eyepos_righteye").replace(",","."));
		float diameter_pupil_lefteye = Float.parseFloat(getnode("diameter_pupil_lefteye").replace(",", "."));
		float diameter_pupil_righteye = Float.parseFloat(getnode("diameter_pupil_righteye").replace(",", "."));
		float distance_lefteye = Float.parseFloat(getnode("distance_lefteye").replace(",", "."));
		float distance_righteye = Float.parseFloat(getnode("distance_righteye").replace(",", "."));
		long validity_lefteye = Long.parseLong(getnode("validity_lefteye"));
		long validity_righteye = Long.parseLong(getnode("validity_righteye"));
		String activity = getnode("activity");
		
		return new EyeTrackerData(sim, datastring, timestamp, x_gazepos_lefteye, y_gazepos_lefteye, x_gazepos_righteye,
				y_gazepos_righteye, x_eyepos_lefteye, y_eyepos_lefteye, x_eyepos_righteye, y_eyepos_righteye, 
				diameter_pupil_lefteye, diameter_pupil_righteye, distance_lefteye, distance_righteye, validity_lefteye, 
				validity_righteye, activity);
	}
	
	
	private String getnode(String nodeid)
	{
		NodeList nodeLst = doc.getElementsByTagName("eyeCoordinates");
        Element element = (Element) nodeLst.item(0);
        NodeList name = element.getElementsByTagName(nodeid);
        
        if(name.getLength()>0)
        {
        	Element line = (Element) name.item(0);
        	return getCharacterDataFromElement(line);
        }
        else
        	return "-1";
	}
	
	
	private static String getCharacterDataFromElement(Element elem) 
	{
		Node child = elem.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "-2";
	}
	

}