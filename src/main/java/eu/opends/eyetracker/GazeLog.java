/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/


package eu.opends.eyetracker;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

public class GazeLog
{
	private String eyeTrackerTimestamp = null;
	private boolean isValidLeftEye = false;
	private boolean isValidRightEye = false;
	private Vector2f gazeCoord = null;
	private Vector2f gazeCoordAverage = null;
	private Vector3f rayOrigin = null;
	private Vector3f rayDirection = null;
	private String closestHit = null;
	private Vector3f closestContactPoint = null;
	
	
	public String getEyeTrackerTimestamp()
	{
		return eyeTrackerTimestamp;
	}

	
	public Long getEyeTrackerMilliseconds()
	{
		if(eyeTrackerTimestamp != null)
		{
			try {
				
				// convert string (format: yyyy-MM-dd HH:mm:ss.SSS) to milliseconds (long)
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Date date = sdf.parse(eyeTrackerTimestamp);
				return date.getTime();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}
	
	
	public void setEyeTrackerTimestamp(String eyeTrackerTimestamp)
	{
		this.eyeTrackerTimestamp = eyeTrackerTimestamp;
	}
	
	
	public Vector2f getGazeCoord()
	{
		return gazeCoord;
	}
	
	
	public void setGazeCoord(Vector2f gazeCoord)
	{
		this.gazeCoord = gazeCoord;
	}
	
	
	public Vector2f getGazeCoordAverage()
	{
		return gazeCoordAverage;
	}
	
	
	public void setGazeCoordAverage(Vector2f gazeCoordAverage)
	{
		this.gazeCoordAverage = gazeCoordAverage;
	}
	
	
	public boolean isValidLeftEye()
	{
		return isValidLeftEye;
	}
	
	
	public void setValidLeftEye(boolean isValidLeftEye)
	{
		this.isValidLeftEye = isValidLeftEye;
	}
	
	
	public boolean isValidRightEye()
	{
		return isValidRightEye;
	}
	
	
	public void setValidRightEye(boolean isValidRightEye)
	{
		this.isValidRightEye = isValidRightEye;
	}
	
	
	public Vector3f getRayOrigin()
	{
		return rayOrigin;
	}
	
	
	public void setRayOrigin(Vector3f rayOrigin)
	{
		this.rayOrigin = rayOrigin;
	}
	
	
	public Vector3f getRayDirection()
	{
		return rayDirection;
	}
	
	
	public void setRayDirection(Vector3f rayDirection)
	{
		this.rayDirection = rayDirection;
	}
	
	
	public String getClosestHit()
	{
		return closestHit;
	}
	
	
	public void setClosestHit(String closestHit)
	{
		this.closestHit = closestHit;
	}
	
	
	public Vector3f getClosestContactPoint()
	{
		return closestContactPoint;
	}
	
	
	public void setClosestContactPoint(Vector3f closestContactPoint)
	{
		this.closestContactPoint = closestContactPoint;
	}



}
