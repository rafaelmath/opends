/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.eyetracker;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;

import eu.opends.car.Car;
import eu.opends.drivingTask.settings.SettingsLoader;
import eu.opends.drivingTask.settings.SettingsLoader.Setting;
import eu.opends.main.SimulationDefaults;
import eu.opends.main.Simulator;
import eu.opends.tools.PanelCenter;
import eu.opends.tools.Util;
import eu.opends.traffic.PhysicalTraffic;
import eu.opends.traffic.TrafficCar;
import eu.opends.traffic.TrafficObject;
import eu.opends.trigger.TriggerCenter;


public class EyetrackerCenter
{	
	public int udpPort = 2010;
	public int packetSize = 4048;
	
	// mean average of gaze position over last x values
	private int smoothingFactor = 10;
	
	private boolean showCrossHairs = true;
	private ColorRGBA crossHairsColor = ColorRGBA.White;
	private float scalingFactor = 2;
	
	private boolean showGazeSphere = true;
	private ColorRGBA sphereColor = ColorRGBA.Red;
	
	private ColorMode colorMode = ColorMode.None;
	private ColorRGBA glowColor = ColorRGBA.Orange;
	
	private boolean showWarningFrame = true;
	// gaze off screen warning will be raised after x milliseconds
	private int warningThreshold = 3000;
	// flashing interval (ms) of warning frame
	private int flashingInterval = 500;
	

	private UDPClientThread udpClientThread;
	private Vector2f screenPos;
	private Simulator sim;
	private Node sceneNode;
	private AssetManager assetManager;
	private Geometry gazeSphere;
	private BitmapText crosshairs;
	private LinkedList<Vector2f> gazeStorage = new LinkedList<Vector2f>();
	private long lastScreenGaze = 0;
	private GazeLog gazeLog = new GazeLog();
	
	
	public GazeLog getGazeLog()
	{
		return gazeLog;
	}
	
	
	private enum ColorMode
	{
		None, VehiclesOnly, All;
	}

	
	public EyetrackerCenter(Simulator sim)
	{
		this.sim = sim;
		this.sceneNode = sim.getSceneNode();
		this.assetManager = sim.getAssetManager();
		
		initSettings();
		
		// init gaze pos
		screenPos = new Vector2f(sim.getSettings().getWidth() / 2f, sim.getSettings().getHeight() / 2f);
			
		// a "+" in the middle of the screen to help aiming
		BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		crosshairs = new BitmapText(guiFont, false);
		crosshairs.setSize(guiFont.getCharSet().getRenderedSize() * scalingFactor);
		crosshairs.setText("+");
		crosshairs.setColor(crossHairsColor);
			
		if(showCrossHairs)
			sim.getGuiNode().attachChild(crosshairs);

		// init a colored sphere to mark the target
		Sphere sphere = new Sphere(30, 30, 0.2f);
		gazeSphere = new Geometry("gazeSphere", sphere);
		Material sphere_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		sphere_mat.setColor("Color", sphereColor);
		gazeSphere.setMaterial(sphere_mat);

		// create log-thread
		udpClientThread = new UDPClientThread(sim, udpPort, packetSize);
		udpClientThread.start();
	}


	private void unsetFlyCam() 
	{
		InputManager inputManager = sim.getInputManager();
		
		// remove FlyCam mapping concerning arrow keys, mouse movement and joystick
		// which interferes the computation of the eye gaze ray
		if(inputManager.hasMapping("FLYCAM_Left"))
			inputManager.deleteMapping("FLYCAM_Left");
		
		if(inputManager.hasMapping("FLYCAM_Right"))
			inputManager.deleteMapping("FLYCAM_Right");
		
		if(inputManager.hasMapping("FLYCAM_Up"))
			inputManager.deleteMapping("FLYCAM_Up");
		
		if(inputManager.hasMapping("FLYCAM_Down"))
			inputManager.deleteMapping("FLYCAM_Down");
		
		if(inputManager.hasMapping("FLYCAM_ZoomIn"))
			inputManager.deleteMapping("FLYCAM_ZoomIn");
		
		if(inputManager.hasMapping("FLYCAM_ZoomOut"))
			inputManager.deleteMapping("FLYCAM_ZoomOut");
		
		if(inputManager.hasMapping("FLYCAM_RotateDrag"))
			inputManager.deleteMapping("FLYCAM_RotateDrag");
		
		if(inputManager.hasMapping("FLYCAM_StrafeLeft"))
			inputManager.deleteMapping("FLYCAM_StrafeLeft");
		
		if(inputManager.hasMapping("FLYCAM_StrafeRight"))
			inputManager.deleteMapping("FLYCAM_StrafeRight");
		
		if(inputManager.hasMapping("FLYCAM_Forward"))
			inputManager.deleteMapping("FLYCAM_Forward");
		
		if(inputManager.hasMapping("FLYCAM_Backward"))
			inputManager.deleteMapping("FLYCAM_Backward");
		
		if(inputManager.hasMapping("FLYCAM_Rise"))
			inputManager.deleteMapping("FLYCAM_Rise");
		
		if(inputManager.hasMapping("FLYCAM_Lower"))
			inputManager.deleteMapping("FLYCAM_Lower");
		
		if(inputManager.hasMapping("FLYCAM_InvertY"))
			inputManager.deleteMapping("FLYCAM_InvertY");
	}


	private void initSettings() 
	{
		SettingsLoader settingsLoader = Simulator.getDrivingTask().getSettingsLoader();
		
		udpPort = settingsLoader.getSetting(Setting.Eyetracker_port, SimulationDefaults.Eyetracker_port);
		smoothingFactor = settingsLoader.getSetting(Setting.Eyetracker_smoothingFactor, SimulationDefaults.Eyetracker_smoothingFactor);
		showCrossHairs = settingsLoader.getSetting(Setting.Eyetracker_crossHairs_show, SimulationDefaults.Eyetracker_crossHairs_show);
		
		String crossHairsColorString = settingsLoader.getSetting(Setting.Eyetracker_crossHairs_color, SimulationDefaults.Eyetracker_crossHairs_color);
		try {
			crossHairsColor = (ColorRGBA) ColorRGBA.class.getField(crossHairsColorString).get(new ColorRGBA());
		} catch (Exception e) {}

		scalingFactor = settingsLoader.getSetting(Setting.Eyetracker_crossHairs_scalingFactor, SimulationDefaults.Eyetracker_crossHairs_scalingFactor);
		
		showGazeSphere = settingsLoader.getSetting(Setting.Eyetracker_gazeSphere_show, SimulationDefaults.Eyetracker_gazeSphere_show);
		
		String sphereColorString = settingsLoader.getSetting(Setting.Eyetracker_gazeSphere_color, SimulationDefaults.Eyetracker_gazeSphere_color);
		try {
			sphereColor = (ColorRGBA) ColorRGBA.class.getField(sphereColorString).get(new ColorRGBA());
		} catch (Exception e) {}
		
		String colorModeString = settingsLoader.getSetting(Setting.Eyetracker_highlightObjects_mode, SimulationDefaults.Eyetracker_highlightObjects_mode);
		try {
			colorMode = ColorMode.valueOf(colorModeString);
		} catch (Exception e) {}
		
		String glowColorString = settingsLoader.getSetting(Setting.Eyetracker_highlightObjects_color, SimulationDefaults.Eyetracker_highlightObjects_color);
		try {
			glowColor = (ColorRGBA) ColorRGBA.class.getField(glowColorString).get(new ColorRGBA());
		} catch (Exception e) {}
		
		showWarningFrame = settingsLoader.getSetting(Setting.Eyetracker_warningFrame_show, SimulationDefaults.Eyetracker_warningFrame_show);

		warningThreshold = settingsLoader.getSetting(Setting.Eyetracker_warningFrame_threshold, SimulationDefaults.Eyetracker_warningFrame_threshold);

		flashingInterval = settingsLoader.getSetting(Setting.Eyetracker_warningFrame_flashingInterval, SimulationDefaults.Eyetracker_warningFrame_flashingInterval);

		/*		
		System.err.println("udpPort: " + udpPort + " smoothingFactor: " + smoothingFactor
				 + " showCrossHairs: " + showCrossHairs + " crossHairsColor: " + crossHairsColor
				 + " scalingFactor: " + scalingFactor + " showGazeSphere: " + showGazeSphere
				 + " sphereColor: " + sphereColor + " colorMode: " + colorMode
				 + " glowColor: " + glowColor + " showWarningFrame: " + showWarningFrame
				 + " warningThreshold: " + warningThreshold + " flashingInterval: " + flashingInterval);
		*/
	}


	private boolean done = false;
	public void update()
	{
		if(!done)
		{
			unsetFlyCam();
			done = true;
		}
		
		// perform activity (if available)
		String triggerID = udpClientThread.getActivity();
		if(triggerID != null && !triggerID.isEmpty() && !triggerID.equals("-1"))
		{
			TriggerCenter.performRemoteTriggerAction(triggerID);
			udpClientThread.setActivity(null);
		}
		
		
		// get the gaze position (screen coordinates)
		Vector2f gazePos = null;
		EyeTrackerData eyeTrackerData = udpClientThread.getLogdata();
		if(eyeTrackerData != null)
		{
			gazePos = eyeTrackerData.getGazePos();
			if(gazePos != null)
			{
				screenPos = doSmoothing(eyeTrackerData.getGazePos());

				// publish screen coordinates
				sim.getMqttConnection().publishGazeCoordinates(screenPos.getX(), screenPos.getY());
			}
		}

		
		if(eyeTrackerData != null)
		{
			gazeLog.setEyeTrackerTimestamp(eyeTrackerData.getTimestamp());
			gazeLog.setValidLeftEye(eyeTrackerData.isValidLeftEye());
			gazeLog.setValidRightEye(eyeTrackerData.isValidRightEye());
		}
		else
		{
			gazeLog.setEyeTrackerTimestamp(null);
			gazeLog.setValidLeftEye(false);
			gazeLog.setValidRightEye(false);
		}
		gazeLog.setGazeCoord(gazePos);
		gazeLog.setGazeCoordAverage(screenPos);
		
		
		if(showWarningFrame)
			checkForOffScreenGaze();
			
		// set cross hairs
		crosshairs.setLocalTranslation(screenPos.getX() - crosshairs.getLineWidth()/2f,
			screenPos.getY() + crosshairs.getLineHeight()/2f, 0);

		// reset previous position of colored sphere
		sceneNode.detachChild(gazeSphere);

		// reset collision results list
		CollisionResults results = new CollisionResults();
		
		// get correct camera (in case more than one camera is available) 
		Camera cam = getCamera(screenPos);
		
		// compute the world position on the far plane
		Vector3f worldPosFar = cam.getWorldCoordinates(screenPos, 1);

		// compute the world position on the near plane
		Vector3f worldPosNear = cam.getWorldCoordinates(screenPos, 0);
			
		// compute direction towards target (from camera)
		Vector3f direction = worldPosFar.subtract(worldPosNear);

		// normalize direction vector
		direction.normalizeLocal();

		// aim a ray from the camera towards the target
		Ray ray = new Ray(worldPosNear, direction);
		gazeLog.setRayOrigin(worldPosNear);
		gazeLog.setRayDirection(direction);
		
		// collect intersections between ray and scene elements in results list.
		sceneNode.collideWith(ray, results);
		
		/*
		// print the results
		System.out.println("number of collisions: " + results.size());
		for (int i = 0; i < results.size(); i++) 
		{
			// for each hit, we know distance, contact point, name of geometry.
			float dist = results.getCollision(i).getDistance();
			Vector3f contactPoint = results.getCollision(i).getContactPoint();
			Geometry geometry = results.getCollision(i).getGeometry();
				
			System.out.println("collision #" + i + ": hit " + geometry.getName() + 
					" at " + contactPoint + ", "	+ dist + " meters away");
		}
		*/
		
		if(colorMode != ColorMode.None)
			uncolor();
		
		gazeLog.setClosestHit(null);
		gazeLog.setClosestContactPoint(null);
		
		// use the results (we mark the hit object)
		if (results.size() > 0) 
		{
			// the closest collision point is what was truly hit
			//CollisionResult closest = results.getClosestCollision();
			CollisionResult closest = getClosestCollision(results); // remove special objects
			if(closest != null)
			{
				if(showGazeSphere)
				{
					// mark the hit with a colored sphere
					gazeSphere.setLocalTranslation(closest.getContactPoint());
					sceneNode.attachChild(gazeSphere);
				}

				colorGeometry(closest.getGeometry());
			
				// report "object in focus"
				reportFixation(closest);

				if(closest.getGeometry() != null)
					gazeLog.setClosestHit(closest.getGeometry().getName());
				
				gazeLog.setClosestContactPoint(closest.getContactPoint());
			}
			else
				// report "no object in focus"
				reportNoFixation();
		}
		else
			// report "no object in focus"
			reportNoFixation();
	}


	private CollisionResult getClosestCollision(CollisionResults results)
	{
		for(int i=0; i<results.size(); i++)
		{
			String geometryName = results.getCollision(i).getGeometry().getName();
		
			if(!geometryName.startsWith("x-") && !geometryName.startsWith("y-") 
					&& !geometryName.startsWith("z-") && !geometryName.startsWith("center")
					&& !geometryName.startsWith("Sky") && !geometryName.startsWith("ODarea")
					&& !geometryName.equals("sideObstacleLeftSensor") && !geometryName.equals("sideObstacleRightSensor"))
			{
				return results.getCollision(i);
			}
		}

		return null;
	}
	
	


	private Camera getCamera(Vector2f coordinates)
	{
		int numberOfScreens = sim.getNumberOfScreens();
	    if(numberOfScreens > 1)
	    {
	    	int totalWidth = sim.getSettings().getWidth();
	    	float screenWidth = (float) totalWidth / numberOfScreens;
	    	int screenIndex = 1 + ((int) (coordinates.getX()/screenWidth));
	    	
	    	// screenIndex must be an integer between 1 (inclusive) and numberOfScreens (inclusive)
	    	screenIndex = Math.max(1, Math.min(numberOfScreens, screenIndex));
	    	
	    	//System.err.println("x: " + coordinates.getX() + " --> screen: " + screenIndex);
	    	
	    	Spatial cameraNode = sim.getCameraFactory().getFrontCameraNode().getChild("CamNode" + screenIndex);
	    	if((cameraNode != null) && (cameraNode instanceof CameraNode))
	    	{
	    		Camera camera = ((CameraNode) cameraNode).getCamera();
	    		if(camera != null)
	    			return camera;
	    		else
	    			System.err.println("EyeTrackerCenter::getCamera(): Camera in 'CamNode" + screenIndex + "' is null!");
	    	}
	    	else
	    		System.err.println("EyeTrackerCenter::getCamera(): Camera node 'CamNode" + screenIndex + "' does not exist!");
	    }

	    return sim.getCamera();
	}


	private Fixation previousFixation = new Fixation(null, null, null);
	private void reportFixation(CollisionResult closest) 
	{/*
		// if gazing at new object
		if(!closest.getGeometry().equals(previousFixation.getGeometry()))
		{
			// write previous fixation to log file
			previousFixation.writeToLog();
			
			// create "new fixation"
			previousFixation = new Fixation(closest.getGeometry(), closest.getContactPoint(), screenPos);
		}
		*/
		// if gazing at new object
		if(!Util.getPath(closest.getGeometry()).equals(previousFixation.getPath()))
		{
			// write previous fixation to log file
			previousFixation.writeToLog();
					
			// create "new fixation"
			previousFixation = new Fixation(closest.getGeometry(), closest.getContactPoint(), screenPos);
		}
	}
	
	
	private void reportNoFixation() 
	{
		// write previous fixation to log file
		previousFixation.writeToLog();

		// create "no fixation"
		previousFixation = new Fixation(null, null, null);
	}


	private Vector2f doSmoothing(Vector2f gazePos) 
	{		
    	Vector2f sum = new Vector2f(0,0);
    	
    	gazeStorage.addLast(gazePos);

        for (Vector2f vector : gazeStorage)
        	sum.addLocal(vector);
        
        if(gazeStorage.size() >= smoothingFactor)
        	gazeStorage.removeFirst();

        return sum.divide(smoothingFactor);
	}
	
	
	private void checkForOffScreenGaze() 
	{
		if(screenPos.getX() < 0 || screenPos.getX() > sim.getSettings().getWidth() ||
		   screenPos.getY() < 0 || screenPos.getY() > sim.getSettings().getHeight())
		{
			// time gaze is off screen
			long offTime = System.currentTimeMillis() - lastScreenGaze;
			
			// if threshold exceeded -> warning
			if(lastScreenGaze != 0 && (offTime > warningThreshold))
				PanelCenter.showWarningFrame(true, flashingInterval);
				//System.err.println("OUT OF SCREEN: " + offTime);
		}		
		else
		{
			// reset timer when in screen again
			lastScreenGaze = System.currentTimeMillis();
			PanelCenter.showWarningFrame(false);
		}
	}
	

	private void uncolor()
	{
		// uncolor all objects
		for(Geometry g : Util.getAllGeometries(sceneNode))
		{
			try{
			
				g.getMaterial().setColor("GlowColor", ColorRGBA.Black);
			
			} catch (IllegalArgumentException e) {
			}
		}
	}
	
	
	private void colorGeometry(Geometry geometry) 
	{
		if(colorMode != ColorMode.None)
		{		
			if(colorMode == ColorMode.VehiclesOnly)
			{
				// color hit traffic and multi-driver vehicles only
				colorTrafficVehicles(geometry);	
				colorMultiDriverVehicles(geometry);
			}
			else
			{
				// color all hit objects
				geometry.getMaterial().setColor("GlowColor", glowColor);
			}
		}
	}


	private void colorTrafficVehicles(Geometry geometry) 
	{
		for(TrafficObject c : PhysicalTraffic.getTrafficObjectList())
		{
			if(c instanceof TrafficCar)
			{
				boolean colorCar = false;
				
				// all geometries of current traffic car
				List<Geometry> carGeometries = Util.getAllGeometries(((Car) c).getCarNode());
				for(Geometry g : carGeometries)
				{
					// if hit geometry is part of current car, color whole car
					if(g.equals(geometry))
					{
						colorCar = true;
						break;
					}
				}
				
				if(colorCar)
				{
					// color all geometries of current car
					for(Geometry g : carGeometries)
						g.getMaterial().setColor("GlowColor", glowColor);
					
					break;
				}
			}
		}
	}


	private void colorMultiDriverVehicles(Geometry geometry)
	{
		ArrayList<String> registeredVehicles = new ArrayList<String>();
		
		if(sim.getMultiDriverClient() != null)
			registeredVehicles = sim.getMultiDriverClient().getRegisteredVehicles();
		
		for(String vehicleID : registeredVehicles)
		{
			Node carNode = (Node) sceneNode.getChild(vehicleID);
			
			if(carNode != null)
			{
				boolean colorCar = false;
			
				// all geometries of current multi-driver car
				List<Geometry> carGeometries = Util.getAllGeometries(carNode);
				for(Geometry g : carGeometries)
				{
					// if hit geometry is part of current car, color whole car
					if(g.equals(geometry))
					{
						colorCar = true;
						break;
					}
				}
			
				if(colorCar)
				{
					// color all geometries of current car
					for(Geometry g : carGeometries)
						g.getMaterial().setColor("GlowColor", glowColor);
				
					break;
				}
			}
		}
	}


	public void close()
	{
		// stop UDP thread
		udpClientThread.requestStop();
	}
}