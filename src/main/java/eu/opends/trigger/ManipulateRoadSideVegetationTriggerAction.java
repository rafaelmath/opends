/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;


import eu.opends.basics.SimulationBasics;

/**
 * This class represents a ManipulateRoadSideVegetation trigger action. Whenever a collision
 * with a related trigger was detected, the given object will be manipulated in 
 * the specified way.
 * 
 * @author Rafael Math
 */
public class ManipulateRoadSideVegetationTriggerAction extends TriggerAction 
{
	private SimulationBasics sim;
	private String referenceID;
	private boolean isVisible;

	
	/**
	 * Creates a new ManipulateRoadSideVegetation trigger action instance, providing maximum
	 * number of repetitions and the object to manipulate. 
	 * 
	 * @param sim
	 * 			Simulator
	 * 
	 * @param delay
	 * 			Amount of seconds (float) to wait before the TriggerAction will be executed.
	 * 
	 * @param maxRepeat
	 * 			Maximum number how often the trigger can be hit (0 = infinite).
	 * 
	 * @param referenceID
	 * 			ID of the road side vegetation to manipulate.
	 * 
	 * @param isVisible
	 * 			Visibility of the road side vegetation to manipulate.
	 */
	public ManipulateRoadSideVegetationTriggerAction(SimulationBasics sim, float delay, int maxRepeat, 
			String executionClass, String referenceID, boolean isVisible) 
	{
		super(delay, maxRepeat, executionClass);
		this.sim = sim;
		this.referenceID = referenceID;
		this.isVisible = isVisible;
	}

	
	/**
	 * Manipulates the given road side vegetation by applying a visibility change. 
	 */
	@Override
	protected void execute()
	{
		if(!isExceeded())
		{
			sim.getVegetationGenerator().setVisibility(referenceID, isVisible);			
			updateCounter();
		}
	}	
	

	/**
	 * Returns a String of the road side vegetation that will be manipulated.
	 */
	@Override
	public String toString()
	{
		return "Manipulate Road Side Vegetation: " + referenceID;
		
	}


}
