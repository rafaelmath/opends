/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;

import com.jme3.material.MatParamTexture;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.texture.Texture;

import eu.opends.basics.SimulationBasics;
import eu.opends.tools.Util;


/**
 * This class represents a ManipulateGeometryByTexturePath trigger action. Whenever a collision
 * with a related trigger was detected, the geometry described by the texture path will
 * be either shown, hidden, or detached from the scene node.
 * 
 * @author Rafael Math
 */
public class ManipulateGeometryByTexturePathTriggerAction extends TriggerAction 
{
	public enum ManipulationType
	{
		Show, Hide, Detach
	}
	
	
	public enum ComparisonType
	{
		StartsWith, Equals, EndsWith
	}


	private SimulationBasics sim;
	private ManipulationType manipulationType;
	private ComparisonType comparisonType;
	private String searchString;

	
	/**
	 * Creates a new ManipulationGeometryByTexturePath trigger action instance, providing maximum
	 * number of repetitions and the geometry's texture path. 
	 * 
	 * @param sim
	 * 			Simulator
	 * 
	 * @param delay
	 * 			Amount of seconds (float) to wait before the TriggerAction will be executed.
	 * 
	 * @param maxRepeat
	 * 			Maximum number how often the trigger can be hit (0 = infinite).
	 * 
	 * @param manipulationType
	 * 			How the geometry will be manipulated (Show, Hide, Detach)
	 * 
	 * @param comparisonType
	 * 			How the texture path string will be compared (StartsWith, Equals, EndsWith)
	 * 
	 * @param searchString
	 * 			(substring of) texture path used to look up the geometry to detach from scene node.		
	 */
	public ManipulateGeometryByTexturePathTriggerAction(SimulationBasics sim, float delay, int maxRepeat, String executionClass,
			ManipulationType manipulationType, ComparisonType comparisonType, String searchString) 
	{
		super(delay, maxRepeat, executionClass);
		this.sim = sim;
		this.manipulationType = manipulationType;
		this.comparisonType = comparisonType;
		this.searchString = searchString;
	}

	
	/**
	 * Manipulates the given geometry.
	 */
	@Override
	protected void execute()
	{
		if(!isExceeded())
		{
			try 
			{
				for(Spatial spatial : sim.getSceneNode().getChildren())
				{
					for(Geometry geometry : Util.getAllGeometries(spatial))
					{
						MatParamTexture diffuseMap = geometry.getMaterial().getTextureParam("DiffuseMap");
						if (diffuseMap != null)
						{
							Texture texture = diffuseMap.getTextureValue();
							String texturePath = texture.getKey().getName();
							
							if((comparisonType.equals(ComparisonType.StartsWith) && texturePath.startsWith(searchString))
								|| (comparisonType.equals(ComparisonType.Equals) && texturePath.equals(searchString))
								|| (comparisonType.equals(ComparisonType.EndsWith) && texturePath.endsWith(searchString)))
							{
								if(manipulationType == ManipulationType.Detach && geometry.getParent() != null)
									// remove from scene node
									geometry.getParent().detachChild(geometry);
								else if(manipulationType == ManipulationType.Hide)
									// hide geometry
									geometry.setCullHint(CullHint.Always);
								else if(manipulationType == ManipulationType.Show)
									// show geometry
									geometry.setCullHint(CullHint.Dynamic);
							}
						}
					}
				}
				
			} catch (Exception e){
				e.printStackTrace();
				System.err.println("Could not manipulate geometry (texture path '" + searchString + "').");
			}
		
			updateCounter();
		}
	}	
	

	/**
	 * Returns a string of the object that will be manipulated.
	 */
	@Override
	public String toString()
	{
		return "ManipulateGeometryByTexturePath: " + manipulationType.toString() + " - " + comparisonType.toString() 
				+ " - " + searchString;
		
	}


}
