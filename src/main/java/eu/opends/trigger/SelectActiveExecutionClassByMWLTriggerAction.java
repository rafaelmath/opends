/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;
import eu.opends.tools.visualOutput.OnScreenVisualizer;

/**
 * 
 * @author Rafael Math
 */
public class SelectActiveExecutionClassByMWLTriggerAction extends TriggerAction 
{
	private SimulationBasics sim;
	private String activeExecutionClassPrefix;
	
	
	public SelectActiveExecutionClassByMWLTriggerAction(float delay, int maxRepeat, String executionClass, 
			SimulationBasics sim, String activeExecutionClassPrefix)
	{
		super(delay, maxRepeat, executionClass);
		this.sim = sim;
		this.activeExecutionClassPrefix = activeExecutionClassPrefix;
	}
	
	
	@Override
	protected void execute() 
	{
		if(!isExceeded() && sim instanceof Simulator)
		{
			int noOfSeconds = 8;
			
			OnScreenVisualizer onScreenVisualizer = ((Simulator)sim).getOnScreenVisualizer();
			float class8Sec = onScreenVisualizer.getClass8Sec();
			float class4Sec = onScreenVisualizer.getClass4Sec();
			float class2Sec = onScreenVisualizer.getClass2Sec();
			
			if(class4Sec < class8Sec && class4Sec < class2Sec)
				noOfSeconds = 4;
			else if(class2Sec < class4Sec && class2Sec < class8Sec)
				noOfSeconds = 2;
			
			String activeExecutionClass = activeExecutionClassPrefix + noOfSeconds;
			TriggerCenter.setActiveExecutionClassList(activeExecutionClass);
			
			System.err.println("Set active execution class to: " + activeExecutionClass);
			
			updateCounter();
		}
	}

}
