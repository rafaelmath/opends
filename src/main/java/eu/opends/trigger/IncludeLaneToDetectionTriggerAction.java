/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;

/**
 * 
 * @author Rafael Math
 */
public class IncludeLaneToDetectionTriggerAction extends TriggerAction 
{
	private SimulationBasics sim;
	private String roadID;
	private Integer lane;
	
	
	/**
	 * Creates a new IncludeLaneToDetection trigger action instance, providing roadID
	 * and lane of the segment to be included to vehicle's position detection.
	 * 
	 * @param sim
	 * 			Simulator
	 * 
	 * @param delay
	 * 			Amount of seconds (float) to wait before the TriggerAction will be executed.
	 * 
	 * @param maxRepeat
	 * 			Number of maximum recurrences
	 * 
	 * @param roadID
	 * 			ID of the road to be included to vehicle's position detection.
	 * 
	 * @param lane
	 * 			Number of the lane to be included to vehicle's position detection.
	 */
	public IncludeLaneToDetectionTriggerAction(SimulationBasics sim, float delay, int maxRepeat, String executionClass,
			String roadID, Integer lane) 
	{
		super(delay, maxRepeat, executionClass);
		this.sim = sim;
		this.roadID = roadID;
		this.lane = lane;
	}
	

	/**
	 * Includes road segment
	 */
	@Override
	protected void execute() 
	{
		if(!isExceeded())
		{
			sim.getOpenDriveCenter().includeToDetection(roadID, lane);			

			updateCounter();
		}
	}
}
