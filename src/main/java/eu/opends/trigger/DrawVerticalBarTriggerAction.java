/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;

import com.jme3.math.ColorRGBA;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;
import eu.opends.tools.visualOutput.ParameterVisualizer;

public class DrawVerticalBarTriggerAction extends TriggerAction
{
	private SimulationBasics sim;
	private String visualizerID;
	private int relativePosition;
	private  ColorRGBA color;
	
	
	public DrawVerticalBarTriggerAction(float delay, int maxRepeat, String executionClass, SimulationBasics sim,
			String visualizerID, int relativePosition, ColorRGBA color)
	{
		super(delay, maxRepeat, executionClass);
		this.sim = sim;
		this.visualizerID = visualizerID;
		this.relativePosition = relativePosition;
		this.color = color;
	}
	

	@Override
	protected void execute()
	{
		if(!isExceeded() && sim instanceof Simulator)
		{
			ParameterVisualizer visualizer = null;
			
			if(visualizerID.equalsIgnoreCase("gazesgevisualizer"))
				visualizer = ((Simulator)sim).getOnScreenVisualizer().getGazeSGEVisualizer();
			else if(visualizerID.equalsIgnoreCase("mwleegvisualizer"))
				visualizer = ((Simulator)sim).getOnScreenVisualizer().getMwlEEGVisualizer();
			else if(visualizerID.equalsIgnoreCase("class2SecVisualizer"))
				visualizer = ((Simulator)sim).getOnScreenVisualizer().getClass2SecVisualizer();
			
			if(visualizer != null)
				visualizer.addVerticalTarget(relativePosition, color);
			
			updateCounter();
		}
	}

}
