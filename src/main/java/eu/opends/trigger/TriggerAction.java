/*
*  This file is part of OpenDS (Open Source Driving Simulator).
*  Copyright (C) 2023 Rafael Math
*
*  OpenDS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  OpenDS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with OpenDS. If not, see <http://www.gnu.org/licenses/>.
*/

package eu.opends.trigger;

import java.util.ArrayList;

import eu.opends.main.Simulator;

/**
 * This abstract class represents a trigger action, which will be 
 * performed whenever a collision with the related trigger was detected.
 * Whether the action will really be executed depends on the execution 
 * class list which must contain at least one active execution class 
 * or null (= always active).
 * 
 * @author Rafael Math
 */
public abstract class TriggerAction 
{
	private float delay;
	private int maxRepeatCounter;
	private boolean unlimitedRepeat;
	private String executionClassList = null;
	
	
	public TriggerAction(float delay, int maxRepeat, String executionClassList)
	{
		this.delay = delay;
		this.maxRepeatCounter = maxRepeat;
		this.unlimitedRepeat = (maxRepeat == 0);
		this.executionClassList = executionClassList;
	}
	
	
	/**
	 * Method will be called on trigger collision, await delay and perform some action.
	 */
	public void performAction()
	{
		if(executionClassList != null)
		{
			// if executionClassList is not null, then it must contain an active execution class to
			// execute the action
			boolean hasActiveExecutionClass = false;
			
			// check for multiple executionClasses separated by ;
			String[] executionClassArray = executionClassList.split(";");
			for(String executionClass : executionClassArray)
			{
				// check whether this execution class is contained in global list of active execution classes
				ArrayList<String> activeExecutionClassList = TriggerCenter.getActiveExecutionClassList();
				if(!executionClass.isEmpty() && activeExecutionClassList.contains(executionClass))
					hasActiveExecutionClass = true;
			}
			
			if(!hasActiveExecutionClass)
				return;
		}
		
		
		if(delay > 0)
		{
			TriggerActionDelayThread triggerActionDelayThread = new TriggerActionDelayThread(this, delay);
			triggerActionDelayThread.start();
		}
		else
			execute();
	}
	

	protected abstract void execute();
	
	
	protected void updateCounter()
	{
		// can be updated at most once per second
		if(!unlimitedRepeat)
			maxRepeatCounter--;
	}
	
	
	protected boolean isExceeded()
	{
		return ((!unlimitedRepeat) && (maxRepeatCounter == 0));
	}
	
	
	protected float getDelay()
	{
		return delay;
	}
}
